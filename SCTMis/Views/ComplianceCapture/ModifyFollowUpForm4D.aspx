﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ModifyForm4D>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Modify Form 4D FollowUp
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("ModifyFollowUpForm4D", "ComplianceCapture", FormMethod.Post, new { id = "CPForm" }))
   { %>

    <div class="col-4">
        <label>
            Region
            <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Woreda
            <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Kebele
            <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
        </label>
    </div>
    <br>
    <div class="col-4">
        <label>
            Gote
            <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Gare
            <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            PSNP HH #
            <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "6", @maxlength="20" })%>
        </label>
    </div>

    <br>

    <div class="col-4">
        <label>
            Name of household head
            <%: Html.TextBoxFor(model => model.NameOfHouseHoldHead, new { tabindex = "8" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Gender of HH Member (Child)
            <%: Html.DropDownListFor(model => model.HouseHoldMemberSex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Age of HH member (Child)
            <%: Html.TextBoxFor(model => model.HouseHoldMemberAge, new { tabindex = "14" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <br>

    <div class="col-4">
        <label>
            Names of HH member (Child)
            <%: Html.TextBoxFor(model => model.HouseHoldMemberName, new { tabindex = "14" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Child Protection ID * 
            <%: Html.TextBoxFor(model => model.ChildProtectionId, new { tabindex = "1" , @maxlength="50" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Child Referred To Case Management *
            <%: Html.DropDownListFor(model => model.CaseManagementReferral,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "2" })%>
        </label>
    </div>
    <br>
    <div class="col-4">
        <label>
            Collection Date  
            <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "3",@Style="width:95%;"})%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Collected By (Social Worker) 
            <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "4" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Remarks
            <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "5" , @maxlength="50" })%>
        </label>
    </div>

    <hr />
    <br>
    <div class="col-4">
        <label>
            Risk Identified
            <%: Html.DropDownListFor(model => model.RiskID,
                    new SelectList(Model.risks, "ID", "Name"), new { tabindex = "3" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Service To Be Provided
            <%: Html.DropDownListFor(model => model.ServiceID,
                    new SelectList(Model.services, "ID", "Name"), new { tabindex = "4" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Service Provider
            <%: Html.DropDownListFor(model => model.ProviderID,
                    new SelectList(Model.providers, "ID", "Name"), new { tabindex = "5" })%>
        </label>
    </div>
    <hr>

    <div class="col-4">
        <label>
            Social Worker * 
            <%: Html.DropDownListFor(model => model.SocialWorkerFollowUp,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "4" })%>
        </label>
    </div>

     <div class="col-7">
        <label>
            VisitDate *
                <%: Html.TextBoxFor(model => model.VisitDate, new { tabindex = "3",@Style="width:95%;"})%>
        </label>
    </div>

    <div id="errors" style="color: red;"></div>


    <br>
    <div class="col-4">
        <label>
            Was Service Accessed? *
            <%: Html.DropDownListFor(model => model.ServiceStatusID,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "5" })%>
        </label>
    </div>

    <div id="divServiceAccessDate" class="col-7">
        <label>
            Date Service Accessed *
                <%: Html.TextBoxFor(model => model.ServiceAccessDate, new { tabindex = "3",@Style="width:95%;"})%>
        </label>
    </div>

    <div id="divServiceNotAccessedReason" class="col-4">
        <label>
            Reason Service Not Accessed *
                <%: Html.DropDownListFor(model => model.ServiceNotAccessedReason,
                    new SelectList(Model.notaccessreason, "ID", "Name"), new { tabindex = "5" })%>
        </label>
    </div>

    <br /><br />

    <div>
        <button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus"
            tabindex="26"><i></i>Update Details</button>
    </div>



    <br>
    <br>

    <div id="gridbox" style="position: relative; width: 1200px; height: 200px;"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div>
    <span id="recfound"></span>


    <br />
    <div class="form-actions">
        <button id="cmdModify" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Update Service Access Status
           </button>
        <button id="cmdBack" type="button"
            class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <%: Html.HiddenFor(model => model.ChildProtectionHeaderID)%>
    <%: Html.HiddenFor(model => model.ColumnID)%>
    <%: Html.HiddenFor(model => model.ProfileDSHeaderID)%>
    <%: Html.HiddenFor(model => model.ProfileDSDetailID)%>
    <%: Html.HiddenFor(model => model.ClientTypeID)%>
    <%: Html.HiddenFor(model => model.CapturedXml)%>
    <%: Html.HiddenFor(model => model.AllowEdit)%>
    <%: Html.ValidationSummary(true) %>

    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../Scripts/jtable/themes/lightcolor/gray/jtable.min.css" rel="stylesheet" />
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script src="../Scripts/jtable/jquery.jtable.min.js"></script>
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>
    <script type="text/javascript">
        var EVENTID = null;
        var ISUPDATED = false;
        var arrObj = [];
        var SerialCount = 0;
        var page_count = 10;
        var mygrid;
        
        var gridHeader =
            'ColumnID,ChildProtectionVisitHeaderID,ChildProtectionDetailID,ChildProtectionHeaderID,RiskId,Risk<span class="HeaderChange">_</span>Name,ServiceId,Service<span class="HeaderChange">_</span>Name,ProviderId,Provider<span class="HeaderChange">_</span>Name,Visit<span class="HeaderChange">_</span>Date,Social<span class="HeaderChange">_</span>Worker,Was<span class="HeaderChange">_</span>Service<span class="HeaderChange">_</span>Accessed,DateServiceAccessed,ServiceNotAccessedReason, Edit, Delete';
  
        var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img';


        $(document).ready(function () {

            $('#loading').hide();
            $('#VisitDate,#ServiceAccessDate').Zebra_DatePicker({
                direction: -1, // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            $("#RegionID,#WoredaID,#KebeleID,#Gote,#Gare,#HouseHoldIDNumber,#NameOfHouseHoldHead,#HouseHoldMemberName,#HouseHoldMemberSex,#HouseHoldMemberAge")
                .prop("disabled", true);
            $('#ChildProtectionId,#CaseManagementReferral,#CollectionDate,#SocialWorker,#Remarks,#RiskID,#ServiceID,#ProviderID,#ChildProtectionId').prop("disabled", true);
            $('#divServiceAccessDate').hide();
            $('#divServiceNotAccessedReason').hide();
            $('#VisitDate,#SocialWorkerFollowUp,#ServiceStatusID,#ServiceAccessDate,#ServiceNotAccessedReason').val('');
            $('#cmdProceed').prop("disabled", true);

            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,0,0,0,300,0,300,0,300,0,0,100,0,0,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,right,right");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () {
                showLoading(true)
            });
            mygrid.init();
            mygrid.loadXML("/ComplianceCapture/FetchGridForm4DFollowUpDetailsByID?RecCount=" + page_count + "&Id=" + $("#ChildProtectionHeaderID").val());
            dhtmlxError.catchError("ALL", my_error_handler);

            $.ajax({
                type: "POST",
                url: "../ComplianceCapture/FetchJsonForm4DFollowUpDetailsByID",
                data: "{  'RecCount':" + page_count + ",ChildProtectionHeaderID : '" + $("#ChildProtectionHeaderID").val() + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                error: function (xx, yy) {
                    
                },
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        MemberCount = data.length;
                        if (MemberCount != undefined) {

                            for (var i = 0; i < data.length; i++) {
                                var theDtls = new Object;
                                var d = data[i];

                                DateServiceAccessed = d.DateServiceAccessed
                                if(d.DateServiceAccessed==null){ DateServiceAccessed = ''; }

                                ServiceNotAccessedReason = d.ServiceNotAccessedReason
                                if(d.ServiceNotAccessedReason==null){ ServiceNotAccessedReason = ''; }

                                SerialCount = SerialCount + 1;
                                theDtls.ColumnId = SerialCount;
                                theDtls.ChildProtectionVisitHeaderID = d.ChildProtectionVisitHeaderID;
                                theDtls.ChildProtectionDetailID = d.ChildProtectionDetailID;
                                theDtls.ChildProtectionHeaderID = d.ChildProtectionHeaderID;
                                theDtls.RiskId = d.RiskId;
                                theDtls.RiskName = d.RiskName;
                                theDtls.ServiceId = d.ServiceId;
                                theDtls.ServiceName = d.ServiceName;
                                theDtls.ProviderId = d.ProviderId;
                                theDtls.ProviderName = d.ProviderName;
                                theDtls.VisitDate = d.VisitDate;
                                theDtls.SocialWorkerFollowUp = d.SocialWorkerFollowUp;
                                theDtls.ServiceStatusId = d.IsServiceAccessed;
                                theDtls.ServiceStatusName = d.IsServiceAccessed;
                                theDtls.ServiceAccessDate = DateServiceAccessed;
                                theDtls.ServiceNotAccessedReasonId = ServiceNotAccessedReason;
                                theDtls.ServiceNotAccessedReasonName = ServiceNotAccessedReason;
                                arrObj.push(theDtls);
                            }
                            
                            $("#CapturedXml").val(JSON.stringify(arrObj));
                        }
                    }
                }
            });

            $("#cmdProceed").click(function () {
                var errors = [];
                var html = '<ul>';
                valid = true;
                $('#errors').empty();

                if ($('#RiskID').val() == '-') {
                    errors.push('<li>Please Select Service</li>');
                    valid = false;
                }
                
                if ($('#ServiceStatusID').val() == null || $('#ServiceStatusID').val() == '-') {
                    errors.push('<li>Service Access Status Required</li>');
                    valid = false;
                }

                if($('#ServiceStatusID').val()=="NO"){
                    if ($('#ServiceNotAccessedReason').val() == null || $('#ServiceStatusID').val() == '-') {
                        errors.push('<li>Service Not Accessed Reason Required</li>');
                        valid = false;
                    }
                }else if($('#ServiceStatusID').val()=="YES"){
                    if ($('#ServiceAccessDate').val() == '') {
                        errors.push('<li>Service Access Date Required</li>');
                        valid = false;
                    }
                }
                

                if (!valid) {
                    html += errors.join('') + '</ul>'
                    $('#errors').show();
                    $('#errors').append(html);
                    return valid;
                } else {
                    $('#errors').hide();
                }

                var theDtls = new Object;
                if (EVENTID == "EDIT") {
                    for (var i in arrObj) {
                        if (arrObj.hasOwnProperty(i)) {
                            if (arrObj[i].ColumnId == $("#ColumnID").val()) {
                                arrObj[i].RiskId = $("#RiskID").val();
                                arrObj[i].RiskName = $("#RiskID").find('option:selected').text();
                                arrObj[i].ServiceId = $("#ServiceID").val();
                                arrObj[i].ServiceName = $("#ServiceID").find('option:selected').text();
                                arrObj[i].ProviderId = $("#ProviderID").val();
                                arrObj[i].ProviderName = $("#ProviderID").find('option:selected').text();
                                arrObj[i].ServiceStatusId = $("#ServiceStatusID").val();
                                arrObj[i].ServiceStatusName = $("#ServiceStatusID").find('option:selected').text();
                                arrObj[i].ServiceAccessDate = $("#ServiceAccessDate").val();
                                arrObj[i].ServiceNotAccessedReasonId = $("#ServiceNotAccessedReason").val();
                                arrObj[i].ServiceNotAccessedReasonName = $("#ServiceNotAccessedReason").find('option:selected').text();
                                
                            }
                        }
                    }
                } else {
                    $('#errors').hide();

                    SerialCount = SerialCount + 1;
                    theDtls.ColumnId = SerialCount;
                    theDtls.RiskId = $("#RiskID").val();
                    theDtls.RiskName = $("#RiskID").find('option:selected').text();
                    theDtls.ServiceId = $("#ServiceID").val();
                    theDtls.ServiceName = $("#ServiceID").find('option:selected').text();
                    theDtls.ProviderId = $("#ProviderID").val();
                    theDtls.ProviderName = $("#ProviderID").find('option:selected').text();
                    theDtls.ServiceStatusId = $("#ServiceStatusID").val();
                    theDtls.ServiceStatusName = $("#ServiceStatusID").find('option:selected').text();
                    theDtls.ServiceAccessDate = $("#ServiceAccessDate").val();
                    theDtls.ServiceNotAccessedReasonId = $("#ServiceNotAccessedReason").val();
                    theDtls.ServiceNotAccessedReasonName = $("#ServiceNotAccessedReason").find('option:selected').text();

                    arrObj.push(theDtls);
                }

                var myXml = createXmlstring(arrObj, 1);

               
                mygrid.clearAll();
                mygrid.parse(myXml);
                
                 $("#RiskID,#ServiceID,#ProviderID").val('-');
                 $("#ServiceStatusID").val('');
                 $("#ServiceAccessDate").val('');
                 $("#ServiceNotAccessedReason").val('-');
                 $('#divServiceAccessDate').hide();
                 $('#divServiceNotAccessedReason').hide();
                 ISUPDATED = true;
            })
            

            $("#cmdBack").click(function () {
                window.location.href = "../ComplianceCapture/Form4DFormList";
            });
        });

        $("#ServiceStatusID" ).on('change', function () {
            var status = $("#ServiceStatusID option:selected").val();
            if(status=="YES"){
                $('#divServiceNotAccessedReason').hide();
                $('#ServiceNotAccessedReason').val('');
                $('#divServiceAccessDate').show();
            }else if (status=="NO"){
                $('#divServiceNotAccessedReason').show();
                $('#divServiceAccessDate').hide();
                $('#ServiceAccessDate').val('');
            }else{
                $('#divServiceNotAccessedReason').hide();
                $('#divServiceAccessDate').hide();
                 $('#ServiceNotAccessedReason').val('');
                $('#ServiceAccessDate').val('');
            }
        });

        function createXmlstring(arrObject, inMemory) {
            var xml;
            var gridID;
            xml = '';
            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    xml = xml + '<row id="' + i + '">';
                    for (var j in arrObj[i]) {
                        if (arrObj[i].hasOwnProperty(j)) {
                            if (j == "ColumnId") {
                                xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                                gridID = arrObj[i][j];
                            }
                        }
                        

                        if (j=="ChildProtectionVisitHeaderID" || j=="ChildProtectionDetailID" || j=="ChildProtectionHeaderID" || j == "RiskId" || j == "RiskName" || j == "ServiceId" || j == "ServiceName" || j == "ProviderId" || j == "ProviderName"  || j == "ServiceStatusName" || j == "SocialWorkerFollowUp" || j == "VisitDate" || j == "ServiceAccessDate" || j == "ServiceNotAccessedReasonName")
                        {
                            xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        }
                    }
                    xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
                    xml = xml + '<cell>../DHTMLX/codebase/imgs/but_cut.gif^Delete record^javascript:DeleteRecord(' + gridID + ');^_self</cell>';
                    xml = xml + '</row>';
                }
            }
            xml = '<rows total_count="' + arrObj.length + '">' + xml + '</rows>';

            $("#CapturedXml").val(JSON.stringify(arrObject));

            $('#cmdProceed').prop("disabled", true);

            return xml;
        }

        function createWindow(_ColumnID) {
            UpdateMembers(_ColumnID);
        }

        
        function UpdateMembers(_ColumnID) {
            var selectedRow = mygrid.getSelectedId();
            _ColumnID = mygrid.cells(selectedRow, 0).getValue();
            $("#RiskID,#ServiceID,#ProviderID").val('-');
            $("#ServiceStatusID").val('');
            for (var i in arrObj) {
                
                if (arrObj.hasOwnProperty(i)) {
                    
                    if (arrObj[i].ColumnId == _ColumnID) {

                        $("#ColumnID").val(arrObj[i].ColumnId);
                        $("#RiskID").val(arrObj[i].RiskId);
                        $("#ServiceID").val(arrObj[i].ServiceId);
                        $("#ProviderID").val(arrObj[i].ProviderId);
                        $("#ServiceStatusID").val(arrObj[i].ServiceStatusId);
                        $("#ServiceAccessDate").val(arrObj[i].ServiceAccessDate);
                        $("#ServiceNotAccessedReason").val(arrObj[i].ServiceNotAccessedReasonId);
                        $("#SocialWorkerFollowUp").val(arrObj[i].SocialWorkerFollowUp);
                        $("#VisitDate").val(arrObj[i].VisitDate);

                        $("#ServiceStatusID").trigger("change");
                    }
                }
            }
            
            $('#cmdProceed').prop("disabled", false);
            EVENTID = "EDIT";
        }

        function DeleteRecord(_ColumnID) {
            var selectedRow = mygrid.getSelectedId();
                var columnID = mygrid.cells(selectedRow, 0).getValue();
                // createWindow(columnID);
                var result = confirm("Do you want to delete The Record?");
                if (result) {
                    for (var i in arrObj) {
                        
                        if (arrObj.hasOwnProperty(i)) {
                            if (arrObj[i].ColumnId == columnID) {
                                var index = arrObj.indexOf(arrObj[i]);
                                if (index > -1) {
                                    arrObj.splice(index, 1);
                                    var myXml = createXmlstring(arrObj, 1);
                                    mygrid.clearAll();
                                    mygrid.parse(myXml);

                                    break;
                                }
                            }
                        }
                    }
            }
        }

        function isValid(str) {
            if (str === "" || str === "0" || str === 0) {
                return false;
            } else {
                return true;
            }
        }

        $("form").submit(function(e) {

            var errors = [];
            var html = '<ul>';
            $('#errors').empty();

            
            if($('#VisitDate').val()=='' || $('#VisitDate').val()==null){
                errors.push('<li>Visit Date Required</li>');
                html += errors.join('') + '</ul>'
                $('#errors').show();
                $('#errors').append(html);
                e.preventDefault();
                e.returnValue = false;
            }

            if($('#SocialWorkerFollowUp').val()=='' || $('#SocialWorkerFollowUp').val()==null){
                errors.push('<li>Social Worker Required</li>');
                html += errors.join('') + '</ul>'
                $('#errors').show();
                $('#errors').append(html);
                e.preventDefault();
                e.returnValue = false;
            }

            if(!ISUPDATED){
                errors.push('<li>No Service Status Has Been Updated!Please Select a Service from the grid and Update</li>');
                html += errors.join('') + '</ul>'
                $('#errors').show();
                $('#errors').append(html);
                e.preventDefault();
                e.returnValue = false;
            }
            
        });

    </script>
</asp:Content>