﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm4D>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Form 4D Household Listing
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("CaptureForm4D", "ComplianceCapture", FormMethod.Post, new { id = "CPForm" }))
   { %>


    <%: Html.Partial("_CommonSearch") %>
    <div id="gridbox" style="position:relative; width: 950px; height: 250px"></div>
    <span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>

    <br>
    <div class="form-actions">
        <button id="cmdNew" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Add Child
            Protection Profile</button>

        <button id="cmdBack" type="button"
            class="btn btn-icon btn-default glyphicons list"><i></i>Cases List</button>

        <button id="cmdBackFup" type="button"
            class="btn btn-icon btn-default glyphicons list"><i></i>Cases FollowUps List</button>
    </div>


    <br>
    <div id="error" style="color: red; display: none"></div>
    <%: Html.Hidden("currentPage", (int)ViewBag.CurrentPage) %>
    <%: Html.ValidationSummary(true) %>
    <%: Html.HiddenFor(model => model.ProfileDSHeaderID)%>
    <%: Html.HiddenFor(model => model.ProfileDSDetailID)%>
    <%: Html.HiddenFor(model => model.ClientTypeID)%>
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../Scripts/jtable/themes/lightcolor/gray/jtable.min.css" rel="stylesheet" />
    <script src="../Scripts/jtable/jquery.jtable.min.js"></script>
    <script type="text/javascript">
        var page_count = 10;
        var gridHeader =
            'UniqueID,ProfileHeaderID,ProfileDetailID,Region,Kebele,KebeleID,Woreda,Gote/Gare,Household,PSNP<span class="HeaderChange">_</span>Number,Household<span class="HeaderChange">_</span>Head,Child<span class="HeaderChange">_</span>Name,Child<span class="HeaderChange">_</span>Sex,Child<span class="HeaderChange">_</span>Age(YRS),Fiscal<span class="HeaderChange">_</span>Year';
        var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
        var mygrid;

        $(document).ready(function () {
            $('#loading').hide();
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,0,100,100,0,100,0,120,150,190,190,0,0,0");
            mygrid.setColAlign(
                "left,left,left,left,left,left,left,left,left,left,left,left,left,left,right"
            );
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () {
                showLoading(true)
            });
            mygrid.init();

            var currentPage = $('#currentPage').val();
            mygrid.load("/ComplianceCapture/FetchForm4DHHList?RecCount=" + page_count,
                function () {
                    mygrid.changePage(currentPage);
                });
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);

                $.ajax({
                    type: "POST",
                    url: "../ComplianceCapture/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });

            mygrid.attachEvent("onRowSelect", function (id, ind) {
                var selectedRow = mygrid.getSelectedId();
                var _ProfileHeaderID = mygrid.cells(selectedRow, 1).getValue();
                var _ProfileDetailID = mygrid.cells(selectedRow, 2).getValue();
                var _ClientTypeID = mygrid.cells(selectedRow, 8).getValue();
                $('#ProfileDSHeaderID').val(_ProfileHeaderID);
                $('#ProfileDSDetailID').val(_ProfileDetailID);
                $('#ClientTypeID').val(_ClientTypeID);

                $('#error').css({
                    'display': 'none'
                });
            });

            dhtmlxError.catchError("ALL", my_error_handler);

            $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Household ID'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Household Head'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 6).text('Woreda'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Kebele'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Gote'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Gare'));

            $("#cmdNew").click(function () {
                var ProfileHeaderID = $('#ProfileDSHeaderID').val();
                var ProfileDetailID = $('#ProfileDSDetailID').val();
                var ClientTypeID = $('#ClientTypeID').val();
                var Records = recordExists(ProfileDetailID);

                if (ProfileHeaderID) {
                    if(parseInt(Records) > 0){
                        $('#error').show();
                        $('#error').empty();
                        $('#error').append('<ul><li>Child Protection Profile Exits for the Selected Child! You can Edit or FollowUp the Case.</li></ul>');
                        return;
                    }else{
                        window.location.href = "/ComplianceCapture/CaptureForm4D?ProfileDetailID=" +
                        ProfileDetailID + "&ClientTypeID=" +
                        ClientTypeID;
                    }
                } else {
                    $('#error').show();
                    $('#error').empty();
                    $('#error').append('<ul><li>Please Select Child</li></ul>');
                }
            });

            $("#cmdBack").click(function () {
                    window.location.href = "/ComplianceCapture/Form4DFormList"
            });

            $("#cmdBackFup").click(function () {
                    window.location.href = "/ComplianceCapture/Form4DFollowUpList"
            });

        });

        function reloadGrid() {
            $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", true);

            var Search_TypeID = $("#SearchTypeID").val();
            var Search_Keyword = $("#SearchKeyword").val();
            showLoading(true);

            mygrid.clearAndLoad("/ComplianceCapture/FetchForm4DHHList?RecCount=" + page_count +
                "&isSearch=1&SearchTypeID=" +
                Search_TypeID + "&SearchKeyword=" + Search_Keyword);

            $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", false);
        }


        function recordExists(ProfileId) {
            var exists = 0;

            var params ={
                ChildDetailID : ProfileId
            }

            $.ajax({
                type: "POST",
                async: false,
                url: "/ComplianceCapture/FetchJsonForm4DIfExists",
                data: JSON.stringify(params),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(parseInt(data.Result) > 0){
                        exists = 1;
                    }
                },
                error: function (xhr,error, errorThrown) {
                    $('#error').show();
                    $('#error').empty();
                }
            });

            return exists;

        }
    </script>
</asp:Content>