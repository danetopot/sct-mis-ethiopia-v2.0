﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ModifyForm4D>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Modify Form 4D
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("CloseForm4D", "ComplianceCapture", FormMethod.Post, new { id = "CPForm" }))
   { %>

    <div class="col-4">
        <label>
            Region
            <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Woreda
            <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Kebele
            <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
        </label>
    </div>
    <br>
    <div class="col-4">
        <label>
            Gote
            <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Gare
            <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            PSNP HH #
            <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "6", @maxlength="20" })%>
        </label>
    </div>

    <br>

    <div class="col-4">
        <label>
            Name of household head
            <%: Html.TextBoxFor(model => model.NameOfHouseHoldHead, new { tabindex = "8" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Gender of HH Member (Child)
            <%: Html.DropDownListFor(model => model.HouseHoldMemberSex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Age of HH member (Child)
            <%: Html.TextBoxFor(model => model.HouseHoldMemberAge, new { tabindex = "14" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <br>

    <div class="col-4">
        <label>
            Names of HH member (Child)
            <%: Html.TextBoxFor(model => model.HouseHoldMemberName, new { tabindex = "14" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Child Protection ID * 
            <%: Html.TextBoxFor(model => model.ChildProtectionId, new { tabindex = "1" , @maxlength="50" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Child Referred To Case Management *
            <%: Html.DropDownListFor(model => model.CaseManagementReferral,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "2" })%>
        </label>
    </div>
    <br>
    <div class="col-4">
        <label>
            Collection Date * 
            <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "3",@Style="width:95%;"})%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Social Worker * 
            <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "4" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Remarks
            <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "5" , @maxlength="50" })%>
        </label>
    </div>

   

    <hr /> 
    <div id="divClosedReason" class="col-12">
        <label>
            Reason for Closing Case
            <%: Html.TextAreaFor(model => model.ClosedReason,3,135,htmlAttributes: new {style="width: 100%; max-width: 100%;" })%>
        </label>
    </div>
    <br />
        <div id="errors" style="color: red;"></div>
    <br>
    <hr /> 
    <br>
    
    
    
    <div id="errors" style="color: red;"></div>
    <br>

    <div id="gridbox" style="position: relative; width: 980px; height: 200px;"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div>
    <span id="recfound"></span>


    <br />
    <div class="form-actions">
        <button id="cmdModify" type="submit" class="btn btn-icon btn-primary glyphicons remove"><i></i>Close Child
            Protection
            Case</button>
        <button id="cmdBack" type="button"
            class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <%: Html.HiddenFor(model => model.ChildProtectionHeaderID)%>
    <%: Html.HiddenFor(model => model.ColumnID)%>
    <%: Html.HiddenFor(model => model.ProfileDSHeaderID)%>
    <%: Html.HiddenFor(model => model.ProfileDSDetailID)%>
    <%: Html.HiddenFor(model => model.ClientTypeID)%>
    <%: Html.HiddenFor(model => model.CapturedXml)%>
    <%: Html.HiddenFor(model => model.AllowEdit)%>
    <%: Html.HiddenFor(model => model.AllowClose)%>
    <%: Html.ValidationSummary(true) %>

    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../Scripts/jtable/themes/lightcolor/gray/jtable.min.css" rel="stylesheet" />
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script src="../Scripts/jtable/jquery.jtable.min.js"></script>
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>
    <script type="text/javascript">
        var EVENTID = null;
        var arrObj = [];
        var SerialCount = 0;
        var page_count = 10;
        var mygrid;
        var gridHeader =
            'ColumnID,ChildProtectionDetailID,RiskID,Risk<span class="HeaderChange">_</span>Identified,ServiceID,Service<span class="HeaderChange">_</span>Provided,ProviderID,Service<span class="HeaderChange">_</span>Provider, Edit, Delete';
        var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,img,img';


        $(document).ready(function () {

            $('#loading').hide();
            $('#CollectionDate').Zebra_DatePicker({
                direction: -1, // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            $("#RegionID,#WoredaID,#KebeleID,#Gote,#Gare,#HouseHoldIDNumber,#NameOfHouseHoldHead,#HouseHoldMemberName,#HouseHoldMemberSex,#HouseHoldMemberAge")
                .prop("disabled", true);
            $('#CaseManagementReferral,#CollectionDate,#SocialWorker,#Remarks,#ChildProtectionId').prop("disabled", true);

            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,0,320,0,335,0,320,0,0");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,right,right");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () {
                showLoading(true)
            });
            mygrid.init();
            mygrid.loadXML("/ComplianceCapture/FetchGridForm4DDetailsByID?RecCount=" + page_count + "&Id=" + $("#ChildProtectionHeaderID").val());
            dhtmlxError.catchError("ALL", my_error_handler);

            $.ajax({
                type: "POST",
                url: "../ComplianceCapture/FetchJsonForm4DDetailsByID",
                data: "{  'RecCount':" + page_count + ",ChildProtectionHeaderID : '" + $("#ChildProtectionHeaderID").val() + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                error: function (xx, yy) {
                    
                },
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        MemberCount = data.length;
                        if (MemberCount != undefined) {

                            for (var i = 0; i < data.length; i++) {
                                var theDtls = new Object;
                                var d = data[i];
                                SerialCount = SerialCount + 1;
                                theDtls.ColumnId = SerialCount;
                                theDtls.ChildProtectionDetailID = d.ChildProtectionDetailID;
                                theDtls.RiskId = d.RiskId;
                                theDtls.RiskName = d.RiskName;
                                theDtls.ServiceId = d.ServiceId;
                                theDtls.ServiceName = d.ServiceName;
                                theDtls.ProviderId = d.ProviderId;
                                theDtls.ProviderName = d.ProviderName;

                                arrObj.push(theDtls);
                            }
                            
                            $("#CapturedXml").val(JSON.stringify(arrObj));
                        }
                    }
                }
            });

            $("#cmdBack").click(function () {
                window.location.href = "../ComplianceCapture/Form4DFormList";
            });

            var allowClose = $('#AllowClose').val();
            if(allowClose == 1){
                $('#divClosedReason').show();
                $('#ClosedReason').val('');
            }else{
                $('#divClosedReason').hide();
            }



            $("form").submit(function(e) {
                
                $('#errors').empty();

                if ($('#ClosedReason').val() == '-') {
                    errors.push('<li>Reason for Closing Case Required</li>');
                    e.preventDefault();
                    e.returnValue = false;
                }
            });
        });

    </script>
</asp:Content>