﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Cases FollowUps Listing
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("CaptureForm4D", "ComplianceCapture", FormMethod.Post, new { id = "CPForm" }))
        { %>
            <%: Html.Partial("_CommonSearch") %>
            <div id="gridbox" style="position:relative; width: 1200px; height: 300px"></div>
            <span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>

            
           
            <br>
            <div class="form-actions">
                <button id="cmdBack" type="button"
                    class="btn btn-icon btn-default glyphicons home"><i></i>Household List</button>

                <button id="cmdForms" type="button"
            class="btn btn-icon btn-primary glyphicons list"><i></i>Cases List</button>

            </div>

            <%: Html.Hidden("currentPage", (int)ViewBag.CurrentPage) %>
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../Scripts/jtable/themes/lightcolor/gray/jtable.min.css" rel="stylesheet" />
    <script src="../Scripts/jtable/jquery.jtable.min.js"></script>
    <script type="text/javascript" src="../Scripts/assets/js/app/CapturedForm4DFollowUpList.js"></script>

</asp:Content>