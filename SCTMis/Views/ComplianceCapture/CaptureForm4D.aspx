﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm4D>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Capture Form 4D
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("CaptureForm4D", "ComplianceCapture", FormMethod.Post, new { id = "CPForm" }))
   { %>

    <div class="col-4">
        <label>
            Region
            <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Woreda
            <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Kebele
            <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
        </label>
    </div>
    <br>
    <div class="col-4">
        <label>
            Gote
            <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Gare
            <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            PSNP HH #
            <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "6", @maxlength="20" })%>
        </label>
    </div>

    <br>

    <div class="col-4">
        <label>
            Name of household head
            <%: Html.TextBoxFor(model => model.NameOfHouseHoldHead, new { tabindex = "8" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Gender of HH Member (Child)
            <%: Html.DropDownListFor(model => model.HouseHoldMemberSex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Age of HH member (Child)
            <%: Html.TextBoxFor(model => model.HouseHoldMemberAge, new { tabindex = "14" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <br>

    <div class="col-4">
        <label>
            Names of HH member (Child)
            <%: Html.TextBoxFor(model => model.HouseHoldMemberName, new { tabindex = "14" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Child Protection ID * 
            <%: Html.TextBoxFor(model => model.ChildProtectionID, new { tabindex = "1" , @maxlength="50" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Child Referred To Case Management *
            <%: Html.DropDownListFor(model => model.ReferredToCaseManagement,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "2" })%>
        </label>
    </div>
    <br>
    <div class="col-4">
        <label>
            Collection Date * 
            <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "3",@Style="width:95%;"})%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Social Worker * 
            <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "4" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Remarks
            <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "5" , @maxlength="50" })%>
        </label>
    </div>

    <hr />
    <br>
   
    <div>
        <b><span id="riskDefinition"></span></b>
    </div>
    <br><br>
    <div class="col-4">
        <label>
            Risk Identified *
            <%: Html.DropDownListFor(model => model.RiskID,
                    new SelectList(Model.risks, "ID", "Name"), new { tabindex = "3" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Service To Be Provided *
            <%: Html.DropDownListFor(model => model.ServiceID,
                    new SelectList(Model.services, "ID", "Name"), new { tabindex = "4" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Service Provider *
            <%: Html.DropDownListFor(model => model.ProviderID,
                    new SelectList(Model.providers, "ID", "Name"), new { tabindex = "5" })%>
        </label>
    </div>
    <button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus"
        tabindex="26"><i></i>Proceed</button>
    <br />
    <div id="errors" style="color: red;"></div>
    <br>

    <div id="gridbox" style="position: relative; width: 980px; height: 200px;"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div>
    <span id="recfound"></span>


    <br />
    <div class="form-actions">
        <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Child
            Protection
            Profile</button>
        <button id="cmdBack" type="button"
            class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <%: Html.HiddenFor(model => model.ColumnID)%>
    <%: Html.HiddenFor(model => model.ProfileDSHeaderID)%>
    <%: Html.HiddenFor(model => model.ProfileDSDetailID)%>
    <%: Html.HiddenFor(model => model.ClientTypeID)%>
    <%: Html.HiddenFor(model => model.CapturedXml)%>
    <%: Html.ValidationSummary(true) %>

    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../Scripts/jtable/themes/lightcolor/gray/jtable.min.css" rel="stylesheet" />
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script src="../Scripts/jtable/jquery.jtable.min.js"></script>
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>
    <script type="text/javascript">
        var EVENTID = null;
        var arrObj = [];
        var SerialCount = 0;
        var page_count = 10;
        var mygrid;
        var gridHeader =
            'ColumnID,RiskID,Risk<span class="HeaderChange">_</span>Identified,ServiceID,Service<span class="HeaderChange">_</span>Provided,ProviderID,Service<span class="HeaderChange">_</span>Provider, Edit';
        var gridColType = 'ro,ro,ro,ro,ro,ro,ro,img';


        $(document).ready(function () {

            $('#loading').hide();

            $('#CollectionDate').Zebra_DatePicker({
                direction: -1, // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            $("#RegionID,#WoredaID,#KebeleID,#Gote,#Gare,#HouseHoldIDNumber,#NameOfHouseHoldHead,#HouseHoldMemberName,#HouseHoldMemberSex,#HouseHoldMemberAge")
                .prop("disabled", true);
            $("#ChildProtectionID,#ReferredToCaseManagement").val('');
            $("#RiskID,#ServiceID,#ProviderID").val('-');
            $('#ChildProtectionID').focus();

            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,300,0,300,0,300,80");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,right");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () {
                showLoading(true)
            });
            mygrid.init();


            $("#cmdProceed").click(function () {
                var errors = [];
                var html = '<ul>';
                valid = true;
                $('#errors').empty();

                if ($('#RiskID').val() == '-') {
                    errors.push('<li>Risk Name Required</li>');
                    valid = false;
                }
                if ($('#ServiceID').val() == '-') {
                    errors.push('<li>Service Name Required</li>');
                    valid = false;
                }
                if ($('#ProviderID').val() == '-') {
                    errors.push('<li>Provider Name Required</li>');
                    valid = false;
                }

                if (!valid) {
                    html += errors.join('') + '</ul>'
                    $('#errors').show();
                    $('#errors').append(html);
                    return valid;
                } else {
                    $('#errors').hide();
                }

                var theDtls = new Object;
                if (EVENTID == "EDIT") {
                    for (var i in arrObj) {
                        if (arrObj.hasOwnProperty(i)) {
                            if (arrObj[i].ColumnID == $("#ColumnID").val()) {
                                arrObj[i].RiskID = $("#RiskID").val();
                                arrObj[i].RiskName = $("#RiskID").find('option:selected').text();
                                arrObj[i].ServiceID = $("#ServiceID").val();
                                arrObj[i].ServiceName = $("#ServiceID").find('option:selected').text();
                                arrObj[i].SexName = $("#Sex").find('option:selected').text();
                                arrObj[i].ProviderID = $("#ProviderID").val();
                                arrObj[i].ProviderName = $("#ProviderID").find('option:selected')
                            .text();
                            }
                        }
                    }
                } else {
                    $('#errors').hide();

                    SerialCount = SerialCount + 1;
                    theDtls.ColumnID = SerialCount;
                    theDtls.RiskID = $("#RiskID").val();
                    theDtls.RiskName = $("#RiskID").find('option:selected').text();
                    theDtls.ServiceID = $("#ServiceID").val();
                    theDtls.ServiceName = $("#ServiceID").find('option:selected').text();
                    theDtls.ProviderID = $("#ProviderID").val();
                    theDtls.ProviderName = $("#ProviderID").find('option:selected').text();

                    arrObj.push(theDtls);
                }
                var myXml = createXmlstring(arrObj, 1);
                mygrid.clearAll();
                mygrid.parse(myXml);
                $("#RiskID,#ServiceID,#ProviderID").val('-');
                $('#RiskID').focus();
                $("#cmdProceed").html('<i></i>Proceed');
                $("#riskDefinition").text('');
                EVENTID = null;
            })

            $("#cmdBack").click(function () {
                window.location.href = "../ComplianceCapture/Form4DHHList";
            });
            $('#loading').hide();

            $(document).on("change", "#RiskID",
            function (e) {
                var riskID = $("#RiskID").val();
                var riskName = DisplayVals(riskID);
                $("#riskDefinition").text(riskName);
            });
        });


        function UpdateMembers(_ColumnID) {
            var selectedRow = mygrid.getSelectedId();
            _ColumnID = mygrid.cells(selectedRow, 0).getValue();
            $("#RiskID,#ServiceID,#ProviderID").val('-');
            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == _ColumnID) {
                        $("#ColumnID").val(arrObj[i].ColumnID);
                        $("#RiskID").val(arrObj[i].RiskID);
                        $("#ServiceID").val(arrObj[i].ServiceID);
                        $("#ProviderID").val(arrObj[i].ProviderID);
                    }
                }
            }

            $("#RiskID,#ServiceID,#ProviderID").prop("disabled", false);
            $("#cmdProceed").html('<i></i>Update Details');
            EVENTID = "EDIT";
        }

        function DeleteRecord(_ColumnID) {
            if ($("#AllowEdit").val().toUpperCase() != "FALSE") {
                var selectedRow = mygrid.getSelectedId();
                var columnID = mygrid.cells(selectedRow, 0).getValue();
                createWindow(columnID);
                var result = confirm("Do you want to delete The Record?");
                if (result) {
                    for (var i in arrObj) {
                        
                        if (arrObj.hasOwnProperty(i)) {
                            if (arrObj[i].ColumnID == columnID) {
                                var index = arrObj.indexOf(arrObj[i]);
                                if (index > -1) {
                                    arrObj.splice(index, 1);

                                    var myXml = createXmlstring(arrObj, 1);
                                    mygrid.clearAll();
                                    mygrid.parse(myXml);

                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        function createXmlstring(arrObject, inMemory) {
            var xml;
            var gridID;
            xml = '';
            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    xml = xml + '<row id="' + i + '">';
                    for (var j in arrObj[i]) {
                        if (arrObj[i].hasOwnProperty(j)) {
                            if (j == "ColumnID") {
                                xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                                gridID = arrObj[i][j];
                            }
                        }
                        if (j == "RiskID" || j == "RiskName" || j == "ServiceID" || j == "ServiceName" || j ==
                            "ProviderID" || j == "ProviderName") {
                            xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        }
                    }
                    xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID +
                        ');^_self</cell>';
                    xml = xml + '</row>';
                }
            }
            xml = '<rows total_count="' + arrObj.length + '">' + xml + '</rows>';

            $("#CapturedXml").val(JSON.stringify(arrObject));
            return xml;
        }

        function DisplayVals(val) {
           var strDisplay = "";

           if(val == "1"){
               strDisplay = "Maltreatment (including violent punishment) involves physical, sexual and psychological/emotional violence; and neglect of infants, children and adolescents by parents, caregivers and other authority figures, most often in the home but also in settings such as schools and orphanages";
           }else if(val == "2"){
               strDisplay = "Bullying (including cyber-bullying) is unwanted aggressive behaviour by another child or group of children who are neither siblings nor in a romantic relationship with the victim. It involves repeated physical, psychological or social harm, and often takes place in schools and other settings where children gather, and online.";
           }else if(val == "3"){
               strDisplay = "Youth violence is concentrated among those aged 10–29 years, occurs most often in community settings between acquaintances and strangers, includes physical assault with weapons (such as guns and knives) or without weapons, and may involve gang violence";
           }else if(val == "4"){
               strDisplay = "Intimate partner violence (or domestic violence) involves violence by an intimate partner or ex-partner. Although males can also be victims, intimate partner violence disproportionately affects females. It commonly occurs against girls within child and early/forced marriages. Among romantically involved but unmarried adolescents it is sometimes called “dating violence.";
           }else if(val == "5"){
               strDisplay = "Sexual violence includes non-consensual completed or attempted sexual contact; non-consensual acts of a sexual nature not involving contact (such as voyeurism or sexual harassment); acts of sexual trafficking committed against someone who is unable to consent or refuse; and online exploitation.";
           }else if(val == "6"){
               strDisplay = "Emotional or psychological violence and witnessing violence includes restricting a child’s movements, denigration, ridicule, threats and intimidation, discrimination, rejection and other non-physical forms of hostile treatment.Witnessing violence can involve forcing a child to observe an act of violence, or the incidental witnessing of violence between two or more other persons.";
           }else if(val == "7"){
               strDisplay = "Gender-based violence is defined as any type of violence directed against girls or boys because of one’s gender.";
           }else if(val == "8"){
               strDisplay = "Child is unaccompanied (without a parent or other relative who, by law or custom, is responsible for providing care); OR child is separated (separated from both parents, or from their previous legal or customary primary caregiver, but not necessarily from other relatives); OR child is orphaned without an appropriate caregiver.";
           }else if(val == "9"){
               strDisplay = "Child is being moved within the country, or across borders, for the purpose of exploiting the child, including through prostitution or other forms of sexual exploitation, forced labour, slavery or slavery-like practices, servitude, or removal of organs.";
           }else if(val == "10"){
               strDisplay = "Child lives with disability/disabilities including long-term physical, mental, intellectual, or sensory impairments which in interaction with various barriers that may hinder the child’s full and effective participation in society on an equal basis with others.";
           }else if(val == "11"){
               strDisplay = "Child is engaged in child labour which interferes with the child’s ability to attend regular school, or that is mentally, physically, socially or morally dangerous and harmful. Ethiopian law prohibits employment of children below 14 years, and of children 14 to 17 years in work which is prejudicial to their life or health.";
           }else if(val == "12"){
               strDisplay = "Child is in conflict with the law, i.e. child is under 18 and  is alleged as, accused of, or recognised as having infringed the penal law; OR Child is a victim and/or witness of crime and thus child is in contact with justice systems; or child is in contact with justice systems for other reasons such as care, custody, protection or inheritance.";
           }else if(val == "13"){
               strDisplay = "Child is moving for a variety of reasons, voluntarily or involuntarily, within or between countries, with or without his/her parents or other primary caregivers, and whose movement might also place the child at risk (or at an increased risk) of economic or sexual exploitation, abuse, neglect and violence.";
           }else if(val == "14"){
               strDisplay = "Child is at risk of formal or informal union before the age of 18 and of female genital mutilation/cutting.";
           }else if(val == "15"){
               strDisplay = "Child who a) depends on the streets to live and/or work, whether alone, with peers or with family; and (b) a wider population of children who have formed strong connections with public spaces and for whom the street plays a vital role in their everyday lives and identities. This wider population includes children who periodically, but not always, live and/or work on the streets and children who do not live or work on the streets but who regularly accompany their peers, siblings or family in the streets. Concerning children in street situations, “being in public spaces” is understood to include spending a significant amount of time on streets or in street markets, public parks, public community spaces, squares and bus and train stations. It does not include public buildings such as schools, hospitals or other comparable institutions.";
           }else{
               strDisplay = val;
           }

           return strDisplay;
        }

        function isValid(str) {
            if (str === "" || str === "0" || str === 0) {
                return false;
            } else {
                return true;
            }
        }

        
    </script>
</asp:Content>