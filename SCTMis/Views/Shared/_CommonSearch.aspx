﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
    <%--<form id="Form1" runat="server" style="padding: 0;">--%>
        <table id="tblInput" style="width:100%; margin-bottom:3px">
            <tr>
                <td style="width:22%;"></td>
                <td style="width:22%;"></td>
                <td style="width:12%;"></td>
                <td style="width:14%;"></td>
                <td style="width:10%;"></td>
                <td style="width:10%;"></td>
                <td style="width:10%;"></td>
            </tr>
            <tr>
                <td colspan="1"><label class="control-label" for="SearchTypeID">Search Criteria</label></td>
                <td colspan="1"><label class="control-label" for="SearchKeyword">Keyword</label></td>
            </tr>
            <tr>
                <td colspan="1">
                    <select id="SearchTypeID" name="SearchTypeID" style="width:96%">
                        </select>
                </td>
                <td colspan="1">
                    <input id="SearchKeyword" name="SearchKeyword" type="text" style="width:96%" />
                </td>
                
                <td>
                    <button id="cmdSearch" onclick="reloadGrid()" type="button" class="btn btn-primary btn-icon glyphicons search"><i></i>Search</button>
                </td>
                   <td></td>                     
            </tr>

        </table>
    <%--</form>--%>
