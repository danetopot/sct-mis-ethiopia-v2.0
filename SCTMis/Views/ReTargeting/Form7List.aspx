﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.Form7SummaryGrid>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Form 7 Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("_CommonSearch") %>
    <div id="gridbox" style="position:relative; width: 800px; height: 250px"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>

    <div class="form-actions">
	    <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <%: Html.Hidden("currentPage", (int)ViewBag.CurrentPage) %>

    <%: Html.HiddenFor(model => model.KebeleID)%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript" src="../Scripts/assets/js/app/Form7List.js"></script>
</asp:Content>
