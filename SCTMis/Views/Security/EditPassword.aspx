﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.PasswordUserModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit User Password
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Edit User Password</h3>        
<% using (Html.BeginForm("UpdatePassword", "Security", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-2">                  
            <label>
                Password <%: Html.ValidationMessageFor(model => model.Password) %>
                <%: Html.PasswordFor(model => model.Password, new { tabindex = "5" })%>
            </label>
            </div>
        
            <div class="col-2">                
            <label>
                Confirm Password <%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
                <%: Html.PasswordFor(model => model.ConfirmPassword, new { tabindex = "6" })%>
            </label>
            </div>
            <%: Html.HiddenFor(m => m.UserID)%>
            <%: Html.HiddenFor(m => m.UserName)%>
            <%: Html.ValidationSummary(true) %><br />

    <div class="form-actions">
	    <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Modify Password</button>
	    <button id="cmdCancel" type="button" onclick="window.location.href = '../Security/Users'" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%--<%: Scripts.Render("~/bundles/jqueryval") %>--%>
    <%--<script src="../jsFile/Security/EditUsers.js"></script>--%>
</asp:Content>