﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.RegisterRoleModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Add New Roles
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Add New Role</h3>        
<% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--<%: Html.AntiForgeryToken() %>--%>
        <%--The First Row--%>
            <div class="col-2">                
            <label>
                Role Name <%: Html.ValidationMessageFor(model => model.RoleName) %>
                <%: Html.TextBoxFor(model => model.RoleName, new { tabindex = "1" })%>
            </label>
            </div>
            <%: Html.ValidationSummary(true) %><br />

    <div class="form-actions">
	    <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Role</button>
	    <button id="cmdCancel" type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Back</button>
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script src="../Scripts/assets/js/app/Roles.js"></script>
</asp:Content>
