﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ReportEngineModel>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Reporting Engine
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("KiparaNgoto", "Security", FormMethod.Post, new { id = "formReportEngine" }))
   { %>
    <%--The First Row--%>
    <div class="col-4">
        <label>
            Report Name
            <%: Html.ValidationMessageFor(model => model.ReportName) %>
            <%: Html.DropDownListFor(model => model.ReportName,
                    new SelectList(Model.rptTypes, "ReportID", "ReportName"), new { tabindex = "3" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Reporting Period <%: Html.ValidationMessageFor(model => model.ReportingPeriod) %>
            <%: Html.DropDownListFor(model => model.ReportingPeriod,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"))%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Client Type
            <%: Html.ValidationMessageFor(model => model.ClientID) %>
            <%: Html.DropDownListFor(model => model.ClientID,
                    new SelectList(Model.clientTypes, "ClientTypeID", "ClientTypeName"), new { tabindex = "1" })%>
        </label>
    </div>
    
    <hr>
    <div class="col-4">
        <label>
            Region
            <%: Html.ValidationMessageFor(model => model.Region) %>
            <%: Html.DropDownListFor(model => model.Region,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { @multiple = "multiple", @class = "form-control multi-select " })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Woreda
            <%: Html.ValidationMessageFor(model => model.Woreda) %>
            <%: Html.DropDownListFor(model => model.Woreda,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { @multiple = "multiple", @class = "form-control multi-select " })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Kebele 
            <%: Html.ValidationMessageFor(model => model.Kebele) %>
            <%: Html.DropDownListFor(model => model.Kebele,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { @multiple = "multiple", @class = "form-control multi-select " })%>
        </label>
    </div>
    <br />
    <div class="form-actions">
        <%: Html.HiddenFor(model => model.LocationType) %>
        <button id="cmdProduce" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i><span>Produce</span></button>
    </div>
    <br />
    <br />

    <div id="errors" style="color: red;"></div>
    <%: Html.ValidationSummary(true) %>
    <% } %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../Scripts/assets/css/bootstrap-multiselect.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/assets/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#Region,#Woreda,#Kebele').multiselect({
            selectAllValue: '0',
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            numberDisplayed: 1,
            maxHeight: 300,
            buttonWidth: '200px',
            buttonClass: 'btn btn-white',
            nonSelectedText: 'Please Select'
        });

        $('#Region,#Woreda,#Kebele').multiselect('disable');
        $('#Region').multiselect('clearSelection');
        $('#Woreda,#Kebele').empty();
        $('#Woreda,#Kebele').multiselect('rebuild');
        
        $("#ClientID, #ReportingPeriod").val("-")
        $("#ClientID").prop("disabled", true);

        $("#cmdProduce").prop("disabled", false);
        $('#loading').hide();
        $('#Region').focus();

        var locationType = $('#LocationType').val();

        switch (locationType) 
        {
            case '1':
                $('#formReportEngine').attr('action', '/security/printpdf');
                $('#Region').attr('required', 'required');
                $('#Woreda').attr('required', 'required');

                $('#cmdProduce span').text('Download Report');

                break;

            case '2':
                $('#formReportEngine').attr('action', '/security/printform6');
                $('#formReportEngine').attr('target', '/security/_blank');

                $('#Woreda').prop('disabled', true);

                $('#Region').attr('required', 'required');
                $('#cmdProduce span').text('View Report');
                break;

            case '3':
                $('#formReportEngine').attr('action', '/security/printform6');
                $('#formReportEngine').attr('target', '/security/_blank');

                $('#Region').prop('disabled', true);
                $('#Woreda').prop('disabled', true);

                $('#cmdProduce span').text('View Report');

                break;
        }
        
        $('#formReportEngine').submit(function(ev) {
            ev.preventDefault()

            var errors = [];
            var html = '<ul>';
            $('#errors').empty();
            $('#loading').hide();
            var locationType = $('#LocationType').val();
            var reportType = $('#ReportName').val();
            var isValid = true;
            
            if ((reportType === null) | (reportType === undefined) | (reportType === '')) {
                isValid = false;
                    $('#ReportName').focus();
                    errors.push('<li>Report Name Required</li>');
            } else {
                isValid = true;
            };

            if(reportType == '4D' || reportType == '4D2'){
                var Region = $('#Region').val();
                var Woreda = $('#Woreda').val();
                var kebeleID = $('#Kebele').val();
                var clientID = $('#ClientID').val();

                if ((clientID === null) | (clientID === undefined) | (clientID === '0')) {
                    isValid = false;
                        $('#ClientID').focus();
                        errors.push('<li>Client Type Required</li>');
                } else {
                    isValid = true;
                    
                };
                
                $('#formReportEngine').attr('action', '/security/printform4');
                $('#formReportEngine').attr('target', '/security/_self');
                $('#cmdProduce span').text('View Report');
            }
            else
            {
                switch (locationType) 
                {
                    case '1':
                        $('#formReportEngine').attr('action', '/security/printpdf');
                        $('#Region').attr('required', 'required');
                        $('#Woreda').attr('required', 'required');

                        $('#cmdProduce span').text('Download Report');

                        break;

                    case '2':
                        $('#formReportEngine').attr('action', '/security/printform6');
                        $('#formReportEngine').attr('target', '/security/_blank');

                        $('#Woreda').prop('disabled', true);

                        $('#Region').attr('required', 'required');
                        $('#cmdProduce span').text('View Report');
                        break;

                    case '3':
                        $('#formReportEngine').attr('action', '/security/printform6');
                        $('#formReportEngine').attr('target', '/security/_blank');

                        $('#Region').prop('disabled', true);
                        $('#Woreda').prop('disabled', true);

                        $('#cmdProduce span').text('View Report');

                        break;
                }
            }


            

            if (!isValid) {
                html += errors.join('') + '</ul>'
                $('#errors').show();
                $('#errors').append(html);
                return isValid;
            } else {
                $('#errors').hide();
            }
            
            if (isValid === true) {
                this.submit();
            }
        });

        $(document).on("change", "#ReportName",
        function (e) {
            var reportType = $('#ReportName').val();
            if(reportType == '4D' || reportType == '4D2'){
                $('#Region,#Woreda,#Kebele').multiselect('enable');
                $("#ClientID,#ReportName,#ReportingPeriod").prop("disabled", false);
            }else{
                $('#Region,#Woreda,#Kebele').multiselect('disable');
                $("#ReportingPeriod,#ClientID").prop("disabled", true);
                $("#Region,#Woreda,#Kebele").multiselect('clearSelection');
                $("#ClientID").val("-")
                $('#errors').empty();
            }
        });

        $(document).on("change", "#Region",
            function (e) {
                regionChange();
        });

        $(document).on("change", "#Woreda",
            function (e) {
                woredaChange();
        });
    });

    function regionChange() {
        var region = $("#Region").val();
        if (region == null) { region = 0; }

        $.ajax({
            type: "POST",
            url: "../Security/SelectWoreda",
            data: "{  'RegionID' : '" + region + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xx, yy) {
                alert(yy);
            },
            success: function (data) {
                $('#Woreda').empty();
                for (var i = 0; i < data.length; i++) {
                    var d = data[i];
                    $('#Woreda').append($("<option></option>").attr("value", d.WoredaID).text(d.WoredaName));
                }
                $('#Woreda').multiselect('rebuild');
                woredaChange();
            }
        });
    }

    function woredaChange() {

        var woreda = $("#Woreda").val();
        if (woreda == null) { woreda = 0; }

        $.ajax({
            type: "POST",
            url: "../Security/SelectKebele",
            data: "{  'WoredaID' : '" + woreda + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xx, yy) {
                alert(yy);
            },
            success: function (data) {
                $('#Kebele').empty();
                for (var i = 0; i < data.length; i++) {
                    var d = data[i];
                    $('#Kebele').append($("<option></option>").attr("value", d.KebeleID).text(d.KebeleName));
                }
                $('#Kebele').multiselect('rebuild');
                kebeleChange();
            }
        });
    }

     function kebeleChange() {
    
        var kebele = $("#ReportKebele").val();
        if (kebele == null) { kebele = 0; }

        $.ajax({
            type: "POST",
            url: "../Security/SelectGote",
            data: "{  'KebeleID' : '" + kebele + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xx, yy) {
                alert(yy);
            },
            success: function (data) {
                $('#ReportGote').empty(); 
                for (var i = 0; i < data.length; i++) {
                    var d = data[i];
                    console.log(d);
                    $('#ReportGote').append($("<option></option>").attr("value", d.GoteName).text(d.GoteName));
                }
                $('#ReportGote').multiselect('rebuild');
            }
        });
     }
    
    
</script>
</asp:Content>