﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ModifyPasswordModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Password Change
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h5>Change Your Password</h5>        
<% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-2">                  
            <label>
                Current Password <%: Html.ValidationMessageFor(model => model.OldPassword) %>
                <%: Html.PasswordFor(model => model.OldPassword, new { tabindex = "1" })%>
            </label>
            </div>
    <br />
            <div class="col-2">                  
            <label>
                New Password <%: Html.ValidationMessageFor(model => model.Password) %>
                <%: Html.PasswordFor(model => model.Password, new { tabindex = "2" })%>
            </label>
            </div>
        
            <div class="col-2">                
            <label>
                Confirm Password <%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
                <%: Html.PasswordFor(model => model.ConfirmPassword, new { tabindex = "3" })%>
            </label>
            </div>
            <%: Html.HiddenFor(m => m.UserID)%>
            <%: Html.HiddenFor(m => m.UserName)%>
            <%: Html.ValidationSummary(true) %><br />

    <div class="form-actions">
	    <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" tabindex="4"><i></i>Change Password</button>
	    <%--<button id="cmdCancel" type="button" onclick="window.location.href = '../Security/Users'" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>--%>
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/bundles/jqueryval") %>
    <%--<script src="../jsFile/Security/EditUsers.js"></script>--%>
</asp:Content>