﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.RegisterRoleModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Role Profiles
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<div id="gridbox" style="position:relative; width:100%;height:550px;background-color:white; margin:20px; min-width:0;font-size: 100px;"></div>--%>
    <div id="gridbox" style="position:relative; width:100%;height:550px;background-color:white; margin:20px; min-width:0;font-size: 100px;"></div>		
    <%: Html.HiddenFor(model => model.RoleID)%>
    <div class="form-actions">
	    <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Profile</button>
	    <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script src="../Scripts/assets/js/app/RoleProfile.js"></script>
</asp:Content>
