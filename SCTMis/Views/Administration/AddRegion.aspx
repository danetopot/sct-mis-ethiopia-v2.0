﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.RegionAdd>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Add New Region
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
<% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--<%: Html.AntiForgeryToken() %>--%>
        <%--The First Row--%>
        <div class="col-4">                
        <label>
            Region Code <%: Html.ValidationMessageFor(model => model.RegionCode) %>
            <%: Html.TextBoxFor(model => model.RegionCode, new { tabindex = "1", @class = "alphaonly", @maxlength="3" })%>
        </label>
        </div>

        <div class="col-2">                
        <label>
            Region Name <%: Html.ValidationMessageFor(model => model.RegionName) %>
            <%: Html.TextBoxFor(model => model.RegionName, new { tabindex = "2", @class = "alphaonly", @maxlength="50" })%>
        </label>
        </div>
        <%: Html.ValidationSummary(true) %><br />

    <div class="form-actions">
	    <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" tabindex = "3"><i></i>Save Region</button>
	    <button id="cmdCancel" type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Back</button>
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script>
        $(document).ready(function () {
            $("#cmdCancel").click(function () {
                window.location.href = "/Administration/Regions";
            });
            $('#loading').hide();
            $("#RegionCode").focus()
        });
    </script>
</asp:Content>
