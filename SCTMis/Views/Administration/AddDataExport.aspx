﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    New Data Export
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("PostDataExport", "Administration", FormMethod.Post, new { id = "MyForm" }))
   { %>
    <div id="gridbox" style="position:relative; width: 230px; height: 320px"></div>
    <br />
    <div id="gridfooter">
        <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
    </div>
    <br />
    <br />
    <div class="form-actions">
	    <button id="cmdLoadData" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Preview Export Data</button>
        <button id="cmdSave" type="submit" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Export Data</button>
    </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        var page_count = 10;
        var gridHeader = 'ColumnID,Source<span class="HeaderChange">_</span>Name,Xml<span class="HeaderChange">_</span>Data,Preview';
        var gridColType = 'ro,ro,ro,img';
        var mygrid;

        $(document).ready(function () {
            $('#loading').hide();
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,230,0,0");
            mygrid.setColAlign("left,right,left,left");
            mygrid.setHeader(gridHeader);

            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");

            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();

            //mygrid.loadXML("/Administration/DataExportList?RecCount=" + page_count);
            dhtmlxError.catchError("ALL", my_error_handler);

            $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Export By'));

            //$("#cmdNew").click(function () {
            //    window.location.href = "../Administration/AddDataExport";
            //});

            $("#cmdLoadData").click(function () {
                hasRecords = 0;
                $("#cmdSave").prop("disabled", false);
                $("#cmdLoadData").prop("disabled", "disabled");

                $.ajax({
                    type: "GET",
                    url: "/Administration/NewDataExportPreviewList?RecCount=" + page_count,
                    dataType: "xml",
                    success: LoadGridWithxmlData
                });
                showLoading(true);
            });
        });

        function LoadGridWithxmlData(myXml) {
            mygrid.clearAll();
            mygrid.parse(myXml);
        }
    </script>
</asp:Content>
