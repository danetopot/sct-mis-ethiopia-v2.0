﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ReportingPeriod>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Add Reporting Period
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("AddReportingPeriod", "Administration", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-2">                
            <label>
                Period Name <%: Html.ValidationMessageFor(model => model.PeriodName) %>
                <%: Html.TextBoxFor(model => model.PeriodName, new { tabindex = "1", @maxlength="40" })%>
            </label>
            </div>
    <br />
            <div class="col-4">                
            <label>
                Start Date  <%: Html.ValidationMessageFor(model => model.StartDate) %>
                <%: Html.TextBoxFor(model => model.StartDate, new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                End Date  <%: Html.ValidationMessageFor(model => model.EndDate) %>
                <%: Html.TextBoxFor(model => model.EndDate, new { tabindex = "3" })%>
            </label>
            </div>
          <br />
            <div class="form-actions">
	            <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Reporting Period</button>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
            <br />

            <%: Html.HiddenFor(model => model.FiscalYear)%>
            <%: Html.Hidden("periodFrom", (string)ViewBag.PeriodFrom) %>
            <%: Html.Hidden("periodTo", (string)ViewBag.PeriodTo) %>

            <%: Html.ValidationSummary(true) %>          
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#loading').hide();
           
            var periodFrom = $('#periodFrom').val();
            var periodTo = $('#periodTo').val();

            $('#StartDate').Zebra_DatePicker({
                format: 'd M Y',
                direction: [periodFrom, periodTo]
            });

            $('#EndDate').Zebra_DatePicker({
                format: 'd M Y',
                direction: [periodFrom, periodTo]
            });

            $("#cmdSave").prop("disabled", false);
            $("#StartDate,#EndDate").attr("readonly", true);
            $("#PeriodName,#StartDate,#EndDate").val('');
            $(":input").blur();
            $('#PeriodName').focus();

            $("#cmdBack").click(function () {
                window.location.href = "/Administration/ReportingPeriod";
            });
        });
    </script>
</asp:Content>
