﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.FinancialYear>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Close Financial Year
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       
<% using (Html.BeginForm("CloseFinancialYear", "Administration", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <br />
        <br />
        <br />
        <br />

        <div>                
            <h4>Close the current Financial Year <strong> <%: ViewBag.FiscalYearName %></strong>?</h4>
            <br />
            <br />
            <h4 style="color: red">This action cannot be reversed</h4>
        </div>
        
        <br/>
        <br/>

        <div class="form-actions">
	        <button id="cmdCloseYear" name="submitButton" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Close Financial Year</button>
            <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
        </div>
            <%: Html.HiddenFor(model => model.FiscalYear)%>
        <br />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">    
	<script type="text/javascript">
	    $(document).ready(function () {
	        $("#MyForm").submit(function (event) {
	            if (!(confirm('Are you sure you want to close this Financial Year?'))) {
	                event.stopImmediatePropagation();
	                event.preventDefault();

	                window.location.href = "/Administration/CloseFinancialYear";
	            }
	        });
            
	        $("#cmdBack").click(function () {
	            window.location.href = "/Home/Index";
	        });	               	        
	    });
	</script>
</asp:Content>
