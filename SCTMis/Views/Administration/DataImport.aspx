﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Data Import
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="gridbox" style="position:relative; width: 410px; height: 320px"></div>
    <div id="gridfooter">
        <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
    </div>

    <div class="form-actions">
	    <button id="cmdNew" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Import Data</button>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        var page_count = 10;
        var gridHeader = 'ColumnID,Imported<span class="HeaderChange">_</span>Table,Import<span class="HeaderChange">_</span>By,Import<span class="HeaderChange">_</span>Date';
        var gridColType = 'ro,ro,ro,ro';
        var mygrid;

        $(document).ready(function () {
            $('#loading').hide();
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,130,130,150");
            mygrid.setColAlign("left,right,left,left");
            mygrid.setHeader(gridHeader);

            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");

            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();

            mygrid.loadXML("/Administration/ImportList?RecCount=" + page_count);
            dhtmlxError.catchError("ALL", my_error_handler);

            $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Import By'));

        });

        $("#cmdNew").click(function () {
            window.location.href = "../Administration/Replicate";
        });
    </script>
</asp:Content>
