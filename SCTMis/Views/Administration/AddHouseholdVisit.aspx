﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.HouseholdVisit>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Add Household Visit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("AddHouseholdVisit", "Administration", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-2">                
            <label>
                Period Name <%: Html.ValidationMessageFor(model => model.HouseholdVisitName) %>
                <%: Html.TextBoxFor(model => model.HouseholdVisitName, new { tabindex = "1" , @class = "alphaonly", @maxlength="40" })%>
            </label>
            </div>
          <br />
            <div class="form-actions">
	            <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Household Visit</button>
            </div>
            <br />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#loading').hide();
            $("#cmdSave").prop("disabled", false);
            $("#HouseholdVisitName").val('');
            $(":input").blur();
            $('#loading').hide();
            $('#HouseholdVisitName').focus();
        });
    </script>
</asp:Content>

