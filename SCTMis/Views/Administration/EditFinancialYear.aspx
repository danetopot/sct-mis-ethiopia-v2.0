﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.FinancialYear>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit Financial Year
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
<% using (Html.BeginForm("UpdateFinancialYear", "Administration", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--<%: Html.AntiForgeryToken() %>--%>
        <%--The First Row--%>
        <div class="col-2">                
            <label>
                Financial Year Name * <%: Html.ValidationMessageFor(model => model.FiscalYearName) %>
                <%: Html.TextBoxFor(model => model.FiscalYearName, new { tabindex = "1", @maxlength = "100" })%>
            </label>
        </div>
        <div class="col-7">                
            <label>
                Start Date *
                <%: Html.TextBoxFor(model => model.StartDate, new { tabindex = "2", @Style="width:95%;" })%>
            </label>
        </div>
        <div class="col-7">                
            <label>
                End Date *
                <%: Html.TextBoxFor(model => model.EndDate, new { tabindex = "3", @Style="width:95%;" })%>
            </label>
        </div>
        <div class="col-2">                
            <label>
                Status
                <%: Html.TextBoxFor(model => model.StatusName, new { tabindex = "4", @Style="width:95%;" })%>
            </label>
        </div>       
        <div class="col-7">                
            <label>
                Closed By
                <%: Html.TextBoxFor(model => model.ClosedBy, new { tabindex = "5", @Style="width:95%;" })%>
            </label>
        </div>
         <div class="col-7">                
            <label>
                Closed On *
                <%: Html.TextBoxFor(model => model.ClosedOn, new { tabindex = "6", @Style="width:95%;" })%>
            </label>
        </div>
        <div class="col-7">                
            <label>
                Opened By
                <%: Html.TextBoxFor(model => model.OpenedBy, new { tabindex = "5", @Style="width:95%;" })%>
            </label>
        </div>
         <div class="col-7">                
            <label>
                Opened On *
                <%: Html.TextBoxFor(model => model.OpenedOn, new { tabindex = "6", @Style="width:95%;" })%>
            </label>
        </div>

         <%: Html.HiddenFor(model => model.FiscalYear)%>
         <%: Html.HiddenFor(model => model.Status)%>
         <%: Html.ValidationSummary(true) %><br />

        <br />
        <br />

        <div class="form-actions">
	        <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" tabindex = "3"><i></i>Update Financial Year</button>
	        <button id="cmdCancel" type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Back</button>
        </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>

    <script>
        $(document).ready(function () {
            $("#StatusName").prop("disabled", true);
            $("#OpenedBy").prop("disabled", true);
            $("#OpenedOn").prop("disabled", true);
            $("#ClosedBy").prop("disabled", true);
            $("#ClosedOn").prop("disabled", true);

            var closed = $("#Status").val();            
            if (closed == 1) {
                $("#FiscalYearName").prop("disabled", true);
                $("#StartDate").prop("disabled", true);
                $("#EndDate").prop("disabled", true);

                $("#cmdSave").hide();
            }

            $('#StartDate').Zebra_DatePicker({
                direction: -1,    // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            $('#EndDate').Zebra_DatePicker({
                direction: -1,    // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            $("#cmdCancel").click(function () {
                window.location.href = "/Administration/FinancialYears";
            });

            $('#loading').hide();
            $("#FiscalYearName").focus()
        });
    </script>
</asp:Content>