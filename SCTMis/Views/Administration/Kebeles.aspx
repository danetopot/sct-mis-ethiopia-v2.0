﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Kebele Defination
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="gridbox" style="position:relative; width: 760px; height: 320px"></div>
    <div id="gridfooter">
        <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
    </div>

    <div class="form-actions">
	    <button id="cmdNew" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Add Kebele</button>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        var page_count = 10;
        var gridHeader = 'ColumnID,KebeleID,Region<span class="HeaderChange">_</span>Name,Woreda<span class="HeaderChange">_</span>Name,Kebele<span class="HeaderChange">_</span>Code,Kebele<span class="HeaderChange">_</span>Name,Edit';
        var gridColType = 'ro,ro,ro,ro,ro,ro,img';
        var mygrid;

        $(document).ready(function () {
            $('#loading').hide();
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,180,180,130,180,90");
            mygrid.setColAlign("left,left,left,right,left,leftleft");
            mygrid.setHeader(gridHeader);

            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");

            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();

            mygrid.loadXML("/Administration/KebelesList?RecCount=" + page_count);
            dhtmlxError.catchError("ALL", my_error_handler);

            $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Region Name'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Woreda Name'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Kebele Name'));

            $("#cmdNew").click(function () {
                window.location.href = "../Administration/AddKebele";
            });
        });

        function createWindow(_KebeleID) {
            var selectedRow = mygrid.getSelectedId();
            _KebeleID = mygrid.cells(selectedRow, 1).getValue();
            post('../Administration/EditKebele', { KebeleID: _KebeleID });
        }
    </script>
</asp:Content>
