﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm1A>" %>

<%@ Register TagPrefix="igsch" Namespace="Infragistics.WebUI.WebSchedule" Assembly="Infragistics2.WebUI.WebDateChooser.v6.2, Version=6.2.20062.34, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Capture Form 1A
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>
    <% using (Html.BeginForm("CaptureForm1A", "Household", FormMethod.Post, new { id = "MyForm" }))
        { %>
    <%--The First Row--%>
    <div class="col-7">
        <label>
            Region * <%: Html.ValidationMessageFor(model => model.RegionID) %>
            <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Woreda *  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
            <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Kebele * <%: Html.ValidationMessageFor(model => model.KebeleID) %>
            <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Gote <%: Html.ValidationMessageFor(model => model.Gote) %>
            <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Gare <%: Html.ValidationMessageFor(model => model.Gare) %>
            <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
        </label>
    </div>
    <%--Second Row--%>
    <div class="col-7">
        <label>
            PSNP HH # * <%: Html.ValidationMessageFor(model => model.HouseHoldIDNumber) %>
            <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "6", @maxlength="20" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            CBHI Membership
                <%: Html.DropDownListFor(model => model.CBHIMembership,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "7" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Name of household head * <%: Html.ValidationMessageFor(model => model.NameOfHouseHoldHead) %>
            <%: Html.TextBoxFor(model => model.NameOfHouseHoldHead, new { tabindex = "8" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Collection Date
                <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "9",@Style="width:95%;"})%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Social Worker
                <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "10" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Remarks:
                <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "11" ,@maxlength="200" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            CCC Member
                <%: Html.TextBoxFor(model => model.CCCCBSPCMember, new { tabindex = "12" , @maxlength="20" })%>
        </label>
    </div>

    <div class="col-7">
        <label>
            CBHI Number:
                <%: Html.TextBoxFor(model => model.CBHINumber, new { tabindex = "13", @maxlength="20" })%>
        </label>
    </div>
    <hr />
    <%--Third Row--%>
    <div class="col-3">
        <label>
            Names of HH member including grandfathers * <%: Html.ValidationMessageFor(model => model.HouseHoldMemberName) %>
            <%: Html.TextBoxFor(model => model.HouseHoldMemberName, new { tabindex = "14" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Individual ID in Family Folder
                <%: Html.TextBoxFor(model => model.IndividualID, new { tabindex = "15",@maxlength="25"})%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Date Of Birth *
                <%: Html.TextBoxFor(model => model.DateOfBirth, new { tabindex = "16",@Style="width:95%;"})%>
        </label>
    </div>
    <%--Fourth Row--%>
    <div class="col-7">
        <label>
            Age (Years.Months)*
                <%: Html.TextBoxFor(model => model.Age, new { tabindex = "17" , @class = "numbersOnly",@maxlength="2" })%>
        </label>
    </div>
    <br />
    <div class="col-7">
        <label>
            Sex *
                <%: Html.DropDownListFor(model => model.Sex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "18" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Pregnant *
                <%: Html.DropDownListFor(model => model.Pregnant,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "19" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Lactating *
                <%: Html.DropDownListFor(model => model.Lactating,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "20" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Disabled *
                <%: Html.DropDownListFor(model => model.Handicapped,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "21" })%>
        </label>
    </div>
    <%--Fifth Row--%>
    <div class="col-7">
        <label>
            Chronically ill *
                <%: Html.DropDownListFor(model => model.ChronicallyIll,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "22" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Nutritional status 0 – 5 and PLW *
                <%: Html.DropDownListFor(model => model.NutritionalStatus,
                    new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "23" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Is child under TSF or CMAM  
                <%: Html.DropDownListFor(model => model.childUnderTSForCMAM,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "24" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Enrolled in School? 
                <%: Html.DropDownListFor(model => model.EnrolledInSchool,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "25" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Grade
                <%: Html.DropDownListFor(model => model.Grade,
                    new SelectList(Model.grades, "ID", "Name"), new { tabindex = "26" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            School Name
                <%: Html.TextBoxFor(model => model.SchoolName, new { tabindex = "27" ,@maxlength="40" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Potential Child Protection Risk? 
                <%: Html.DropDownListFor(model => model.ChildProtectionRisk,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "28" })%>
        </label>
    </div>
    <%--Sixth Row--%>
    <button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus" tabindex="26"><i></i>Proceed</button>
    <br />
    <div id="errors2" style="color: red;"></div>
    <br />
    
    <!-- <div id="gridbox" style="position: relative; width: 980px; height: 150px; overflow-y:auto;min-width: auto"></div> -->
    <div id="gridbox" style="position: relative; width: 980px; height: 160px;"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div>
    <span id="recfound"></span>


    <br />
    <div class="form-actions">
        <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Household Profile</button>
        <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <%: Html.HiddenFor(model => model.ColumnID)%>
    <%: Html.HiddenFor(model => model.Kebele)%>
    <%: Html.HiddenFor(model => model.MemberXml)%>
    <br />
    <%: Html.ValidationSummary(true) %>
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script src="../Scripts/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="../Scripts/assets/js/jquery.mask.min.js"></script>
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>
    <script type="text/javascript" src="../Scripts/assets/js/app/CaptureForm1AList.js"></script>
</asp:Content>
