﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ModifyForm1B>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Modify Form 1B1
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>
    <% using (Html.BeginForm("UpdateForm1B", "Household", FormMethod.Post, new { id = "MyForm" }))
   { %>
    <%--The First Row--%>
    <div class="col-7">
        <label>
            Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
            <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Woreda <%: Html.ValidationMessageFor(model => model.WoredaID) %>
            <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>
            <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Gote <%: Html.ValidationMessageFor(model => model.Gote) %>
            <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Gare <%: Html.ValidationMessageFor(model => model.Gare) %>
            <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            CBHI Membership
            <%: Html.DropDownListFor(model => model.CBHIMembership,
                            new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "6" })%>
        </label>
    </div>
    <br />
    <%--Second Row--%>
    <div class="col-4">
        <label>
            Collection Date
            <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "7"})%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Social Worker
            <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "8" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            CCC/CBSPC member
            <%: Html.TextBoxFor(model => model.CCCCBSPCMember, new { tabindex = "9" , @maxlength="25" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Pregnant/Lactating
            <%: Html.DropDownListFor(model => model.PLW,
                    new SelectList(Model.plws, "ID", "Name"), new { tabindex = "10" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            CBHI Number:
            <%: Html.TextBoxFor(model => model.CBHINumber, new { tabindex = "11" ,@maxlength="20" })%>
        </label>
    </div>
    <div id="errors" style="color: red;"></div>
    <hr />
    <%--Third Row--%>
    <div class="col-4">
        <label>
            Name of PLW incl. name of grandfather
            <%: Html.TextBoxFor(model => model.NameOfPLW, new { tabindex = "12" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            PSNP HH Number
            <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "13", @maxlength="20" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Individual ID in Health Family Folder
            <%: Html.TextBoxFor(model => model.MedicalRecordNumber, new { tabindex = "14" , @maxlength="20" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Age of PLW
            <%: Html.TextBoxFor(model => model.PLWAge, new { tabindex = "15", @class = "numbersOnly", @maxlength="3"})%>
        </label>
    </div>
    <br />
    <%--Fourth Row--%>
    <div class="col-4">
        <label>
            Transitioning from PW to temporary
            <%: Html.TextBoxFor(model => model.StartDateTDS, new { tabindex = "16"})%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Expected temporary DS end Date
            <%: Html.TextBoxFor(model => model.EndDateTDS, new { tabindex = "17"})%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Nutritional status of PLW
            <%: Html.DropDownListFor(model => model.NutritionalStatusPLW,
                            new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "18" })%>
        </label>
    </div>

    <%--Fifth Row--%>
    <br />
    <div class="col-2">
        <label>
            Remarks (like client passed away, moved, etc.):
            <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "19" ,@maxlength="200" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Potential Child Protection Risk?
            <%: Html.DropDownListFor(model => model.ChildProtectionRisk,
                            new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "20" })%>
        </label>
    </div>
    
    <br />
    <hr />

    
    <div class="form-actions">
        <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"
            tabindex="21"><i></i>Modify Form 1B1</button>
        <button id="cmdBack" type="button"
            class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <%: Html.HiddenFor(model => model.ColumnID)%>
    <%: Html.HiddenFor(model => model.ProfileTDSPLWID)%>
    <%: Html.HiddenFor(model => model.Kebele)%>
    <%: Html.HiddenFor(model => model.MemberXml)%>
    <%: Html.HiddenFor(model => model.AllowEdit)%>
    <%: Html.HiddenFor(model => model.KebeleID,new{@id="KebeleId"})%>
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript" src="../Scripts/assets/js/jquery.mask.min.js"></script>
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>

    <script type="text/javascript">
        var EVENTID = null;
        var arrObj = [];
        var SerialCount = 0;
       

        //var mycalStartTDS, mycalSEndTDS, myBabyDOB, myColDate;
        $(document).ready(function () {
            $("#PLW").val("P");
            $("#PLW option[value='L']").attr('disabled', true);

            $('#CollectionDate').Zebra_DatePicker({
                direction: -1,    // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            //plwChange();

            $('#EndDateTDS').Zebra_DatePicker({
                //direction: +1,    // boolean true would've made the date picker future only
                format: 'M/Y'
            });

            $('#StartDateTDS').Zebra_DatePicker({
                //direction: 1,    // boolean true would've made the date picker future only
                format: 'M/Y'
            });

            $("#RegionID,#WoredaID,#KebeleID").prop("disabled", true);

            $('#PLW').change(function (e) {
                plwChange();
            });

            $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", true);
            if ($("#AllowEdit").val() == 'True') {
                
            }

            
            $("#cmdProceed").click(function () {
                //EVENTID = "ADD";
                var errors = [];
                var html = '<ul>';
                var valid = true;
                $('#errors2').empty();

                if (!valid) {
                    html += errors.join('') + '</ul>';
                    $('#errors2').show();
                    $('#errors2').append(html);
                    return valid;
                }
                else {
                    $('#errors2').hide();
                }

                

                $("#cmdProceed").prop("disabled", true);
            });

            

            if ($("#AllowEdit").val() == 'True') {
                $("#cmdSave").prop("disabled", false);
                $("#CollectionDate,#StartDateTDS,#EndDateTDS,#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember,#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#EndDateTDS,#NutritionalStatusPLW,#Remarks").prop("disabled", false);
                $("#cmdSave").val('Modify Details');
            } else {
                $("#cmdSave").prop("disabled", true);
                $("#cmdSave").val('Modify Already Done');

                $("#CollectionDate,#StartDateTDS,#EndDateTDS,#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember,#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#EndDateTDS,#NutritionalStatusPLW,#Remarks").prop("disabled", true);
            }



            $("#cmdBack").click(function () {
                window.location.href = "/Household/Form1B";
            });
            $('#PLW').focus();

            $('#CBHIMembership').change();
        });
        

        $('#CBHIMembership').change(function () {
            var membership = $('#CBHIMembership').val();
            if (membership == 'YES') {
                $('#CBHINumber').prop("disabled", false);
            } else {
                $('#CBHINumber').prop("disabled", true);
                $('#CBHINumber').val("");
            }
        });
    </script>
</asp:Content>