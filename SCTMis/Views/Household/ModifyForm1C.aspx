﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ModifyForm1C>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Modify Form 1C
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>     
<% using (Html.BeginForm("UpdateForm1C", "Household", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-7">                
            <label>
                Region
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Woreda
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele 
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
            <div class="col-7">                  
            <label>
                Gote <%: Html.ValidationMessageFor(model => model.Gote) %>
                <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
            </label>
            </div>
        <%--Second Row--%>
            <div class="col-7">                
            <label>
                Gare
                <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
            </label>
            </div>
            <div class="col-7">
                <label>
                    CBHI Membership
                        <%: Html.DropDownListFor(model => model.CBHIMembership,
                            new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "6" })%>
                </label>
            </div>
    <br />
            <div class="col-4">                
            <label>
                Collection Date
                <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "7"})%>
            </label>
            </div>    
            <div class="col-4">                              
            <label>
                Social Worker
                <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "8" })%>
            </label>
            </div>
            <div class="col-4">                  
            <label>
                CCC member
                <%: Html.TextBoxFor(model => model.CCCCBSPCMember, new { tabindex = "9" ,@maxlength="20" })%>
            </label>
            </div>
            <div class="col-7">
                <label>
                    CBHI Number:
                        <%: Html.TextBoxFor(model => model.CBHINumber, new { tabindex = "10" ,@maxlength="20" })%>
                </label>
            </div>
    <hr />
        <%--Third Row--%>
            <div class="col-4">                
            <label>
                Name of Caretaker incl. Grandfather
                <%: Html.TextBoxFor(model => model.NameOfCareTaker, new { tabindex = "11" , @class = "alphaonly", @maxlength="50" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                PSNP Household ID
                <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "12", @maxlength="25" })%>
            </label>
            </div>

            <div class="col-9">                
            <label>
                Individual ID of caretaker in Health Family Folder
                <%: Html.TextBoxFor(model => model.CaretakerID, new { tabindex = "13" , @maxlength="25" })%>
            </label>
            </div>
            <br />

            <div class="col-4">                
            <label>
                Name of malnourished child
                <%: Html.TextBoxFor(model => model.MalnourishedChildName, new { tabindex = "14" , @maxlength="50" })%>
            </label>
            </div>
            <div class="col-2">                
            <label>
                Individual ID of child in Health Family Folder
                <%: Html.TextBoxFor(model => model.ChildID, new { tabindex = "15" , @maxlength="25" })%>
            </label>
            </div>
            <br />

            <div class="col-4">                
            <label>
                Malnourished child Sex
                <%: Html.DropDownListFor(model => model.MalnourishedChildSex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "16" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Child Birth Date 
                <%: Html.TextBoxFor(model => model.ChildDateOfBirth, new { tabindex = "17"})%>
            </label>
            </div>
            <div class="col-4">
                <label>
                    Age (Years.Months) 
                    <%: Html.TextBoxFor(model => model.Age, new { tabindex = "18",  disabled = "disabled"})%>
                </label>
            </div>
    <%--Fourth Row--%>
            <div class="col-4">                  
            <label>
                Malnourishment Start Date
                <%: Html.TextBoxFor(model => model.DateTypeCertificate, new { tabindex = "19" })%>
            </label>
            </div>
        <br />
            <div class="col-4">                
            <label>
                Degree of malnourishment 
                <%: Html.DropDownListFor(model => model.MalnourishmentDegree,
                    new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "20" })%>
            </label>
            </div>
    
            <div class="col-4">                
            <label>
                TSF or CMAM Start Date
                <%: Html.TextBoxFor(model => model.StartDateTDS, new { tabindex = "21"})%>
            </label>
            </div>
    <%--Fifth Row--%>
            <div class="col-4">                              
            <label>
                Next nutritional status Check
                <%: Html.TextBoxFor(model => model.NextCNStatusDate, new { tabindex = "22"})%>
            </label>
            </div>
    
        <div class="col-4">
        <label>
            Potential Child Protection Risk? 
                <%: Html.DropDownListFor(model => model.ChildProtectionRisk,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "23" })%>
        </label>
    </div>
    <br />
            <div class="col-4">                
            <label>
                End of treatment Date
                <%: Html.TextBoxFor(model => model.EndDateTDS, new { tabindex = "24"})%>
            </label>
            </div>
            <div class="col-2">                  
            <label>
                Remarks (like client passed away, moved, etc.):
                <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "25" ,@maxlength="200" })%>
            </label>
            </div>
          <hr />
            <div class="form-actions">
	            <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" tabindex="22"><i></i>Modify Form 1C</button>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
            <%: Html.HiddenFor(model => model.ProfileTDSCMCID)%>
            <%: Html.HiddenFor(model => model.Kebele)%>
                <%: Html.HiddenFor(model => model.AllowEdit)%>
                <%: Html.HiddenFor(model => model.isUpdate)%>
    
            <br />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script src="../Scripts/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="../Scripts/assets/js/jquery.mask.min.js"></script>
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var age = getAge($('#ChildDateOfBirth').val());
            $("#Age").val(age);

            $('#loading').hide();
            $('#isUpdate').val("YES");

            $('#CollectionDate,#ChildDateOfBirth').Zebra_DatePicker({
                direction: -1,    // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            $('#NextCNStatusDate').Zebra_DatePicker({
                //direction: +1,    // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            $('#EndDateTDS').Zebra_DatePicker({
                //direction: +1,    // boolean true would've made the date picker future only
                format: 'M/Y'
            });

            $('#StartDateTDS').Zebra_DatePicker({
                //direction: 1,    // boolean true would've made the date picker future only
                format: 'M/Y'
            });

            $('#ChildDateOfBirth').focusout(function () {
                var age = getAge($('#ChildDateOfBirth').val());
                $("#Age").val(age);
            });

            $('#DateTypeCertificate').Zebra_DatePicker({
                direction: -1,    // boolean true would've made the date picker future only
                //format: 'd/M/Y'
            });
            

            $("CBHIMembership").val("-");
            $("#RegionID,#WoredaID,#Age").prop("disabled", true);
            $("#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember,#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#EndDateTDS,#ChildID,#CaretakerID,#cmdProceed,#Remarks,#ChildProtectionRisk,#CBHIMembership,#CBHINumber").prop("disabled", false);

            if ($("#AllowEdit").val() == 'True') {
                $("#cmdSave").prop("disabled", false);
                $("#cmdSave").val('Approve Details');
            } else {
                $("#cmdSave").prop("disabled", true);
                $("#cmdSave").val('Approve Already Done');
                $("#CollectionDate,#StartDateTDS,#EndDateTDS,#BabyDateOfBirth").attr("readonly", false);
                $("#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#EndDateTDS,#ChildID,#CaretakerID,#cmdProceed,#Remarks,#ChildProtectionRisk").prop("disabled", true);
            }

            $('#CBHIMembership').change();
        });

        $("#cmdBack").click(function () {
            window.location.href = "/Household/Form1C";
        });

        $('#CBHIMembership').change(function () {
            var membership = $('#CBHIMembership').val();
            if (membership== 'YES') {
                $('#CBHINumber').prop("disabled", false);
            } else {
                $('#CBHINumber').prop("disabled", true);
                $('#CBHINumber').val("");
            }
        });
    </script>
</asp:Content>

