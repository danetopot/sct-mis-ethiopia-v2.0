﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Captured Form 5A1 Listing
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("_CommonSearch") %>
    <div id="gridbox" style="position:relative; width: 770px; height: 250px"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>

    <div class="form-actions">
	    <button id="cmdNew" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Capture Form 5A1 Details</button>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        var page_count = 10;
        var gridHeader = 'ColumnID,ProfileDSHeaderID,Kebele,Reporting<span class="HeaderChange">_</span>Period,Client<span class="HeaderChange">_</span>Name,PSNP<span class="HeaderChange">_</span>Number,Action<span class="HeaderChange">_</span>Taken';
        var gridColType = 'ro,ro,ro,ro,ro,ro,ro';
        var mygrid;

        $(document).ready(function () {
            $('#loading').hide();
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,130,130,200,110,200");
            mygrid.setColAlign("left,left,left,right,left,right,right,right,right");
            mygrid.setHeader(gridHeader);

            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");

            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();

            mygrid.loadXML("/MonitoringCapture/CapturedForm5A1List?RecCount=" + page_count);
            dhtmlxError.catchError("ALL", my_error_handler);

            $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Kebele'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Reporting Period'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Client Name'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Household ID'));
            $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Action Taken'));

            $("#cmdNew").click(function () {
                window.location.href = "../MonitoringCapture/CaptureForm5A1";
            });
        });
    </script>
</asp:Content>
