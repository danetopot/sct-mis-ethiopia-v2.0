﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm5MIS>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Capture Form 5A1 MIS Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>     
<% using (Html.BeginForm("SaveForm5A1MIS", "MonitoringCapture", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-4">                
            <label>
                Region * <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Woreda *  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele * <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
            <div class="col-4">                  
            <label>
                Gote/Gare <%: Html.ValidationMessageFor(model => model.Gote) %>
                <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="20" })%>
            </label>
            </div>
        <%--Second Row--%>
            <div class="col-4">                
            <label>
                Name of household head * <%: Html.ValidationMessageFor(model => model.NameOfHouseHoldHead) %>
                <%: Html.TextBoxFor(model => model.NameOfHouseHoldHead, new { tabindex = "6" , @class = "alphaonly", @maxlength="40" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                PSNP Household number * <%: Html.ValidationMessageFor(model => model.HouseHoldIDNumber) %>
                <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "7", @maxlength="20" })%>
            </label>
            </div>
            <div class="col-4">                  
            <label>
                Date Completed *
                <%: Html.TextBoxFor(model => model.CompletedDate, new { tabindex = "8" , @class = "alphaonly", @maxlength="20" })%>
            </label>
            </div>
            <div class="col-4">                  
            <label>
                Social Worker *
                <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "9" })%>
            </label>
            </div>	
    <hr />        
        <%--Sixth Row--%>
            <div id="divGrid" style="position:relative; width: 790px; height: 140px"></div>
            <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
          <br />
          <p>Summary for MIS: Tick all relevant reasons and actions</p>
            <div id="gridbox2" style="position:relative; width: 760px; height: 140px"></div>
            <div><span id="pagingAreaMIS"></span>&nbsp;<span id="infoAreaMIS"></span></div><span id="recfoundMIS"></span>
          <br />

            <div class="form-actions">
	            <button id="cmdApprove" type="submit" name="submitButton" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Details</button>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
                <%: Html.HiddenFor(model => model.ColumnID)%>
                <%: Html.HiddenFor(model => model.ProfileDSHeaderID)%>
                <%: Html.HiddenFor(model => model.ReportingPeriodID)%>
                <%: Html.HiddenFor(model => model.Kebele)%>
                <%: Html.HiddenFor(model => model.CapturedXml)%>
                <%: Html.HiddenFor(model => model.MemberXml)%>    
            <br />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        var page_count = 5;
        var mygrid, mygridMIS;
        var arrMIS = [];
        var arrMember = [];
        var gridHeader = 'ColumnID,Child Name,Sex,Age,Enrolled,Attending<span class="HeaderChange">_</span>Classes,School<span class="HeaderChange">_</span>Remarks';
        var gridColType = 'ro,ro,ro,ro,ro,ro,ro';

        var gridHeaderMIS = 'ColumnID,ID,Reasons<span class="HeaderChange">_</span>For<span class="HeaderChange">_</span>Not<span class="HeaderChange">_</span>Complying,Yes,Mitigating<span class="HeaderChange">_</span>actions<span class="HeaderChange">_</span>suggested,Yes';
        var gridColTypeMIS = 'ro,ro,ro,ch,ro,ch';

        $(document).ready(function () {
            $('#loading').hide();
            $('#CompletedDate').datepicker({
                dateFormat: "dd/M/yy",
                changeMonth: true,
                changeYear: true,
                maxDate: '0',
                "showAnim": 'fold'
            }).inputmask('mm/dd/yyyy');
            $("#cmdApprove").prop("disabled", true);
            $("#Kebele").val($("#KebeleID").val());
            $("#RegionID, #WoredaID, #KebeleID").prop("disabled", true);
            $("#Gote, #NameOfHouseHoldHead, #HouseHoldIDNumber").attr("readonly", true);

            $('#CompletedDate').focus();
            //gridbox
            mygrid = new dhtmlXGridObject('divGrid');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,160,50,50,80,140,310");
            mygrid.setColAlign("left,left,left,right,left,right,right");
            mygrid.setHeader(gridHeader);

            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");

            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            mygrid.load("/MonitoringCapture/FetchGridForm5A1MISDetailsByID?RecCount=" + page_count + "&ProfileDSHeaderID=" + $("#ProfileDSHeaderID").val());
            dhtmlxError.catchError("ALL", my_error_handler);
            
            //MIS GRID BOX
            mygridMIS = new dhtmlXGridObject('gridbox2');
            mygridMIS.clearAll();
            mygridMIS.setImagePath("../DHTMLX/codebase/imgs/");
            mygridMIS.setInitWidths("0,0,250,50,410,50");
            mygridMIS.setColAlign("left,left,left,justify,right,justify");
            mygridMIS.setHeader(gridHeaderMIS);

            mygridMIS.setColTypes(gridColTypeMIS);
            mygridMIS.enablePaging(true, page_count, page_count, "pagingAreaMIS", true, "infoAreaMIS");
            mygridMIS.setPagingSkin("bricks");
            mygridMIS.setSkin("dhx_skyblue");

            mygridMIS.attachEvent("onXLE", showLoading);
            mygridMIS.attachEvent("onXLS", function () { showLoading(true) });
            mygridMIS.attachEvent("onCheck", doOnCheckBoxSelected);
            mygridMIS.init();
            mygridMIS.load("/MonitoringCapture/FetchGridForm5AMISSummary?RecCount=" + page_count + "&FormName=5A1");

            //mygridMIS.load("/MonitoringCapture/FetchGridForm5AMISSummary?RecCount=" + page_count + "&FormName=5A1", function () {  //loading data to the grid
            //    mygridMIS.forEachRow(function (id) {  //iterating through the rows
            //        mygridMIS.cells(id, 5).setDisabled(true);
            //    });
            //});

            $("#cmdBack").click(function () {
                window.location.href = "/MonitoringCapture/CaptureForm5A1";
            });

            function doOnCheckBoxSelected(rID, cInd, state) {
                //if (state == '1') {
                //    mygridMIS.cells(rID, 5).setValue('1');
                //}
                //else {
                //    mygridMIS.cells(rID, 3).setValue('0');
                //    mygridMIS.cells(rID, 5).setValue('0');
                //}
                //MIS Options Listing
                $("#cmdApprove").prop("disabled", true);
                arrMIS = [];
                for (var i = 0; i < mygridMIS.getRowsNum() ; i++) {
                    if (mygridMIS.cellByIndex(i, 3).getValue() == 1) {
                        $("#cmdApprove").prop("disabled", false);
                        
                        var theDtls = new Object;
                        theDtls.ColumnID = mygridMIS.cellByIndex(i, 0).getValue();
                        theDtls.ID = mygridMIS.cellByIndex(i, 1).getValue();
                        arrMIS.push(theDtls);
                    }
                };
                $("#CapturedXml").val(JSON.stringify(arrMIS));
                return true;
            }
        });
    </script>
</asp:Content>

