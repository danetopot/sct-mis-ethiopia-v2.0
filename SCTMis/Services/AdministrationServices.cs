﻿using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Web.Security;

namespace SCTMis.Services
{
    public class AdministrationServices
    {
        public bool CloseFinancialYear(FinancialYear model)
        {
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec CloseFinancialYear @FiscalYear, @CreatedBy",
                    new
                    {
                        FiscalYear = model.FiscalYear,
                        CreatedBy = Membership.GetUser().UserName.ToString()
                    });

                return true;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return false;
            }
        }

        public FinancialYear GetFetchFinancialYear(FinancialYear newModel)
        {
            FinancialYear financialYear = new FinancialYear();
            
            try
            {
                financialYear
                    = SCTMis.Services.GeneralServices.NpocoConnection().Single<FinancialYear>(";Exec FetchFinancialYear @FiscalYear",
                     new
                     {
                         FiscalYear = newModel.FiscalYear
                     });
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return financialYear;
            }

            return financialYear;
        }

        public bool UpdateFinancialYear(FinancialYear newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec ModifyFinancialYear @FiscalYear, @FiscalYearName, @StartDate, @EndDate",
                     new
                     {
                         FiscalYear = newModel.FiscalYear,
                         FiscalYearName = newModel.FiscalYearName,
                         StartDate = newModel.StartDate,
                         EndDate = newModel.EndDate
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

        public bool InsertRegion(RegionAdd newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertRegion @RegionCode,@RegionName,@CreatedBy",
                     new
                     {
                         RegionCode = newModel.RegionCode,
                         RegionName = newModel.RegionName,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

        public bool UpdateRegion(RegionModifyModel newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec ModifyRegion @RegionID,@RegionName,@CreatedBy",
                     new
                     {
                         RegionID = newModel.RegionID,
                         RegionName = newModel.RegionName,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

        public RegionModifyModel GetRegionByID(RegionModifyModel newModel)
        {
            RegionModifyModel regionDtl = new RegionModifyModel();
            //errMsg = string.Empty;

            try
            {
                regionDtl
                    = SCTMis.Services.GeneralServices.NpocoConnection().Single<RegionModifyModel>(";Exec FetchRegionByID @RegionID",
                     new
                     {
                         RegionID = newModel.RegionID
                     });
            }
            catch (Exception ee)
            {
                GeneralServices.LogError(ee);
                return regionDtl;
            }
            return regionDtl;
        }

        public WoredaModifyModel GetWoredaByID(WoredaModifyModel newModel)
        {
            WoredaModifyModel woredaDtl = new WoredaModifyModel();
            //errMsg = string.Empty;

            try
            {
                woredaDtl
                    = SCTMis.Services.GeneralServices.NpocoConnection().Single<WoredaModifyModel>(";Exec FetchWoredaByID @WoredaID",
                     new
                     {
                         WoredaID = newModel.WoredaID
                     });
            }
            catch (Exception ee)
            {
                GeneralServices.LogError(ee);
                return woredaDtl;
            }
            return woredaDtl;
        }

        public KebeleModifyModel GetKebeleByID(KebeleModifyModel newModel)
        {
            KebeleModifyModel kebeleDtl = new KebeleModifyModel();
            //errMsg = string.Empty;

            try
            {
                kebeleDtl
                    = SCTMis.Services.GeneralServices.NpocoConnection().Single<KebeleModifyModel>(";Exec FetchKebeleByID @KebeleID",
                     new
                     {
                         KebeleID = newModel.KebeleID
                     });
            }
            catch (Exception ee)
            {
                GeneralServices.LogError(ee);
                return kebeleDtl;
            }
            return kebeleDtl;
        }
        //
        public bool InsertWoreda(WoredaAdd newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertWoreda @RegionID,@WoredaCode,@WoredaName,@CreatedBy",
                     new
                     {
                         RegionID = newModel.RegionID,
                         WoredaCode = newModel.WoredaCode,
                         WoredaName = newModel.WoredaName,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

        public bool UpdateWoreda(WoredaModifyModel newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec UpdateWoreda @WoredaID,@WoredaName,@CreatedBy",
                     new
                     {
                         WoredaID = newModel.WoredaID,
                         WoredaName = newModel.WoredaName,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }
        public bool InsertKebele(KebeleAdd newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertKebele @WoredaID,@KebeleCode,@KebeleName,@CreatedBy",
                     new
                     {
                         WoredaID = newModel.WoredaID,
                         KebeleCode = newModel.KebeleCode,
                         KebeleName = newModel.KebeleName,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                return false;
            }
            return true;
        }

        public bool UpdateKebele(KebeleModifyModel newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec UpdateKebele @KebeleID,@KebeleName,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         KebeleName = newModel.KebeleName,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

//public int ColumnID { get; set; }
//public string TableName { get; set; }
//public string xmlString { get; set; }
//public string filePath { get; set; }

        public bool AddReplication(List<DataExportPreviewGrid> RepDetail, string ZipfileName, int createdby, out string errMessage)
        {
            errMessage = string.Empty;

            try
            {
                foreach (var array in RepDetail)
                {
                    List<RecordCount> isSuccess = GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertReplicationData @CreatedBy,@SourceName,@SourceDetails,@ZipfileName",
                         new
                         {
                             CreatedBy = Membership.GetUser().UserName.ToString(),
                             SourceName = array.TableName,
                             SourceDetails = array.xmlString,
                             ZipfileName = ZipfileName
                         });
                }

            }
            catch (Exception ee)
            {
                errMessage = ee.Message;
                GeneralServices.LogError(ee);
                GeneralServices.LogError("AddReplication");

                return false;
            }
            return true;
        }

    }
}
