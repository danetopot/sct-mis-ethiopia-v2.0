﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CsvHelper;
using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SCTMis.Services
{
    public class MonitoringServices
    {
        public CaptureForm5CMIS FetchForm5CMISDetailsByID(string _ProfileTDSCMCID)
        {
            CaptureForm5CMIS dtls = new CaptureForm5CMIS();

            List<CaptureForm5CMIS> ApproveResult
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<CaptureForm5CMIS>(";Exec FetchForm5CMISDetailsByID @ProfileTDSCMCID",
                 new
                 {
                     ProfileTDSCMCID = _ProfileTDSCMCID
                 });

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CaptureForm5CMIS>(ApproveResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                dtls.ProfileTDSCMCID = row["ProfileTDSCMCID"].ToString();
                dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.Gote = row["Gote"].ToString();
                dtls.NameOfCareTaker = row["NameOfCareTaker"].ToString();
                dtls.MalnourishedChildName = row["MalnourishedChildName"].ToString();
                dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
                dtls.SocialWorker = row["SocialWorker"].ToString();
                dtls.CreatedBy = row["CreatedBy"].ToString();
            }
            return dtls;
        }

        public CaptureForm5BMIS FetchForm5BMISDetailsByID(string _ProfileTDSPLWID)
        {
            CaptureForm5BMIS dtls = new CaptureForm5BMIS();

            List<CaptureForm5BMIS> ApproveResult
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<CaptureForm5BMIS>(";Exec FetchForm5BMISDetailsByID @ProfileTDSPLWID",
                 new
                 {
                     ProfileTDSPLWID = _ProfileTDSPLWID
                 });

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CaptureForm5BMIS>(ApproveResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                dtls.ProfileTDSPLWID = row["ProfileTDSPLWID"].ToString();
                dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.Gote = row["Gote"].ToString();
                dtls.NameOfPLW = row["NameOfPLW"].ToString();
                dtls.BabyName = row["BabyName"].ToString();
                dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
                dtls.SocialWorker = row["SocialWorker"].ToString();
                dtls.CreatedBy = row["CreatedBy"].ToString();
            }
            return dtls;
        }

        public CaptureForm5MIS FetchForm5AMISDetailsByID(string _ProfileDSHeaderID)
        {
            CaptureForm5MIS dtls = new CaptureForm5MIS();

            List<CaptureForm5MIS> ApproveResult
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<CaptureForm5MIS>(";Exec FetchForm5AMISDetailsByID @ProfileDSHeaderID",
                 new
                 {
                     ProfileDSHeaderID = _ProfileDSHeaderID
                 });

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CaptureForm5MIS>(ApproveResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                dtls.ProfileDSHeaderID = row["ProfileDSHeaderID"].ToString();
                dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.Gote = row["Gote"].ToString();
                dtls.NameOfHouseHoldHead = row["NameOfHouseHoldHead"].ToString();
                dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
                dtls.SocialWorker = row["SocialWorker"].ToString();
                dtls.CreatedBy = row["CreatedBy"].ToString();
            }
            return dtls;
        }

        //public CaptureForm5MIS FetchForm5A2MISDetailsByID(string _ProfileDSHeaderID)
        //{
        //    CaptureForm5MIS dtls = new CaptureForm5MIS();

        //    List<CaptureForm5MIS> ApproveResult
        //        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<CaptureForm5MIS>(";Exec FetchForm5AMISDetailsByID @ProfileDSHeaderID",
        //         new
        //         {
        //             ProfileDSHeaderID = _ProfileDSHeaderID
        //         });

        //    DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CaptureForm5MIS>(ApproveResult);

        //    if (dtDetails.Rows.Count > 0)
        //    {
        //        DataRow row = dtDetails.Rows[0];

        //        dtls.ProfileDSHeaderID = row["ProfileDSHeaderID"].ToString();
        //        dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
        //        dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
        //        dtls.RegionID = int.Parse(row["RegionID"].ToString());
        //        dtls.Gote = row["Gote"].ToString();
        //        dtls.NameOfHouseHoldHead = row["NameOfHouseHoldHead"].ToString();
        //        dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
        //        dtls.SocialWorker = row["SocialWorker"].ToString();
        //        dtls.CreatedBy = row["CreatedBy"].ToString();
        //    }
        //    return dtls;
        //}
        //Produce Form 5A1
        public bool ProduceForm5A1(ProduceForm5A1Model newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {
                List<Form5A1NonCompliance> resultset = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form5A1NonCompliance>(";Exec getForm5A1NonCompliance @KebeleID, @ReportingPeriod", 
                    new {
                        KebeleID = newModel.KebeleID,
                        ReportingPeriod = newModel.ReportingPeriod
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form5A1NonCompliance>(resultset);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                if (dtDetails.Rows.Count > 0)
                {

                    string filename = DateTime.Now.ToFileTime().ToString();

                    filename = dtDetails.Rows[0]["KebeleName"].ToString() + dtDetails.Rows[0]["ReportingPeriod"].ToString() + dtDetails.Rows[0]["HouseHoldVisit"].ToString() + filename;
                    filename = filename.Replace(' ', '_') + ".pdf";
                    // + ".pdf"

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form5A1RptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    //myReportDocument.Database.Tables[0].SetDataSource(dtAdminStructure);

                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm5A1 @KebeleID,@ReportingPeriod,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportingPeriod = newModel.ReportingPeriod,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool ProduceForm5A(ProduceForm5AModel newModel,out string errMsg)
        {
            errMsg = string.Empty;
            try
            {

                List<Form5ANonCompliance> resultset = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form5ANonCompliance>(";Exec getForm5ANonCompliance @KebeleID, @ReportingPeriod",
                    new
                    {
                        KebeleID = newModel.KebeleID,
                        ReportingPeriod = newModel.ReportingPeriod
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form5ANonCompliance>(resultset);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }
                if (dtDetails.Rows.Count > 0)
                {

                    string filename = DateTime.Now.ToFileTime().ToString();

                    //filename = dtAdminStructure.Rows[0]["Kebele"].ToString() + dtAdminStructure.Rows[0]["ReportingPeriod"].ToString() + filename + ".pdf";

                    filename = dtDetails.Rows[0]["KebeleName"].ToString() + dtDetails.Rows[0]["ReportingPeriod"].ToString() + dtDetails.Rows[0]["HouseHoldVisit"].ToString() + filename;
                    filename = filename.Replace(' ', '_') + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/5A2Report.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm5A @KebeleID,@ReportingPeriod,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportingPeriod = newModel.ReportingPeriod,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool ProduceForm5B(ProduceForm5AModel newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {

                List<Form5BNonCompliance> resultset = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form5BNonCompliance>(";Exec getForm5BNonCompliance @KebeleID, @ReportingPeriod",
                    new
                    {
                        KebeleID = newModel.KebeleID,
                        ReportingPeriod = newModel.ReportingPeriod
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form5BNonCompliance>(resultset);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                if (dtDetails.Rows.Count > 0)
                {

                    string filename = DateTime.Now.ToFileTime().ToString();

                    filename = dtDetails.Rows[0]["KebeleName"].ToString() + dtDetails.Rows[0]["ReportingPeriod"].ToString() + dtDetails.Rows[0]["HouseHoldVisit"].ToString() + filename;
                    filename = filename.Replace(' ', '_') + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/5BReport.rpt");
                    //string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form5BRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());


                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm5B @KebeleID,@ReportingPeriod,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportingPeriod = newModel.ReportingPeriod,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //ProduceForm4B
        //public bool ProduceForm5B(ProduceForm5AModel newModel, out string errMsg)
        //{
        //    errMsg = string.Empty;
        //    try
        //    {

        //        List<Form5BNonCompliance> resultset = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form5BNonCompliance>(";Exec getForm5BNonCompliance @KebeleID, @ReportingPeriod,@HouseholdVisit",
        //            new
        //            {
        //                KebeleID = newModel.KebeleID,
        //                ReportingPeriod = newModel.ReportingPeriod,
        //                HouseholdVisit = newModel.HouseholdVisit
        //            });

        //        DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form5BNonCompliance>(resultset);

        //        if (dtDetails.Rows.Count > 0)
        //        {
        //        string filename = DateTime.Now.ToFileTime().ToString();

        //        filename = dtDetails.Rows[0]["KebeleName"].ToString() + dtDetails.Rows[0]["ReportingPeriod"].ToString() + dtDetails.Rows[0]["HouseHoldVisit"].ToString() + filename;
        //        filename = filename.Replace(' ', '_') + ".pdf";

        //        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form5BRptFile.rpt");
        //        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

        //        ReportDocument myReportDocument;
        //        myReportDocument = new ReportDocument();
        //        myReportDocument.Load(strRptPath);
        //        myReportDocument.FileName = strRptPath;
        //        myReportDocument.Database.Tables[0].SetDataSource(dtDetails);

        //        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
        //        myReportDocument.Close();

        //        //OTHER SETTINGS TO GO HERE

        //        List<RecordCount> isSuccess
        //        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm5B @KebeleID,@ReportingPeriod,@ReportFileName,@HouseholdVisit,@CreatedBy",
        //            new
        //            {
        //                KebeleID = newModel.KebeleID,
        //                ReportingPeriod = newModel.ReportingPeriod,
        //                ReportFileName = filename,
        //                HouseholdVisit = newModel.HouseholdVisit,
        //                CreatedBy = Membership.GetUser().UserName.ToString()
        //            });
        //        }
        //        else { errMsg = "No Records to produce Report"; }
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        errMsg = e.Message;
        //        return false;
        //    }
        //}

        //ProduceForm4C
        public bool ProduceForm5C(ProduceForm5AModel newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {
                List<Form5CNonCompliance> resultset = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form5CNonCompliance>(";Exec getForm5CNonCompliance @KebeleID, @ReportingPeriod",
                    new
                    {
                        KebeleID = newModel.KebeleID,
                        ReportingPeriod = newModel.ReportingPeriod
                    });

                //DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<ReportAdminStructure>(resultset.Item1);
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form5CNonCompliance>(resultset);
                if (dtDetails.Rows.Count > 0)
                {
                string filename = DateTime.Now.ToFileTime().ToString();

                filename = dtDetails.Rows[0]["KebeleName"].ToString() + dtDetails.Rows[0]["ReportingPeriod"].ToString() + dtDetails.Rows[0]["HouseHoldVisit"].ToString() + filename;
                filename = filename.Replace(' ', '_') + ".pdf";

                //filename = dtAdminStructure.Rows[0]["Kebele"].ToString() + dtAdminStructure.Rows[0]["ReportingPeriod"].ToString() + filename + ".pdf";

                string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/5CReport.rpt");
                string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                ReportDocument myReportDocument;
                myReportDocument = new ReportDocument();
                myReportDocument.Load(strRptPath);
                myReportDocument.FileName = strRptPath;
                myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                myReportDocument.Close();

                //OTHER SETTINGS TO GO HERE
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm5C @KebeleID,@ReportingPeriod,@ReportFileName,@CreatedBy",
                        new
                        {
                            KebeleID = newModel.KebeleID,
                            ReportingPeriod = newModel.ReportingPeriod,
                            ReportFileName = filename,
                            CreatedBy = Membership.GetUser().UserName.ToString()
                        });
                }
                else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //Captured Form 5 A Listing details
        public bool InsertCapturedForm5A1MIS(CaptureForm5MIS newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm5MISXMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm5MISXMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<ColumnID>" + array.ColumnID + "</ColumnID>";
                    strXML = strXML + "<ID>" + array.ID + "</ID>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm5A1 @ProfileDSHeaderID, @KebeleID, @ReportingPeriod, @CompletedDate, @SocialWorker, @MemberXML, @CreatedBy",
                     new
                     {
                         ProfileDSHeaderID = newModel.ProfileDSHeaderID,
                         KebeleID = newModel.Kebele,
                         ReportingPeriod = newModel.ReportingPeriodID,
                         CompletedDate = newModel.CompletedDate,
                         SocialWorker = newModel.SocialWorker,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //Captured Form 5 A Listing details
        public bool InsertCapturedForm5A2MIS(CaptureForm5MIS newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm5MISXMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm5MISXMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<ColumnID>" + array.ColumnID + "</ColumnID>";
                    strXML = strXML + "<ID>" + array.ID + "</ID>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm5A2 @ProfileDSHeaderID, @KebeleID, @ReportingPeriod, @CompletedDate, @SocialWorker, @MemberXML, @CreatedBy",
                     new
                     {
                         ProfileDSHeaderID = newModel.ProfileDSHeaderID,
                         KebeleID = newModel.Kebele,
                         ReportingPeriod = newModel.ReportingPeriodID,
                         CompletedDate = newModel.CompletedDate,
                         SocialWorker = newModel.SocialWorker,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //Captured Form 4B Listing details
        public bool InsertCapturedForm5B(CaptureForm5BMIS newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm5MISXMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm5MISXMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<ColumnID>" + array.ColumnID + "</ColumnID>";
                    strXML = strXML + "<ID>" + array.ID + "</ID>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm5B @ProfileTDSPLWID,@KebeleID,@ReportingPeriod,@CompletedDate,@SocialWorker,@MemberXML,@CreatedBy",
                     new
                     {
                         ProfileTDSPLWID = newModel.ProfileTDSPLWID,
                         KebeleID = newModel.Kebele,
                         ReportingPeriod = newModel.ReportingPeriodID,
                         CompletedDate = newModel.CompletedDate,
                         SocialWorker = newModel.SocialWorker,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //Captured Form 4C Listing details
        public bool InsertCapturedForm5C(CaptureForm5CMIS newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm5MISXMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm5MISXMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<ColumnID>" + array.ColumnID + "</ColumnID>";
                    strXML = strXML + "<ID>" + array.ID + "</ID>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm5C @ProfileTDSCMCID,@KebeleID,@ReportingPeriod,@CompletedDate,@SocialWorker,@MemberXML,@CreatedBy",
                     new
                     {
                         ProfileTDSCMCID = newModel.ProfileTDSCMCID,
                         KebeleID = newModel.Kebele,
                         ReportingPeriod = newModel.ReportingPeriodID,
                         CompletedDate = newModel.CompletedDate,
                         SocialWorker = newModel.SocialWorker,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool PdfReport(ReportEngineModel model, string _createdBy, out string errMessage)
        {
            string rptFileName = string.Empty;
            string rptSP = string.Empty;
            errMessage = string.Empty;

            try
            {
                switch (model.ReportName)
                {
                    case "6A":
                        rptFileName = "Form6AReportFile";
                        rptSP = "getForm6AReport";
                        break;
                    case "6B":
                        rptFileName = "Form6BReportFile";
                        rptSP = "getForm6BReport";
                        break;
                    case "6C":
                        rptFileName = "Form6CReportFile";
                        rptSP = "getForm6CReport";
                        break;
                }

                Tuple<List<Form6ReportHeader>, List<Form6Report>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<Form6ReportHeader, Form6Report>(";Exec " + rptSP + " @RegionID, @WoredaID, @ReportingPeriod",
                    new
                    {
                        RegionID = model.RegionID,
                        WoredaID = model.WoredaID,
                        ReportingPeriod = model.ReportingPeriod
                    });

                DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<Form6ReportHeader>(resultset.Item1);
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form6Report>(resultset.Item2);

                if (dtDetails.Rows.Count > 0)
                {

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/" + rptFileName + ".rpt");

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    if (model.ReportName == "6A")
                    {
                        myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                        myReportDocument.Database.Tables[1].SetDataSource(dtAdminStructure);
                    }
                    else
                    {
                        myReportDocument.Database.Tables[0].SetDataSource(dtAdminStructure);
                        myReportDocument.Database.Tables[1].SetDataSource(dtDetails);
                    }
                    myReportDocument.SetParameterValue("GeneratedBy", _createdBy);
                    myReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, true, DateTime.Now.ToFileTime().ToString());
                    //myReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, true, "demo");
                    myReportDocument.Close();

                }
            }
            catch (Exception ee)
            {
                errMessage = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

        public bool ExcelExportReport(ReportExportModel model, string _createdBy, out string errMessage, out string rptFileName)
        {
            //string rptFileName = string.Empty;
            string rptSP = string.Empty;
            var products = new System.Data.DataTable("teste");
            errMessage = string.Empty;
            rptFileName = DateTime.Now.ToFileTime().ToString();
            rptFileName = "Form" + model.ReportName + "_" + rptFileName + Membership.GetUser().ProviderUserKey.ToString() + ".csv";

            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + rptFileName);

            int? regionID = model.RegionCodeID > 0 ? (int?)model.RegionCodeID : null;
            int? woredaID = model.WoredaCodeID > 0 ? (int?)model.WoredaCodeID : null;
            int? kebeleID = model.KebeleCodeID > 0 ? (int?)model.KebeleCodeID : null;

            try
            {
                switch (model.ReportName)
                {
                    case "1A":
                        List<ExportForm1AModel> ExportForm1As
                            = GeneralServices.NpocoConnection().Fetch<ExportForm1AModel>(";Exec ExportForm1A @RegionID, @WoredaID, @KebeleID, @FiscalYear",
                            new
                            {
                                RegionID = regionID,
                                WoredaID = woredaID,
                                KebeleID = kebeleID,
                                FiscalYear = model.FiscalYear
                            });

                            using (TextWriter textWriter = File.CreateText(strRptPath))
                            {
                                var csv = new CsvWriter(textWriter);
                                csv.WriteRecords(ExportForm1As);
                            }                            
                        break;

                    case "1B":
                        List<ExportForm1BModel> ExportForm1Bs
                            = GeneralServices.NpocoConnection().Fetch<ExportForm1BModel>(";Exec ExportForm1B @RegionID, @WoredaID, @KebeleID, @FiscalYear",
                            new
                            {
                                RegionID = regionID,
                                WoredaID = woredaID,
                                KebeleID = kebeleID,
                                FiscalYear = model.FiscalYear
                            });

                            using (TextWriter textWriter = File.CreateText(strRptPath))
                            {
                                var csv = new CsvWriter(textWriter);
                                csv.WriteRecords(ExportForm1Bs);
                            }
                        break;

                    case "1C":
                        List<ExportForm1CModel> ExportForm1Cs
                            = GeneralServices.NpocoConnection().Fetch<ExportForm1CModel>(";Exec ExportForm1C @RegionID, @WoredaID, @KebeleID, @FiscalYear",
                            new
                            {
                                RegionID = regionID,
                                WoredaID = woredaID,
                                KebeleID = kebeleID,
                                FiscalYear = model.FiscalYear
                            });

                        using (TextWriter textWriter = File.CreateText(strRptPath))
                        {
                            var csv = new CsvWriter(textWriter);
                            csv.WriteRecords(ExportForm1Cs);
                        }

                        break;
                }
            }
            catch (Exception ee)
            {
                errMessage = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }
    }
}
