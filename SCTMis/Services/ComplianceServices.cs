﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace SCTMis.Services
{
    public class ComplianceServices
    {
        public bool ProduceForm4ASchool(ProduceForm4A newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {
                Tuple<List<ServiceIDModel>, List<Form4AReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceIDModel, Form4AReport>(";Exec form4AReportSchool @KebeleID,@ReportingPeriodID",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID
                     });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4AReport>(resultset.Item2);
                DataTable dtServices = SCTMis.Services.GeneralServices.ToDataTable<ServiceIDModel>(resultset.Item1);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                    //DataRow[] dRresult = dtDetails.Select("ServiceID = " + ds["ServiceID"]);
                    //DataTable dtRpt = dRresult.CopyToDataTable();

                if (dtDetails.Rows.Count > 0)
                    {
                        string filename = DateTime.Now.ToFileTime().ToString();

                        filename = dtDetails.Rows[0]["KebeleName"].ToString() + dtDetails.Rows[0]["ReportingPeriodName"].ToString() + filename + ".pdf";

                        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4AComplianceDemo.rpt");
                        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.FileName = strRptPath;
                        myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                        myReportDocument.Close();
                        myReportDocument.Dispose();

                        string strXML = string.Empty;

                        foreach (DataRow array in dtDetails.Rows)
                        {
                            strXML = strXML + "<row>";
                            strXML = strXML + "<ProfileDSDetailID>" + array["ProfileDSDetailID"] + "</ProfileDSDetailID>";
                            strXML = strXML + "</row>";
                        }

                        strXML = "<members>" + strXML + "</members>";

                        List<RecordCount> isSuccess
                            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4A @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                             new
                             {
                                 KebeleID = newModel.KebeleID,
                                 ServiceID = dtServices.Rows[0][0],//newModel.ServiceID,
                                 ReportFileName = filename,
                                 ReportingPeriodID = newModel.ReportingPeriodID,
                                 MemberXML = strXML,
                                 CreatedBy = Membership.GetUser().UserName.ToString()
                             });
                    }
                    //else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool ProduceForm4A(ProduceForm4A newModel,out string errMsg)
        {
            errMsg = string.Empty;
            try
            {
                Tuple<List<ServiceIDModel>, List<Form4AReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceIDModel, Form4AReport>(";Exec form4AReport @KebeleID,@ReportingPeriodID",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID
                     });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4AReport>(resultset.Item2);
                DataTable dtServices = SCTMis.Services.GeneralServices.ToDataTable<ServiceIDModel>(resultset.Item1);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                foreach (DataRow ds in dtServices.Rows)
                {
                    DataRow[] dRresult = dtDetails.Select("ServiceID = '" + ds["ServiceID"] + "'");

                    if (dRresult.Count() == 0) continue;

                    DataTable dtRpt = dRresult.CopyToDataTable();

                    if (dtRpt.Rows.Count > 0)
                    {
                        string filename = DateTime.Now.ToFileTime().ToString();

                        filename = dtRpt.Rows[0]["KebeleName"].ToString() + dtRpt.Rows[0]["ReportingPeriodName"].ToString() + filename + ".pdf";

                        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4AComplianceDemo.rpt");
                        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        ReportDocument myReportDocument;

                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.FileName = strRptPath;
                        myReportDocument.Database.Tables[0].SetDataSource(dtRpt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                        myReportDocument.Close();
                        myReportDocument.Dispose();

                        string strXML = string.Empty;

                        foreach (DataRow array in dtRpt.Rows)
                        {
                            strXML = strXML + "<row>";
                            strXML = strXML + "<ProfileDSDetailID>" + array["ProfileDSDetailID"] + "</ProfileDSDetailID>";
                            strXML = strXML + "</row>";
                        }

                        strXML = "<members>" + strXML + "</members>";

                        List<RecordCount> isSuccess
                            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4A @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                             new
                             {
                                 KebeleID = newModel.KebeleID,
                                 ServiceID = ds["ServiceID"],//newModel.ServiceID,
                                 ReportFileName = filename,
                                 ReportingPeriodID = newModel.ReportingPeriodID,
                                 MemberXML = strXML,
                                 CreatedBy = Membership.GetUser().UserName.ToString()
                             });
                    }
                }
                //else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //ProduceForm4B
        public bool ProduceForm4B(ProduceForm4B newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {

                //Tuple<List<ServiceAdminStructure>, List<Form4BCompliance>> resultset
                //    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceAdminStructure, Form4BCompliance>(";Exec getForm4BCompliance @KebeleID, @ServiceID,@ReportingPeriodID",
                //    new
                //    {
                //        KebeleID = newModel.KebeleID,
                //        ServiceID = newModel.ServiceID,
                //        ReportingPeriodID = newModel.ReportingPeriodID
                //    });

                Tuple<List<ServiceIDModel>, List<Form4BReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceIDModel, Form4BReport>(";Exec form4BReport @KebeleID,@ReportingPeriodID",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID
                     });
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4BReport>(resultset.Item2);
                DataTable dtServices = SCTMis.Services.GeneralServices.ToDataTable<ServiceIDModel>(resultset.Item1);

                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                foreach (DataRow ds in dtServices.Rows)
                {
                    //DataRow[] dRresult = dtDetails.Select("ServiceID = " + ds["ServiceID"]);
                    DataRow[] dRresult = dtDetails.Select("ServiceID = '" + ds["ServiceID"] + "'");

                    if (dRresult.Count() == 0) continue;

                    DataTable dtRpt = dRresult.CopyToDataTable();                                       

                    if (dtRpt.Rows.Count > 0)
                    {
                        string filename = DateTime.Now.ToFileTime().ToString();

                        filename = dtRpt.Rows[0]["KebeleName"].ToString() + dtRpt.Rows[0]["ReportingPeriodName"].ToString() + filename + ".pdf";

                        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4BComplianceDemo.rpt");
                        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.FileName = strRptPath;
                        myReportDocument.Database.Tables[0].SetDataSource(dtRpt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                        myReportDocument.Close();
                        myReportDocument.Dispose();

                        string strXML = string.Empty;

                        foreach (DataRow array in dtRpt.Rows)
                        {
                            strXML = strXML + "<row>";
                            strXML = strXML + "<ProfileTDSPLWID>" + array["ProfileTDSPLWID"] + "</ProfileTDSPLWID>";
                            strXML = strXML + "</row>";
                        }

                        strXML = "<members>" + strXML + "</members>";  

                        List<RecordCount> isSuccess
                            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4B @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                             new
                             {
                                 KebeleID = newModel.KebeleID,
                                 ServiceID = ds["ServiceID"],//newModel.ServiceID,
                                 ReportFileName = filename,
                                 ReportingPeriodID = newModel.ReportingPeriodID,
                                 MemberXML = strXML,
                                 CreatedBy = Membership.GetUser().UserName.ToString()
                             });
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //ProduceForm4C
        public bool ProduceForm4C(ProduceForm4C newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {

                //Tuple<List<ServiceAdminStructure>, List<Form4CCompliance>> resultset
                //    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceAdminStructure, Form4CCompliance>(";Exec getForm4CCompliance @KebeleID, @ServiceID,@ReportingPeriodID",
                //    new
                //    {
                //        KebeleID = newModel.KebeleID,
                //        ServiceID = newModel.ServiceID,
                //        ReportingPeriodID = newModel.ReportingPeriodID
                //    });
                Tuple<List<ServiceIDModel>, List<Form4CReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceIDModel, Form4CReport>(";Exec form4CReport @KebeleID,@ReportingPeriodID",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID
                     });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4CReport>(resultset.Item2);
                DataTable dtServices = SCTMis.Services.GeneralServices.ToDataTable<ServiceIDModel>(resultset.Item1);

                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                foreach (DataRow ds in dtServices.Rows)
                {
                    Int32 intServiceID = Int32.Parse(ds.ItemArray[0].ToString());
                    //DataRow[] dRresult = dtDetails.Select("ServiceID = " + ds["ServiceID"]);
                    

                    var dr = from row in dtDetails.AsEnumerable()
                             where row.Field<string>("ServiceID") == intServiceID.ToString()
                             select row;
                    DataTable dtRpt = dr.CopyToDataTable();

                    if (dtRpt.Rows.Count > 0)
                    {
                        string filename = DateTime.Now.ToFileTime().ToString();

                        filename = dtRpt.Rows[0]["KebeleName"].ToString() + dtRpt.Rows[0]["ReportingPeriodName"].ToString() + filename + ".pdf";

                        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4CComplianceDemo.rpt");
                        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.FileName = strRptPath;
                        myReportDocument.Database.Tables[0].SetDataSource(dtRpt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                        myReportDocument.Close();
                        myReportDocument.Dispose();

                        string strXML = string.Empty;

                        foreach (DataRow array in dtRpt.Rows)
                        {
                            strXML = strXML + "<row>";
                            strXML = strXML + "<ProfileTDSCMCID>" + array["ProfileTDSCMCID"] + "</ProfileTDSCMCID>";
                            strXML = strXML + "</row>";
                        }

                        strXML = "<members>" + strXML + "</members>";

                        List<RecordCount> isSuccess
                            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4C @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                             new
                             {
                                 KebeleID = newModel.KebeleID,
                                 ServiceID = ds["ServiceID"],//newModel.ServiceID,
                                 ReportFileName = filename,
                                 ReportingPeriodID = newModel.ReportingPeriodID,
                                 MemberXML = strXML,
                                 CreatedBy = Membership.GetUser().UserName.ToString()
                             });
                    }
                }
                //if (dtDetails.Rows.Count > 0)
                //{
                //string filename = DateTime.Now.ToFileTime().ToString();

                //filename = dtAdminStructure.Rows[0]["Kebele"].ToString() + dtAdminStructure.Rows[0]["ServiceProviderName"].ToString() + filename + ".pdf";

                //string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4CComplianceRptFile.rpt");
                //string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                //ReportDocument myReportDocument;
                //myReportDocument = new ReportDocument();
                //myReportDocument.Load(strRptPath);
                //myReportDocument.FileName = strRptPath;
                //myReportDocument.Database.Tables[0].SetDataSource(dtAdminStructure);
                //myReportDocument.Database.Tables[1].SetDataSource(dtDetails);

                //myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                //myReportDocument.Close();

                ////OTHER SETTINGS TO GO HERE
                ////convert data to xml and pass it to the procedure to save. This is safer

                //string strXML = string.Empty;

                //foreach (DataRow array in dtDetails.Rows)
                //{
                //    strXML = strXML + "<row>";
                //    strXML = strXML + "<ProfileTDSCMCID>" + array["ProfileTDSCMCID"] + "</ProfileTDSCMCID>";
                //    strXML = strXML + "</row>";
                //}

                //strXML = "<members>" + strXML + "</members>";

                //    List<RecordCount> isSuccess
                //        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4C @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                //         new
                //         {
                //             KebeleID = newModel.KebeleID,
                //             ServiceID = newModel.ServiceID,
                //             ReportFileName = filename,
                //             ReportingPeriodID = newModel.ReportingPeriodID,
                //             MemberXML = strXML,
                //             CreatedBy = Membership.GetUser().UserName.ToString()
                //         });
                //}
                //else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //Single Form 4A captured Insert
        public bool InsertCapturedForm4ANew(CapturedForm4Model newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4ANew @KebeleID,@ReportingPeriodID,@ProfileDSDetailID,@ServiceID,@HasComplied,@Remarks,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID,
                         ProfileDSDetailID = newModel.ProfileDSDetailID,
                         ServiceID = newModel.ServiceID,
                         HasComplied=newModel.HasComplied,
                         Remarks=newModel.Remarks,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //Captured Form 4 A Listing details
        public bool InsertCapturedForm4A(CaptureForm4 newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm4XMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4XMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<UniqueID>" + array.UniqueID + "</UniqueID>";
                    strXML = strXML + "<HasComplied>" + array.HasComplied + "</HasComplied>";
                    strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess 
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4A @KebeleID,@ServiceID,@MemberXML,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.Kebele,
                         ServiceID = newModel.Service,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //Single Form 4A captured Insert
        public bool InsertCapturedForm4BNew(CapturedForm4BModel newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedFormBANew @KebeleID,@ReportingPeriodID,@ProfileTDSPLWID,@ServiceID,@HasComplied,@Remarks,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID,
                         ProfileTDSPLWID = newModel.ProfileTDSPLWID,
                         ServiceID = newModel.ServiceID,
                         HasComplied = newModel.HasComplied,
                         Remarks = newModel.Remarks,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //InsertCapturedForm4CNew
        public bool InsertCapturedForm4CNew(CapturedForm4CModel newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4CNew @KebeleID,@ReportingPeriodID,@ProfileTDSCMCID,@ServiceID,@HasComplied,@Remarks,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID,
                         ProfileTDSCMCID = newModel.ProfileTDSCMCID,
                         ServiceID = newModel.ServiceID,
                         HasComplied = newModel.HasComplied,
                         Remarks = newModel.Remarks,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        ////Captured Form 4B Listing details
        //public bool InsertCapturedForm4B(CaptureForm4 newModel, out string errMessage)
        //{
        //    errMessage = string.Empty;
        //    try
        //    {

        //        string strXML = string.Empty;
        //        JavaScriptSerializer objJavascript = new JavaScriptSerializer();
        //        CapturedForm4XMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4XMLDetails[]>(newModel.CapturedXml);

        //        foreach (var array in CapturedList)
        //        {
        //            strXML = strXML + "<row>";
        //            strXML = strXML + "<UniqueID>" + array.UniqueID + "</UniqueID>";
        //            strXML = strXML + "<HasComplied>" + array.HasComplied + "</HasComplied>";
        //            strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";
        //            strXML = strXML + "</row>";
        //            Console.WriteLine(strXML);
        //        }

        //        strXML = "<members>" + strXML + "</members>";

        //        List<RecordCount> isSuccess
        //            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4B @KebeleID,@ServiceID,@MemberXML,@CreatedBy",
        //             new
        //             {
        //                 KebeleID = newModel.Kebele,
        //                 ServiceID = newModel.Service,
        //                 MemberXML = strXML,
        //                 CreatedBy = Membership.GetUser().UserName.ToString()
        //             });

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        return false;
        //    }
        //}

        //Captured Form 4C Listing details
        public bool InsertCapturedForm4C(CaptureForm4 newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm4XMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4XMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<UniqueID>" + array.UniqueID + "</UniqueID>";
                    strXML = strXML + "<HasComplied>" + array.HasComplied + "</HasComplied>";
                    strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4C @KebeleID,@ServiceID,@MemberXML,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.Kebele,
                         ServiceID = newModel.Service,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //ProduceForm4C
        public bool ProduceForm4D(ProduceForm4D newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {

                List<Form4DReport> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form4DReport>(";Exec form4DReport @KebeleID,@ReportingPeriodID,@ClientType",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID,
                         ClientType = newModel.ClientType
                     });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4DReport>(resultset);

                if (dt.Rows.Count > 1)
                {
                    string datetime = (DateTime.Now).ToString("yyyy_MM_dd");
                    string rptFileName = "Generated_Form4D_" + datetime + ".pdf";
                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4DComplianceRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + rptFileName);
                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dt);
                    myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4D @KebeleID,@HouseholdCount,@ReportFileName,@ReportingPeriod,@CreatedBy,@CreatedOn",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             HouseholdCount = dt.Rows.Count,
                             ReportFileName = rptFileName,
                             ReportingPeriod = newModel.ReportingPeriodID,
                             CreatedBy = Membership.GetUser().UserName.ToString(),
                             CreatedOn = DateTime.Today
                         });
                }
                else { errMsg = "No Records to produce Report"; }

                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public ModifyForm4D FetchCapturedForm4DDetailsByID(string ChildProtectionHeaderID)
        {
            Form4DDetails bioDtls = new Form4DDetails();
            ModifyForm4D dataDtls = new ModifyForm4D();

            List<ModifyForm4D> data
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ModifyForm4D>(";Exec FetchForm4DDetailsByID @ChildProtectionHeaderID, @LoggedInUser",
                 new
                 {
                     ChildProtectionHeaderID = ChildProtectionHeaderID,
                     LoggedInUser = Membership.GetUser().UserName.ToString()
                 });
            DataTable dtData = SCTMis.Services.GeneralServices.ToDataTable<ModifyForm4D>(data);

            if (dtData.Rows.Count > 0)
            {
                DataRow row = dtData.Rows[0];
                dataDtls.ChildProtectionHeaderID = row["ChildProtectionHeaderID"].ToString();
                dataDtls.ChildProtectionId = row["ChildProtectionId"].ToString();
                dataDtls.CaseManagementReferral = row["CaseManagementReferral"].ToString();
                dataDtls.CaseStatusId = int.Parse(row["CaseStatusId"].ToString());
                dataDtls.ClientTypeID = row["ClientTypeID"].ToString();
                dataDtls.CollectionDate = row["CollectionDate"].ToString();
                dataDtls.SocialWorker = row["SocialWorker"].ToString();
                dataDtls.SocialWorkerFollowUp = row["SocialWorkerFollowUp"].ToString();
                dataDtls.Remarks = row["Remarks"].ToString();
                dataDtls.FiscalYear = row["FiscalYear"].ToString();
                dataDtls.AllowEdit = int.Parse(row["AllowEdit"].ToString());


                List<Form4DDetails> bio
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form4DDetails>(";Exec GetChildProtectionDetailsByID @HHTypeID,@ProfileDSHeaderID",
                     new
                     {
                         HHTypeID = row["ClientTypeID"].ToString(),
                         ProfileDSHeaderID = row["ChildDetailID"].ToString()
                     });
                DataTable dtBio = SCTMis.Services.GeneralServices.ToDataTable<Form4DDetails>(bio);
                DataRow row_ = dtBio.Rows[0];
                dataDtls.KebeleID = int.Parse(row_["KebeleID"].ToString());
                dataDtls.WoredaID = int.Parse(row_["WoredaID"].ToString());
                dataDtls.RegionID = int.Parse(row_["RegionID"].ToString());
                dataDtls.Gote = row_["Gote"].ToString();
                dataDtls.Gare = row_["Gare"].ToString();
                dataDtls.HouseHoldIDNumber = row_["HouseHoldIDNumber"].ToString();
                dataDtls.NameOfHouseHoldHead = row_["NameOfHouseHoldHead"].ToString();
                dataDtls.HouseHoldMemberName = row_["HouseHoldMemberName"].ToString();
                dataDtls.HouseHoldMemberSex = row_["HouseHoldMemberSex"].ToString();
                dataDtls.HouseHoldMemberAge = int.Parse(row_["HouseHoldMemberAge"].ToString());
                dataDtls.FiscalYear = row_["FiscalYear"].ToString();
                dataDtls.ProfileDSHeaderID = row_["ProfileDSHeaderID"].ToString();
                dataDtls.ProfileDSDetailID = row_["ProfileDSDetailID"].ToString();
            }


            return dataDtls;
        }

        public Form4DDetails FetchForm4DDetailsByID(string _HHTypeID, string _ProfileDSHeaderID)
        {
            Form4DDetails dtls = new Form4DDetails();

            List<Form4DDetails> CPResult
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form4DDetails>(";Exec GetChildProtectionDetailsByID @HHTypeID,@ProfileDSHeaderID",
                 new
                 {
                     HHTypeID = _HHTypeID,
                     ProfileDSHeaderID = _ProfileDSHeaderID
                 });

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4DDetails>(CPResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];
                dtls.UniqueID = int.Parse(row["UniqueID"].ToString());
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.Gote = row["Gote"].ToString();
                dtls.Gare = row["Gare"].ToString();
                dtls.NameOfHouseHoldHead = row["NameOfHouseHoldHead"].ToString();
                dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
                dtls.HouseHoldMemberName = row["HouseHoldMemberName"].ToString();
                dtls.HouseHoldMemberSex = row["HouseHoldMemberSex"].ToString();
                dtls.HouseHoldMemberAge = int.Parse(row["HouseHoldMemberAge"].ToString());
                dtls.FiscalYear = row["FiscalYear"].ToString();
                dtls.ProfileDSHeaderID = row["ProfileDSHeaderID"].ToString();
                dtls.ProfileDSDetailID = row["ProfileDSDetailID"].ToString();
            }
            return dtls;
        }

        public bool InsertCapturedForm4D(CaptureForm4D newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm4DXMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4DXMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<RiskID>" + array.RiskId + "</RiskID>";
                    strXML = strXML + "<ServiceID>" + array.ServiceId + "</ServiceID>";
                    strXML = strXML + "<ProviderID>" + array.ProviderId + "</ProviderID>";
                    strXML = strXML + "</row>";
                }
                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec InsertCapturedForm4D @ChildProtectionId,@ChildDetailID,@CaseManagementReferral,@CaseStatusId,@ClientTypeID,@CollectionDate,@SocialWorker,@Remarks,@CreatedBy,@MemberXML",
                    new
                    {
                        ChildProtectionId = newModel.ChildProtectionID,
                        ChildDetailID =newModel.ProfileDSDetailID,
                        CaseManagementReferral = newModel.ReferredToCaseManagement,
                        CaseStatusId = 1,
                        ClientTypeID = newModel.ClientTypeID,
                        CollectionDate = newModel.CollectionDate,
                        SocialWorker = newModel.SocialWorker,
                        Remarks = newModel.Remarks,
                        CreatedBy = Membership.GetUser().UserName.ToString(),
                        MemberXML = strXML

                    });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool UpdateCapturedForm4D(ModifyForm4D newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm4DXMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4DXMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<RiskID>" + array.RiskId + "</RiskID>";
                    strXML = strXML + "<ServiceID>" + array.ServiceId + "</ServiceID>";
                    strXML = strXML + "<ProviderID>" + array.ProviderId + "</ProviderID>";
                    strXML = strXML + "</row>";
                }
                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec ModifyCapturedForm4D @ChildProtectionHeaderID,@ChildProtectionId,@CaseManagementReferral,@CaseStatusId,@ClientTypeID,@CollectionDate,@SocialWorker,@Remarks,@UpdatedBy,@MemberXML",
                    new
                    {
                        ChildProtectionHeaderID = newModel.ChildProtectionHeaderID,
                        ChildProtectionId = newModel.ChildProtectionId,
                        CaseManagementReferral = newModel.CaseManagementReferral,
                        CaseStatusId = 1,
                        ClientTypeID = newModel.ClientTypeID,
                        CollectionDate = newModel.CollectionDate,
                        SocialWorker = newModel.SocialWorker,
                        Remarks = newModel.Remarks,
                        UpdatedBy = Membership.GetUser().UserName.ToString(),
                        MemberXML = strXML

                    });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool InsertCapturedForm4DFollowUp(ModifyForm4D newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXML = string.Empty;
                string serviceAccessDate = string.Empty;
                string serviceNotAccessReason = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm4DXMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4DXMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<ChildProtectionDetailID>" + array.ChildProtectionDetailID + "</ChildProtectionDetailID>";
                    strXML = strXML + "<ServiceStatusID>" + array.ServiceStatusId + "</ServiceStatusID>";
                    strXML = strXML + "<ServiceAccessDate>" + array.ServiceAccessDate + "</ServiceAccessDate>";
                    strXML = strXML + "<ServiceNotAccessedReasonID>" + array.ServiceNotAccessedReasonId + "</ServiceNotAccessedReasonID>";
                    strXML = strXML + "</row>";
                }
                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec InsertForm4DFollowUp @ChildProtectionHeaderID,@VisitDate,@SocialWorker,@CreatedBy,@MemberXML",
                    new
                    {
                        ChildProtectionHeaderID = newModel.ChildProtectionHeaderID,
                        VisitDate = newModel.VisitDate,
                        SocialWorker = newModel.SocialWorkerFollowUp,
                        CreatedBy = Membership.GetUser().UserName.ToString(),
                        MemberXML = strXML

                    });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool UpdateCapturedForm4DFollowUp(ModifyForm4D newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXML = string.Empty;
                string visitHeaderId = string.Empty;
                string serviceAccessDate = string.Empty;
                string serviceNotAccessReason = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm4DXMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4DXMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<ChildProtectionDetailID>" + array.ChildProtectionDetailID + "</ChildProtectionDetailID>";
                    strXML = strXML + "<RiskID>" + array.RiskId + "</RiskID>";
                    strXML = strXML + "<ServiceID>" + array.ServiceId + "</ServiceID>";
                    strXML = strXML + "<ProviderID>" + array.ProviderId + "</ProviderID>";
                    strXML = strXML + "<ServiceStatusID>" + array.ServiceStatusId + "</ServiceStatusID>";
                    strXML = strXML + "<ServiceAccessDate>" + array.ServiceAccessDate + "</ServiceAccessDate>";
                    strXML = strXML + "<ServiceNotAccessedReasonID>" + array.ServiceNotAccessedReasonId + "</ServiceNotAccessedReasonID>";
                    strXML = strXML + "</row>";

                    visitHeaderId = array.ChildProtectionVisitHeaderID;
                }
                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec ModifyForm4DFollowUp @ChildProtectionVisitHeaderID,@VisitDate,@SocialWorker,@UpdatedBy,@MemberXML",
                    new
                    {
                        ChildProtectionVisitHeaderID = visitHeaderId,
                        VisitDate = newModel.VisitDate,
                        SocialWorker = newModel.SocialWorkerFollowUp,
                        UpdatedBy = Membership.GetUser().UserName.ToString(),
                        MemberXML = strXML
 
                    });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool ApproveCapturedForm4D(ModifyForm4D newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");


                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec ApproveForm4D @ChildProtectionHeaderID,@ApprovedBy",
                    new
                    {
                        ChildProtectionHeaderID = newModel.ChildProtectionHeaderID,
                        ApprovedBy = Membership.GetUser().UserName.ToString()

                    });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool CloseCapturedForm4D(ModifyForm4D newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec CloseForm4D @ChildProtectionHeaderID, @ClosedReason,@ClosedBy",
                    new
                    {
                        ChildProtectionHeaderID = newModel.ChildProtectionHeaderID,
                        ClosedReason = newModel.ClosedReason,
                        ClosedBy = Membership.GetUser().UserName.ToString()

                    });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
    }
}
