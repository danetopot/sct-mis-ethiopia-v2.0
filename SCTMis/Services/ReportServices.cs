﻿using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Xml.Linq;
using System.Web.Mvc;
using System.Web;
using ClosedXML.Excel;
using System.IO;

namespace SCTMis.Services
{
    public class ReportServices
    {
        AccountServices dbService = new AccountServices();

        public string SerializeDataTable(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        public Dictionary<string, string> GenerateReport(
            string username,
            string regionID, string woredaID, string kebeleID, string goteID,
            string rptType, string rptLevel,string rptDisaggregation, string rptPeriod, string socialWorker,
            string fiscalYear, DateTime rptBeginDate, DateTime rptStopDate,
            string [] rpt1Filters, string[] rpt2Filters, string[] rpt3Filters, string[] rpt4Filters, 
            string[] rpt5Filters, string[] rpt6Filters, string[] rpt7Filters, string[] rpt8Filters, 
            string[] rpt9Filters, string[] rpt10Filters, string[] rpt11Filters, string[] rpt12Filters, string[] rpt13Filters)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            string Key = "Result";
            string Msg = "Success";
            string Error = "Error";
            string NoData = "NoData";
            string strReportFilePath = string.Empty;

            string rptStartDate = rptBeginDate.ToString("MM/dd/yyyy");
            string rptEndDate = rptStopDate.ToString("MM/dd/yyyy"); ;
            if (rptStartDate == "01/01/0001") { rptStartDate = null; }
            if (rptEndDate == "01/01/0001") { rptEndDate = null; }


            try
            {

                if (rptType == "RPT1")
                {
                    String AgeFilter = GeneralServices.GetFilterString("AGE", rptType, rpt1Filters[10]);

                    Tuple<List<HouseholdProfilePDSDetailReport>,List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfilePDSDetailReport, int>(
                         ";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, " +
                         "@Gender, @Pregnant, @Lactating, @Handicapped, @ChronicallyIll, @NutritionalStatus, @ChildUnderTSForCMAM," +
                         "@EnrolledInSchool, @ChildProtectionRisk, @CBHIMembership, @AgeFilter",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod)? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear, 
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         Gender = rpt1Filters[0],
                         Pregnant = rpt1Filters[1],
                         Lactating = rpt1Filters[2],
                         Handicapped = rpt1Filters[3],
                         ChronicallyIll = rpt1Filters[4],
                         NutritionalStatus = rpt1Filters[5],
                         ChildUnderTSForCMAM = rpt1Filters[6],
                         EnrolledInSchool = rpt1Filters[7],
                         ChildProtectionRisk = rpt1Filters[8],
                         CBHIMembership = rpt1Filters[9],
                         AgeFilter = AgeFilter
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT2")
                {
                    string startDateTDS = rpt2Filters[0];
                    string endDateTDS = rpt2Filters[1];
                    if (startDateTDS == "Jan/0001") { startDateTDS = null; }
                    if (endDateTDS == "Jan/0001") { endDateTDS = null; }

                    Tuple<List<HouseholdProfileTDSPLWDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSPLWDetailReport, int>(";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, " +
                        "null, null, null, null, null, null, null,null, @ChildProtectionRisk, @CBHIMembership,@StartDateTDS, @EndDateTDS, @NutritionalStatusPLW, @NutritionalStatusInfant,null",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ChildProtectionRisk = rpt2Filters[4],
                         CBHIMembership = rpt2Filters[5],
                         StartDateTDS = startDateTDS,
                         EndDateTDS = endDateTDS,
                         NutritionalStatusPLW = rpt2Filters[2],
                         NutritionalStatusInfant = rpt2Filters[3]
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT3")
                {
                    string startDateTDS = rpt2Filters[0];
                    string endDateTDS = rpt2Filters[1];
                    if (startDateTDS == "Jan/0001") { startDateTDS = null; }
                    if (endDateTDS == "Jan/0001") { endDateTDS = null; }

                    Tuple<List<HouseholdProfileTDSCMCDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSCMCDetailReport, int>(";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate," +
                        "null, null, null, null, null, null, null,null, @ChildProtectionRisk, @CBHIMembership,@StartDateTDS, @EndDateTDS, null, null, @MalnutritionDegree",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ChildProtectionRisk = rpt3Filters[3],
                         CBHIMembership = rpt3Filters[4],
                         StartDateTDS = startDateTDS,
                         EndDateTDS = endDateTDS,
                         MalnutritionDegree = rpt3Filters[2],
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT4")
                {
                    Tuple<List<SocialServicesNeedsDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<SocialServicesNeedsDetailReport, int>(";Exec SocialServicesNeedsReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ClientType = rpt4Filters[0],
                         ServiceType = rpt4Filters[1]
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT5")
                {
                    Tuple<List<ServiceComplianceDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, int>(";Exec ServiceComplianceReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ClientType = rpt5Filters[0],
                         ServiceType = rpt5Filters[1]
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT6")
                {
                    Tuple<List<ServiceComplianceDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, int>(";Exec ServiceNonComplianceReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ClientType = rpt6Filters[0],
                         ServiceType = rpt6Filters[1]
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT7")
                {
                    Tuple<List<CaseManagementComplianceDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, int>(";Exec CaseManagementComplianceReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ClientType = rpt7Filters[0],
                         ServiceType = rpt7Filters[1]
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT8")
                {
                    Tuple<List<CaseManagementComplianceDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, int>(";Exec CaseManagementNonComplianceReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ClientType = rpt8Filters[0],
                         ServiceType = rpt8Filters[1]
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT9")
                {
                    Tuple<List<RetargetingDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, int>(";Exec RetargetingNewReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT10")
                {
                    Tuple<List<RetargetingDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, int>(";Exec RetargetingExitReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }


                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT11")
                {
                    String AgeFilter = GeneralServices.GetFilterString("AGE", rptType, rpt1Filters[10]);

                    Tuple<List<HouseholdProfilePDSDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfilePDSDetailReport, int>(
                         ";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, " +
                         "@Gender, @Pregnant, @Lactating, @Handicapped, @ChronicallyIll, @NutritionalStatus, @ChildUnderTSForCMAM," +
                         "@EnrolledInSchool, @ChildProtectionRisk, @CBHIMembership, @AgeFilter",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         Gender = rpt1Filters[0],
                         Pregnant = rpt1Filters[1],
                         Lactating = rpt1Filters[2],
                         Handicapped = rpt1Filters[3],
                         ChronicallyIll = rpt1Filters[4],
                         NutritionalStatus = rpt1Filters[5],
                         ChildUnderTSForCMAM = rpt1Filters[6],
                         EnrolledInSchool = rpt1Filters[7],
                         ChildProtectionRisk = rpt1Filters[8],
                         CBHIMembership = rpt1Filters[9],
                         AgeFilter = AgeFilter
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT12")
                {
                    string startDateTDS = rpt2Filters[0];
                    string endDateTDS = rpt2Filters[1];
                    if (startDateTDS == "Jan/0001") { startDateTDS = null; }
                    if (endDateTDS == "Jan/0001") { endDateTDS = null; }

                    Tuple<List<HouseholdProfileTDSPLWDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSPLWDetailReport, int>(";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate, " +
                        "null, null, null, null, null, null, null,null, @ChildProtectionRisk, @CBHIMembership,@StartDateTDS, @EndDateTDS, @NutritionalStatusPLW, @NutritionalStatusInfant,null",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ChildProtectionRisk = rpt2Filters[4],
                         CBHIMembership = rpt2Filters[5],
                         StartDateTDS = startDateTDS,
                         EndDateTDS = endDateTDS,
                         NutritionalStatusPLW = rpt2Filters[2],
                         NutritionalStatusInfant = rpt2Filters[3]
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT13")
                {
                    string startDateTDS = rpt2Filters[0];
                    string endDateTDS = rpt2Filters[1];
                    if (startDateTDS == "Jan/0001") { startDateTDS = null; }
                    if (endDateTDS == "Jan/0001") { endDateTDS = null; }

                    Tuple<List<HouseholdProfileTDSCMCDetailReport>, List<int>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSCMCDetailReport, int>(";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @GoteID, @ReportType, @ReportLevel, @ReportAggregation, @ReportPeriod, @SocialWorker,@FiscalYear, @ReportStartDate, @ReportEndDate," +
                        "null, null, null, null, null, null, null,null, @ChildProtectionRisk, @CBHIMembership,@StartDateTDS, @EndDateTDS, null, null, @MalnutritionDegree",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         GoteID = goteID,
                         ReportType = rptType,
                         ReportLevel = rptLevel,
                         ReportAggregation = rptDisaggregation,
                         ReportPeriod = string.IsNullOrEmpty(rptPeriod) ? 0 : Convert.ToInt32(rptPeriod),
                         SocialWorker = socialWorker,
                         FiscalYear = fiscalYear,
                         ReportStartDate = rptStartDate,
                         ReportEndDate = rptEndDate,
                         ChildProtectionRisk = rpt3Filters[3],
                         CBHIMembership = rpt3Filters[4],
                         StartDateTDS = startDateTDS,
                         EndDateTDS = endDateTDS,
                         MalnutritionDegree = rpt3Filters[2],
                     });

                    int result = resultset.Item2[0];
                    if (result == 0)
                    {
                        dict.Add(Key, NoData);
                        return dict;
                    }

                    dict.Add(Key, Msg);
                    return dict;
                }

                dict.Add(Key, Error);
                return dict;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                dict.Add(Key, Error);
                return dict;
            }
        }

        public string[] ExportReport(string reportName, string reportLevel, string reportFormat, string reportCenter, string reportFilter)
        {
            DataTable dt;
            string strReportFilePath = string.Empty;
            string filename = string.Empty;
            string reportfile = string.Empty;

            switch (reportCenter)
            {
                case "FEDERAL":
                    reportfile = "_Region_File.rpt";
                    break;
                case "REGIONAL":
                    reportfile = "_Region_File.rpt";
                    break;
                case "WOREDA":
                    reportfile = "_Woreda_File.rpt";
                    break;
                case "KEBELE":
                    reportfile = "_Kebele_File.rpt";
                    break;
                default:
                    reportfile = "File.rpt";
                    break;
            }

            try
            {
                if (reportName == "RPT1")
                {

                    Tuple<List<HouseholdProfilePDSDetailReport>, List<int>, List<HouseholdProfilePDSSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfilePDSDetailReport, int, HouseholdProfilePDSSummaryReport, int>(
                        ";Exec GetStandardRpt1 0,10,0,NULL,NULL,@GroupBy, 1",
                        new
                        {
                            GroupBy = reportCenter
                        });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfilePDSDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfilePDSDetailsRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "HouseholdProfile_PDS_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }
                            
                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfilePDSSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfilePDSSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "HouseholdProfile_PDS_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT2")
                {
                    Tuple<List<HouseholdProfileTDSPLWDetailReport>, List<int>, List<HouseholdProfileTDSPLWSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSPLWDetailReport, int, HouseholdProfileTDSPLWSummaryReport, int>(
                        ";Exec GetStandardRpt2 0,10,0,NULL,NULL,@GroupBy, 1",
                        new
                        {
                            GroupBy = reportCenter
                        });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSPLWDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSPLWDetailsRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "HouseholdProfile_PLW_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSPLWSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSPLWSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "HouseholdProfile_PLW_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT3")
                {
                    Tuple<List<HouseholdProfileTDSCMCDetailReport>, List<int>, List<HouseholdProfileTDSCMCSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSCMCDetailReport, int, HouseholdProfileTDSCMCSummaryReport, int>(
                        ";Exec GetStandardRpt3 0,10,0,NULL,NULL,@GroupBy,1",
                        new
                        {
                            GroupBy = reportCenter
                        });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSCMCDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSCMCDetailsRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "HouseholdProfile_CMC_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSCMCSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSCMCSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "HouseholdProfile_CMC_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT4")
                {
                    Tuple<List<SocialServicesNeedsDetailReport>, List<int>, List<SocialServicesNeedsSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<SocialServicesNeedsDetailReport, int, SocialServicesNeedsSummaryReport, int>(
                        ";Exec GetStandardRpt4 0,10,0,NULL,NULL,@GroupBy,1",
                        new
                        {
                            GroupBy = reportCenter
                        });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<SocialServicesNeedsDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/SocialServicesNeedsDetailsRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "SocialServicesNeeds_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<SocialServicesNeedsSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/SocialServicesNeedsSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "SocialServicesNeeds_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT5")
                {
                    Tuple<List<ServiceComplianceDetailReport>, List<int>, List<ServiceComplianceSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, int, ServiceComplianceSummaryReport, int>(
                        ";Exec GetStandardRpt5 0,10,0,NULL,NULL,@GroupBy,1",
                        new
                        {
                            GroupBy = reportCenter
                        });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/ServiceComplianceDetailsRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "ServiceCompliance_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/ServiceComplianceSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "ServiceCompliance_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT6")
                {

                    Tuple<List<ServiceComplianceDetailReport>, List<int>, List<ServiceComplianceSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, int, ServiceComplianceSummaryReport, int>(
                        ";Exec GetStandardRpt6 0,10,0,NULL,NULL,@GroupBy, 1",
                        new
                        {
                            GroupBy = reportCenter
                        });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/ServiceNonComplianceDetailsRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "ServiceNonCompliance_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/ServiceNonComplianceSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "ServiceNonCompliance_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT7")
                {

                    Tuple<List<CaseManagementComplianceDetailReport>, List<int>, List<CaseManagementComplianceSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, int, CaseManagementComplianceSummaryReport, int>(
                        ";Exec GetStandardRpt7 0,10,0,NULL,NULL,@GroupBy,1",
                        new
                        {
                            GroupBy = reportCenter
                        });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementComplianceDetailRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "CaseManagementCompliance_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementComplianceSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "CaseManagementCompliance_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT8")
                {
                    Tuple<List<CaseManagementComplianceDetailReport>, List<int>, List<CaseManagementComplianceSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, int, CaseManagementComplianceSummaryReport, int>(
                        ";Exec GetStandardRpt8 0,10,0,NULL,NULL,@GroupBy,1",
                        new
                        {
                            GroupBy = reportCenter
                        });


                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementNonComplianceDetailRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "CaseManagementNonCompliance_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementNonComplianceSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "CaseManagementNonCompliance_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT9")
                {
                    Tuple<List<RetargetingDetailReport>, List<int>, List<RetargetingSummaryReport>, List<int>> resultset
                           = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, int, RetargetingSummaryReport, int>(
                           ";Exec GetStandardRpt9 0,10,0,NULL,NULL,@GroupBy,1",
                           new
                           {
                               GroupBy = reportCenter
                           });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingNewDetailsRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "RetargetingNew_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingNewSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "RetargetingNew_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT10")
                {

                    Tuple<List<RetargetingDetailReport>, List<int>, List<RetargetingSummaryReport>, List<int>> resultset
                           = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, int, RetargetingSummaryReport, int>(
                           ";Exec GetStandardRpt10 0,10,0,NULL,NULL,@GroupBy,1",
                           new
                           {
                               GroupBy = reportCenter
                           });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingExitDetailsRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "RetargetingExit_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingSummaryReport>(resultset.Item3);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingExitSummaryRpt" + reportfile);
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                        myReportDocument.SetParameterValue("FilteredBy", reportFilter);

                        filename = "RetargetingExit_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT11")
                {

                    Tuple<List<HouseholdProfilePDSDetailReport>, List<int>, List<HouseholdProfilePDSDisaggregatedSummaryReport>, List<int>> resultset
                           = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfilePDSDetailReport, int, HouseholdProfilePDSDisaggregatedSummaryReport, int>(
                           ";Exec GetStandardRpt11 0,10,0,NULL,NULL,@GroupBy,1",
                           new
                           {
                               GroupBy = reportCenter
                           });

                    if (reportLevel == "DT")
                    {
                        
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfilePDSDisaggregatedSummaryReport>(resultset.Item3);
                        filename = "HouseholdProfile_Disaggregated_PDS_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + ".xls";
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        
                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }
                        }
                    }
                }

                if (reportName == "RPT12")
                {

                    Tuple<List<HouseholdProfileTDSPLWDetailReport>, List<int>, List<HouseholdProfilePLWDisaggregatedSummaryReport>, List<int>> resultset
                           = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSPLWDetailReport, int, HouseholdProfilePLWDisaggregatedSummaryReport, int>(
                           ";Exec GetStandardRpt12 0,10,0,NULL,NULL,@GroupBy,1",
                           new
                           {
                               GroupBy = reportCenter
                           });

                    if (reportLevel == "DT")
                    {

                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfilePLWDisaggregatedSummaryReport>(resultset.Item3);
                        filename = "HouseholdProfile_Disaggregated_PLW_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + ".xls";
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }
                        }
                    }
                }

                if (reportName == "RPT13")
                {

                    Tuple<List<HouseholdProfileTDSCMCDetailReport>, List<int>, List<HouseholdProfileCMCDisaggregatedSummaryReport>, List<int>> resultset
                           = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSCMCDetailReport, int, HouseholdProfileCMCDisaggregatedSummaryReport, int>(
                           ";Exec GetStandardRpt13 0,10,0,NULL,NULL,@GroupBy,1",
                           new
                           {
                               GroupBy = reportCenter
                           });

                    if (reportLevel == "DT")
                    {

                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileCMCDisaggregatedSummaryReport>(resultset.Item3);
                        filename = "HouseholdProfile_Disaggregated_CMC_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + ".xls";
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }
                        }
                    }
                }

                string[] path = new string[] { strReportFilePath, filename };
                return path;

            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return null;
            }
        }

        public IEnumerable<ReportWoreda> GetWoredas(string region)
        {
            List<ReportWoreda> woredas = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ReportWoreda>(";Exec GetReportWoredas @RegionID", new { RegionID = region });

            return woredas;
        }

        public IEnumerable<ReportKebele> GetKebeles(string woreda)
        {
            List<ReportKebele> kebeles = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ReportKebele>(";Exec GetReportKebeles @WoredaID", new { WoredaID = woreda });

            return kebeles;
        }

        public IEnumerable<ReportGote> GetGotes(string kebele)
        {
            List<ReportGote> gotes = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ReportGote>(";Exec GetReportGotes @KebelenID", new { KebelenID = kebele });

            return gotes;
        }
    }


}
