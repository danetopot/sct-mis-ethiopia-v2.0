﻿using Newtonsoft.Json;
using NPoco;
using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Xml;

namespace SCTMis.Services
{
    public class AppConstants
    {
        public const string Form1A = "1A";
        public const string Form1B = "1B";
        public const string Form1C = "1C";

        public const string Form4D = "4D";
    }


    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString CurrencyFormat(this HtmlHelper helper, string value)
        {
            var result = string.Format("{0:C2}", value);
            return new MvcHtmlString(result);
        }
    }

    public class GeneralServices
    {
        public static void LogError(Exception exception)
        {
            var logFile = string.Format("~/Logs/Errors_{0}.txt", DateTime.Now.ToString("yyyyMMddHHmm"));
            string logFilePath = HttpContext.Current.Server.MapPath(logFile);

            var errorMessage = exception.Message + Environment.NewLine + exception.InnerException + Environment.NewLine + exception + Environment.NewLine + Environment.NewLine;
            using (var streamWriter = new StreamWriter(logFilePath, true))
            {
                streamWriter.WriteLine(errorMessage);
            }
        }

        public static void LogError(string errorMessage)
        {
            var logFile = string.Format("~/Logs/Errors_{0}.txt", DateTime.Now.ToString("yyyyMMddHHmm"));
            string logFilePath = HttpContext.Current.Server.MapPath(logFile);

            using (var streamWriter = new StreamWriter(logFilePath, true))
            {
                streamWriter.WriteLine(errorMessage);
            }
        }

        public static void WriteTrxLog(String log)
        {
            try
            {
                string LogPath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/");
                    
                string fileName = LogPath + "Sct-mis.log" + String.Format("{0:yyyy-MM-dd}", DateTime.Now).ToString();
                System.IO.TextWriter ErrHan = new System.IO.StreamWriter(fileName, true);
                ErrHan.WriteLine(String.Format("{0:yyyy MMM dd:HH:mm:ss}:", DateTime.Now) + log);
                ErrHan.Flush();
                ErrHan.Close();
                
            }
            catch { }
        }

        public static string GetAge(string dateOfBirth)
        {
            var parsedDateOfBirth = DateTime.Parse(dateOfBirth);
            var currentDate = DateTime.Today;

            var months = ((currentDate.Year - parsedDateOfBirth.Year) * 12) + currentDate.Month - parsedDateOfBirth.Month;

            var ageInYears = Math.Floor(Convert.ToDecimal(months / 12));

            var ageInMonths = months % 12;

            if (ageInYears < 1)
            {
                string age = string.Format("{0}.{1}", ageInYears, ageInMonths);

                return age;
            }

            return ageInYears.ToString();
        }

        public IEnumerable<Form1AMembers> GetForm1AMemberListing(string MemberXml)
        {
            string strXML = string.Empty;
            JavaScriptSerializer objJavascript = new JavaScriptSerializer();
            List<Form1AMembers> memberListing = null;
            try
            {
                Form1AMembers[] MemberList = objJavascript.Deserialize<Form1AMembers[]>(MemberXml);


                foreach (var array in MemberList)
                {
                    memberListing = new List<Form1AMembers>
                {
                    new Form1AMembers{  
                        ColumnID = array.ColumnID,
                        HouseHoldMemberName = array.HouseHoldMemberName,
                        IndividualID = array.IndividualID,
                        MedicalRecordNumber = array.MedicalRecordNumber,
                        DateOfBirth = array.DateOfBirth,
                        Age = array.Age,
                        Sex = array.Sex,
                        SexName = array.SexName,
                        PWL = array.PWL,
                        PWLName = array.PWLName,
                        Handicapped = array.Handicapped,
                        HandicappedName = array.HandicappedName,
                        ChronicallyIll = array.ChronicallyIll,
                        ChronicallyIllName = array.ChronicallyIllName,
                        NutritionalStatus = array.NutritionalStatus,
                        NutritionalStatusName = array.NutritionalStatusName,
                        EnrolledInSchool = array.EnrolledInSchool,
                        EnrolledInSchoolName = array.EnrolledInSchoolName,
                        SchoolName = array.SchoolName
                    }
                };
                }

                return memberListing;
            }
            catch (Exception e)
            {
                return memberListing;
            }
        }

        public IEnumerable<ReportingPeriod> GetReportingPeriods()
        {
            int? fiscalYear = null;
            List<ReportingPeriod> reportingperiod = NpocoConnection().Fetch<ReportingPeriod>(";Exec getReportPeriod @FiscalYear", new { FiscalYear = fiscalYear });

            return reportingperiod;
        }

        public List<ReportingPeriod> GetReportingPeriods(int fiscalYear)
        {
            List<ReportingPeriod> reportingperiods = NpocoConnection().Fetch<ReportingPeriod>(";Exec getReportPeriod @FiscalYear", new { FiscalYear = fiscalYear });

            return reportingperiods;
        }

        public IEnumerable<HouseholdVisit> GetHouseholdVisits()
        {
            List<HouseholdVisit> householdvisits = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<HouseholdVisit>(";Exec getHouseholdVisit");

            return householdvisits;
        }

        public FinancialYear GetCurrentFiscalYear()
        {
            var currentFiscalYear = NpocoConnection().Single<FinancialYear>(";Exec GetCurrentFiscalYear");

            return currentFiscalYear;
        }

        public static bool PnspNumberExists(string pnspNumber, string formNumber, string formId, bool editing)
        {
            var returnedRecords = NpocoConnection().Fetch<PsnpCheck>(";Exec CheckPNSPNumber @PNSPNumber, @FormNumber", new { PNSPNumber = pnspNumber, FormNumber = formNumber });

            if (returnedRecords == null || returnedRecords.Count == 0) return false;

            if (editing)
            {
                var sameRecord = returnedRecords.FirstOrDefault(r => r.FormId == formId);
                return sameRecord == null;
            }

            return true;
        }

        public static bool CBHINumberExists(string cbhiNumber, string formNumber, string formId, bool editing)
        {
            var returnedRecords = NpocoConnection().Fetch<CbhiCheck>(";Exec CheckCBHINumber @CBHINumber, @FormNumber", new { CBHINumber = cbhiNumber, FormNumber = formNumber });

            if (returnedRecords == null || returnedRecords.Count == 0) return false;

            if (editing)
            {
                var sameRecord = returnedRecords.FirstOrDefault(r => r.FormId == formId);
                return sameRecord == null;
            }

            return true;
        }

        public static bool ChildProtectionNumberExists(string childProtectionNumber, string formNumber, string formId, bool editing)
        {
            var returnedRecords = NpocoConnection().Fetch<CpIdCheck>(";Exec CheckChildProtectionNumber @ChildProtectionId, @FormNumber", new { ChildProtectionId = childProtectionNumber, FormNumber = formNumber });

            if (returnedRecords == null || returnedRecords.Count == 0) return false;

            if (editing)
            {
                var sameRecord = returnedRecords.FirstOrDefault(r => r.FormId == formId);
                return sameRecord == null;
            }

            return true;
        }

        public IEnumerable<ReportType> GetReportTypes()
        {
            List<ReportType> reporttypes = new List<ReportType> {
                new ReportType{ ReportID="",ReportName = "-- Select Report Name --"},
                new ReportType{ ReportID="4D",ReportName = "Form 4D (With Age Disaggregation)"},
                new ReportType{ ReportID="4D2",ReportName = "Form 4D (Without Age Disaggregation)"},
                new ReportType{ ReportID="6A",ReportName = "Form 6A"},
                new ReportType{ ReportID="6B",ReportName = "Form 6B"},
                new ReportType{ ReportID="6C",ReportName = "Form 6C"}
                };

            return reporttypes;
        }

        public IEnumerable<ReportType> GetStandardReportTypes()
        {
            List<ReportType> reporttypes = new List<ReportType> {
                new ReportType{ ReportID="RPT1",ReportName = "Household Profile Report - PDS"},
                new ReportType{ ReportID="RPT11",ReportName = "Household Profile Report - PDS Disaggregated"},
                new ReportType{ ReportID="RPT2",ReportName = "Household Profile Report - TDS(PLW)"},
                new ReportType{ ReportID="RPT12",ReportName = "Household Profile Report - TDS(PLW) Disaggregated"},
                new ReportType{ ReportID="RPT3",ReportName = "Household Profile Report - TDS(CMC)"},
                new ReportType{ ReportID="RPT13",ReportName = "Household Profile Report - TDS(CMC) Disaggregated"},
                new ReportType{ ReportID="RPT4",ReportName = "Social Services Needs Report"},
                new ReportType{ ReportID="RPT5",ReportName = "Service Compliance Report"},
                new ReportType{ ReportID="RPT6",ReportName = "Service Non-Compliance Report"},
                new ReportType{ ReportID="RPT7",ReportName = "Case Management Compliance Report"},
                new ReportType{ ReportID="RPT8",ReportName = "Case Management Non-Compliance Report"},
                new ReportType{ ReportID="RPT9",ReportName = "Re-Targeting Report - New"},
                new ReportType{ ReportID="RPT10",ReportName = "Re-Targeting Report - Exit"}
                };

            return reporttypes;
        }

        public IEnumerable<ReportType> GetExportReportTypes()
        {
            List<ReportType> reporttypes = new List<ReportType> {
                new ReportType{ ReportID="1A",ReportName = "Form 1A"},
                new ReportType{ ReportID="1B",ReportName = "Form 1B"},
                new ReportType{ ReportID="1C",ReportName = "Form 1C"}
                };

            return reporttypes;
        }

        public IEnumerable<ReportDisaggregation> GetReportDisaggregationTypes()
        {
            List<ReportDisaggregation> reporttypes = new List<ReportDisaggregation> {
                new ReportDisaggregation{ DisaggregationID="SM",DisaggregationName = "Summary"},
                new ReportDisaggregation{ DisaggregationID="DT",DisaggregationName = "Detail"}
                };

            return reporttypes;
        }

        public IEnumerable<ReportLevel> GetReportLevels()
        {
            List<ReportLevel> reportlevels = new List<ReportLevel> {
                new ReportLevel{ LevelID="FEDERAL",LevelName = "Federal"},
                new ReportLevel{ LevelID="REGIONAL",LevelName = "Regional"},
                new ReportLevel{ LevelID="WOREDA",LevelName = "Woreda"},
                new ReportLevel{ LevelID="KEBELE",LevelName = "Kebele"}
                //new ReportLevel{ LevelID="GOTE",LevelName = "Gote"}
                };

            return reportlevels;
        }

        public IEnumerable<ReportAgeRange> GetReportAgeRanges()
        {
            List<ReportAgeRange> reporttypes = new List<ReportAgeRange> {
                new ReportAgeRange{ ID="1",Name = "Under 1"},
                new ReportAgeRange{ ID="2",Name = "1 - 2"},
                new ReportAgeRange{ ID="3",Name = "3 - 5"},
                new ReportAgeRange{ ID="4",Name = "6 - 12"},
                new ReportAgeRange{ ID="5",Name = "13 - 19"},
                new ReportAgeRange{ ID="6",Name = "20 - 24"},
                new ReportAgeRange{ ID="7",Name = "25 - 49"},
                new ReportAgeRange{ ID="8",Name = "46 - 59"},
                new ReportAgeRange{ ID="9",Name = "Over 60"}
                };

            return reporttypes;
        }

        public IEnumerable<ServiceProviders> GetServiceProviders()
        {
            List<ServiceProviders> serviceprovider = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ServiceProviders>(";Exec GetServiceProviders");

            return serviceprovider;
        }
        //ServiceProviderModel model
        public IEnumerable<ServiceProviders> GetServiceProvidersUpdated(ServiceProviderModel model)
        {
            List<ServiceProviders> serviceprovider
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ServiceProviders>(";Exec getPendingProviders @FormCode,@KebeleID,@ReportingPeriodID",
                                new
                                {
                                    FormCode = model.FormCode,
                                    KebeleID = model.KebeleID,
                                    ReportingPeriodID = model.ReportingPeriodID
                                }); ;

            return serviceprovider;
        }

        public IEnumerable<IntegratedServices> GetIntegratedServices(int _ServiceProviderID, int _FormID)
        {
            List<IntegratedServices> intgrtedservices
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<IntegratedServices>(";Exec GetIntegratedServices @ServiceProviderID,@FormID", 
                new { 
                    ServiceProviderID = _ServiceProviderID ,
                    FormID = _FormID 
                });

            return intgrtedservices;
        }

        //UPDATES FOR FORM 4S
        //Form 4A
        public IEnumerable<Kebele> GetForm4AKebeles()
        {

            List<Kebele> kebeles = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Kebele>(";Exec getForm4AKebele");

            return kebeles;
        }
        public IEnumerable<Form4ServiceProviders> GetForm4AServiceProviders(int _KebeleID)
        {
            List<Form4ServiceProviders> serviceprovider
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form4ServiceProviders>(";Exec getForm4AServiceProvider @KebeleID",
                new
                {
                    KebeleID = _KebeleID
                });

            return serviceprovider;
        }


        public IEnumerable<IntegratedServices> GetForm4AServices(int _KebeleID, int _ServiceProviderID)
        {
            List<IntegratedServices> intgrtedservices
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<IntegratedServices>(";Exec getForm4AServices @KebeleID, @ServiceProviderID",
                new
                {
                    KebeleID = _KebeleID,
                    ServiceProviderID = _ServiceProviderID
                });

            return intgrtedservices;
        }

        //Form 4B
        public IEnumerable<Kebele> GetForm4BKebeles()
        {

            List<Kebele> kebeles = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Kebele>(";Exec getForm4BKebele");

            return kebeles;
        }
        public IEnumerable<Form4ServiceProviders> GetForm4BServiceProviders(int _KebeleID)
        {
            List<Form4ServiceProviders> serviceprovider
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form4ServiceProviders>(";Exec getForm4BServiceProvider @KebeleID",
                new
                {
                    KebeleID = _KebeleID
                });

            return serviceprovider;
        }

        public IEnumerable<IntegratedServices> GetForm4BServices(int _KebeleID, int _ServiceProviderID)
        {
            List<IntegratedServices> intgrtedservices
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<IntegratedServices>(";Exec getForm4BServices @KebeleID, @ServiceProviderID",
                new
                {
                    KebeleID = _KebeleID,
                    ServiceProviderID = _ServiceProviderID
                });

            return intgrtedservices;
        }

        //Form 4c
        public IEnumerable<Kebele> GetForm4CKebeles()
        {

            List<Kebele> kebeles = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Kebele>(";Exec getForm4CKebele");

            return kebeles;
        }
        public IEnumerable<Form4ServiceProviders> GetForm4CServiceProviders(int _KebeleID)
        {
            List<Form4ServiceProviders> serviceprovider
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form4ServiceProviders>(";Exec getForm4CServiceProvider @KebeleID",
                new
                {
                    KebeleID = _KebeleID
                });

            return serviceprovider;
        }

        public IEnumerable<IntegratedServices> GetForm4CServices(int _KebeleID, int _ServiceProviderID)
        {
            List<IntegratedServices> intgrtedservices
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<IntegratedServices>(";Exec getForm4CServices @KebeleID, @ServiceProviderID",
                new
                {
                    KebeleID = _KebeleID,
                    ServiceProviderID = _ServiceProviderID
                });

            return intgrtedservices;
        }

        //UPDATES FOR FORM 4S ENDS HERE

        public IEnumerable<IntegratedServices> GetAllIntegratedServices()
        {
            List<IntegratedServices> intgrtedservices
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<IntegratedServices>(";Exec GetAllIntegratedServices");

            return intgrtedservices;
        }

        public IEnumerable<Region> GetDashRegions()
        {
            List<Region> regions = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Region>(";Exec GetDashRegions");

            return regions;
        }

        public IEnumerable<Woreda> GetDashWoredas(int _RegionID)
        {
            List<Woreda> woredas = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Woreda>(";Exec GetDashWoredas @RegionID", new { RegionID = _RegionID });

            return woredas;
        }

        public IEnumerable<Kebele> GetDashKebeles(int _WoredaID)
        {

            List<Kebele> kebeles = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Kebele>(";Exec GetDashKebeles @WoredaID", new { WoredaID = _WoredaID });

            return kebeles;
        }

        //Dashboard Ends here
        public IEnumerable<Region> GetRegions()
        {
            List<Region> regions = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Region>(";Exec GetRegions");

            return regions;
        }

        public IEnumerable<Woreda> GetWoredas(int _RegionID)
        {
            List<Woreda> woredas = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Woreda>(";Exec GetWoredas @RegionID", new { RegionID = _RegionID });

            return woredas;
        }
        
        public IEnumerable<Kebele> GetDataKebeles(string _FormNumber,int _WoredaID)
        {

            List<Kebele> kebeles = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Kebele>(";Exec GetDataKebeles @FormNumber,@WoredaID", new { FormNumber = _FormNumber, WoredaID = _WoredaID });

            return kebeles;
        }

        public IEnumerable<Kebele> GetKebeles(int _WoredaID)
        {

            List<Kebele> kebeles = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Kebele>(";Exec GetKebeles @WoredaID", new { WoredaID = _WoredaID });

            return kebeles;
        }

        public IEnumerable<Gote> GetGotes(int _KebeleID)
        {

            List<Gote> gotes = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Gote>(";Exec GetReportGotes @KebeleID", new { KebeleID = _KebeleID });

            return gotes;
        }

        // extra info here
        public IEnumerable<Woreda> GetWoredaByIDs(int _RegionID, int _WoredaID)
        {
            List<Woreda> woredas = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Woreda>(";Exec GetWoredaByID @RegionID, @WoredaID", new { RegionID = _RegionID, WoredaID = _WoredaID });

            return woredas;
        }

        public IEnumerable<Region> GetRegionByIDs(int _RegionID)
        {
            List<Region> regions = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Region>(";Exec GetRegionsByID @RegionID", new { RegionID = _RegionID });

            return regions;
        }

        public IEnumerable<FinancialYear> GetFiscalYears(int? status)
        {
            try
            {
                List<FinancialYear> fiscalYears = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<FinancialYear>(";Exec GetFiscalYears @Status", new { Status = status });

                return fiscalYears;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return null;
            }            
        }

        public IEnumerable<ReportingIndicator> GetReportingIndicators()
        {
            try
            {
                List<ReportingIndicator> reportingIndicators = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ReportingIndicator>(";Exec GetReportingIndicators");

                return reportingIndicators;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return null;
            }
        }

        //Extra Info ends here
        public IEnumerable<ClientType> GetClientTypes()
        {
            List<ClientType> clientType = new List<ClientType> {
                new ClientType{ ClientTypeID= "", ClientTypeName = "-- Select Client Type --" },
                new ClientType{ ClientTypeID= "PDS", ClientTypeName = "Permanent Direct Support(PDS)" },
                new ClientType{ ClientTypeID= "PLW", ClientTypeName = "Temporary Direct Support(PLW)" },
                new ClientType{ ClientTypeID= "CMC", ClientTypeName = "Temporary Direct Support(CMC)" }
                };

            return clientType;
        }

        public IEnumerable<ServiceType> GetServiceTypes()
        {
            List<ServiceType> serviceType = new List<ServiceType> {
                new ServiceType{ ServiceID= "1",ServiceName = "Ante & post natal care" },
                new ServiceType{ ServiceID= "2",ServiceName = "Immunization" },
                new ServiceType{ ServiceID= "3",ServiceName = "Check up for children under TSF or CMAM" },
                new ServiceType{ ServiceID= "4",ServiceName = "Participation in monthly GMP sessions" },
                new ServiceType{ ServiceID= "5",ServiceName = "School enrolment & attendance" },
                new ServiceType{ ServiceID= "6",ServiceName = "Participation in BCC sessions" },
                new ServiceType{ ServiceID= "7",ServiceName = "Birth registration" },
                new ServiceType{ ServiceID= "8",ServiceName = "3 pre natal care visits at health post" },
                new ServiceType{ ServiceID= "9",ServiceName = "1 pre natal care visit at health centre" },
                new ServiceType{ ServiceID= "10",ServiceName = "Check -up of child at health post twice a month" },
                new ServiceType{ ServiceID= "11",ServiceName = "Therapeutic or supplementary feeding" },
                new ServiceType{ ServiceID= "12",ServiceName = "CBHI Membership Access & Utilization" },
                new ServiceType{ ServiceID= "13",ServiceName = "Child Protection Case Management" }
                };

            return serviceType;
        }

        public IEnumerable<Risk> GetCPRiskTypes()
        {
            List<Risk> risktypes = new List<Risk> {
                new Risk{ ID="-",Name = "-- Select Risk --"},
                new Risk{ ID="1",Name = "Maltreatment"},
                new Risk{ ID="2",Name = "Bullying"},
                new Risk{ ID="3",Name = "Youth violence"},
                new Risk{ ID="4",Name = "Intimate partner violence"},
                new Risk{ ID="5",Name = "Sexual violence"},
                new Risk{ ID="6",Name = "Emotional or psychological violence"},
                new Risk{ ID="7",Name = "Gender-based violence"},
                new Risk{ ID="8",Name = "Children without adequate parental care"},
                new Risk{ ID="9",Name = "Child trafficking"},
                new Risk{ ID="10",Name = "Children living with disabilities at risk of exclusion"},
                new Risk{ ID="11",Name = "Child labour"},
                new Risk{ ID="12",Name = "Children in contact with the law"},
                new Risk{ ID="13",Name = "Children on the move"},
                new Risk{ ID="14",Name = "Children at risk of harmful practices"},
                new Risk{ ID="15",Name = "Children in street situations"}
                };

            return risktypes;
        }

        public IEnumerable<Service> GetCPServiceTypes()
        {
            List<Service> servicetypes = new List<Service> {
                new Service{ ID="-",Name = "-- Select Service --"},
                new Service{ ID="0",Name = "No Services Provided"},
                new Service{ ID="1",Name = "Food and Nutrition"},
                new Service{ ID="2",Name = "Shelter"},
                new Service{ ID="3",Name = "Economic Strengthening"},
                new Service{ ID="4",Name = "Care"},
                new Service{ ID="5",Name = "Justice Services(including by police, Office of Justice and courts)"},
                new Service{ ID="6",Name = "Health"},
                new Service{ ID="7",Name = "Psycho-Social"},
                new Service{ ID="8",Name = "Education and Skills Training"}
                };

            return servicetypes;
        }

        public IEnumerable<Provider> GetCPProviderTypes()
        {
            List<Provider> providertypes = new List<Provider> {
                new Provider{ ID="-",Name = "-- Select Provider --"},
                new Provider{ ID="1",Name = "Woreda Office of Women and Children Affairs"},
                new Provider{ ID="2",Name = "Woreda Office of Labour and Social Affairs"},
                new Provider{ ID="3",Name = "Woreda Office of Agriculture/Food Security"},
                new Provider{ ID="4",Name = "Woreda Micro Enterprise Development Office"},
                new Provider{ ID="5",Name = "Health Center"},
                new Provider{ ID="6",Name = "Private Health Facility"},
                new Provider{ ID="7",Name = "School"},
                new Provider{ ID="8",Name = "NGO"},
                new Provider{ ID="9",Name = "Police"},
                new Provider{ ID="10",Name = "Justice Office"},
                new Provider{ ID="11",Name = "Court"},
                new Provider{ ID="12",Name = "CCC"},
                new Provider{ ID="13",Name = "Community-Based Organization"},
                new Provider{ ID="14",Name = "Other"}
                };

            return providertypes;
        }

        public IEnumerable<ServiceAccess> GetCPReasonNotAccessService()
        {
            List<ServiceAccess> reasonsList = new List<ServiceAccess> {
                new ServiceAccess{ ID="-",Name = "-- Select Reason --"},
                new ServiceAccess{ ID="1",Name = "Services not available at the time the client sought the services"},
                new ServiceAccess{ ID="2",Name = "Client not willing to access services"},
                new ServiceAccess{ ID="3",Name = "Service site too far for client"},
                new ServiceAccess{ ID="4",Name = "Service provider not willing to provide services"},
                new ServiceAccess{ ID="5",Name = "Client has no trust in services or service provider for personal reasons"},
                new ServiceAccess{ ID="6",Name = "Client prevented to access services by a third party including apprehension"},
                new ServiceAccess{ ID="7",Name = "Other"}
                };

            return reasonsList;
        }

        public IEnumerable<Gender> GetGenders()
        {
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ GenderName="-- Select Gender --",   GenderID = ""},
                                             new Gender{ GenderName="Male",   GenderID = "M"},
                                             new Gender{ GenderName="Female", GenderID = "F"}
                                             };

            return genderList;
        }

        public IEnumerable<YesNo> GetYesNo()
        {
            List<YesNo> yesno = new List<YesNo> {                                             
                                             new YesNo{ Name="N/A", ID = "-"},
                                             new YesNo{ Name="NO", ID = "NO"},
                                             new YesNo{ Name="YES",   ID = "YES"}
                                             };

            return yesno;
        }

        public IEnumerable<NutritionalStatus> GetNutritionalStatus()
        {
            List<NutritionalStatus> nutrStatus = new List<NutritionalStatus> {
                                             new NutritionalStatus{ Name="N/A", ID = "-"},
                                             new NutritionalStatus{ Name="Normal", ID = "N"},
                                             new NutritionalStatus{ Name="Mod.", ID = "M"},
                                             new NutritionalStatus{ Name="Sev. Malnourished", ID = "SM"}
                                             };

            return nutrStatus;
        }

        public IEnumerable<ClientStatus> GetClientStatus()
        {
            List<ClientStatus> clientStatus = new List<ClientStatus> {
                                             new ClientStatus{ Name="N/A", ID = "-"},
                                             new ClientStatus{ Name="Alive", ID = "1"},
                                             new ClientStatus{ Name="Deceased", ID = "2"},
                                             new ClientStatus{ Name="Divorced", ID = "3"},
                                             new ClientStatus{ Name="Graduated", ID = "4"}
                                             };

            return clientStatus;
        }

        public IEnumerable<SchoolGrade> GetSchoolGrade()
        {
            List<SchoolGrade> classgrade = new List<SchoolGrade> {
                                             new SchoolGrade{ Name="Select Grade",   ID = ""},
                                             new SchoolGrade{ Name="Grade 1",   ID = "1"},
                                             new SchoolGrade{ Name="Grade 2",   ID = "2"},
                                             new SchoolGrade{ Name="Grade 3",   ID = "3"},
                                             new SchoolGrade{ Name="Grade 4", ID = "4"},
                                             new SchoolGrade{ Name="Grade 5",   ID = "5"},
                                             new SchoolGrade{ Name="Grade 6",   ID = "6"},
                                             new SchoolGrade{ Name="Grade 7",   ID = "7"},
                                             new SchoolGrade{ Name="Grade 8", ID = "8"},
                                             new SchoolGrade{ Name="Grade 9",   ID = "9"},
                                             new SchoolGrade{ Name="Grade 10",   ID = "10"},
                                             new SchoolGrade{ Name="Grade 11",   ID = "11"},
                                             new SchoolGrade{ Name="Grade 12", ID = "12"},
                                             new SchoolGrade{ Name="Grade 13",   ID = "13"},
                                             new SchoolGrade{ Name="Grade 14",   ID = "14"}
                                             };

            return classgrade;
        }

        public IEnumerable<SocialWorker> GetSocialWorkers()
        {
            List<SocialWorker> worker = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<SocialWorker>(";Exec getSocialWorker");

            //List<SocialWorker> worker = new List<SocialWorker> {
            //                                 new SocialWorker{ Name="-- Select Worker --",   ID = ""},
            //                                 new SocialWorker{ Name="Social Worker",   ID = "N"},
            //                                 new SocialWorker{ Name="Demo Worker", ID = "SM"}
            //                                 };

            return worker;
        }

        public IEnumerable<PLW> GetPLW()
        {
            List<PLW> plws = new List<PLW> {                                             
                                             new PLW{ Name="Pregnant", ID = "P"},
                                             new PLW{ Name="Lactating",   ID = "L"}
                                             };

            return plws;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public static string GetDhtmlxXML(DataTable dt, Int64 RowCount, int Pos, int uniqueIDCol, int iconID, params KeyValuePair<string, object>[] pairs)
        {
            StringBuilder sb = new StringBuilder();
            string HeadStr = string.Empty;
            string TempStr = HeadStr;
            string itemVal;

            int rowID;
            string LastColumnData = string.Empty;
            
            sb.Append("<?xml version='1.0' encoding='UTF-8'?>");

            sb.Append("<rows total_count='" + RowCount + "' pos='" + Pos + "'>");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                rowID = i;
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<row id='" + dt.Rows[i][j].ToString() + "'>");
                    }

                    sb.Append("<cell>");
                    itemVal = dt.Rows[i][j].ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
                    if (itemVal == "False") { itemVal = "0"; }

                    sb.Append(itemVal);

                    sb.Append("</cell>");
                    LastColumnData = dt.Rows[i][j].ToString();

                    if (dt.TableName == "CapturedForm4DGrid")
                    {
                        LastColumnData = dt.Rows[i][16].ToString();
                    }
                }
                switch (iconID)
                {
                    case 0:
                        break;
                    case 1:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 2:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 3:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        //sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^Approve record^javascript:createVerifyWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        if (LastColumnData == "Approved")
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        }
                        else
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^Approve record^javascript:createVerifyWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        }
                        break;
                    case 4:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/folder_zipper.png^Download file^javascript:DownloadFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 5:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        sb.Append("<cell>../DHTMLX/codebase/imgs/but_cut.gif^Delete record^javascript:DeleteRecord(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");

                        break;
                    case 6:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 7: //blank approval, not allowed to approve                        
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        if (LastColumnData == "Approved")
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        }
                        else
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                        }
                        break;
                    case 8:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/file_xls.png^Preview report^javascript:PreviewXlsFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        //sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 9:
                        sb.Append("<cell></cell>");
                        //sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 10:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/leaf.gif^capture new^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 11:
                        if (LastColumnData == "Approved")
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^Follow case^javascript:followUpWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/dialog_close.png^Close case^javascript:closeWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        }
                        else
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^Approve record^javascript:createVerifyWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                        }
                        break;
                    case 12:
                        if (LastColumnData == "Approved")
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^Follow case^javascript:followUpWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/dialog_close.png^Close case^javascript:closeWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        }
                        else
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                        }

                        break;
                    default:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                }
                sb.Append("</row>");

            }
            sb.Append("</rows>");

            string encodedXml = PrintXML(sb.ToString());

            XmlDocument contentxml = new XmlDocument();
            contentxml.LoadXml(@encodedXml);

            return encodedXml;
        }

        public static string GetDhtmlxXMLForm7(DataTable dt, Int64 RowCount, int Pos, int uniqueIDCol,int iconID, params KeyValuePair<string, object>[] pairs)
        {
            StringBuilder sb = new StringBuilder();
            string HeadStr = string.Empty;
            string TempStr = HeadStr;
            string itemVal;

            bool shouldApprove = true;

            int rowID;
            string LastColumnData = string.Empty;
            sb.Append("<?xml version='1.0' encoding='UTF-8'?>");

            sb.Append("<rows total_count='" + RowCount + "' pos='" + Pos + "'>");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                rowID = i;
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (dt.Rows[i]["CreatedBy"].ToString().ToUpper() == Membership.GetUser().UserName.ToString().ToUpper())
                    {
                        shouldApprove = false;
                    }

                    if (j == 0)
                    {
                        sb.Append("<row id='" + dt.Rows[i][j].ToString() + "'>");
                    }

                    sb.Append("<cell>");
                    itemVal = dt.Rows[i][j].ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
                    if (itemVal == "False") { itemVal = "0"; }

                    sb.Append(itemVal);

                    sb.Append("</cell>");
                    LastColumnData = dt.Rows[i][j].ToString();
                }
                switch (iconID)
                {
                    case 1:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 2:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 3:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        //sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^Approve record^javascript:createVerifyWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        if (LastColumnData == "Approved")
                        {
                            sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                            sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        }
                        else
                        {
                            if (shouldApprove)
                            {
                                sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^Approve record^javascript:createVerifyWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                                sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                            }
                            else
                            {
                                sb.Append("<cell>../DHTMLX/codebase/imgs/blank.gif</cell>");
                                sb.Append("<cell>../DHTMLX/codebase/imgs/file_pdf.png^Preview report^javascript:PreviewPdfFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                            }                           
                        }
                        break;
                    case 4:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/folder_zipper.png^Download file^javascript:DownloadFile(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                    case 5:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        sb.Append("<cell>../DHTMLX/codebase/imgs/but_cut.gif^Delete record^javascript:DeleteRecord(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        
                        break;
                    default:
                        sb.Append("<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit record^javascript:createWindow(" + dt.Rows[rowID][uniqueIDCol].ToString() + ");^_self</cell>");
                        break;
                }
                sb.Append("</row>");

            }
            sb.Append("</rows>");

            string encodedXml = PrintXML(sb.ToString());

            XmlDocument contentxml = new XmlDocument();
            contentxml.LoadXml(@encodedXml);

            return encodedXml;
        }

        public static String PrintXML(String XML)
        {
            String Result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(XML);

                writer.Formatting = System.Xml.Formatting.Indented;// Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                String FormattedXML = sReader.ReadToEnd();

                Result = FormattedXML;
            }
            catch (XmlException)
            {
            }

            mStream.Close();
            writer.Close();

            return Result;
        }

        public static IDatabase NpocoConnection()
        {
            IDatabase db = new Database("conString");
            db.OneTimeCommandTimeout = 3600;
            return db;
        }

        public static int intQueryString(string paramName, int defaultValue)
        {
            int value;
            if (!int.TryParse(HttpContext.Current.Request.QueryString[paramName], out value))
                return defaultValue;
            return value;
        }

        public static string strQueryString(string paramName, string defaultValue)
        {
            if (HttpContext.Current.Request.QueryString[paramName] == null)
            {
                return string.Empty;
            }
            else
            {
                return HttpContext.Current.Request.QueryString[paramName];
            }
        }

        public DefaultWoredaModel FetchLoggedInWoreda()
        {
            DefaultWoredaModel dtls = new DefaultWoredaModel();

            List<DefaultWoredaModel> woredaResult
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<DefaultWoredaModel>(";Exec GetDefaultWoreda");

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<DefaultWoredaModel>(woredaResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.WoredaName = row["WoredaName"].ToString();
                dtls.RegionName = row["RegionName"].ToString();

            }
            return dtls;
        }

        public int GetLocationType()
        {
            var settingsResult = NpocoConnection().Fetch<Settings>(";Exec GetLocationType");

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Settings>(settingsResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                return int.Parse(row["LocationType"].ToString());

            }

            return 1;
        }

        public bool FetchLoggedInWoreda(DefaultWoredaModel newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess = NpocoConnection().Fetch<RecordCount>(";Exec UpdateDefaultWoreda @WoredaID,@CreatedBy",
                     new
                     {
                         WoredaID = newModel.WoredaID,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch(Exception ee)
            {
                errMsg = ee.Message;
                return false;
            }
            return true;
        }

        public bool InsertReportingPeriod(ReportingPeriod newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess = NpocoConnection().Fetch<RecordCount>(";Exec InsertReportingPeriod @PeriodName, @StartDate, @EndDate, @FiscalYear, @CreatedBy",
                     new
                     {
                         PeriodName = newModel.PeriodName,
                         StartDate = newModel.StartDate,
                         EndDate = newModel.EndDate,
                         FiscalYear = newModel.FiscalYear,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                return false;
            }

            return true;
        }

        public bool InsertHouseholdVisit(HouseholdVisit newModel, out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertHouseholdVisit @HouseholdVisitName,@CreatedBy",
                     new
                     {
                         HouseholdVisitName = newModel.HouseholdVisitName,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });
            }
            catch (Exception ee)
            {
                errMsg = ee.Message;
                return false;
            }
            return true;
        }

        public string SerializeDataTable(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        public string SerializeXmlNode(XmlDocument xmlNode)
        {
            var xml = JsonConvert.SerializeXmlNode(xmlNode);
            return xml;
        }

        public static string GetFilterString(string filterType, string rptType, string rawString)
        {
            List<string> SqlFilterList = new List<string>();
            List<string> SqlStringList = new List<string>();
            string SqlString = String.Empty;

            if(rawString.Length==0 || rawString == null)
            {
                return null;
            }

            try
            {
                if (filterType == "AGE")
                {
                    if (rptType == "RPT1" || rptType == "RPT11")
                    {
                        SqlFilterList = rawString.Split(',').ToList();
                        foreach (string f in SqlFilterList)
                        {
                            int s = int.Parse(f);

                            if (s.Equals(1))
                            {
                                SqlStringList.Add(" DateDiff(Year, DateOfBirth, Getdate()) < 1 ");
                            }
                            if (s.Equals(2))
                            {
                                SqlStringList.Add(" (DateDiff(Year, DateOfBirth, Getdate()) >= 1) AND (DateDiff(Year, DateOfBirth, Getdate()) <= 2) ");
                            }
                            if (s.Equals(3))
                            {
                                SqlStringList.Add(" (DateDiff(Year, DateOfBirth, Getdate()) >= 3) AND (DateDiff(Year, DateOfBirth, Getdate()) <= 5) ");
                            }
                            if (s.Equals(4))
                            {
                                SqlStringList.Add(" (DateDiff(Year, DateOfBirth, Getdate()) >= 6) AND (DateDiff(Year, DateOfBirth, Getdate()) <= 12) ");
                            }
                            if (s.Equals(5))
                            {
                                SqlStringList.Add(" (DateDiff(Year, DateOfBirth, Getdate()) >= 13) AND (DateDiff(Year, DateOfBirth, Getdate()) <= 19) ");
                            }
                            if (s.Equals(6))
                            {
                                SqlStringList.Add(" (DateDiff(Year, DateOfBirth, Getdate()) >= 20) AND (DateDiff(Year, DateOfBirth, Getdate()) <= 24) ");
                            }
                            if (s.Equals(7))
                            {
                                SqlStringList.Add(" (DateDiff(Year, DateOfBirth, Getdate()) >= 25) AND (DateDiff(Year, DateOfBirth, Getdate()) <= 49) ");
                            }
                            if (s.Equals(8))
                            {
                                SqlStringList.Add(" (DateDiff(Year, DateOfBirth, Getdate()) >= 46) AND (DateDiff(Year, DateOfBirth, Getdate()) <= 59) ");
                            }
                            if (s.Equals(9))
                            {
                                SqlStringList.Add(" DateDiff(Year, DateOfBirth, Getdate()) > 60 ");
                            }
                        }
                    }

                    if (rptType == "RPT2" || rptType == "RPT12")
                    {
                        SqlFilterList = rawString.Split(',').ToList();
                        foreach (string f in SqlFilterList)
                        {
                            int s = int.Parse(f);

                            if (s.Equals(1))
                            {
                                SqlStringList.Add(" (DateDiff(Year, BabyDateOfBirth, Getdate()) < 1 OR PLWAge < 1) ");
                            }
                            if (s.Equals(2))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, BabyDateOfBirth, Getdate()) >= 1 AND DateDiff(Year, BabyDateOfBirth, Getdate()) <=2) OR (PLWAge>=1 AND PLWAge<=2)) ");
                            }
                            if (s.Equals(3))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, BabyDateOfBirth, Getdate()) >= 3 AND DateDiff(Year, BabyDateOfBirth, Getdate()) <=5) OR (PLWAge>=3 AND PLWAge<=5)) ");
                            }
                            if (s.Equals(4))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, BabyDateOfBirth, Getdate()) >= 6 AND DateDiff(Year, BabyDateOfBirth, Getdate()) <=12) OR (PLWAge>=6 AND PLWAge<=12)) ");
                            }
                            if (s.Equals(5))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, BabyDateOfBirth, Getdate()) >= 13 AND DateDiff(Year, BabyDateOfBirth, Getdate()) <=19) OR (PLWAge>=13 AND PLWAge<=19)) ");
                            }
                            if (s.Equals(6))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, BabyDateOfBirth, Getdate()) >= 20 AND DateDiff(Year, BabyDateOfBirth, Getdate()) <=24) OR (PLWAge>=20 AND PLWAge<=24)) ");
                            }
                            if (s.Equals(7))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, BabyDateOfBirth, Getdate()) >= 25 AND DateDiff(Year, BabyDateOfBirth, Getdate()) <=49) OR (PLWAge>=25 AND PLWAge<=49)) ");
                            }
                            if (s.Equals(8))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, BabyDateOfBirth, Getdate()) >= 46 AND DateDiff(Year, BabyDateOfBirth, Getdate()) <=59) OR (PLWAge>=46 AND PLWAge<=59)) ");
                            }
                            if (s.Equals(9))
                            {
                                SqlStringList.Add(" (DateDiff(Year, BabyDateOfBirth, Getdate()) > 60 OR PLWAge>60) ");
                            }
                        }
                    }

                    if (rptType == "RPT3" || rptType == "RPT13")
                    {
                        SqlFilterList = rawString.Split(',').ToList();
                        foreach (string f in SqlFilterList)
                        {
                            int s = int.Parse(f);

                            if (s.Equals(1))
                            {
                                SqlStringList.Add(" (DateDiff(Year, ChildDateOfBirth, Getdate()) < 1 ) ");
                            }
                            if (s.Equals(2))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, ChildDateOfBirth, Getdate()) >= 1 AND DateDiff(Year, ChildDateOfBirth, Getdate()) <=2)) ");
                            }
                            if (s.Equals(3))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, ChildDateOfBirth, Getdate()) >= 3 AND DateDiff(Year, ChildDateOfBirth, Getdate()) <=5)) ");
                            }
                            if (s.Equals(4))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, ChildDateOfBirth, Getdate()) >= 6 AND DateDiff(Year, ChildDateOfBirth, Getdate()) <=12)) ");
                            }
                            if (s.Equals(5))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, ChildDateOfBirth, Getdate()) >= 13 AND DateDiff(Year, ChildDateOfBirth, Getdate()) <=19)) ");
                            }
                            if (s.Equals(6))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, ChildDateOfBirth, Getdate()) >= 20 AND DateDiff(Year, ChildDateOfBirth, Getdate()) <=24)) ");
                            }
                            if (s.Equals(7))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, ChildDateOfBirth, Getdate()) >= 25 AND DateDiff(Year, ChildDateOfBirth, Getdate()) <=49)) ");
                            }
                            if (s.Equals(8))
                            {
                                SqlStringList.Add(" ((DateDiff(Year, ChildDateOfBirth, Getdate()) >= 46 AND DateDiff(Year, ChildDateOfBirth, Getdate()) <=59)) ");
                            }
                            if (s.Equals(9))
                            {
                                SqlStringList.Add(" (DateDiff(Year, ChildDateOfBirth, Getdate()) > 60 ) ");
                            }
                        }
                    }

                    SqlString = String.Join(" OR ", SqlStringList);
                }
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return null;
            }

            return SqlString;
        }
    }
}
