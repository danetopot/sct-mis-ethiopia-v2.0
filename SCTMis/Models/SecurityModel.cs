﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCTMis.Models
{
    public class RoleList
    {
        public int ColumnID { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
    }

    public class AuditTrailModel
    {
        public int ColumnID { get; set; }
        public string UserName { get; set; }
        public string ScreenName { get; set; }
        public string ComputerName { get; set; }
        public string IpAddress { get; set; }
        public string TransactionDetails { get; set; }
        public string DateCreated { get; set; }
    }

    public class ClientType
    {
        public string ClientTypeID { get; set; }
        public string ClientTypeName { get; set; }
    }

    public class ServiceType
    {
        public string ServiceID { get; set; }
        public string ServiceName { get; set; }
    }

    public class ReportLogModel
    {
        public string Id { get; set; }
        public string ReportName { get; set; }
        public string ReportPeriod { get; set; }
        public string ReportFilePath { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime GeneratedOn { get; set; }
        public bool IsReplicated { get; set; }
        public string ReplicatedBy { get; set; }
        public DateTime ReplicatedOn { get; set; }
    }

    public class ReportType
    {
        public string ReportID { get; set; }
        public string ReportName { get; set; }
    }

    public class ReportDisaggregation
    {
        public string DisaggregationID { get; set; }
        public string DisaggregationName { get; set; }
    }

    public class ReportAgeRange
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class ReportLevel
    {
        public string LevelID { get; set; }
        public string LevelName { get; set; }
    }

    public class ReportRegion
    {
        public string RegionID { get; set; }
        public string RegionName { get; set; }
    }

    public class ReportWoreda
    {
        public string WoredaID { get; set; }
        public string RegionID { get; set; }
        public string WoredaName { get; set; }
    }

    public class ReportKebele
    {
        public string KebeleID { get; set; }
        public string WoredaID { get; set; }
        public string KebeleName { get; set; }
    }

    public class ReportGote
    {
        public string KebeleID { get; set; }
        public string GoteName { get; set; }
    }

    public class HouseholdProfilePDSDetailReport
    {
        public int UniqueID { get; set; }
        //public string ProfileDSDetailID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        //public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string HouseholdName { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string ClientName { get; set; }
        public string IndividualID { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ApprovedBy { get; set; }
        //public DateTime ApprovedOn { get; set; }
        //public List<HouseholdProfilePDSDetailReport> PdsDetailList { get; set; }
    }

    public class HouseholdProfilePDSSummaryReport
    {
        public int UniqueID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        //public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountOfHouseholds { get; set; }
        public int CountOfHouseholdMembers { get; set; }
        public int CountEnrolledInCBHI { get; set; }
        public int CountOfChildProtectionCases { get; set; }
        public int CountNonMalnuorished { get; set; }
        public int CountMalnourished { get; set; }
        public int CountSeverelyMalnourished { get; set; }
        public int CountHandicapped { get; set; }
        public int CountChronicallyIll { get; set; }
        public int CountLactating { get; set; }
        public int CountPregnant { get; set; }
        public int CountEnrolledInSchool { get; set; }
        public int CountChildUnderTSForCMAM { get; set; }
    }

    public class HouseholdProfilePDSDisaggregatedSummaryReport
    {
        public int UniqueID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public int Households { get; set; }
        public int HouseholdMembers { get; set; }
        public int HouseholdMembersMale { get; set; }
        public int HouseholdMembersFemale { get; set; }
        public int HouseholdMembersUnder1Male { get; set; }
        public int HouseholdMembers1To2Male { get; set; }
        public int HouseholdMembers3To5Male { get; set; }
        public int HouseholdMembers6To12Male { get; set; }
        public int HouseholdMembers13To19Male { get; set; }
        public int HouseholdMembers20To24Male { get; set; }
        public int HouseholdMembers25To49Male { get; set; }
        public int HouseholdMembers46To59Male { get; set; }
        public int HouseholdMembersOver60Male { get; set; }
        public int HouseholdMembersUnder1Female { get; set; }
        public int HouseholdMembers1To2Female { get; set; }
        public int HouseholdMembers3To5Female { get; set; }
        public int HouseholdMembers6To12Female { get; set; }
        public int HouseholdMembers13To19Female { get; set; }
        public int HouseholdMembers20To24Female { get; set; }
        public int HouseholdMembers25To49Female { get; set; }
        public int HouseholdMembers46To59Female { get; set; }
        public int HouseholdMembersOver60Female { get; set; }
        public int EnrolledInCBHIUnder1Male { get; set; }
        public int EnrolledInCBHI1To2Male { get; set; }
        public int EnrolledInCBHI3To5Male { get; set; }
        public int EnrolledInCBHI6To12Male { get; set; }
        public int EnrolledInCBHI13To19Male { get; set; }
        public int EnrolledInCBHI20To24Male { get; set; }
        public int EnrolledInCBHI25To49Male { get; set; }
        public int EnrolledInCBHI46To59Male { get; set; }
        public int EnrolledInCBHIOver60Male { get; set; }
        public int EnrolledInCBHIUnder1Female { get; set; }
        public int EnrolledInCBHI1To2Female { get; set; }
        public int EnrolledInCBHI3To5Female { get; set; }
        public int EnrolledInCBHI6To12Female { get; set; }
        public int EnrolledInCBHI13To19Female { get; set; }
        public int EnrolledInCBHI20To24Female { get; set; }
        public int EnrolledInCBHI25To49Female { get; set; }
        public int EnrolledInCBHI46To59Female { get; set; }
        public int EnrolledInCBHIOver60Female { get; set; }
        public int ChildProtectionCasesUnder1Male { get; set; }
        public int ChildProtectionCases1To2Male { get; set; }
        public int ChildProtectionCases3To5Male { get; set; }
        public int ChildProtectionCases6To12Male { get; set; }
        public int ChildProtectionCases13To19Male { get; set; }
        public int ChildProtectionCases20To24Male { get; set; }
        public int ChildProtectionCases25To49Male { get; set; }
        public int ChildProtectionCases46To59Male { get; set; }
        public int ChildProtectionCasesOver60Male { get; set; }
        public int ChildProtectionCasesUnder1Female { get; set; }
        public int ChildProtectionCases1To2Female { get; set; }
        public int ChildProtectionCases3To5Female { get; set; }
        public int ChildProtectionCases6To12Female { get; set; }
        public int ChildProtectionCases13To19Female { get; set; }
        public int ChildProtectionCases20To24Female { get; set; }
        public int ChildProtectionCases25To49Female { get; set; }
        public int ChildProtectionCases46To59Female { get; set; }
        public int ChildProtectionCasesOver60Female { get; set; }
        public int NonMalnuorishedUnder1Male { get; set; }
        public int NonMalnuorished1To2Male { get; set; }
        public int NonMalnuorished3To5Male { get; set; }
        public int NonMalnuorished6To12Male { get; set; }
        public int NonMalnuorished13To19Male { get; set; }
        public int NonMalnuorished20To24Male { get; set; }
        public int NonMalnuorished25To49Male { get; set; }
        public int NonMalnuorished46To59Male { get; set; }
        public int NonMalnuorishedOver60Male { get; set; }
        public int NonMalnuorishedUnder1Female { get; set; }
        public int NonMalnuorished1To2Female { get; set; }
        public int NonMalnuorished3To5Female { get; set; }
        public int NonMalnuorished6To12Female { get; set; }
        public int NonMalnuorished13To19Female { get; set; }
        public int NonMalnuorished20To24Female { get; set; }
        public int NonMalnuorished25To49Female { get; set; }
        public int NonMalnuorished46To59Female { get; set; }
        public int NonMalnuorishedOver60Female { get; set; }
        public int MalnuorishedUnder1Male { get; set; }
        public int Malnuorished1To2Male { get; set; }
        public int Malnuorished3To5Male { get; set; }
        public int Malnuorished6To12Male { get; set; }
        public int Malnuorished13To19Male { get; set; }
        public int Malnuorished20To24Male { get; set; }
        public int Malnuorished25To49Male { get; set; }
        public int Malnuorished46To59Male { get; set; }
        public int MalnuorishedOver60Male { get; set; }
        public int MalnuorishedUnder1Female { get; set; }
        public int Malnuorished1To2Female { get; set; }
        public int Malnuorished3To5Female { get; set; }
        public int Malnuorished6To12Female { get; set; }
        public int Malnuorished13To19Female { get; set; }
        public int Malnuorished20To24Female { get; set; }
        public int Malnuorished25To49Female { get; set; }
        public int Malnuorished46To59Female { get; set; }
        public int MalnuorishedOver60Female { get; set; }
        public int SeverelyMalnourishedUnder1Male { get; set; }
        public int SeverelyMalnourished1To2Male { get; set; }
        public int SeverelyMalnourished3To5Male { get; set; }
        public int SeverelyMalnourished6To12Male { get; set; }
        public int SeverelyMalnourished13To19Male { get; set; }
        public int SeverelyMalnourished20To24Male { get; set; }
        public int SeverelyMalnourished25To49Male { get; set; }
        public int SeverelyMalnourished46To59Male { get; set; }
        public int SeverelyMalnourishedOver60Male { get; set; }
        public int SeverelyMalnourishedUnder1Female { get; set; }
        public int SeverelyMalnourished1To2Female { get; set; }
        public int SeverelyMalnourished3To5Female { get; set; }
        public int SeverelyMalnourished6To12Female { get; set; }
        public int SeverelyMalnourished13To19Female { get; set; }
        public int SeverelyMalnourished20To24Female { get; set; }
        public int SeverelyMalnourished25To49Female { get; set; }
        public int SeverelyMalnourished46To59Female { get; set; }
        public int SeverelyMalnourishedOver60Female { get; set; }
        public int  HandicappedUnder1Male { get; set; }
        public int  Handicapped1To2Male { get; set; }
        public int  Handicapped3To5Male { get; set; }
        public int  Handicapped6To12Male { get; set; }
        public int  Handicapped13To19Male { get; set; }
        public int  Handicapped20To24Male { get; set; }
        public int  Handicapped25To49Male { get; set; }
        public int  Handicapped46To59Male { get; set; }
        public int  HandicappedOver60Male { get; set; }
        public int  HandicappedUnder1Female { get; set; }
        public int  Handicapped1To2Female { get; set; }
        public int  Handicapped3To5Female { get; set; }
        public int  Handicapped6To12Female { get; set; }
        public int  Handicapped13To19Female { get; set; }
        public int  Handicapped20To24Female { get; set; }
        public int  Handicapped25To49Female { get; set; }
        public int  Handicapped46To59Female { get; set; }
        public int  HandicappedOver60Female { get; set; }
        public int ChronicallyIllUnder1Male { get; set; }
        public int ChronicallyIll1To2Male { get; set; }
        public int ChronicallyIll3To5Male { get; set; }
        public int ChronicallyIll6To12Male { get; set; }
        public int ChronicallyIll13To19Male { get; set; }
        public int ChronicallyIll20To24Male { get; set; }
        public int ChronicallyIll25To49Male { get; set; }
        public int ChronicallyIll46To59Male { get; set; }
        public int ChronicallyIllOver60Male { get; set; }
        public int ChronicallyIllUnder1Female { get; set; }
        public int ChronicallyIll1To2Female { get; set; }
        public int ChronicallyIll3To5Female { get; set; }
        public int ChronicallyIll6To12Female { get; set; }
        public int ChronicallyIll13To19Female { get; set; }
        public int ChronicallyIll20To24Female { get; set; }
        public int ChronicallyIll25To49Female { get; set; }
        public int ChronicallyIll46To59Female { get; set; }
        public int ChronicallyIllOver60Female { get; set; }
        public int LactatingUnder1Male { get; set; }
        public int Lactating1To2Male { get; set; }
        public int Lactating3To5Male { get; set; }
        public int Lactating6To12Male { get; set; }
        public int Lactating13To19Male { get; set; }
        public int Lactating20To24Male { get; set; }
        public int Lactating25To49Male { get; set; }
        public int Lactating46To59Male { get; set; }
        public int LactatingOver60Male { get; set; }
        public int LactatingUnder1Female { get; set; }
        public int Lactating1To2Female { get; set; }
        public int Lactating3To5Female { get; set; }
        public int Lactating6To12Female { get; set; }
        public int Lactating13To19Female { get; set; }
        public int Lactating20To24Female { get; set; }
        public int Lactating25To49Female { get; set; }
        public int Lactating46To59Female { get; set; }
        public int LactatingOver60Female { get; set; }
        public int PregnantUnder1Male { get; set; }
        public int Pregnant1To2Male { get; set; }
        public int Pregnant3To5Male { get; set; }
        public int Pregnant6To12Male { get; set; }
        public int Pregnant13To19Male { get; set; }
        public int Pregnant20To24Male { get; set; }
        public int Pregnant25To49Male { get; set; }
        public int Pregnant46To59Male { get; set; }
        public int PregnantOver60Male { get; set; }
        public int PregnantUnder1Female { get; set; }
        public int Pregnant1To2Female { get; set; }
        public int Pregnant3To5Female { get; set; }
        public int Pregnant6To12Female { get; set; }
        public int Pregnant13To19Female { get; set; }
        public int Pregnant20To24Female { get; set; }
        public int Pregnant25To49Female { get; set; }
        public int Pregnant46To59Female { get; set; }
        public int PregnantOver60Female { get; set; }
        public int EnrolledInSchoolUnder1Male { get; set; }
        public int EnrolledInSchool1To2Male { get; set; }
        public int EnrolledInSchool3To5Male { get; set; }
        public int EnrolledInSchool6To12Male { get; set; }
        public int EnrolledInSchool13To19Male { get; set; }
        public int EnrolledInSchool20To24Male { get; set; }
        public int EnrolledInSchool25To49Male { get; set; }
        public int EnrolledInSchool46To59Male { get; set; }
        public int EnrolledInSchoolOver60Male { get; set; }
        public int EnrolledInSchoolUnder1Female { get; set; }
        public int EnrolledInSchool1To2Female { get; set; }
        public int EnrolledInSchool3To5Female { get; set; }
        public int EnrolledInSchool6To12Female { get; set; }
        public int EnrolledInSchool13To19Female { get; set; }
        public int EnrolledInSchool20To24Female { get; set; }
        public int EnrolledInSchool25To49Female { get; set; }
        public int EnrolledInSchool46To59Female { get; set; }
        public int EnrolledInSchoolOver60Female { get; set; }
        public int ChildUnderTSForCMAMUnder1Male { get; set; }
        public int ChildUnderTSForCMAM1To2Male { get; set; }
        public int ChildUnderTSForCMAM3To5Male { get; set; }
        public int ChildUnderTSForCMAM6To12Male { get; set; }
        public int ChildUnderTSForCMAM13To19Male { get; set; }
        public int ChildUnderTSForCMAM20To24Male { get; set; }
        public int ChildUnderTSForCMAM25To49Male { get; set; }
        public int ChildUnderTSForCMAM46To59Male { get; set; }
        public int ChildUnderTSForCMAMOver60Male { get; set; }
        public int ChildUnderTSForCMAMUnder1Female { get; set; }
        public int ChildUnderTSForCMAM1To2Female { get; set; }
        public int ChildUnderTSForCMAM3To5Female { get; set; }
        public int ChildUnderTSForCMAM6To12Female { get; set; }
        public int ChildUnderTSForCMAM13To19Female { get; set; }
        public int ChildUnderTSForCMAM20To24Female { get; set; }
        public int ChildUnderTSForCMAM25To49Female { get; set; }
        public int ChildUnderTSForCMAM46To59Female { get; set; }
        public int ChildUnderTSForCMAMOver60Female { get; set; }
    }

    public class HouseholdProfileTDSPLWDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string ProfileTDSPLWDetailID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string NameOfPLW { get; set; }
        public string MedicalRecordNumber { get; set; }
        public string PLWAge { get; set; }
        public string StartDateTDS { get; set; }
        public string EndDateTDS { get; set; }
        public string BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApprovedOn { get; set; }
    }

    public class HouseholdProfileTDSPLWSummaryReport
    {
        public int UniqueID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        //public int KebeleID { get; set; }
        public int CountOfHouseholds { get; set; }
        public int CountOfHouseholdMembers { get; set; }
        public int CountEnrolledInCBHI { get; set; }
        public int CountOfChildProtectionCases { get; set; }
        public int CountNonMalnuorishedPLW { get; set; }
        public int CountMalnourishedPLW { get; set; }
        public int CountSeverelyMalnourishedPLW { get; set; }
        public int CountNonMalnuorishedInfant { get; set; }
        public int CountMalnourishedInfant { get; set; }
        public int CountSeverelyMalnourishedInfant { get; set; }
    }

    public class HouseholdProfilePLWDisaggregatedSummaryReport
    {
        public int UniqueID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public int Households { get; set; }
        public int HouseholdMembers { get; set; }
        public int HouseholdMembersMale { get; set; }
        public int HouseholdMembersFemale { get; set; }
        public int HouseholdMembersUnder1Male { get; set; }
        public int HouseholdMembers1To2Male { get; set; }
        public int HouseholdMembers3To5Male { get; set; }
        public int HouseholdMembers6To12Male { get; set; }
        public int HouseholdMembers13To19Male { get; set; }
        public int HouseholdMembers20To24Male { get; set; }
        public int HouseholdMembers25To49Male { get; set; }
        public int HouseholdMembers46To59Male { get; set; }
        public int HouseholdMembersOver60Male { get; set; }
        public int HouseholdMembersUnder1Female { get; set; }
        public int HouseholdMembers1To2Female { get; set; }
        public int HouseholdMembers3To5Female { get; set; }
        public int HouseholdMembers6To12Female { get; set; }
        public int HouseholdMembers13To19Female { get; set; }
        public int HouseholdMembers20To24Female { get; set; }
        public int HouseholdMembers25To49Female { get; set; }
        public int HouseholdMembers46To59Female { get; set; }
        public int HouseholdMembersOver60Female { get; set; }
        public int EnrolledInCBHIUnder1Male { get; set; }
        public int EnrolledInCBHI1To2Male { get; set; }
        public int EnrolledInCBHI3To5Male { get; set; }
        public int EnrolledInCBHI6To12Male { get; set; }
        public int EnrolledInCBHI13To19Male { get; set; }
        public int EnrolledInCBHI20To24Male { get; set; }
        public int EnrolledInCBHI25To49Male { get; set; }
        public int EnrolledInCBHI46To59Male { get; set; }
        public int EnrolledInCBHIOver60Male { get; set; }
        public int EnrolledInCBHIUnder1Female { get; set; }
        public int EnrolledInCBHI1To2Female { get; set; }
        public int EnrolledInCBHI3To5Female { get; set; }
        public int EnrolledInCBHI6To12Female { get; set; }
        public int EnrolledInCBHI13To19Female { get; set; }
        public int EnrolledInCBHI20To24Female { get; set; }
        public int EnrolledInCBHI25To49Female { get; set; }
        public int EnrolledInCBHI46To59Female { get; set; }
        public int EnrolledInCBHIOver60Female { get; set; }
        public int ChildProtectionCasesUnder1Male { get; set; }
        public int ChildProtectionCases1To2Male { get; set; }
        public int ChildProtectionCases3To5Male { get; set; }
        public int ChildProtectionCases6To12Male { get; set; }
        public int ChildProtectionCases13To19Male { get; set; }
        public int ChildProtectionCases20To24Male { get; set; }
        public int ChildProtectionCases25To49Male { get; set; }
        public int ChildProtectionCases46To59Male { get; set; }
        public int ChildProtectionCasesOver60Male { get; set; }
        public int ChildProtectionCasesUnder1Female { get; set; }
        public int ChildProtectionCases1To2Female { get; set; }
        public int ChildProtectionCases3To5Female { get; set; }
        public int ChildProtectionCases6To12Female { get; set; }
        public int ChildProtectionCases13To19Female { get; set; }
        public int ChildProtectionCases20To24Female { get; set; }
        public int ChildProtectionCases25To49Female { get; set; }
        public int ChildProtectionCases46To59Female { get; set; }
        public int ChildProtectionCasesOver60Female { get; set; }
        public int NonMalnourishedPLWUnder1Male { get; set; }
        public int NonMalnourishedPLW1To2Male { get; set; }
        public int NonMalnourishedPLW3To5Male { get; set; }
        public int NonMalnourishedPLW6To12Male { get; set; }
        public int NonMalnourishedPLW13To19Male { get; set; }
        public int NonMalnourishedPLW20To24Male { get; set; }
        public int NonMalnourishedPLW25To49Male { get; set; }
        public int NonMalnourishedPLW46To59Male { get; set; }
        public int NonMalnourishedPLWOver60Male { get; set; }
        public int NonMalnourishedPLWUnder1Female { get; set; }
        public int NonMalnourishedPLW1To2Female { get; set; }
        public int NonMalnourishedPLW3To5Female { get; set; }
        public int NonMalnourishedPLW6To12Female { get; set; }
        public int NonMalnourishedPLW13To19Female { get; set; }
        public int NonMalnourishedPLW20To24Female { get; set; }
        public int NonMalnourishedPLW25To49Female { get; set; }
        public int NonMalnourishedPLW46To59Female { get; set; }
        public int NonMalnourishedPLWOver60Female { get; set; }
        public int MalnourishedPLWUnder1Male { get; set; }
        public int MalnourishedPLW1To2Male { get; set; }
        public int MalnourishedPLW3To5Male { get; set; }
        public int MalnourishedPLW6To12Male { get; set; }
        public int MalnourishedPLW13To19Male { get; set; }
        public int MalnourishedPLW20To24Male { get; set; }
        public int MalnourishedPLW25To49Male { get; set; }
        public int MalnourishedPLW46To59Male { get; set; }
        public int MalnourishedPLWOver60Male { get; set; }
        public int MalnourishedPLWUnder1Female { get; set; }
        public int MalnourishedPLW1To2Female { get; set; }
        public int MalnourishedPLW3To5Female { get; set; }
        public int MalnourishedPLW6To12Female { get; set; }
        public int MalnourishedPLW13To19Female { get; set; }
        public int MalnourishedPLW20To24Female { get; set; }
        public int MalnourishedPLW25To49Female { get; set; }
        public int MalnourishedPLW46To59Female { get; set; }
        public int MalnourishedPLWOver60Female { get; set; }
        public int SeverelyMalnourishedPLWUnder1Male { get; set; }
        public int SeverelyMalnourishedPLW1To2Male { get; set; }
        public int SeverelyMalnourishedPLW3To5Male { get; set; }
        public int SeverelyMalnourishedPLW6To12Male { get; set; }
        public int SeverelyMalnourishedPLW13To19Male { get; set; }
        public int SeverelyMalnourishedPLW20To24Male { get; set; }
        public int SeverelyMalnourishedPLW25To49Male { get; set; }
        public int SeverelyMalnourishedPLW46To59Male { get; set; }
        public int SeverelyMalnourishedPLWOver60Male { get; set; }
        public int SeverelyMalnourishedPLWUnder1Female { get; set; }
        public int SeverelyMalnourishedPLW1To2Female { get; set; }
        public int SeverelyMalnourishedPLW3To5Female { get; set; }
        public int SeverelyMalnourishedPLW6To12Female { get; set; }
        public int SeverelyMalnourishedPLW13To19Female { get; set; }
        public int SeverelyMalnourishedPLW20To24Female { get; set; }
        public int SeverelyMalnourishedPLW25To49Female { get; set; }
        public int SeverelyMalnourishedPLW46To59Female { get; set; }
        public int SeverelyMalnourishedPLWOver60Female { get; set; }
        public int NonMalnourishedInfantUnder1Male { get; set; }
        public int NonMalnourishedInfant1To2Male { get; set; }
        public int NonMalnourishedInfant3To5Male { get; set; }
        public int NonMalnourishedInfant6To12Male { get; set; }
        public int NonMalnourishedInfant13To19Male { get; set; }
        public int NonMalnourishedInfant20To24Male { get; set; }
        public int NonMalnourishedInfant25To49Male { get; set; }
        public int NonMalnourishedInfant46To59Male { get; set; }
        public int NonMalnourishedInfantOver60Male { get; set; }
        public int NonMalnourishedInfantUnder1Female { get; set; }
        public int NonMalnourishedInfant1To2Female { get; set; }
        public int NonMalnourishedInfant3To5Female { get; set; }
        public int NonMalnourishedInfant6To12Female { get; set; }
        public int NonMalnourishedInfant13To19Female { get; set; }
        public int NonMalnourishedInfant20To24Female { get; set; }
        public int NonMalnourishedInfant25To49Female { get; set; }
        public int NonMalnourishedInfant46To59Female { get; set; }
        public int NonMalnourishedInfantOver60Female { get; set; }
        public int MalnourishedInfantUnder1Male { get; set; }
        public int MalnourishedInfant1To2Male { get; set; }
        public int MalnourishedInfant3To5Male { get; set; }
        public int MalnourishedInfant6To12Male { get; set; }
        public int MalnourishedInfant13To19Male { get; set; }
        public int MalnourishedInfant20To24Male { get; set; }
        public int MalnourishedInfant25To49Male { get; set; }
        public int MalnourishedInfant46To59Male { get; set; }
        public int MalnourishedInfantOver60Male { get; set; }
        public int MalnourishedInfantUnder1Female { get; set; }
        public int MalnourishedInfant1To2Female { get; set; }
        public int MalnourishedInfant3To5Female { get; set; }
        public int MalnourishedInfant6To12Female { get; set; }
        public int MalnourishedInfant13To19Female { get; set; }
        public int MalnourishedInfant20To24Female { get; set; }
        public int MalnourishedInfant25To49Female { get; set; }
        public int MalnourishedInfant46To59Female { get; set; }
        public int MalnourishedInfantOver60Female { get; set; }
        public int SeverelyMalnourishedInfantUnder1Male { get; set; }
        public int SeverelyMalnourishedInfant1To2Male { get; set; }
        public int SeverelyMalnourishedInfant3To5Male { get; set; }
        public int SeverelyMalnourishedInfant6To12Male { get; set; }
        public int SeverelyMalnourishedInfant13To19Male { get; set; }
        public int SeverelyMalnourishedInfant20To24Male { get; set; }
        public int SeverelyMalnourishedInfant25To49Male { get; set; }
        public int SeverelyMalnourishedInfant46To59Male { get; set; }
        public int SeverelyMalnourishedInfantOver60Male { get; set; }
        public int SeverelyMalnourishedInfantUnder1Female { get; set; }
        public int SeverelyMalnourishedInfant1To2Female { get; set; }
        public int SeverelyMalnourishedInfant3To5Female { get; set; }
        public int SeverelyMalnourishedInfant6To12Female { get; set; }
        public int SeverelyMalnourishedInfant13To19Female { get; set; }
        public int SeverelyMalnourishedInfant20To24Female { get; set; }
        public int SeverelyMalnourishedInfant25To49Female { get; set; }
        public int SeverelyMalnourishedInfant46To59Female { get; set; }
        public int SeverelyMalnourishedInfantOver60Female { get; set; }
    }

    public class HouseholdProfileTDSCMCDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        //public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string CaretakerID { get; set; }
        public string NameOfCareTaker { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string MalnourishedChildName { get; set; }
        public string MalnourishedChildSex { get; set; }
        public string ChildID { get; set; }
        public string ChildDateOfBirth { get; set; }
        public string DateTypeCertificate { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string StartDateTDS { get; set; }
        public string EndDateTDS { get; set; }
        public string NextCNStatusDate { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string CCCCBSPCMember { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ApprovedBy { get; set; }
        //public DateTime ApprovedOn { get; set; }
    }

    public class HouseholdProfileTDSCMCSummaryReport
    {
        public int UniqueID { get; set; }
        //public string ProfileTDSCMCID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        //public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountOfHouseholds { get; set; }
        public int CountOfHouseholdMembers { get; set; }
        public int CountEnrolledInCBHI { get; set; }
        public int CountOfChildProtectionCases { get; set; }
        public int CountNonMalnuorishedChildren { get; set; }
        public int CountMalnourishedChildren { get; set; }
        public int CountSeverelyMalnourishedChildren { get; set; }
    }

    public class HouseholdProfileCMCDisaggregatedSummaryReport
    {
        public int UniqueID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public int Households { get; set; }
        public int HouseholdMembers { get; set; }
        public int HouseholdMembersMale { get; set; }
        public int HouseholdMembersFemale { get; set; }
        public int HouseholdMembersUnder1Male { get; set; }
        public int HouseholdMembers1To2Male { get; set; }
        public int HouseholdMembers3To5Male { get; set; }
        public int HouseholdMembers6To12Male { get; set; }
        public int HouseholdMembers13To19Male { get; set; }
        public int HouseholdMembers20To24Male { get; set; }
        public int HouseholdMembers25To49Male { get; set; }
        public int HouseholdMembers46To59Male { get; set; }
        public int HouseholdMembersOver60Male { get; set; }
        public int HouseholdMembersUnder1Female { get; set; }
        public int HouseholdMembers1To2Female { get; set; }
        public int HouseholdMembers3To5Female { get; set; }
        public int HouseholdMembers6To12Female { get; set; }
        public int HouseholdMembers13To19Female { get; set; }
        public int HouseholdMembers20To24Female { get; set; }
        public int HouseholdMembers25To49Female { get; set; }
        public int HouseholdMembers46To59Female { get; set; }
        public int HouseholdMembersOver60Female { get; set; }
        public int EnrolledInCBHIUnder1Male { get; set; }
        public int EnrolledInCBHI1To2Male { get; set; }
        public int EnrolledInCBHI3To5Male { get; set; }
        public int EnrolledInCBHI6To12Male { get; set; }
        public int EnrolledInCBHI13To19Male { get; set; }
        public int EnrolledInCBHI20To24Male { get; set; }
        public int EnrolledInCBHI25To49Male { get; set; }
        public int EnrolledInCBHI46To59Male { get; set; }
        public int EnrolledInCBHIOver60Male { get; set; }
        public int EnrolledInCBHIUnder1Female { get; set; }
        public int EnrolledInCBHI1To2Female { get; set; }
        public int EnrolledInCBHI3To5Female { get; set; }
        public int EnrolledInCBHI6To12Female { get; set; }
        public int EnrolledInCBHI13To19Female { get; set; }
        public int EnrolledInCBHI20To24Female { get; set; }
        public int EnrolledInCBHI25To49Female { get; set; }
        public int EnrolledInCBHI46To59Female { get; set; }
        public int EnrolledInCBHIOver60Female { get; set; }
        public int ChildProtectionCasesUnder1Male { get; set; }
        public int ChildProtectionCases1To2Male { get; set; }
        public int ChildProtectionCases3To5Male { get; set; }
        public int ChildProtectionCases6To12Male { get; set; }
        public int ChildProtectionCases13To19Male { get; set; }
        public int ChildProtectionCases20To24Male { get; set; }
        public int ChildProtectionCases25To49Male { get; set; }
        public int ChildProtectionCases46To59Male { get; set; }
        public int ChildProtectionCasesOver60Male { get; set; }
        public int ChildProtectionCasesUnder1Female { get; set; }
        public int ChildProtectionCases1To2Female { get; set; }
        public int ChildProtectionCases3To5Female { get; set; }
        public int ChildProtectionCases6To12Female { get; set; }
        public int ChildProtectionCases13To19Female { get; set; }
        public int ChildProtectionCases20To24Female { get; set; }
        public int ChildProtectionCases25To49Female { get; set; }
        public int ChildProtectionCases46To59Female { get; set; }
        public int ChildProtectionCasesOver60Female { get; set; }
        public int NonMalnourishedInfantUnder1Male { get; set; }
        public int NonMalnourishedInfant1To2Male { get; set; }
        public int NonMalnourishedInfant3To5Male { get; set; }
        public int NonMalnourishedInfant6To12Male { get; set; }
        public int NonMalnourishedInfant13To19Male { get; set; }
        public int NonMalnourishedInfant20To24Male { get; set; }
        public int NonMalnourishedInfant25To49Male { get; set; }
        public int NonMalnourishedInfant46To59Male { get; set; }
        public int NonMalnourishedInfantOver60Male { get; set; }
        public int NonMalnourishedInfantUnder1Female { get; set; }
        public int NonMalnourishedInfant1To2Female { get; set; }
        public int NonMalnourishedInfant3To5Female { get; set; }
        public int NonMalnourishedInfant6To12Female { get; set; }
        public int NonMalnourishedInfant13To19Female { get; set; }
        public int NonMalnourishedInfant20To24Female { get; set; }
        public int NonMalnourishedInfant25To49Female { get; set; }
        public int NonMalnourishedInfant46To59Female { get; set; }
        public int NonMalnourishedInfantOver60Female { get; set; }
        public int MalnourishedInfantUnder1Male { get; set; }
        public int MalnourishedInfant1To2Male { get; set; }
        public int MalnourishedInfant3To5Male { get; set; }
        public int MalnourishedInfant6To12Male { get; set; }
        public int MalnourishedInfant13To19Male { get; set; }
        public int MalnourishedInfant20To24Male { get; set; }
        public int MalnourishedInfant25To49Male { get; set; }
        public int MalnourishedInfant46To59Male { get; set; }
        public int MalnourishedInfantOver60Male { get; set; }
        public int MalnourishedInfantUnder1Female { get; set; }
        public int MalnourishedInfant1To2Female { get; set; }
        public int MalnourishedInfant3To5Female { get; set; }
        public int MalnourishedInfant6To12Female { get; set; }
        public int MalnourishedInfant13To19Female { get; set; }
        public int MalnourishedInfant20To24Female { get; set; }
        public int MalnourishedInfant25To49Female { get; set; }
        public int MalnourishedInfant46To59Female { get; set; }
        public int MalnourishedInfantOver60Female { get; set; }
        public int SeverelyMalnourishedInfantUnder1Male { get; set; }
        public int SeverelyMalnourishedInfant1To2Male { get; set; }
        public int SeverelyMalnourishedInfant3To5Male { get; set; }
        public int SeverelyMalnourishedInfant6To12Male { get; set; }
        public int SeverelyMalnourishedInfant13To19Male { get; set; }
        public int SeverelyMalnourishedInfant20To24Male { get; set; }
        public int SeverelyMalnourishedInfant25To49Male { get; set; }
        public int SeverelyMalnourishedInfant46To59Male { get; set; }
        public int SeverelyMalnourishedInfantOver60Male { get; set; }
        public int SeverelyMalnourishedInfantUnder1Female { get; set; }
        public int SeverelyMalnourishedInfant1To2Female { get; set; }
        public int SeverelyMalnourishedInfant3To5Female { get; set; }
        public int SeverelyMalnourishedInfant6To12Female { get; set; }
        public int SeverelyMalnourishedInfant13To19Female { get; set; }
        public int SeverelyMalnourishedInfant20To24Female { get; set; }
        public int SeverelyMalnourishedInfant25To49Female { get; set; }
        public int SeverelyMalnourishedInfant46To59Female { get; set; }
        public int SeverelyMalnourishedInfantOver60Female { get; set; }
    }

    public class SocialServicesNeedsDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string IndividualID { get; set; }
        public string ClientType { get; set; }
        public string NameOfHouseHoldMember { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string PreAnteNatalCare { get; set; }
        public string Immunization { get; set; }
        public string SupplementaryFeeding { get; set; }
        public string MonthlyGMPSessions { get; set; }
        public string EnrolmentAttendance { get; set; }
        public string BCCSessions { get; set; }
        public string UnderTSF { get; set; }
        public string BirthRegistration { get; set; }
        public string HealthPostCheckUp { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string PreNatalVisit1 { get; set; }
        public string PreNatalVisit3 { get; set; }
        public string PostNatalVisit { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class SocialServicesNeedsSummaryReport
    {
        public string UniqueID { get; set; }
        //public string ProfileHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountOfHouseholds { get; set; }
        public int CountOfHouseholdMembers { get; set; }
        public int CountPreAnteNatalCare { get; set; }
        public int CountImmunization { get; set; }
        public int CountSupplementaryFeeding { get; set; }
        public int CountMonthlyGMPSessions { get; set; }
        public int CountEnrolmentAttendance { get; set; }
        public int CountBCCSessions { get; set; }
        public int CountUnderTSF { get; set; }
        public int CountBirthRegistration { get; set; }
        public int CountCBHIMembership { get; set; }
        public int CountChildProtectionRisk { get; set; }
        public int CountPreNatalVisit1 { get; set; }
        public int CountPreNatalVisit3 { get; set; }
        public int CountPostNatalVisit { get; set; }
        //public DateTime CollectionDate { get; set; }
        //public string SocialWorker { get; set; }
    }

    public class ServiceComplianceDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        //public string ProfileDSDetailID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }

        public string Gote { get; set; }
        public string ClientName { get; set; }
        public string ClientType { get; set; }
        public string IndividualID { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string Complied { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class ServiceComplianceSummaryReport
    {
        public int UniqueID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        //public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountOfHouseholds { get; set; }
        public int CountOfHouseholdMembers { get; set; }
        public int CountAntePostNatalCare { get; set; }
        public int CountImmunization { get; set; }
        public int CountTSFCMAM { get; set; }
        public int CountGMPSessions { get; set; }
        public int CountSchoolEnrolment { get; set; }
        public int CountBCCSessions { get; set; }
        public int CountBirthRegistration { get; set; }
        public int Count3PrenatalCareVisits { get; set; }
        public int Count1PrenatalCareVisits { get; set; }
        public int CountCheckUpOfChildAtHealthPost { get; set; }
        public int CountSupplementaryFeeding { get; set; }
        public int CountCBHIMembership { get; set; }
        public int CountChildProtection { get; set; }
    }

    public class CaseManagementComplianceDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }

        public string IndividualID { get; set; }
        public string ClientName { get; set; }
        public string ClientType { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string ServiceName { get; set; }
        /*
        public int AntePostNatal { get; set; }
        public int Immunization { get; set; }
        public int UnderTSFCMAM { get; set; }
        public int GMPSessions { get; set; }
        public int SchoolEnrolment { get; set; }
        public int BCCSessions { get; set; }
        public int BirthRegistration { get; set; }
        public int PrenatalCareVisits3 { get; set; }
        public int PrenatalCareVisits1 { get; set; }
        public int CheckuUpTwiceaMonthAtHealthPost { get; set; }
        public int SupplementaryFeeding { get; set; }
        public int CBHIMembership { get; set; }
        public int ChildProtection { get; set; }
        //public DateTime CollectionDate { get; set; }
        //public string SocialWorker { get; set; }
        */
    }

    public class CaseManagementComplianceSummaryReport
    {
        public int UniqueID { get; set; }
        //public string ProfileDSHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        //public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountAntePostNatalCare { get; set; }
        public int CountImmunization { get; set; }
        public int CountTSFCMAM { get; set; }
        public int CountGMPSessions { get; set; }
        public int CountSchoolEnrolment { get; set; }
        public int CountBCCSessions { get; set; }
        public int CountBirthRegistration { get; set; }
        public int Count3PrenatalCareVisits { get; set; }
        public int Count1PrenatalCareVisits { get; set; }
        public int CountCheckUpOfChildAtHealthPost { get; set; }
        public int CountSupplementaryFeeding { get; set; }
        public int CountCBHIMembership { get; set; }
        public int CountChildProtection { get; set; }
        //public DateTime CollectionDate { get; set; }
        //public string SocialWorker { get; set; }
    }

    public class RetargetingDetailReport
    {
        public int UniqueID { get; set; }
        public string ProfileHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string IndividualID { get; set; }
        public string ClientType { get; set; }
        public string NameOfHouseHoldMember { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string PreAnteNatalCare { get; set; }
        public string Immunization { get; set; }
        public string SupplementaryFeeding { get; set; }
        public string MonthlyGMPSessions { get; set; }
        public string EnrolmentAttendance { get; set; }
        public string BCCSessions { get; set; }
        public string UnderTSF { get; set; }
        public string BirthRegistration { get; set; }
        public string HealthPostCheckUp { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string PreNatalVisit1 { get; set; }
        public string PreNatalVisit3 { get; set; }
        public string PostNatalVisit { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class RetargetingSummaryReport
    {
        public int UniqueID { get; set; }
        //public string ProfileHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        //public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountPreAnteNatalCare { get; set; }
        public int CountImmunization { get; set; }
        public int CountSupplementaryFeeding { get; set; }
        public int CountMonthlyGMPSessions { get; set; }
        public int CountEnrolmentAttendance { get; set; }
        public int CountBCCSessions { get; set; }
        public int CountUnderTSF { get; set; }
        public int CountBirthRegistration { get; set; }
        public int CountHealthPostCheckUp { get; set; }
        public int CountCBHIMembership { get; set; }
        public int CountChildProtectionRisk { get; set; }
        public int CountPreNatalVisit1 { get; set; }
        public int CountPreNatalVisit3 { get; set; }
        public int CountPostNatalVisit { get; set; }
    }

    public class StandardReportModel
    {
        public List<string> Region { get; set; }
        public List<string> Woreda { get; set; }
        public List<string> Kebele { get; set; }
        public List<string> Gote { get; set; }
        public string ReportRegion { get; set; }
        public string ReportWoreda { get; set; }
        public string ReportKebele { get; set; }
        public string ReportGote { get; set; }
        public string ReportName { get; set; }
        public string ReportingLevel { get; set; }
        public string ReportDisaggregation { get; set; }
        //public string SocialWorker { get; set; }
        //public string FiscalYear { get; set; }
        //public string ReportingPeriod { get; set; }
        public List<string> SocialWorker { get; set; }
        public List<string> AgeRange { get; set; }
        public string FiscalYear { get; set; }
        public string ReportingPeriod { get; set; }
        public DateTime ReportStartDate { get; set; }
        public DateTime ReportEndDate { get; set; }

        // RPT 1
        public string Gender { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }

        // RPT 2
        public DateTime StartDateTDSPLW { get; set; }
        public DateTime EndDateTDSPLW { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string NutritionalStatusInfant { get; set; }

        // RPT 3
        public DateTime StartDateTDSCMC { get; set; }
        public DateTime EndDateTDSCMC { get; set; }
        public string MalnourishmentDegree { get; set; }

        // RPT 4
        public string ServiceType { get; set; }
        public string ClientType { get; set; }

        public DataTable JsonData { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<Gote> gotes { get; set; }
        public IEnumerable<ReportingPeriod> rptPeriod { get; set; }
        public IEnumerable<ReportType> rptTypes { get; set; }
        public IEnumerable<ReportLevel> rptLevels { get; set; }
        public IEnumerable<ReportDisaggregation> rptDisaggregation { get; set; }
        public IEnumerable<ServiceType> services { get; set; }
        public IEnumerable<Gender> sex { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }
        public IEnumerable<YesNo> yesnos { get; set; }
        public IEnumerable<NutritionalStatus> nutrStatus { get; set; }
        public IEnumerable<ClientType> clients { get; set; }
        public IEnumerable<FinancialYear> financialYear { get; set; }
        public IEnumerable<ReportAgeRange> ranges { get; set; }
    }

    public class ReportEngineModel
    {
        public int ColumnID { get; set; }
        public int? RegionID { get; set; }
        public int? WoredaID { get; set; }
        public int? KebeleID { get; set; }

        public List<string> Region { get; set; }
        public List<string> Woreda { get; set; }
        public List<string> Kebele { get; set; }

        public string ReportName { get; set; }
        public string ReportingPeriod { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int LocationType { get; set; }
        public string ClientID { get; set; }
        public string FiscalYear { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
        public IEnumerable<ReportType> rptTypes { get; set; }
        public IEnumerable<ClientType> clientTypes { get; set; }
        public IEnumerable<FinancialYear> financialYear { get; set; }
    }


    public class ReportExportModel
    {
        public int ColumnID { get; set; }
        public int RegionCodeID { get; set; }
        public int WoredaCodeID { get; set; }
        public int KebeleCodeID { get; set; }
        public int FiscalYear { get; set; }
        [Required]
        public string ReportName { get; set; }
        public string ReportingPeriod { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ReportType> rptTypes { get; set; }
        public IEnumerable<FinancialYear> fiscalYears { get; set; }
    }

    public class Form6ReportHeader
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string ReportingPeriod { get; set; }
        public int DSHousholds { get; set; }
        public int DSMembers { get; set; }
        public int DSHouseholdCo { get; set; }
        public int DsMembersCo { get; set; }
        public int DsHouseholdCBHI { get; set; }
        public int PLWCount { get; set; }
        public int CaretakerCount { get; set; }
    }

    public class Form6Report
    {
        public string ServiceName { get; set; }
        public int NoOfClients { get; set; }
        public int Failedonce { get; set; }
        public int Failedtwice { get; set; }
        public int Failedmore { get; set; }
        public int HouseVisits { get; set; }
        public string Remarks { get; set; }
    }

    public class DefaultWoredaModel
    {
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int WoredaID { get; set; }
        public string WoredaName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
    }

    public class RegionGrid
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
    }

    public class RegionAdd
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        [Required(ErrorMessage = "Region Code is Required")]
        public string RegionCode { get; set; }
        [Required(ErrorMessage = "Region Name is Required")]
        public string RegionName { get; set; }
    }

    public class RegionModifyModel
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
    }

    public class WoredaAdd
    {
        public int ColumnID { get; set; }
        public int WoredaID { get; set; }
        [Required(ErrorMessage = "Woreda Code is Required")]
        public string WoredaCode { get; set; }
        [Required(ErrorMessage = "Region is Required")]
        public int RegionID { get; set; }
        [Required(ErrorMessage = "Woreda Name is Required")]
        public string WoredaName { get; set; }
        public IEnumerable<Region> regions { get; set; }
    }

    public class WoredaModifyModel
    {
        public int ColumnID { get; set; }
        public int WoredaID { get; set; }
        public string WoredaCode { get; set; }
        public int RegionID { get; set; }
        public string WoredaName { get; set; }
        public IEnumerable<Region> regions { get; set; }
    }

    public class KebeleAdd
    {
        public int ColumnID { get; set; }
        [Required(ErrorMessage = "Region is Required")]
        public int RegionID { get; set; }
        [Required(ErrorMessage = "Woreda is Required")]
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        [Required(ErrorMessage = "Kebele Code is Required")]
        public string KebeleCode { get; set; }
        [Required(ErrorMessage = "Kebele Name is Required")]
        public string KebeleName { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
    }

    public class KebeleModifyModel
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string KebeleCode { get; set; }
        public string KebeleName { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
    }

    public class WoredaGrid
    {
        public int ColumnID { get; set; }
        public int WoredaID { get; set; }
        public string WoredaCode { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
    }

    public class KebeleGrid
    {
        public int ColumnID { get; set; }
        public int KebeleID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleCode { get; set; }
        public string KebeleName { get; set; }
    }

    public class DataExportGrid
    {
        public int ColumnID { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOn { get; set; }
        public string ZipfileName { get; set; }
    }

    public class DataImportGrid
    {
        public int ColumnID { get; set; }
        public string TableName { get; set; }
        public string ReplicatedBy { get; set; }
        public string ReplicatedOn { get; set; }
        public int ImportedDataID { get; set; }
        public string FileName { get; set; }        
        public string ReplicateXML { get; set; }       
    }

    public class DataExportPreviewGrid
    {
        public int ColumnID { get; set; }
        public string TableName { get; set; }
        public string xmlString { get; set; }
        public string filePath { get; set; }
    }

    public class ImportListModel
    {
        public int ImportID { get; set; }
        public DateTime ImportDate { get; set; }
        public string ImportPath { get; set; }
        public int CreatedBy { get; set; }
    }
    public class ReplicationDetail
    {
        public int GeneratedBy { get; set; }
        public string SourceName { get; set; }
        public string SourceDetails { get; set; }
        public string filePath { get; set; }
    }

    public class Settings
    {
        public int LocationType { get; set; }
    }

    //1 - Woreda, 2 - Regional Office, 3 - Federal HQ
    public enum LocationTypes
    {
        Nana,
        Woreda,
        Regional,
        Federal
    }

    public class ExportForm4D
    {
        public string REGION { get; set; }
        public string WOREDA { get; set; }
        public string KEBELE { get; set; }
        public string HOUSEHOLDS { get; set; }
        public string HOUSEHOLD_MEMBERS { get; set; }
        public string HOUSEHOLDS_WITH_CHILD_PROTECTION_CASES { get; set; }
        public string HOUSEHOLDS_MEMBERS_WITH_CHILD_PROTECTION_CASES { get; set; }
        public string HOUSEHOLDS_MEMBERS_SERVED { get; set; }
        public string MALTREATMENT_0YRS_10YRS_MALE { get; set; }
        public string MALTREATMENT_11YRS_14YRS_MALE { get; set; }
        public string MALTREATMENT_15YRS_17YRS_MALE { get; set; }
        public string MALTREATMENT_0YRS_10YRS_FEMALE { get; set; }
        public string MALTREATMENT_11YRS_14YRS_FEMALE { get; set; }
        public string MALTREATMENT_15YRS_17YRS_FEMALE { get; set; }
        public string BULLYING_0YRS_10YRS_MALE { get; set; }
        public string BULLYING_11YRS_14YRS_MALE { get; set; }
        public string BULLYING_15YRS_17YRS_MALE { get; set; }
        public string BULLYING_0YRS_10YRS_FEMALE { get; set; }
        public string BULLYING_11YRS_14YRS_FEMALE { get; set; }
        public string BULLYING_15YRS_17YRS_FEMALE { get; set; }
        public string YOUTHVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string YOUTHVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string YOUTHVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string YOUTHVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string YOUTHVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string YOUTHVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string INTIMATEPARTNERVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string INTIMATEPARTNERVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string INTIMATEPARTNERVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string INTIMATEPARTNERVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string INTIMATEPARTNERVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string INTIMATEPARTNERVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string SEXUALVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string SEXUALVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string SEXUALVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string SEXUALVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string SEXUALVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string SEXUALVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string EMOTIONALPSYCHOLOGICALVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string EMOTIONALPSYCHOLOGICALVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string EMOTIONALPSYCHOLOGICALVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string EMOTIONALPSYCHOLOGICALVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string EMOTIONALPSYCHOLOGICALVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string EMOTIONALPSYCHOLOGICALVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string GENDERBASEDVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string GENDERBASEDVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string GENDERBASEDVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string GENDERBASEDVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string GENDERBASEDVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string GENDERBASEDVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string CHILDRENWITHOUTADEQUATEPARENTALCARE_0YRS_10YRS_MALE { get; set; }
        public string CHILDRENWITHOUTADEQUATEPARENTALCARE_11YRS_14YRS_MALE { get; set; }
        public string CHILDRENWITHOUTADEQUATEPARENTALCARE_15YRS_17YRS_MALE { get; set; }
        public string CHILDRENWITHOUTADEQUATEPARENTALCARE_0YRS_10YRS_FEMALE { get; set; }
        public string CHILDRENWITHOUTADEQUATEPARENTALCARE_11YRS_14YRS_FEMALE { get; set; }
        public string CHILDRENWITHOUTADEQUATEPARENTALCARE_15YRS_17YRS_FEMALE { get; set; }
        public string CHILDTRAFFICKING_0YRS_10YRS_MALE { get; set; }
        public string CHILDTRAFFICKING_11YRS_14YRS_MALE { get; set; }
        public string CHILDTRAFFICKING_15YRS_17YRS_MALE { get; set; }
        public string CHILDTRAFFICKING_0YRS_10YRS_FEMALE { get; set; }
        public string CHILDTRAFFICKING_11YRS_14YRS_FEMALE { get; set; }
        public string CHILDTRAFFICKING_15YRS_17YRS_FEMALE { get; set; }
        public string CHILDRENLIVINGWITHDISABILITIES_0YRS_10YRS_MALE { get; set; }
        public string CHILDRENLIVINGWITHDISABILITIES_11YRS_14YRS_MALE { get; set; }
        public string CHILDRENLIVINGWITHDISABILITIES_15YRS_17YRS_MALE { get; set; }
        public string CHILDRENLIVINGWITHDISABILITIES_0YRS_10YRS_FEMALE { get; set; }
        public string CHILDRENLIVINGWITHDISABILITIES_11YRS_14YRS_FEMALE { get; set; }
        public string CHILDRENLIVINGWITHDISABILITIES_15YRS_17YRS_FEMALE { get; set; }
        public string CHILDLABOUR_0YRS_10YRS_MALE { get; set; }
        public string CHILDLABOUR_11YRS_14YRS_MALE { get; set; }
        public string CHILDLABOUR_15YRS_17YRS_MALE { get; set; }
        public string CHILDLABOUR_0YRS_10YRS_FEMALE { get; set; }
        public string CHILDLABOUR_11YRS_14YRS_FEMALE { get; set; }
        public string CHILDLABOUR_15YRS_17YRS_FEMALE { get; set; }
        public string CHILDRENINCONTACTWITHTHELAW_0YRS_10YRS_MALE { get; set; }
        public string CHILDRENINCONTACTWITHTHELAW_11YRS_14YRS_MALE { get; set; }
        public string CHILDRENINCONTACTWITHTHELAW_15YRS_17YRS_MALE { get; set; }
        public string CHILDRENINCONTACTWITHTHELAW_0YRS_10YRS_FEMALE { get; set; }
        public string CHILDRENINCONTACTWITHTHELAW_11YRS_14YRS_FEMALE { get; set; }
        public string CHILDRENINCONTACTWITHTHELAW_15YRS_17YRS_FEMALE { get; set; }
        public string CHILDRENINONTHEMOVE_0YRS_10YRS_MALE { get; set; }
        public string CHILDRENINONTHEMOVE_11YRS_14YRS_MALE { get; set; }
        public string CHILDRENINONTHEMOVE_15YRS_17YRS_MALE { get; set; }
        public string CHILDRENINONTHEMOVE_0YRS_10YRS_FEMALE { get; set; }
        public string CHILDRENINONTHEMOVE_11YRS_14YRS_FEMALE { get; set; }
        public string CHILDRENINONTHEMOVE_15YRS_17YRS_FEMALE { get; set; }
        public string CHILDRENINATRISKOFHARMFULPRACTICES_0YRS_10YRS_MALE { get; set; }
        public string CHILDRENINATRISKOFHARMFULPRACTICES_11YRS_14YRS_MALE { get; set; }
        public string CHILDRENINATRISKOFHARMFULPRACTICES_15YRS_17YRS_MALE { get; set; }
        public string CHILDRENINATRISKOFHARMFULPRACTICES_0YRS_10YRS_FEMALE { get; set; }
        public string CHILDRENINATRISKOFHARMFULPRACTICES_11YRS_14YRS_FEMALE { get; set; }
        public string CHILDRENINATRISKOFHARMFULPRACTICES_15YRS_17YRS_FEMALE { get; set; }
        public string CHILDRENINSTREETSITUATIONS_0YRS_10YRS_MALE { get; set; }
        public string CHILDRENINSTREETSITUATIONS_11YRS_14YRS_MALE { get; set; }
        public string CHILDRENINSTREETSITUATIONS_15YRS_17YRS_MALE { get; set; }
        public string CHILDRENINSTREETSITUATIONS_0YRS_10YRS_FEMALE { get; set; }
        public string CHILDRENINSTREETSITUATIONS_11YRS_14YRS_FEMALE { get; set; }
        public string CHILDRENINSTREETSITUATIONS_15YRS_17YRS_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_MALTREATMENT_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_BULLYING_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_YOUTHVIOLENCE_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_INTIMATEPARTNERVIOLENCE_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_SEXUALVIOLENCE_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_GENDERBASEDVIOLENCE_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDTRAFFICKING_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENLIVINGWITHDISABILITIES_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDLABOUR_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENINCONTACTWITHTHELAW_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENINONTHEMOVE_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENINSTREETSITUATIONS_MALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_MALTREATMENT_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_BULLYING_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_YOUTHVIOLENCE_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_INTIMATEPARTNERVIOLENCE_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_SEXUALVIOLENCE_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_GENDERBASEDVIOLENCE_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDTRAFFICKING_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENLIVINGWITHDISABILITIES_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDLABOUR_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENINCONTACTWITHTHELAW_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENINONTHEMOVE_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_FEMALE { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES_CHILDRENINSTREETSITUATIONS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_MALTREATMENT_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_MALTREATMENT_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_MALTREATMENT_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_MALTREATMENT_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_MALTREATMENT_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_MALTREATMENT_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_BULLYING_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_BULLYING_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_BULLYING_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_BULLYING_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_BULLYING_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_BULLYING_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_YOUTHVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_YOUTHVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_YOUTHVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_YOUTHVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_YOUTHVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_YOUTHVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_SEXUALVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_SEXUALVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_SEXUALVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_SEXUALVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_SEXUALVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_SEXUALVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_GENDERBASEDVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_GENDERBASEDVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_GENDERBASEDVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_GENDERBASEDVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_GENDERBASEDVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_GENDERBASEDVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDTRAFFICKING_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDTRAFFICKING_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDTRAFFICKING_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDTRAFFICKING_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDTRAFFICKING_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDTRAFFICKING_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDLABOUR_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDLABOUR_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDLABOUR_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDLABOUR_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDLABOUR_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDLABOUR_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINONTHEMOVE_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINONTHEMOVE_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINONTHEMOVE_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINONTHEMOVE_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINONTHEMOVE_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINONTHEMOVE_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_15YRS_17YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_0YRS_10YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_11YRS_14YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_15YRS_17YRS_MALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_0YRS_10YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_11YRS_14YRS_FEMALE { get; set; }
        public string RECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_MALTREATMENT_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_MALTREATMENT_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_MALTREATMENT_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_MALTREATMENT_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_MALTREATMENT_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_MALTREATMENT_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_BULLYING_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_BULLYING_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_BULLYING_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_BULLYING_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_BULLYING_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_BULLYING_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_YOUTHVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_YOUTHVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_YOUTHVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_YOUTHVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_YOUTHVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_YOUTHVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_INTIMATEPARTNERVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_SEXUALVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_SEXUALVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_SEXUALVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_SEXUALVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_SEXUALVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_SEXUALVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_EMOTIONALPSYCHOLOGICALVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_GENDERBASEDVIOLENCE_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_GENDERBASEDVIOLENCE_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_GENDERBASEDVIOLENCE_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_GENDERBASEDVIOLENCE_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_GENDERBASEDVIOLENCE_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_GENDERBASEDVIOLENCE_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENWITHOUTADEQUATEPARENTALCARE_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDTRAFFICKING_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDTRAFFICKING_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDTRAFFICKING_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDTRAFFICKING_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDTRAFFICKING_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDTRAFFICKING_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENLIVINGWITHDISABILITIES_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDLABOUR_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDLABOUR_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDLABOUR_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDLABOUR_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDLABOUR_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDLABOUR_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINCONTACTWITHTHELAW_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINONTHEMOVE_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINONTHEMOVE_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINONTHEMOVE_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINONTHEMOVE_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINONTHEMOVE_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINONTHEMOVE_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINATRISKOFHARMFULPRACTICES_15YRS_17YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_0YRS_10YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_11YRS_14YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_15YRS_17YRS_MALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_0YRS_10YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_11YRS_14YRS_FEMALE { get; set; }
        public string NOTRECEIVEDSERVICES_CHILDRENINSTREETSITUATIONS_15YRS_17YRS_FEMALE { get; set; }
        public string OPENCASES_MALTREATMENT { get; set; }
        public string OPENCASES_BULLYING { get; set; }
        public string OPENCASES_YOUTHVIOLENCE { get; set; }
        public string OPENCASES_INTIMATEPARTNERVIOLENCE { get; set; }
        public string OPENCASES_SEXUALVIOLENCE { get; set; }
        public string OPENCASES_EMOTIONALPSYCHOLOGICALVIOLENCE { get; set; }
        public string OPENCASES_GENDERBASEDVIOLENCE { get; set; }
        public string OPENCASES_CHILDRENWITHOUTADEQUATEPARENTALCARE { get; set; }
        public string OPENCASES_CHILDTRAFFICKING { get; set; }
        public string OPENCASES_CHILDRENLIVINGWITHDISABILITIES { get; set; }
        public string OPENCASES_CHILDLABOUR { get; set; }
        public string OPENCASES_CHILDRENINCONTACTWITHTHELAW { get; set; }
        public string OPENCASES_CHILDRENINONTHEMOVE { get; set; }
        public string OPENCASES_CHILDRENINATRISKOFHARMFULPRACTICES { get; set; }
        public string OPENCASES_CHILDRENINSTREETSITUATIONS { get; set; }
        public string CLOSEDCASES_MALTREATMENT { get; set; }
        public string CLOSEDCASES_BULLYING { get; set; }
        public string CLOSEDCASES_YOUTHVIOLENCE { get; set; }
        public string CLOSEDCASES_INTIMATEPARTNERVIOLENCE { get; set; }
        public string CLOSEDCASES_SEXUALVIOLENCE { get; set; }
        public string CLOSEDCASES_EMOTIONALPSYCHOLOGICALVIOLENCE { get; set; }
        public string CLOSEDCASES_GENDERBASEDVIOLENCE { get; set; }
        public string CLOSEDCASES_CHILDRENWITHOUTADEQUATEPARENTALCARE { get; set; }
        public string CLOSEDCASES_CHILDTRAFFICKING { get; set; }
        public string CLOSEDCASES_CHILDRENLIVINGWITHDISABILITIES { get; set; }
        public string CLOSEDCASES_CHILDLABOUR { get; set; }
        public string CLOSEDCASES_CHILDRENINCONTACTWITHTHELAW { get; set; }
        public string CLOSEDCASES_CHILDRENINONTHEMOVE { get; set; }
        public string CLOSEDCASES_CHILDRENINATRISKOFHARMFULPRACTICES { get; set; }
        public string CLOSEDCASES_CHILDRENINSTREETSITUATIONS { get; set; }
        public string VISITS_MALTREATMENT { get; set; }
        public string VISITS_BULLYING { get; set; }
        public string VISITS_YOUTHVIOLENCE { get; set; }
        public string VISITS_INTIMATEPARTNERVIOLENCE { get; set; }
        public string VISITS_SEXUALVIOLENCE { get; set; }
        public string VISITS_EMOTIONALPSYCHOLOGICALVIOLENCE { get; set; }
        public string VISITS_GENDERBASEDVIOLENCE { get; set; }
        public string VISITS_CHILDRENWITHOUTADEQUATEPARENTALCARE { get; set; }
        public string VISITS_CHILDTRAFFICKING { get; set; }
        public string VISITS_CHILDRENLIVINGWITHDISABILITIES { get; set; }
        public string VISITS_CHILDLABOUR { get; set; }
        public string VISITS_CHILDRENINCONTACTWITHTHELAW { get; set; }
        public string VISITS_CHILDRENINONTHEMOVE { get; set; }
        public string VISITS_CHILDRENINATRISKOFHARMFULPRACTICES { get; set; }
        public string VISITS_CHILDRENINSTREETSITUATIONS { get; set; }
        public string REASONSERVICENOTACCESSED_MALTREATMENT { get; set; }
        public string REASONSERVICENOTACCESSED_BULLYING { get; set; }
        public string REASONSERVICENOTACCESSED_YOUTHVIOLENCE { get; set; }
        public string REASONSERVICENOTACCESSED_INTIMATEPARTNERVIOLENCE { get; set; }
        public string REASONSERVICENOTACCESSED_SEXUALVIOLENCE { get; set; }
        public string REASONSERVICENOTACCESSED_EMOTIONALPSYCHOLOGICALVIOLENCE { get; set; }
        public string REASONSERVICENOTACCESSED_GENDERBASEDVIOLENCE { get; set; }
        public string REASONSERVICENOTACCESSED_CHILDRENWITHOUTADEQUATEPARENTALCARE { get; set; }
        public string REASONSERVICENOTACCESSED_CHILDTRAFFICKING { get; set; }
        public string REASONSERVICENOTACCESSED_CHILDRENLIVINGWITHDISABILITIES { get; set; }
        public string REASONSERVICENOTACCESSED_CHILDLABOUR { get; set; }
        public string REASONSERVICENOTACCESSED_CHILDRENINCONTACTWITHTHELAW { get; set; }
        public string REASONSERVICENOTACCESSED_CHILDRENINONTHEMOVE { get; set; }
        public string REASONSERVICENOTACCESSED_CHILDRENINATRISKOFHARMFULPRACTICES { get; set; }
        public string REASONSERVICENOTACCESSED_CHILDRENINSTREETSITUATIONS { get; set; }

    }

    public class ExportForm4D2
    {
        public string REGION { get; set; }
        public string WOREDA { get; set; }
        public string KEBELE { get; set; }
        public string HOUSEHOLDS { get; set; }
        public string HOUSEHOLD_MEMBERS { get; set; }
        public string HOUSEHOLDS_WITH_CHILD_PROTECTION_CASES { get; set; }
        public string HOUSEHOLDS_MEMBERS_WITH_CHILD_PROTECTION_CASES { get; set; }
        public string HOUSEHOLDS_MEMBERS_SERVED { get; set; }
        public string MALTREATMENT { get; set; }
        public string BULLYING { get; set; }
        public string YOUTHVIOLENCE { get; set; }
        public string INTIMATEPARTNERVIOLENCE { get; set; }
        public string SEXUALVIOLENCE { get; set; }
        public string EMOTIONALPSYCHOLOGICALVIOLENCE { get; set; }
        public string GENDERBASEDVIOLENCE { get; set; }
        public string CHILDRENWITHOUTADEQUATEPARENTALCARE { get; set; }
        public string CHILDTRAFFICKING { get; set; }
        public string CHILDRENLIVINGWITHDISABILITIES { get; set; }
        public string CHILDLABOUR { get; set; }
        public string CHILDRENINCONTACTWITHTHELAW { get; set; }
        public string CHILDRENINONTHEMOVE { get; set; }
        public string CHILDRENINATRISKOFHARMFULPRACTICES { get; set; }
        public string CHILDRENINSTREETSITUATIONS { get; set; }
        public string REFERREDTOCASEMANAGEMENTSERVICES { get; set; }
        public string RECEIVEDSERVICES { get; set; }
        public string NOTRECEIVEDSERVICES { get; set; }
        public string OPENCASES { get; set; }
        public string CLOSEDCASES { get; set; }
        public string VISITS { get; set; }
        public string REASONSERVICENOTACCESSED { get; set; }

    }
}
