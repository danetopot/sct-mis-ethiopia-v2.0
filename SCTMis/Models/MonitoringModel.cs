﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCTMis.Models
{
    public class MISSummaryModel
    {
        public string jtStartIndex { get; set; }
        public string jtPageSize { get; set; }
        public string FormName { get; set; }
    }

    public class CaptureForm5CMIS
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string ReportingPeriodID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Remarks { get; set; }
        public DateTime CompletedDate { get; set; }
        public DateTime CompletedDateID { get; set; }
        public string SocialWorker { get; set; }
        public string CapturedXml { get; set; }
        public string MemberXml { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; } 
    }

    public class CaptureForm5BMIS
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfPLW { get; set; }
        public string BabyName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string ReportingPeriodID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Remarks { get; set; }
        public DateTime CompletedDate { get; set; }
        public DateTime CompletedDateID { get; set; }
        public string SocialWorker { get; set; }
        public string CapturedXml { get; set; }
        public string MemberXml { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; } 
    }

    public class Form5AMISSummary
    {
        public int ColumnID { get; set; }
        public int ID { get; set; }
        public string Reason { get; set; }
        public string ReasonResponse { get; set; }
        public string Action { get; set; }
        public string ActionResponse { get; set; }
    }

    public class Form5A1MISGrid
    {
        public int ColumnID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }
        public string EnrolledInSchool { get; set; }
        public string AttendClasses { get; set; }
        public string Remarks { get; set; }
    }

    public class OptionsSummary
    {
        public string Value { get; set; }
        public string DisplayText { get; set; }
    }

    public class Form5AMISGrid
    {
        public int ColumnID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string Prenatal { get; set; }
        public string PostNatal { get; set; }
        public string Immunization { get; set; }
        public string Checkup { get; set; }
        public string GMP { get; set; }
        public string BCC { get; set; }
        public string Birth { get; set; }
        public string Remarks { get; set; }
    }

    public class Form5BMISGrid
    {
        public int ColumnID { get; set; }
        public string NameOfPLW { get; set; }
        public int PLWAge { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string Remarks { get; set; }
        //public string PreNatalCare3 { get; set; }
        //public string PreNatalCare1 { get; set; }
        //public string PostNatalCare { get; set; }
        //public string Immunization { get; set; }
        //public string TSFCheckup { get; set; }
        //public string GMP { get; set; }
        //public string BCC { get; set; }
        //public string Birth { get; set; }

    }

    public class Form5A1Grid
    {
        public int ColumnID { get; set; }
		public string NonComplianceID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseholdVisit { get; set; }
		public string CompletedDate { get; set; }
		public string KebeleName { get; set; }
        public int Clients { get; set; }
        public string ReportFileName { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOn { get; set; }
    }

    public class Form5AGrid
    {
        public int ColumnID { get; set; }
        public string NonComplianceID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseholdVisit { get; set; }
        public string CompletedDate { get; set; }
        public string KebeleName { get; set; }
        public int Clients { get; set; }
        public string ReportFileName { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOn { get; set; }
    }

    public class ProduceForm5A1Model
    {
        public int ColumnID { get; set; }
        public string ComplianceID { get; set; }
        public string ReportingPeriod { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
    }

    public class ProduceForm5AModel
    {
        public int ColumnID { get; set; }
        public string ComplianceID { get; set; }
        public string ReportingPeriod { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
        public IEnumerable<HouseholdVisit> householdvsts { get; set; }
    }

    public class Form5ACompliance
    {
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
    }

    public class Form5A1NonCompliance
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string Gote { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string SchoolName { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string EnrolledInSchool { get; set; }
        public string Remarks { get; set; }
        public string HouseHoldVisit { get; set; }
        public string ReportingPeriod { get; set; }
        public string CreatedBy { get; set; }        
    }

    public class Form5ANonCompliance
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string Gote { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string Remarks { get; set; }
        public string HouseHoldVisit { get; set; }
        public string ReportingPeriod { get; set; }
        public string PreNatalCare { get; set; }
        public string PostNatalCare { get; set; }
        public string Immunization { get; set; }
        public string TSFCheckup { get; set; }
        public string GMP { get; set; }
        public string BCC { get; set; }
        public string BirthRegistration { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
    }

    public class Form5A2NonCompliance
    {
        public string UniqueID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string Gote { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string PreNatalCare { get; set; }
        public string PostNatalCare { get; set; }
        public string Immunization { get; set; }
        public string TSFCheckup { get; set; }
        public string GMP { get; set; }
        public string BCC { get; set; }
        public string BirthRegistration { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class Form5BCompliance
    {
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int PLWAge { get; set; }
        public string BabySex { get; set; }
    }

    public class Form5BNonCompliance
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string NameOfPLW { get; set; }
        public string MedicalRecordNumber { get; set; }
        public string PLWAge { get; set; }
        public string Gote { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string BabyName { get; set; }
        public string BabyDateOfBirth { get; set; }
        public string BabySex { get; set; }
        public string StartDateTDS { get; set; }
        public string EndDateTDS { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string Remarks { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldVisit { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string PreNatalCare3 { get; set; }
        public string PreNatalCare1 { get; set; }
        public string PostNatalCare { get; set; }
        public string Immunization { get; set; }
        public string TSFCheckup { get; set; }
        public string GMP { get; set; }
        public string BCC { get; set; }
        public string CreatedBy { get; set; }        
    }

    public class Form5CCompliance
    {
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
    }

    public class Form5CNonCompliance
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string NameOfCareTaker { get; set; }
        public string CaretakerID { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string Gote { get; set; }
        public string ChildID { get; set; }
        public string MalnourishedChildSex { get; set; }
        public string ChildDateOfBirth { get; set; }
        public string StartDateTDS { get; set; }
        public string EndDateTDS { get; set; }
        public string DateTypeCertificate { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string Remarks { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldVisit { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string ChildCheckup { get; set; }
        public string GMP { get; set; }
        public string BCC { get; set; }
    }

    public class CapturedForm5AGrid
    {
        public int ColumnID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string KebeleName { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string ActionTaken { get; set; }
    }

    public class CapturedForm5BGrid
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string KebeleName { get; set; }
        public string ReportingPeriod { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string ActionTaken { get; set; }
    }

    public class CapturedForm5CGrid
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string KebeleName { get; set; }
        public string ReportingPeriod { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string ActionTaken { get; set; }
    }

    public class CaptureForm5MISModel
    {
        public int ColumnID { get; set; }
        public int ID { get; set; }
        [Required(ErrorMessage = "Unique Identifier is required")]
        public string ProfileDSDetailID { get; set; }
        [Required(ErrorMessage = "Kebele is required")]
        public int KebeleID { get; set; }
        [Required(ErrorMessage = "Reporting Period is required")]
        public string ReportingPeriodID { get; set; }
        [Required(ErrorMessage = "Completed Date is required")]
        [DataType(DataType.Date)]
        public DateTime CompletedDate { get; set; }
        public string ActionResponse { get; set; }
        public string SocialWorker { get; set; }
    }

    public class CaptureForm5BMISModel
    {
        public int ColumnID { get; set; }
        public int ID { get; set; }
        [Required(ErrorMessage = "Unique Identifier is required")]
        public string ProfileTDSPLWID { get; set; }
        [Required(ErrorMessage = "Kebele is required")]
        public int KebeleID { get; set; }
        [Required(ErrorMessage = "Reporting Period is required")]
        public string ReportingPeriodID { get; set; }
        [Required(ErrorMessage = "Completed Date is required")]
        [DataType(DataType.Date)]
        public DateTime CompletedDate { get; set; }
        public string ActionResponse { get; set; }
        public string SocialWorker { get; set; }
    }

    public class CaptureForm5CMISModel
    {
        public int ColumnID { get; set; }
        public int ID { get; set; }
        [Required(ErrorMessage = "Unique Identifier is required")]
        public string ProfileTDSCMCID { get; set; }
        [Required(ErrorMessage = "Kebele is required")]
        public int KebeleID { get; set; }
        [Required(ErrorMessage = "Reporting Period is required")]
        public string ReportingPeriodID { get; set; }
        [Required(ErrorMessage = "Completed Date is required")]
        [DataType(DataType.Date)]
        public DateTime CompletedDate { get; set; }
        public string ActionResponse { get; set; }
        public string SocialWorker { get; set; }
    }

    public class CaptureForm5MIS
    {
        public int ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Remarks { get; set; }
        public string ReportingPeriodID { get; set; }
        public DateTime CompletedDate { get; set; }
        public DateTime CompletedDateID { get; set; }
        public string SocialWorker { get; set; }
        public string CapturedXml { get; set; }
        public string MemberXml { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }   
    }

    public class CaptureForm5
    {
        public int ColumnID { get; set; }
        public string ComplianceID { get; set; }
        public Int16 UniqueID { get; set; }
        public string ReportingPeriod { get; set; }
        public int HouseholdVisitID { get; set; }
        public string rptPeriod { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
        public string CapturedXml { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
    }

    public class CapturedForm5XMLDetails
    {
        public string Remarks { get; set; }
        public Int16 UniqueID { get; set; }
    }

    public class Form5A1DSModel
    {
        public Int32 KebeleID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
    }

    public class CapturedForm5MISXMLDetails
    {
        public int ColumnID { get; set; }
        public int ID { get; set; }
    }

    public class getForm5ADSList
    {
        public int ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldVisit { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public int Members { get; set; }
        //public string ActionTaken { get; set; }
    }

    public class getForm5BTDSPLWList
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldVisit { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfPLW { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string BabyDateOfBirth { get; set; }
    }

    public class getForm5CTDSCMCList
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldVisit { get; set; }
        public string HouseHoldIDNumber { get; set; } 
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string MalnourishedChildSex { get; set; }
        public string ChildDateOfBirth { get; set; }

    }
}