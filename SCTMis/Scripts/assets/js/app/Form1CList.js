﻿var page_count = 10;
var gridHeader = 'ColumnID,MalnourishmentDegree,ProfileTDSPLWID,PSNP<span class="HeaderChange">_</span>Number,Caretaker<span class="HeaderChange">_</span>Name,';
gridHeader = gridHeader + 'Child<span class="HeaderChange">_</span>Name,Kebele,Woreda,Gote/Gare,Creator,Approver, rpt,Status, Edit,Verify,print';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img,img';
var mygrid;

function createVerifyWindow(_ProfileTDSCMCID) {

    var selectedRow = mygrid.getSelectedId();
    _ProfileTDSCMCID = mygrid.cells(selectedRow, 2).getValue();
    post('../Household/ApproveForm1C', { ProfileTDSCMCID: _ProfileTDSCMCID });
}

function createWindow(_ProfileTDSCMCID) {

    var selectedRow = mygrid.getSelectedId();
    _ProfileTDSCMCID = mygrid.cells(selectedRow, 2).getValue();
    post('../Household/ModifyForm1C', { ProfileTDSCMCID: _ProfileTDSCMCID });
}

function PreviewPdfFile(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 11).getValue();
    console.log(_ID);
    get('/Home/downloadpdf', { GenerationID: _ID });
}

$(document).ready(function () {
    $('#loading').hide();
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");   
    mygrid.setInitWidths("0,0,0,105,130,110,75,90,90,66,75,0,64,35,50,50");
    mygrid.setColAlign("left,left,left,left,left,right,left,right,right,right,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    //mygrid.loadXML("/Household/Form1CTDSCMCList?RecCount=" + page_count);

    // On edit of a record, user want's the grid to show the edited records page
    var currentPage = $('#currentPage').val();
    mygrid.load("/Household/Form1CTDSCMCList?RecCount=" + page_count, function () {
        mygrid.changePage(currentPage);
    })

    mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
        $('#currentPage').val(ind);

        $.ajax({
            type: "POST",
            url: "../Household/UpdateCurrentPage",
            data: "{ 'CurrentPage':" + ind + "}",
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        });
    });
    //end here, user edit page demand

    //paka rangi based on afya   
    mygrid.setColumnHidden(2, true);
    mygrid.attachEvent("onRowCreated", function (rId, rObj, rXml) {
        var cellValue = mygrid.cells(rId, 1).getValue();
        switch (cellValue) {
            case 'N':
                mygrid.setRowTextStyle(rId, "color: #3bb449;");                
                break;

            case 'M':
                mygrid.setRowTextStyle(rId, "color: orange;");
                break;

            case 'SM':
                //mygrid.setRowColor(rId, "red");
                mygrid.setRowTextStyle(rId, "color: red;");
                break;
        }
    });

    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Caretaker Name'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Malnourished Child'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Household ID'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 7).text('Woreda'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Kebele'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Gote'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 6).text('Gare'));

    $("#cmdNew").click(function () {
        window.location.href = "../Household/CaptureForm1C";
    });
});
function reloadGrid() {
    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", true);

    var Search_TypeID = $("#SearchTypeID").val();
    var Search_Keyword = $("#SearchKeyword").val();
    showLoading(true);

    mygrid.clearAndLoad("/Household/Form1CTDSCMCList?RecCount=" + page_count + "&isSearch=1&SearchTypeID=" + Search_TypeID + "&SearchKeyword=" + Search_Keyword);

    //$.ajax({
    //    type: "GET",
    //    url: "/Household/Form1CTDSCMCList?RecCount=" + page_count + "&isSearch=1&SearchTypeID=" + Search_TypeID + "&SearchKeyword=" + Search_Keyword,
    //    dataType: "xml",
    //    success: LoadGridWithSearchxmlData
    //});

    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", false);
}