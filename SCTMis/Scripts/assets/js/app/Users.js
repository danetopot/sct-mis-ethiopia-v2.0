﻿$(document).ready(function () {
    //$("#FirstName,#LastName,#UserName,#Password,#ConfirmPassword,#Email,#Mobile").val("");
    $("#cmdAdd").prop("disabled", false);
    $('#UserName').focus();
    $("#cmdCancel").click(function () {
        window.location.href = "../Security/Users";
    });

    $('#UserName').bind('KeyPress', function (event) {
        if (keyCode >= 97 && keyCode < 123) {
            oEvent.keyCode = keyCode - 32;
            gE('UserName').value += oEvent.keyCode;
        }
        else if ((keyCode >= 65 && keyCode < 91) || (keyCode >= 48 && keyCode < 58))
            gE('UserName').value += oEvent.keyCode;
        else
            oEvent.cancel = true;
    });

    $('#loading').hide();
});
