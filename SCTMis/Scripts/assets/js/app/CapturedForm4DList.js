﻿var page_count = 10;
var gridHeader =
    'UniqueID,ChildProtectionHeaderID,ProfileDSHeaderID,Region,Kebele,KebeleID,Woreda,GoteGare,ClientType,PSNP<span class="HeaderChange">_</span>#,HouseHold<span class="HeaderChange">_</span>Head,Child<span class="HeaderChange">_</span>Name,FiscalYear,Created<span class="HeaderChange">_</span>On,Creator,Approver,Approved,Case<span class="HeaderChange">_</span>Status,Closed<span class="HeaderChange">_</span>By,Edit,Verify,FollowUp,Close';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img,img,img';
var mygrid;

$(document).ready(function () {
    $('#loading').show();
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,0,0,95,0,95,0,0,75,125,125,0,0,90,75,120,90,80,50,50,80,50");
    mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,justify,justify,justify,justify");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    mygrid.loadXML("/ComplianceCapture/FetchForm4DFormList?RecCount=" + page_count);
    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('HouseHoldIDNumber'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('NameOfHouseHoldHead'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 6).text('WoredaName'));

    /*
    $("#cmdNew").click(function () {
        window.location.href = "../ComplianceCapture/FetchForm4DFormList";
    });
    */

    $("#cmdBack").click(function () {
        window.location.href = "../ComplianceCapture/Form4DHHList";
    });

    $("#cmdBackFup").click(function () {
        window.location.href = "/ComplianceCapture/Form4DFollowUpList"
    });

    $('#loading').hide();
});


function createWindow(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 1).getValue();
    window.location.href = "../ComplianceCapture/ModifyForm4D?Id=" + _ID;
}

function createVerifyWindow(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 1).getValue();
    window.location.href = "../ComplianceCapture/ApproveForm4D?Id=" + _ID;
}

function closeWindow(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 1).getValue();
    window.location.href = "../ComplianceCapture/CloseForm4D?Id=" + _ID;
    
}

function followUpWindow(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 1).getValue();
    window.location.href = "../ComplianceCapture/CaptureFollowUpForm4D?Id=" + _ID;
    
}

function reloadGrid() {
    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", true);

    var Search_TypeID = $("#SearchTypeID").val();
    var Search_Keyword = $("#SearchKeyword").val();
    showLoading(true);

    mygrid.clearAndLoad("/ComplianceCapture/FetchForm4DFormList?RecCount=" + page_count + "&isSearch=1&SearchTypeID=" + Search_TypeID + "&SearchKeyword=" + Search_Keyword);
    
    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", false);
}

