﻿var gridHeader = 'ColumnID,User<span class="HeaderChange">_</span>ID,User<span class="HeaderChange">_</span>Name,First<span class="HeaderChange">_</span>Name,Last<span class="HeaderChange">_</span>Name,Email,Mobile,Edit';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,img';
var page_count = 10;

function createWindow(_UserID) {
    var selectedRow = mygrid.getSelectedId();
    _UserID = mygrid.cells(selectedRow, 1).getValue();
    post('/Security/EditUserRegister', { UserID: _UserID });
}

$(document).ready(function () {
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("DHTMLX/codebase/img/");
    mygrid.setInitWidths("0,0,130,120,120,180,120,70");
    mygrid.setColAlign("right, right, left, left, left, left, left, left");
    mygrid.setHeader(gridHeader);
    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");
    mygrid.attachEvent("onRowSelect", fnGridRowSelected);

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();
    mygrid.loadXML("/Security/GridUsersData?RecCount=" + page_count);

    dhtmlxError.catchError("ALL", my_error_handler);
    $('#loading').hide();
});

function fnGridRowSelected() {

}
function my_error_handler() {
}

function reloadGrid() {
    var Search_TypeID = $("#SearchTypeID").val();
    var Search_Keyword = $("#SearchKeyword").val();
    //showLoading(true)
    //mygrid.clearAndLoad("../XmlData/xmlGridUsersData.aspx?RecCount=" + page_count + "&isSearch=1&SearchTypeID=" + Search_TypeID + "&SearchKeyword=" + Search_Keyword);
    //if (window.a_direction)
    //    mygrid.setSortImgState(true, window.s_col, window.a_direction);
}
