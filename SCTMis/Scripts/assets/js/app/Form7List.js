﻿var page_count = 10;
// ColumnID,RetargetingHeaderID,HouseHoldIDNumber,NameOfHouseHoldHead,Members,Kebele,Gote/Gare,FiscalYear,CreatedBy,Status
var gridHeader = 'ColumnID,RetargetingHeaderID,PSNP<span class="HeaderChange">_</span>Number,HouseHold<span class="HeaderChange">_</span>Head,Members,';
gridHeader = gridHeader + 'Kebele,Gote/Gare,F<span class="HeaderChange">_</span>Year,Creator,Status,Edit,Verify';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img';
var mygrid;

function createWindow(_RetargetingHeaderID) {

    var selectedRow = mygrid.getSelectedId();
    _RetargetingHeaderID = mygrid.cells(selectedRow, 1).getValue();
    post('../ReTargeting/ModifyForm7', { retargetingHeaderID: _RetargetingHeaderID });
}

function createVerifyWindow(_RetargetingHeaderID) {

    var selectedRow = mygrid.getSelectedId();
    _RetargetingHeaderID = mygrid.cells(selectedRow, 1).getValue();
    post('../ReTargeting/ApproveForm7', { retargetingHeaderID: _RetargetingHeaderID });
}

function PreviewPdfFile(_RetargetingHeaderID) {
    var selectedRow = mygrid.getSelectedId();
    _RetargetingHeaderID = mygrid.cells(selectedRow, 1).getValue();
    
    get('../ReTargeting/PrintForm7', { retargetingHeaderID: _RetargetingHeaderID });
}

$(document).ready(function () {
    $('#loading').hide();
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,110,130,70,100,90,70,70,65,35,70");
    mygrid.setColAlign("left,left,left,right,left,right,right,right,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    //mygrid.loadXML("/Household/Form1AHHList?RecCount=" + page_count);

    // On edit of a record, user want's the grid to show the edited records page
    var currentPage = $('#currentPage').val();
    var kebeleID = $("#KebeleID").val();
    mygrid.load("/ReTargeting/Form7List?RecCount=" + page_count + "&KebeleID=" + kebeleID, function () {
        mygrid.changePage(currentPage);
    })

    mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
        $('#currentPage').val(ind);

        $.ajax({
            type: "POST",
            url: "../ReTargeting/UpdateCurrentPage",
            data: "{ 'CurrentPage':" + ind + "}",
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        });
    });
    //end here, user edit page demand

    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Household ID'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Household Head'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Kebele'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Gote'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Gare'));

    $("#cmdBack").click(function () {
        window.location.href = "/ReTargeting/Form7Summary";
    });
});

function reloadGrid() {
    var kebeleID = $("#KebeleID").val();    
    showLoading(true);

    mygrid.clearAndLoad("/ReTargeting/Form7List?RecCount=" + page_count + "&KebeleID=" + kebeleID);
}


