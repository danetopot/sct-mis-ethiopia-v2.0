﻿
var EVENTID = null;
var arrObj = [];
var SerialCount = 0;
var mycalStartTDS, mycalSEndTDS;
$(document).ready(function () {

    $('#loading').hide();

    $('#CollectionDate').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        //direction: ['2012-08-01', '2012-08-12']
        format: 'd/M/Y'
    });

    $('#BabyDateOfBirth').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y',
        view: 'years'
    });

    $('#EndDateTDS').Zebra_DatePicker({
        //direction: +1,    // boolean true would've made the date picker future only
        format: 'M/Y',
        view: 'years'
    });

    $('#StartDateTDS').Zebra_DatePicker({
        //direction: 1,    // boolean true would've made the date picker future only
        //direction: -1,
        format: 'M/Y',
        view: 'years'
    });

    $("#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember").prop("disabled", false);
    $("#CBHINumber").prop("disabled", true);
    $("#PLW").val("P");
    $("#PLW option[value='L']").attr('disabled', true);

    $("#RegionID,#WoredaID").attr("disabled", true);
    $("#KebeleID,#Gote,#NameOfPLW,#HouseHoldIDNumber,#Gare,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#Remarks,#CBHINumber").val("");
    $("#SocialWorker,#KebeleID").val("0");
    $("#BabySex,#EndDateTDS,#CollectionDate,#CCCCBSPCMember").val("");
    $("#NutritionalStatusPLW,#CBHIMembership,#ChildProtectionRisk").val("-");
    $('#KebeleID').focus();

    $("#cmdBack").click(function () {
        window.location.href = "/Household/Form1B";
    });

    $('#KebeleID,#CollectionDate,#SocialWorker,#CCCCBSPCMember').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

   $('#CCCCBSPCMember').bind('focusout', function (event) {
        if (isValid($(this).val())) {
            if($("#CBHINumber").is(":disabled")){
                $('#RegionID,#WoredaID').prop("disabled", true);
                $("#Kebele").val($("#KebeleID").val());
                $("#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant,#Remarks,#ChildProtectionRisk,#CBHIMembership").prop("disabled", false);
                $('#NameOfPLW').focus();
            }
            else
            {
                $("#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant,#Remarks,#ChildProtectionRisk,#CBHIMembership").prop("disabled", false);
                $('#CBHINumber').focus();
            }
        }
    });

    $('#CBHINumber').bind('focusout', function (event) {
        $('#RegionID,#WoredaID').prop("disabled", true);
        $("#Kebele").val($("#KebeleID").val());
        $('#NameOfPLW').focus();
        $('#cmdProceed').prop("disabled", false);
    });
    


    $('#CollectionDate').change(function (e) {
        if (isValid($(this).val())) {
            $('#SocialWorker').focus();
        } else { $('#CollectionDate').focus(); }
    });

    $("#CollectionDate").focusout(function () {
        if (isValid($(this).val())) {
            $('#SocialWorker').focus();
        } else { $('#CollectionDate').focus(); }
    });

    //#CBHIMembership
    $('#CBHIMembership').change(function (e) {
        var membership = $('#CBHIMembership').val();
        if (membership== 'YES') {
            $('#CBHINumber').prop("disabled", false);
        } else {
            $('#CBHINumber').prop("disabled", true);
            $('#CBHINumber').val("");
        }
    });


    $("#cmdProceed").click(function () {
        //EVENTID = "ADD";
        var errors = [];
        var html = '<ul>';
        valid = true;
        var errorMsg = '';
        $('#errors2').empty();
        
        
        if ($('#PLW').val() == '') {
            alert($('#PLW').val());
            errorMsg = 'Pregnant or Lactating is Required';
            valid = false;
        }
        

        if ($('#NameOfPLW').val() == '') {
            //errors.push('<li>PLW Name is Required</li>');
            errorMsg = 'PLW Name is Required';
            valid = false;
        }
        
        if ($('#HouseHoldIDNumber').val() == '') {
            //errors.push('<li>Household ID Number is required</li>');
            errorMsg = errorMsg + ' | Household ID Number is required';
            valid = false;
        }

        if ($('#MedicalRecordNumber').val() == '') {
            //errors.push('<li>Medical Record Number Required</li>');
            errorMsg = errorMsg + ' | Medical Record Number Required';
            valid = false;
        }

        if (isNaN($('#PLWAge').val()) == true) {
            //errors.push('<li>Age Of PLW Should be numeric</li>');
            errorMsg = errorMsg + ' | Age Of PLW Should be numeric';
            valid = false;
        }

        if ($('#PLWAge').val() == '') {
            //errors.push('<li>Age of PLW required</li>');
            errorMsg = errorMsg + ' | Age of PLW required';
            valid = false;
        }

        if ($('#PLWAge').val() < 15) {
            //errors.push('<li>Age of PLW required</li>');
            errorMsg = errorMsg + ' | PLW age should be between 15 and 60 years';
            valid = false;
        }

        if ($('#PLWAge').val() > 46) {
            //errors.push('<li>Age of PLW required</li>');
            errorMsg = errorMsg + ' | PLW age should be between 15 and 60 years';
            valid = false;
        }

        if ($('#StartDateTDS').val() == '') {
           // errors.push('<li>Start of being temporary DS client Required</li>');
            errorMsg = errorMsg + ' | Start of being temporary DS client Required';
            valid = false;
        }

        if (jsDateDiff("01/" + $('#StartDateTDS').val(), $('#CollectionDate').val(), "months") > 20) {
            errors.push('<li>TDS start date cannot be earlier than 20 months before date of data collection </li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#StartDateTDS').val(), "months") > 1) {
            errors.push('<li>TDS start date cannot be later than 1 month after date of data collection</li>');
            valid = false;
        }

        if ($('#EndDateTDS').val() == '') {
            //errors.push('<li>Date when temporary DS status will end Required</li>');
            errorMsg = errorMsg + ' | Date when temporary DS status will end Required';
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#EndDateTDS').val(), "days") > 730) {
            errors.push('<li>Temporary DS end date cannot be more than 2 years</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#EndDateTDS').val(), "days") < 1) {
            errors.push('<li>Temporary DS end date cannot be a past date from collection date</li>');
            valid = false;
        }

        if ($('#NutritionalStatusPLW').val() == '') {
            //errors.push('<li>Nutritional status of PLW required</li>');
            errorMsg = errorMsg + ' | Nutritional status of PLW is required';
            valid = false;
        }
        if ($('#CollectionDate').val() == '') {
            //errors.push('<li>Please collected Date</li>');
            errorMsg = errorMsg + ' | Please select collected Date';
            valid = false;
        }
        
        if ($('#SocialWorker').val() == '') {
            //errors.push('<li>Please select Social Worker</li>');
            errorMsg = errorMsg + ' | Please select Social Worker';
            valid = false;
        }

        if ($('#CCCCBSPCMember').val() == '') {
            //errors.push('<li>CCC/CBSPC member required</li>');
            errorMsg = errorMsg + ' | CCC/CBSPC member is required';
            valid = false;
        } 

        if ($('#CBHIMembership').val() ==null) {
            //errors.push('<li>CCC/CBSPC member required</li>');
            errorMsg = errorMsg + ' | CBHI Membership is required';
            valid = false;
        }


        if ($('#CBHIMembership').val() == 'YES') {
            if ($('#CBHINumber').val() == '') {
                //errors.push('<li>CCC/CBSPC member required</li>');
                errorMsg = errorMsg + ' | CBHI Number is required';
                valid = false;
            }
        }

        if ($('#ChildProtectionRisk').val() ==null) {
                //errors.push('<li>CCC/CBSPC member required</li>');
                errorMsg = errorMsg + ' | Potential Child Protection Risk is required';
                valid = false;
            }
            

        if (!valid) {
            errors.push(errorMsg);
            html += errors.join('') + '</ul>'
            $('#errors2').show();
            $('#errors2').append(html);
            return valid;
        }
        else {
            $('#errors2').hide();
        }

        

        $("#cmdSave").prop("disabled", false);

        EVENTID = null;
       
    });

});



