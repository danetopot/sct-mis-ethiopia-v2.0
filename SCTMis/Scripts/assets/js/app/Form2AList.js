﻿var page_count = 10;
var gridHeader = 'ColumnID,CoRespDSHeaderID,Region,Woreda,Kebele,Clients,Generated<span class="HeaderChange">_</span>By,Generated<span class="HeaderChange">_</span>ON,ReportFileName, Preview';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,img';
var mygrid;

$(document).ready(function () {

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,125,125,120,100,110,110,0,70");
    mygrid.setColAlign("left,left,left,right,right,right,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    mygrid.loadXML("/CoResponsibility/Form2AListAdmin?RecCount=" + page_count);
    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Region'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Woreda'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Kebele'));

    $("#cmdNew").click(function () {
        window.location.href = "../CoResponsibility/ProduceForm2A";
    });
    $('#loading').hide();
});

function PreviewPdfFile(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 8).getValue();
    console.log(_ID);
    get('/Home/downloadpdf', { GenerationID: _ID });
}
