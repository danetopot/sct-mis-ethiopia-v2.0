﻿$(document).ready(function () {
    $("#UserName").prop("readonly", true);
    $("#cmdCancel").click(function () {
        window.location.href = "/Security/Users";
    });
    $("#cmdPassword").click(function () {        
        var _UserID = $("#UserID").val();
        post('/Security/EditPassword', { UserID: _UserID });
    });
    //var isValid = $("#MyForm").valid();
    $('#loading').hide();
});
