﻿
$(document).ready(function () {
    $('#StartDateTDS,#EndDateTDS').datepicker({ dateFormat: "M/yy", changeMonth: true, changeYear: true }).inputmask('mm/yyyy');
    $('#BabyDateOfBirth,#CollectionDate').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        maxDate: '-1D',
        "showAnim": 'fold'
    }).inputmask('mm/dd/yyyy');

    
    //$("#CollectionDate,#StartDateTDS,#EndDateTDS,#BabyDateOfBirth").attr("readonly", true);

    $("#RegionID,#WoredaID,#KebeleID").prop("disabled", true);
    $("#Gote,#Gare,#CollectionDate,#SocialWorker,#CCCCBSPCMember,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#BabyDateOfBirth,#BabyName,#BabySex,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant").prop("disabled", true);
    
    if ($("#AllowEdit").val() == 'True') {
        $("#cmdSave").prop("disabled", false);
        $("#cmdSave").val('Approve Details');
    } else {
        $("#cmdSave").prop("disabled", true);        
        $("#cmdSave").val('Approve Already Done');
    }
    $("#cmdBack").click(function () {
        window.location.href = "/Household/Form1B";
    });

    $('#loading').hide();
});