﻿var page_count = 10;
var gridHeader = 'ColumnID,ProfileDSHeaderID,Kebele,Reporting<span class="HeaderChange">_</span>Period,';
gridHeader = gridHeader + 'PLW<span class="HeaderChange">_</span>Name,HouseHold<span class="HeaderChange">_</span>ID,Action<span class="HeaderChange">_</span>Taken';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro';
var mygrid;

$(document).ready(function () {

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,130,130,200,110,200");
    mygrid.setColAlign("left,left,left,right,left,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    mygrid.loadXML("/MonitoringCapture/CapturedForm5BList?RecCount=" + page_count);
    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Kebele'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Reporting Period'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('PLW Name'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Household ID'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Action Taken'));

    $("#cmdNew").click(function () {
        window.location.href = "../MonitoringCapture/CaptureForm5B";
    });
    $('#loading').hide();
});