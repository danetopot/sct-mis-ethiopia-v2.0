﻿var page_count = 10;
var gridHeader =
    'UniqueID,ChildProtectionVisitHeaderId,ChildProtectionHeaderID,ChildDetailID,ClientTypeID,Region,Woreda,Kebele,KebeleID,Visit<span class="HeaderChange">_</span>Date,HouseHold<span class="HeaderChange">_</span>Head,HouseHold Member<span class="HeaderChange">_</span>Name,Household Member<span class="HeaderChange">_</span>Age,Household Member<span class="HeaderChange">_</span>Sex,Social<span class="HeaderChange">_</span>Worker,Fiscal Year,Edit';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img';
var mygrid;

$(document).ready(function () {
    $('#loading').show();
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,0,0,0,100,100,100,0,80,180,180,100,100,110,90,60");
    mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,justify,justify,justify,justify");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    mygrid.loadXML("/ComplianceCapture/FetchForm4DFollowUpList?RecCount=" + page_count);
    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('NameOfHouseHoldHead'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('HouseholdMemberName'));

    $("#cmdBack").click(function () {
        window.location.href = "../ComplianceCapture/Form4DHHList";
    });

    $("#cmdForms").click(function () {
        window.location.href = "../ComplianceCapture/Form4DFormList";
    });

    $('#loading').hide();
});


function createWindow(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 2).getValue();
    window.location.href = "../ComplianceCapture/ModifyFollowUpForm4D?Id=" + _ID;
}

function reloadGrid() {
    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", true);

    var Search_TypeID = $("#SearchTypeID").val();
    var Search_Keyword = $("#SearchKeyword").val();
    showLoading(true);

    mygrid.clearAndLoad("/ComplianceCapture/FetchForm4DFollowUpList?RecCount=" + page_count + "&isSearch=1&SearchTypeID=" + Search_TypeID + "&SearchKeyword=" + Search_Keyword);
    
    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", false);
}

