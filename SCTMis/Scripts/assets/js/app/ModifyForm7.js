﻿var EVENTID = null;
var arrObj = [];
var SerialCount = 0;
var page_count = 5;
var mygrid;
var gridHeader = 'ColumnID,RetargetingDetailID,Member<span class="HeaderChange">_</span>Name,Individual<span class="HeaderChange">_</span>ID';
gridHeader = gridHeader + ',DOB,Age,Sex,Nutritional<span class="HeaderChange">_</span>Status,Category,Status';
gridHeader = gridHeader + ',Status,Pregnant,Lactating,Handicapped,ChronicallyIll,NutritionalStatus,ChildUnderTSForCMAM,EnrolledInSchool,SchoolName';
gridHeader = gridHeader + ',Edit';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img';

//var myDOB, myColDate;
$(document).ready(function () {
    $('#DateUpdated').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y'
    });

    $('#DateOfBirth').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y',
        //view: 'years',
        onChange: function (view, elements) {
            DobChange();
        }
    });

    if ($('#Approving').val() == 'True') {
        $("#cmdModify").html('Approve Form 7');
        $("#cmdProceed,#cmdNewMember").hide();
    } else {
        $("#cmdModify").html('Save Modified Details');
        $("#cmdProceed,#cmdNewMember").show();
    }

    if ($("#AllowEdit").val() == 'True') {
        $("#DateUpdated,#SocialWorker,#CCCCBSPCMember").prop("disabled", false);
    } else {
        $("#DateUpdated,#SocialWorker,#CCCCBSPCMember").prop("disabled", true);
    }

    $("#RegionName,#WoredaName,#KebeleName,#Gote,#Gare,#FiscalYear").prop("disabled", true); //always disabled
    $("#NameOfHouseHoldHead,#HouseHoldIDNumber").prop("disabled", true); //always disabled  
    $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#ClientCategoryName").prop("disabled", true); //always disabled

    $("#Status,#Pregnant,#Lactating,#Handicapped,#ChronicallyIll,#NutritionalStatus,#ChildUnderTSForCMAM,#EnrolledInSchool,#Grade,#SchoolName").prop("disabled", true); //enable on edit

    $("#Status,#Pregnant,#Lactating,#Handicapped,#ChronicallyIll,#NutritionalStatus,#ChildUnderTSForCMAM,#EnrolledInSchool").val("-"); //enable on edit
    
    var age = getAge($('#DateOfBirth').val());
    if (!(isNaN(age))) {
        $("#Age").val(age);
    }

    $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", true);
    if ($('#Approving').val() == 'True') {
        $("#cmdModify").prop("disabled", false);
    }
        
    $('#loading').hide();
    //RetargetingHeaderID
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,150,110,0,50,65,155,140,110,0,0,0,0,0,0,0,0,0,40");
    mygrid.setColAlign("left,left,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();
    mygrid.loadXML("/ReTargeting/FetchGridForm7DetailsByID?RecCount=" + page_count + "&RetargetingHeaderID=" + $("#RetargetingHeaderID").val());
    dhtmlxError.catchError("ALL", my_error_handler);

    $.ajax({
        type: "POST",
        url: "../ReTargeting/FetchJsonForm7DetailsByID",
        data: "{  'RecCount':" + page_count + ",RetargetingHeaderID : '" + $("#RetargetingHeaderID").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            //alert(yy);
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                MemberCount = data.length;
                if (MemberCount != undefined) {

                    for (var i = 0; i < data.length; i++) {
                        var theDtls = new Object;
                        var d = data[i];
                        SerialCount = SerialCount + 1;
                        theDtls.ColumnID = SerialCount;
                        theDtls.RetargetingDetailID = d.RetargetingDetailID;
                        theDtls.HouseHoldMemberName = d.HouseHoldMemberName;
                        theDtls.IndividualID = d.IndividualID;
                        theDtls.DateOfBirth = d.DateOfBirth;
                        theDtls.Age = getAge(d.DateOfBirth);
                        theDtls.Sex = d.Sex;
                        theDtls.NutritionalStatusName = d.NutritionalStatusName;
                        theDtls.ClientCategoryName = d.ClientCategoryName;
                        theDtls.StatusName = d.StatusName;
                        theDtls.Pregnant = d.Pregnant;
                        theDtls.Lactating = d.Lactating;
                        theDtls.Handicapped = d.Handicapped;
                        theDtls.ChronicallyIll = d.ChronicallyIll;
                        theDtls.ChildUnderTSForCMAM = d.ChildUnderTSForCMAM;
                        theDtls.EnrolledInSchool = d.EnrolledInSchool;
                        theDtls.SchoolName = d.SchoolName;
                        theDtls.NutritionalStatus = d.NutritionalStatus;
                        theDtls.ClientCategory = d.ClientCategory;
                        theDtls.Status = d.Status;

                        arrObj.push(theDtls);
                    }
                    $("#MemberXml").val(JSON.stringify(arrObj));
                }
            }
        }
    });
    
    $("#cmdBack").click(function () {
        var kebeleID = $("#KebeleID").val();
        window.location.href = "/ReTargeting/Form7?KebeleID=" + kebeleID;
    });
        
    $('#KebeleID,#NameOfHouseHoldHead,#HouseHoldIDNumber,#CollectionDate,#SocialWorker,#CCCCBSPCMember').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    $('#HouseHoldMemberName,#DateOfBirth,#Handicapped,#ChronicallyIll,#NutritionalStatus').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });
    
    $("#Status").change(function (e) {        
        DobChange();
    });

    $("#Pregnant,#Lactating").change(function (e) {        
        DobChange();
    });

    $("#DateUpdated,#SocialWorker").change(function (e) {
        $("#cmdModify").prop("disabled", false);
    });

    //#EnrolledInSchool
    $('#EnrolledInSchool').change(function (e) {
        var TheAge = getAge($('#DateOfBirth').val());

        if ($('#EnrolledInSchool').val() !== 'YES') {
            $('#Grade,#SchoolName').prop("disabled", true);
        } else {
            $('#Grade,#SchoolName').prop("disabled", false);
        }
    });

    $("#cmdNewMember").click(function () {
        EVENTID = "ADD";
        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#Grade").val("");

        $("#Handicapped,#ChronicallyIll,#NutritionalStatus,#ChildUnderTSForCMAM,#EnrolledInSchool,#SchoolName").val("-");
        $("#cmdNewMember").prop("disabled", true);
        $("#cmdProceed").prop("disabled", false);
        $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#ChildUnderTSForCMAM,#DateOfBirth,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName").prop("disabled", false);

        $('#HouseHoldMemberName').focus();
    });

    $("#cmdProceed").click(function () {
        //EVENTID = "ADD";
        var errors = [];
        var html = '<ul>';
        valid = true;
        $('#errors2').empty();
        if ($('#Status').val() == '' || $('#Status').val() == null) {
            errors.push('<li>Status is Required</li>');
            valid = false;
        }               
        
        var status = $("#Status").val();

        if (status == 1) { // alive
            if ($('#Handicapped').val() == '' || $('#Handicapped').val() == null) {
                errors.push('<li>Please specify if Handicapped</li>');
                valid = false;
            }

            if ($('#ChronicallyIll').val() == '' || $('#ChronicallyIll').val() == null) {
                errors.push('<li>Please specify if Chronically Ill</li>');
                valid = false;
            }

            //if age going age
            var TheAge = getAge($('#DateOfBirth').val());
            $("#Age").val(TheAge);
            if (parseInt(TheAge) > 5 && parseInt(TheAge) < 18) {
                if ($('#EnrolledInSchool').val() == '' || $('#EnrolledInSchool').val() == null) {
                    errors.push('<li>Please specify if Enrolled in school</li>');
                    valid = false;
                }

                var enrolledInSchool = $("#EnrolledInSchool").val();
                if (enrolledInSchool == 'YES') {
                    if ($('#Grade').val() == '' || $('#Grade').val() == null) {
                        errors.push('<li>Please specify if Grade</li>');
                        valid = false;
                    }

                    if ($('#SchoolName').val() == '' || $('#SchoolName').val() == null) {
                        errors.push('<li>Please specify if School Name</li>');
                        valid = false;
                    }
                }
            }

            // if female and child bearing age
            if (($("#Sex").val() == "F")) {
                if (parseInt(TheAge) >= 12 && parseInt(TheAge) <= 50) {
                    if ($('#Pregnant').val() == '' || $('#Pregnant').val() == null) {
                        errors.push('<li>Please specify if Pregnant</li>');
                        valid = false;
                    }

                    if ($('#Lactating').val() == '' || $('#Lactating').val() == null) {
                        errors.push('<li>Please specify if Lactating</li>');
                        valid = false;
                    }

                    if (($("#Pregnant").val() == "YES" || $("#Lactating").val() == "YES")) {
                        if ($('#NutritionalStatus').val() == '' || $('#NutritionalStatus').val() == null) {
                            errors.push('<li>Please specify Nutritional Status</li>');
                            valid = false;
                        }
                    }
                }
            }

            // if children under age 5
            if (parseInt(TheAge) <= 5) {
                if ($('#NutritionalStatus').val() == '' || $('#NutritionalStatus').val() == null) {
                    errors.push('<li>Please specify Nutritional Status</li>');
                    valid = false;
                }

                if ($("#NutritionalStatus").val() == "M" || $("#NutritionalStatus").val() == "SM") {
                    if ($('#ChildUnderTSForCMAM').val() == '' || $('#ChildUnderTSForCMAM').val() == null) {
                        errors.push('<li>Please specify if Child is Under TSF or CMAM</li>');
                        valid = false;
                    }
                }
            }            
        }

        if (!valid) {
            html += errors.join('') + '</ul>'
            $('#errors2').show();
            $('#errors2').append(html);
            return valid;
        }
        else {
            $('#errors2').hide();
        }

        var theDtls = new Object;
        if (EVENTID == "EDIT") {
            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == $("#ColumnID").val()) {
                        arrObj[i].RetargetingDetailID = $("#RetargetingDetailID").val();
                        arrObj[i].HouseHoldMemberName = $("#HouseHoldMemberName").val();
                        arrObj[i].IndividualID = $("#IndividualID").val();                        
                        arrObj[i].DateOfBirth = $("#DateOfBirth").val();
                        arrObj[i].Age = $("#Age").val();
                        arrObj[i].Sex = $("#Sex").val();
                        arrObj[i].SexName = $("#Sex").find('option:selected').text();
                        arrObj[i].NutritionalStatus = $("#NutritionalStatus").val();
                        arrObj[i].NutritionalStatusName = $("#NutritionalStatus").find('option:selected').text();
                        arrObj[i].ClientCategory = $("#ClientCategory").val();
                        arrObj[i].ClientCategoryName = $("#ClientCategoryName").val();
                        arrObj[i].Status = $("#Status").val();
                        arrObj[i].StatusName = $("#Status").find('option:selected').text();
                        arrObj[i].Pregnant = $("#Pregnant").val();
                        arrObj[i].Lactating = $("#Lactating").val();
                        arrObj[i].Handicapped = $("#Handicapped").val();
                        arrObj[i].HandicappedName = $("#Handicapped").find('option:selected').text();
                        arrObj[i].ChronicallyIll = $("#ChronicallyIll").val();
                        arrObj[i].ChronicallyIllName = $("#ChronicallyIll").find('option:selected').text();                        
                        arrObj[i].ChildUnderTSForCMAM = $("#ChildUnderTSForCMAM").val();
                        arrObj[i].EnrolledInSchool = $("#EnrolledInSchool").val();
                        arrObj[i].EnrolledInSchoolName = $("#EnrolledInSchool").find('option:selected').text();                        
                        arrObj[i].SchoolName = $("#SchoolName").val();
                    }
                }
            }
        }
        else {
            $('#errors2').hide();

            SerialCount = SerialCount + 1;
            theDtls.ColumnID = SerialCount;
            theDtls.RetargetingDetailID = $("#RetargetingDetailID").val();
            theDtls.HouseHoldMemberName = $("#HouseHoldMemberName").val();
            theDtls.IndividualID = $("#IndividualID").val();            
            theDtls.DateOfBirth = $("#DateOfBirth").val();
            theDtls.Age = $("#Age").val();
            theDtls.Sex = $("#Sex").val();
            theDtls.SexName = $("#Sex").find('option:selected').text();
            theDtls.NutritionalStatus = $("#NutritionalStatus").val();
            theDtls.NutritionalStatusName = $("#NutritionalStatus").find('option:selected').text();
            theDtls.ClientCategory = $("#ClientCategory").val();
            theDtls.ClientCategoryName = $("#ClientCategoryName").val();
            theDtls.Status = $("#Status").val();
            theDtls.StatusName = $("#Status").find('option:selected').text();
            theDtls.Pregnant = $("#Pregnant").val();
            theDtls.Lactating = $("#Lactating").val();
            theDtls.Handicapped = $("#Handicapped").val();
            theDtls.HandicappedName = $("#Handicapped").find('option:selected').text();
            theDtls.ChronicallyIll = $("#ChronicallyIll").val();
            theDtls.ChronicallyIllName = $("#ChronicallyIll").find('option:selected').text();            
            theDtls.ChildUnderTSForCMAM = $("#childUnderTSForCMAM").val();
            theDtls.EnrolledInSchool = $("#EnrolledInSchool").val();
            theDtls.EnrolledInSchoolName = $("#EnrolledInSchool").find('option:selected').text();
            theDtls.SchoolName = $("#SchoolName").val();      

            arrObj.push(theDtls);
        }
        var myXml = createXmlstring(arrObj, 1);
        mygrid.clearAll();
        mygrid.parse(myXml);

        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#ChildUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#SchoolName").val('');
        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#ChildUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#SchoolName").prop("disabled", true);

        if ($("#AllowEdit").val() == 'True') {
            if ($('#Approving').val() == 'True') { //if approving then we need all SW, CCCMember # and Data Updated
                if (validateHeader()) {
                    $("#cmdModify").prop("disabled", false);
                }                
            } else {
                $("#cmdModify").prop("disabled", false);
            }
        }

        $("#cmdProceed").prop("disabled", true);
    });
});

function createWindow(_ColumnID) {
    var selectedRow = mygrid.getSelectedId();
    _ColumnID = mygrid.cells(selectedRow, 0).getValue();
    
    $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#ChildUnderTSForCMAM,#DateOfBirth,#Age,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#SchoolName").val('')
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            if (arrObj[i].ColumnID == _ColumnID) {
                $("#ColumnID").val(arrObj[i].ColumnID);
                $("#RetargetingDetailID").val(arrObj[i].RetargetingDetailID);
                $("#HouseHoldMemberName").val(arrObj[i].HouseHoldMemberName);
                $("#IndividualID").val(arrObj[i].IndividualID);
                $("#DateOfBirth").val(arrObj[i].DateOfBirth);
                $("#Age").val(arrObj[i].Age);
                $("#Sex").val(arrObj[i].Sex);
                $("#ClientCategoryName").val(arrObj[i].ClientCategoryName);
                $("#Status").val(arrObj[i].Status);
                $("#Pregnant").val(arrObj[i].Pregnant);
                $("#Lactating").val(arrObj[i].Lactating);
                $("#Handicapped").val(arrObj[i].Handicapped);
                $("#ChronicallyIll").val(arrObj[i].ChronicallyIll);
                $("#NutritionalStatus").val(arrObj[i].NutritionalStatus);
                $("#ChildUnderTSForCMAM").val((arrObj[i].ChildUnderTSForCMAM == "N/A" ? "-" : arrObj[i].ChildUnderTSForCMAM));
                $("#EnrolledInSchool").val((arrObj[i].EnrolledInSchool == "N/A" ? "-" : arrObj[i].EnrolledInSchool));
                $("#SchoolName").val(arrObj[i].SchoolName);

                $('#Sex').change();
                $('#DateOfBirth').change();
            }
        }
    }

    $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", true);
    if ($("#AllowEdit").val() == 'True') {
        $("#cmdProceed,#cmdNewMember").prop("disabled", false);
        $('#HouseHoldMemberName').focus();

        $("#Status").prop("disabled", false);
    }
    
    if ($('#Approving').val() == 'True') {
        $("#cmdModify").prop("disabled", false);
    }

    $('#EnrolledInSchool').change();
    EVENTID = "EDIT";
}

function validateHeader() {
    var errors = [];
    var html = '<ul>';
    valid = true;
    $('#errors2').empty();

    if ($('#DateUpdated').val() == '' || $('#DateUpdated').val() == null) {
        errors.push('<li>Date Updated is Required</li>');
        valid = false;
    }

    if ($('#SocialWorker').val() == '' || $('#SocialWorker').val() == null) {
        errors.push('<li>Social Worker is Required</li>');
        valid = false;
    }

    if ($('#CCCCBSPCMember').val() == '' || $('#CCCCBSPCMember').val() == null) {
        errors.push('<li>CCCC/BSPC Member number is Required</li>');
        valid = false;
    }

    if (!valid) {
        html += errors.join('') + '</ul>'
        $('#errors2').show();
        $('#errors2').append(html);

    }
    else {
        $('#errors2').hide();
    }

    return valid;
}

function UpdateMembers(_ColumnID) {
    createWindow(_ColumnID);
}

function createXmlstring(arrObject, inMemory) {
    var xml;
    var gridID;
    var reclength = 0;
    xml = '';
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            reclength + reclength + 1;
            xml = xml + '<row id="' + i + '">';
            for (var j in arrObj[i]) {
                if (arrObj[i].hasOwnProperty(j)) {
                    if (j == "ColumnID") {
                        xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        gridID = arrObj[i][j];
                    }
                }
                if (j == "RetargetingDetailID" || j == "HouseHoldMemberName" || j == "IndividualID" || j == "DateOfBirth"
                    || j == "Age" || j == "Sex" || j == "NutritionalStatusName" || j == "ClientCategoryName" || j == "StatusName"
                    || j == "Status" || j == "Pregnant" || j == "Lactating" || j == "Handicapped" || j == "ChronicallyIll"
                    || j == "NutritionalStatus" || j == "ChildUnderTSForCMAM" || j == "EnrolledInSchool" || j == "SchoolName") {
                    xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                }
            }
            xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
            xml = xml + '</row>';
        }
    }
    xml = '<rows total_count="' + reclength + '">' + xml + '</rows>';
    $("#MemberXml").val(JSON.stringify(arrObject));
    return xml;
}

function DobChange() {
    $("#Pregnant,#Lactating,#Handicapped,#ChronicallyIll,#NutritionalStatus,#ChildUnderTSForCMAM,#EnrolledInSchool,#Grade,#SchoolName").attr('readonly', true);
    $("#Pregnant,#Lactating,#Handicapped,#ChronicallyIll,#NutritionalStatus,#ChildUnderTSForCMAM,#EnrolledInSchool,#Grade,#SchoolName").prop('disabled', true);

    var status = $("#Status").val();    
    
    if (status != 1) { // alive
        return;
    }

    $('#Handicapped,#ChronicallyIll').attr('readonly', false);
    $('#Handicapped,#ChronicallyIll').prop('disabled', false);
        
    $("#NutritionalStatus option[value='N']").attr("disabled", "disabled");
    $("#NutritionalStatus option[value='M']").attr("disabled", "disabled");
    $("#NutritionalStatus option[value='SM']").attr("disabled", "disabled");

    $("#ChronicallyIll option[value='-']").attr("disabled", "disabled");
    $("#Handicapped option[value='-']").attr("disabled", "disabled");

    var TheAge = getAge($('#DateOfBirth').val());

    $("#Age").val(TheAge);

    $('#EnrolledInSchool,#SchoolName').attr('readonly', true);
    $('#EnrolledInSchool,#SchoolName').prop('disabled', true);

    if (parseInt(TheAge) > 5 && parseInt(TheAge) < 18) {
        $('#EnrolledInSchool,#SchoolName').attr('readonly', false);
        $('#EnrolledInSchool,#SchoolName').attr('disabled', false);

        $("#ChildUnderTSForCMAM").val("-");
        $('#ChildUnderTSForCMAM').attr('readonly', true);
        $("#ChildUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
        $("#ChildUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

        $("#EnrolledInSchool").attr("disabled", false);
        $("#EnrolledInSchool").attr("disabled", false);
    }

    //THIS IS FOR PREGNANT WOMEN ABOVE 12 YEARS BUT NOT OLDER THAN 50 YEARS
    if (parseInt(TheAge) >= 12 && parseInt(TheAge) <= 50) {
        // if female the enable pregnant and lactating
        if (($("#Sex").val() == "F")) {
            $('#Pregnant,#Lactating').attr('readonly', false);
            $('#Pregnant,#Lactating').prop('disabled', false);
        }

        if (($("#Sex").val() == "F") && ($("#Pregnant").val() == "YES" || $("#Lactating").val() == "YES")) {

            $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM').attr('readonly', true);
            $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM').prop('disabled', true);
            $('#EnrolledInSchool,#Grade,#ChildUnderTSForCMAM').val('-');

            $("#EnrolledInSchool,#Grade,#ChildUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#EnrolledInSchool,#Grade,#ChildUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

            $("#NutritionalStatus option[value='N']").attr("disabled", false);
            $("#NutritionalStatus option[value='M']").attr("disabled", false);
            $("#NutritionalStatus option[value='SM']").attr("disabled", false);

            $('#NutritionalStatus').attr('readonly', false);
            $('#NutritionalStatus').attr('disabled', false);
        }
    }
    //FOR INFANTS BELOW 5 YEARS
    if (parseInt(TheAge) <= 5) {
        $('#EnrolledInSchool,#Grade,#SchoolName,#ChildUnderTSForCMAM,#NutritionalStatus').attr('readonly', true);
        $('#EnrolledInSchool,#Grade,#SchoolName,#ChildUnderTSForCMAM,#NutritionalStatus').prop('disabled', true);

        $("#EnrolledInSchool,#Grade,#ChildUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
        $("#EnrolledInSchool,#Grade,#ChildUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

        $("#NutritionalStatus option[value='N']").attr("disabled", false);
        $("#NutritionalStatus option[value='M']").attr("disabled", false);
        $("#NutritionalStatus option[value='SM']").attr("disabled", false);

        $('#NutritionalStatus').attr('readonly', false);
        $('#NutritionalStatus').attr('disabled', false);
    }
}

$('#NutritionalStatus').change(function (e) {
    var TheAge = getAge($('#DateOfBirth').val());
    if (parseInt(TheAge) <= 5) {
        if ($("#NutritionalStatus").val() == "M" || $("#NutritionalStatus").val() == "SM") {
            //Not sure if this part should be enabled.
            $('#ChildUnderTSForCMAM').attr('readonly', false);
            $('#ChildUnderTSForCMAM').prop('disabled', false);

            $("#ChildUnderTSForCMAM option[value='NO']").attr("disabled", false);
            $("#ChildUnderTSForCMAM option[value='YES']").attr("disabled", false);
        } else {
            $("#ChildUnderTSForCMAM").val("-");
            $('#ChildUnderTSForCMAM').attr('readonly', true);
            $('#ChildUnderTSForCMAM').prop('disabled', true);

            $("#ChildUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#ChildUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");
        }
    }
});
