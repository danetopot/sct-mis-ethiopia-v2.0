﻿
$(document).ready(function () {
    $('#ContinuityMCDate,#EndDateTDS').datepicker({
        dateFormat: "M/yy",
        changeMonth: true,
        changeYear: true,
        "showAnim": 'fold'
    }).inputmask('mm/yyyy');;

    $('#StartDateTDS').datepicker({
        dateFormat: "M/yy",
        changeMonth: true,
        changeYear: true,
        maxDate: '-1D',
        "showAnim": 'fold'
    }).inputmask('mm/yyyy');;

    $('#ChildDateOfBirth,#CollectionDate').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        maxDate: '-1D',
        "showAnim": 'fold'
    }).inputmask('mm/dd/yyyy');;

    $('#NextCNStatusDate').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        minDate: '+0',
        "showAnim": 'fold'
    }).inputmask('mm/dd/yyyy');;
    
    $("#CollectionDate,#StartDateTDS,#EndDateTDS,#BabyDateOfBirth").attr("readonly", true);

    $("#RegionID,#WoredaID,#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember,#ChildID,#CaretakerID").prop("disabled", true);
    $("#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#ContinuityMCDate,#EndDateTDS").prop("disabled", true);

    if ($("#AllowEdit").val() == 'True') {
        $("#cmdSave").prop("disabled", false);
        $("#cmdSave").val('Approve Details');
    } else {
        $("#cmdSave").prop("disabled", true);
        $("#cmdSave").val('Approve Already Done');
    }

    $('#loading').hide();
});

$("#cmdBack").click(function () {
    window.location.href = "/Household/Form1C";
});

