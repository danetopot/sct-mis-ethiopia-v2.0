﻿//var page_count = 10;
//var gridHeader = 'ColumnID,ProfileTDSPLWID,Kebele,Service<span class="HeaderChange">_</span>Provider,PLW<span class="HeaderChange">_</span>Name,';
//gridHeader = gridHeader + 'HouseHold<span class="HeaderChange">_</span>ID,PLW<span class="HeaderChange">_</span>Age,Capturer,Capture<span class="HeaderChange">_</span>On';
//var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro';
//var mygrid;

$(document).ready(function () {
    $('#thebox').jtable({
        title: 'Form 4B Compliant Listing',
        paging: true,
        actions: {
            listAction: '/ComplianceCapture/CapturedForm4BMainList',
        },
        fields: {
            ColumnID: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            //CHILD TABLE DEFINITION FOR "PHONE NUMBERS"
            Members: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (memberData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="../DHTMLX/codebase/imgs/plus2.gif" title="view Household members" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#thebox').jtable('openChildTable',
                                $img.closest('tr'),
                                {
                                    title: memberData.record.ServiceName + ' - Household members',
                                    paging: true,
                                    actions: {
                                        listAction: '/ComplianceCapture/CapturedForm4BList?ReportingPeriodID=' + memberData.record.ReportingPeriodID + '&ServiceID=' + memberData.record.ServiceID + '&KebeleID=' + memberData.record.KebeleID,
                                    },
                                    fields: {
                                        ProfileTDSPLWID: {
                                            key: true,
                                            create: false,
                                            edit: false,
                                            list: false
                                        },
                                        NameOfPLW: {
                                            title: 'HouseHold Member Name',
                                            width: '15%',
                                            create: false,
                                            edit: false
                                        },
                                        HouseHoldIDNumber: {
                                            title: 'PSNP HH #',
                                            width: '10%',
                                            create: false,
                                            edit: false
                                        },
                                        PLWAge: {
                                            title: 'Age',
                                            width: '5%',
                                            create: false,
                                            edit: false
                                        },
                                        Complied: {
                                            title: 'Complied',
                                            width: '5%',
                                            create: false,
                                            edit: false
                                        },
                                        Remarks: {
                                            title: 'Remarks',
                                            width: '15%',
                                            create: false,
                                            edit: false
                                        },
                                        CapturedOn: {
                                            title: 'Captured Date',
                                            width: '8%',
                                            //type: 'date',
                                            //displayFormat: 'dd-mm-yy',
                                            create: false,
                                            edit: false
                                        }
                                    }
                                }, function (data) { //opened handler
                                    data.childTable.jtable('load');
                                });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            ReportingPeriodID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            ReportingPeriod: {
                title: 'Reporting Period',
                width: '10%',
                create: false,
                edit: false,
            },
            KebeleName: {
                title: 'Kebele Name',
                width: '10%',
                create: false,
                edit: false,
            },
            ServiceProvider: {
                title: 'Service Provider',
                width: '10%',
                create: false,
                edit: false,
            },
            ServiceID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            KebeleID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            ServiceName: {
                title: 'Service Name',
                width: '20%',
                create: false,
                edit: false,
            },
            CreatedBy: {
                title: 'Created By',
                create: false,
                edit: false,
                width: '5%'
            }
        }
    });
    $('#thebox').jtable('load');
    //mygrid = new dhtmlXGridObject('gridbox');
    //mygrid.clearAll();
    //mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    //mygrid.setInitWidths("0,0,120,190,160,150,110,90,90");
    //mygrid.setColAlign("left,left,left,right,left,right,right,right,right");
    //mygrid.setHeader(gridHeader);

    //mygrid.setColTypes(gridColType);
    //mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    //mygrid.setPagingSkin("bricks");
    //mygrid.setSkin("dhx_skyblue");

    //mygrid.attachEvent("onXLE", showLoading);
    //mygrid.attachEvent("onXLS", function () { showLoading(true) });
    //mygrid.init();

    //mygrid.loadXML("/ComplianceCapture/CapturedForm4BList?RecCount=" + page_count);
    //dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Household Head Name'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Household ID'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Gote'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Gare'));

    $("#cmdNew").click(function () {
        window.location.href = "../ComplianceCapture/CaptureForm4B";
    });
    $('#loading').hide();
});