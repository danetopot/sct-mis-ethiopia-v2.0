﻿
var EVENTID = null;
var arrObj = [];
var SerialCount = 0;
var page_count = 5;
var mygrid;
var gridHeader = 'ColumnID,ProfileDSDetailID,Member<span class="HeaderChange">_</span>Name,Individual<span class="HeaderChange">_</span>ID';
gridHeader = gridHeader + ',Pregnant,Lactating,DOB,Age,Gender,Handicapped,Chronically<span class="HeaderChange">_</span>Ill';
gridHeader = gridHeader + ',NSN,Nutritional<span class="HeaderChange">_</span>Status,childUnderTSForCMAM';
gridHeader = gridHeader + ',School<span class="HeaderChange">_</span>Enrolled,Grade,School<span class="HeaderChange">_</span>Name,Edit';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img';

$(document).ready(function () {
    $("#RegionID,#WoredaID,#KebeleID").prop("disabled", true);
    $("#DateOfBirth,#HouseHoldMemberName,#Pregnant,#Lactating,#childUnderTSForCMAM,#Age,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#Remarks").prop("disabled", true);
    $("#HouseHoldMemberName,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Age,#SchoolName,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#Grade,#EnrolledInSchool").val("");
    $('#Gote,#NameOfHouseHoldHead,#HouseHoldIDNumber,#Gare,#HouseHoldIDNumber,#CollectionDate,#SocialWorker,#CCCCBSPCMember').prop("disabled", true);
    if ($("#AllowEdit").val() == 'True') {
        $("#cmdApprove").prop("disabled", false);
    } else {
        $("#cmdApprove").prop("disabled", true);        
    }

    //ProfileDSHeaderID
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,150,110,0,0,0,50,65,0,0,0,155,0,140,0,110,90");
    mygrid.setColAlign("left,left,left,right,left,right,right,left,left,right,left,right,right,right,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();
    mygrid.loadXML("/Household/FetchGridForm1ADetailsByID?RecCount=" + page_count + "&ProfileDSHeaderID=" + $("#ProfileDSHeaderID").val());
    dhtmlxError.catchError("ALL", my_error_handler);

    $.ajax({
        type: "POST",
        url: "../Household/FetchJsonForm1ADetailsByID",
        data: "{  'RecCount':" + page_count + ",ProfileDSHeaderID : '" + $("#ProfileDSHeaderID").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            //alert(yy);
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                MemberCount = data.length;
                if (MemberCount != undefined) {

                    for (var i = 0; i < data.length; i++) {
                        var theDtls = new Object;
                        var d = data[i];
                        SerialCount = SerialCount + 1;
                        theDtls.ColumnID = SerialCount;
                        theDtls.ProfileDSDetailID = d.ProfileDSDetailID;
                        theDtls.HouseHoldMemberName = d.HouseHoldMemberName;
                        theDtls.IndividualID = d.IndividualID;
                        theDtls.Pregnant = d.Pregnant;
                        theDtls.Lactating = d.Lactating;
                        theDtls.DateOfBirth = d.DateOfBirth;
                        theDtls.Age = d.Age;
                        theDtls.Sex = d.Sex;
                        theDtls.Handicapped = d.Handicapped;
                        theDtls.ChronicallyIll = d.ChronicallyIll;
                        theDtls.NutritionalStatus = d.NutritionalStatus;
                        theDtls.NutritionalStatusName = d.NutritionalStatusName;
                        theDtls.childUnderTSForCMAM = d.childUnderTSForCMAM;
                        theDtls.EnrolledInSchool = d.EnrolledInSchool;
                        theDtls.Grade = d.Grade;
                        theDtls.SchoolName = d.SchoolName;

                        arrObj.push(theDtls);
                    }
                    $("#MemberXml").val(JSON.stringify(arrObj));
                }                
            }
        }
    });

    $("#cmdBack").click(function () {
        window.location.href = "/Household/Form1A";
    });

    $('#DateOfBirth').change(function (e) {
        var TheAge = getAge($('#DateOfBirth').val());

        $("#Age").val(TheAge);

        if (parseInt(TheAge) > 5 && parseInt(TheAge) <= 18) {
            $('#EnrolledInSchool,#SchoolName').attr('readonly', false);
        }
        else {
            $('#EnrolledInSchool,#SchoolName').attr('readonly', true);
            $('#EnrolledInSchool').val('NO');
        }
        $('#Sex').focus();
    });

    $('#Sex').change(function (e) {
        if ($('#Sex').val() == 'M') {
            $('#Pregnant,#Lactating').val('NO');
            $('#Pregnant,#Lactating').prop("disabled", true);
            //$('#Handicapped').focus();
        } else {
            $('#Pregnant,#Lactating').prop("disabled", false);
        }
        var TheAge = getAge($('#DateOfBirth').val());
        if (parseInt(TheAge) > 5 && parseInt(TheAge) <= 18) {
            $('#Pregnant,#Lactating').val('NO');
            $('#Pregnant,#Lactating').prop("disabled", true);
        }
    });

    //#EnrolledInSchool
    $('#EnrolledInSchool').change(function (e) {
        if ($('#EnrolledInSchool').val() == 'NO') {
            if (parseInt(TheAge) > 5 && parseInt(TheAge) <= 18) {
                $('#SchoolName').attr('readonly', false);
            } else {
                $('#SchoolName').attr('readonly', true);
            }
        } else {
            $('#SchoolName').attr('readonly', false);
        }
    });


    //$("#cmdApprove").click(function () {
    //    post('../Household/UpdateApprovedForm1A', { ProfileDSHeaderID: $("#ProfileDSHeaderID").val() });
    //});
    $('#loading').hide();
});

function createWindow(_ColumnID) {
    var selectedRow = mygrid.getSelectedId();
    _ColumnID = mygrid.cells(selectedRow, 0).getValue();
    $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Age,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#SchoolName").val('')
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            if (arrObj[i].ColumnID == _ColumnID) {
                $("#ColumnID").val(arrObj[i].ColumnID);
                $("#HouseHoldMemberName").val(arrObj[i].HouseHoldMemberName);
                $("#IndividualID").val(arrObj[i].IndividualID);
                $("#DateOfBirth").val(arrObj[i].DateOfBirth);
                $("#Age").val(arrObj[i].Age);
                $("#Sex").val(arrObj[i].Sex);
                $("#Pregnant").val(arrObj[i].Pregnant);
                $("#Lactating").val(arrObj[i].Lactating);
                $("#Handicapped").val(arrObj[i].Handicapped);
                $("#ChronicallyIll").val(arrObj[i].ChronicallyIll);
                $("#NutritionalStatus").val(arrObj[i].NutritionalStatus);
                $("#childUnderTSForCMAM").val(arrObj[i].childUnderTSForCMAM);
                $("#EnrolledInSchool").val(arrObj[i].EnrolledInSchool);
                $("#Grade").val(arrObj[i].Grade);
                $("#SchoolName").val(arrObj[i].SchoolName);
            }
        }
    }
    
    EVENTID = "EDIT";
}

function UpdateMembers(_ColumnID) {
    createWindow(_ColumnID);
}
function createXmlstring(arrObject, inMemory) {
    var xml;
    var gridID;
    xml = '';
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            xml = xml + '<row id="' + i + '">';
            for (var j in arrObj[i]) {
                if (arrObj[i].hasOwnProperty(j)) {
                    if (j == "ColumnID") {
                        xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        gridID = arrObj[i][j];
                    }
                }
                if (j == "ProfileDSDetailID" || j == "HouseHoldMemberName" || j == "IndividualID" || j == "Pregnant"
                    || j == "Lactating" || j == "DateOfBirth" || j == "Age" || j == "Sex"
                    || j == "Handicapped" || j == "ChronicallyIll" || j == "NutritionalStatusName" || j == "NutritionalStatus"
                    || j == "childUnderTSForCMAM" || j == "EnrolledInSchool" || j == "SchoolName") {
                    xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                }
            }
            xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
            xml = xml + '</row>';
        }
    }
    xml = '<rows total_count="' + arrObj.length + '">' + xml + '</rows>';
    $("#MemberXml").val(JSON.stringify(arrObject));
    return xml;
}

