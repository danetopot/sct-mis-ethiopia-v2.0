﻿var reportToExportName = '';
var reportToExportLevel = '';
var reportToExportCenter = '';
var reportFilters = '';

$(document).ready(function () {

    $('#ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote,#SocialWorker,#AgeRange').multiselect({
        selectAllValue: '0',
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        maxHeight: 300,
        buttonWidth: '200px',
        buttonClass: 'btn btn-white',
        nonSelectedText: 'Please Select'
    });

    $('#cmdExportPdf').attr('disabled', 'disabled');
    $('#cmdExportXls').attr('disabled', 'disabled');
    $("#ReportingLevel,#ReportingPeriod,#ReportName,#ReportDisaggregation,#SocialWorker,#FiscalYear").prop("disabled", false);
    $('#ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote').multiselect('disable');
    $("#ReportingLevel,#ReportingPeriod,#ReportName,#ReportDisaggregation,#SocialWorker,#FiscalYear").val('');
    $("#ReportWoreda,#ReportKebele,#ReportingGote").empty();
    $('#ReportWoreda,#ReportKebele,#ReportingGote').multiselect('rebuild');
    $('#ReportStartDate,#ReportEndDate').val('');
    //$('#ReportEndDate').attr('disabled', 'disabled');


    $('#StartDateTDSPLW,#EndDateTDSPLW,#StartDateTDSCMC,#EndDateTDSCMC').Zebra_DatePicker({
        //direction: 1,    // boolean true would've made the date picker future only
        //direction: -1,
        format: 'M/Y',
        view: 'years'
    });
    
    $('#ReportStartDate').Zebra_DatePicker({
        direction: -1,
        pair: $('#ReportEndDate')
    });
     
    $('#ReportEndDate').Zebra_DatePicker({
        direction: 1
    });

    $(document).on("change", "#ReportName",
        function (e) {
            $("#error").hide();
            $("#success").hide();
            $("#validationerrors").hide();
            $("#rptViewSection").hide();

            var reportName = $("#ReportName").val();
            ManageReportNameFilters(reportName);

            $('#cmdExportPdf').attr('disabled', 'disabled');
            $('#cmdExportXls').attr('disabled', 'disabled');
        });

    $(document).on("change", "#ReportingLevel",
        function (e) {
            var reportLevel = $("#ReportingLevel").val();
            ManageReportLevelFilters(reportLevel);
        });

    $(document).on("change", "#ReportRegion",
        function (e) {
            regionChange();
        });

    $(document).on("change", "#ReportWoreda",
        function (e) {
            woredaChange();
        });

    $(document).on("change", "#ReportKebele",
        function (e) {
            kebeleChange();
        });

    $(document).on("change", "#FiscalYear",
        function (e) {
            fiscalYearChange();
        });
});

$("#cmdProduceReport").click(function () {
    $('#success').empty();
    $('#errors').empty();
    $('#validationerrors').empty();

    var reportName = $("#ReportName").val();
    if (reportName == 'RPT7' || reportName == 'RPT8') {
        var clienttype = $('#ClientType').val();
        if (isEmpty(clienttype)) {
            $('#validationerrors').show();
            $('#validationerrors').append('<ul><li><b>Please Select Type Of Client</b></li></ul>');
            return;
        }
    }

    var reportLevel = $("#ReportingLevel").val();
    if (reportLevel == 'REGIONAL' && isEmpty($('#ReportRegion').val())) {
        $('#validationerrors').show();
        $('#validationerrors').append('<ul><li><b>Please Select Region</b></li></ul>');
        return;
    }

    if (reportLevel == 'WOREDA' && isEmpty($('#ReportWoreda').val())) {
        $('#validationerrors').show();
        $('#validationerrors').append('<ul><li><b>Please Select Region and Woreda</b></li></ul>');
        return;
    }

    if (reportLevel == 'KEBELE' && isEmpty($('#ReportKebele').val())) {
        $('#validationerrors').show();
        $('#validationerrors').append('<ul><li><b>Please Select Region,Woreda and Kebele</b></li></ul>');
        return;
    }

    $("#progress").show();
    ProduceReport();

});

function ProduceReport() {
    var _ReportRegion = $("#ReportRegion").val();
    var _ReportWoreda = $("#ReportWoreda").val();
    var _ReportKebele = $("#ReportKebele").val();
    var _ReportGote = $("#ReportGote").val();
    var _ReportingPeriod = $("#ReportingPeriod").val();
    var _ReportName = $("#ReportName").val();
    var _ReportDisaggregation = $("#ReportDisaggregation").val();
    var _ReportLevel = $("#ReportingLevel").val();
    var _FiscalYear = $("#FiscalYear").val();
    var _ReportStartDate = $("#ReportStartDate").val();
    var _ReportEndDate = $("#ReportEndDate").val();

    /* RPT1 Filters */
    var _SocialWorker = $("#SocialWorker").val();
    var _Gender = $("#Gender").val();
    var _Pregnant = $("#Pregnant").val();
    var _Lactating = $("#Lactating").val();
    var _Handicapped = $("#Handicapped").val();
    var _ChronicallyIll = $("#ChronicallyIll").val();
    var _NutritionalStatus = $("#NutritionalStatus").val();
    var _ChildUnderTSForCMAM = $("#ChildUnderTSForCMAM").val();
    var _EnrolledInSchool = $("#EnrolledInSchool").val();
    var _ChildProtectionRisk = $("#ChildProtectionRisk").val();
    var _CBHIMembership = $("#CBHIMembership").val();
    var _AgeRange = $("#AgeRange").val();

    /* RPT2 Filters */
    var _StartDateTDSPLW = $("#StartDateTDSPLW").val();
    var _EndDateTDSPLW = $("#EndDateTDSPLW").val();
    var _NutritionalStatusPLW = $("#NutritionalStatusPLW").val();
    var _NutritionalStatusInfant = $("#NutritionalStatusInfant").val();

    /* RPT3 Filters */
    var _StartDateTDSCMC = $("#StartDateTDSCMC").val();
    var _EndDateTDSCMC = $("#EndDateTDSCMC").val();
    var _MalnourishmentDegree = $("#MalnourishmentDegree").val();

    /* RPT4 Filters */
    var _ClientType = $("#ClientType").val();
    var _ServiceType = $("#ServiceType").val();

    // Validate ReportName & Report Disaggregation
    var html = '<ul>';
    var errors = [];
    var valid = true;
    if (_ReportName == '' || _ReportName == null) {
        errors.push('<li><b>Please Select Report Name</b></li>');
        valid = false;
    }
    if (_ReportLevel == '' || _ReportLevel == null) {
        errors.push('<li><b>Please Select Reporting Level</b></li>');
        valid = false;
    }

    if (_ReportDisaggregation == '' || _ReportDisaggregation == null) {
        errors.push('<li><b>Please Select Reporting Disaggregation</b></li>');
        valid = false;
    }

    if ((_ReportEndDate == '' || _ReportEndDate == null) && _ReportStartDate != '') {
        errors.push('<li><b>Please Select Report End Date</b></li>');
        valid = false;
    }

    if (!valid) {
        html += errors.join('') + '</ul>'
        $('#validationerrors').show();
        $('#validationerrors').empty();
        $('#validationerrors').append(html);
        $("#progress").hide();
        $("#cmdProduceReport").prop("disabled", false);
        return;
    }
    else {
        $("#progress").show();
        $('#validationerrors').hide();
    }

    var rptFilters = {
        Region: _ReportRegion,
        Woreda: _ReportWoreda,
        Kebele: _ReportKebele,
        Gote: _ReportGote,
        ReportingPeriod: _ReportingPeriod,
        ReportName: _ReportName,
        ReportDisaggregation: _ReportDisaggregation,
        ReportingLevel: _ReportLevel,
        SocialWorker: _SocialWorker,
        FiscalYear: _FiscalYear,
        ReportStartDate: _ReportStartDate,
        ReportEndDate: _ReportEndDate,
        Gender: _Gender,
        Pregnant: _Pregnant,
        Lactating: _Lactating,
        Handicapped: _Handicapped,
        ChronicallyIll: _ChronicallyIll,
        NutritionalStatus: _NutritionalStatus,
        ChildUnderTSForCMAM: _ChildUnderTSForCMAM,
        EnrolledInSchool: _EnrolledInSchool,
        ChildProtectionRisk: _ChildProtectionRisk,
        CBHIMembership: _CBHIMembership,
        AgeRange: _AgeRange,

        StartDateTDSPLW: _StartDateTDSPLW,
        EndDateTDSPLW: _EndDateTDSPLW,
        NutritionalStatusPLW: _NutritionalStatusPLW,
        NutritionalStatusInfant: _NutritionalStatusInfant,

        StartDateTDSCMC: _StartDateTDSCMC,
        EndDateTDSCMC: _EndDateTDSCMC,
        MalnourishmentDegree: _MalnourishmentDegree,

        ClientType: _ClientType,
        ServiceType: _ServiceType
    };

    reportFilters = buildFilterString(rptFilters);

    $.ajax({
        type: "POST",
        url: "/Security/StandardReports",
        data: JSON.stringify(rptFilters),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var report = $('#ReportName option:selected').text();
            var message = data.Value;
            if (message == 'Success') {
                $('#success').show();
                $('#success').empty();
                $('#success').append('<span><b>' + report + ' Successfully Generated<br>'+ reportFilters + '</b></span>');
                $('#error').hide();
                $("#progress").hide();
                $("#cmdProduceReport").removeAttr('disabled');

                reportToExportName = $("#ReportName").val();
                reportToExportLevel = $("#ReportDisaggregation").val();
                reportToExportCenter = $("#ReportingLevel").val();
                previewData();
                resetFilters();
            }
            else if (message == 'NoData') {
                $('#success').show();
                $('#success').empty();
                $('#success').append('<ul><li><b>No Data To Generate ' + report + '</b></li></ul>');
                $('#error').hide();
                $("#progress").hide();
                $("#cmdProduceReport").prop("disabled", false);
            } else {
                $('#error').show();
                $('#error').empty();
                $('#error').append('<ul><li><b>An error occured when generating ' + report + '</b></li></ul>');
                $('#success').hide();
                $("#progress").hide();
                $("#cmdProduceReport").prop("disabled", false);
            }


        },
        error: function (xhr, error, errorThrown) {
            $('#error').show();
            $('#error').empty();
            $('#error').append('<ul><li><b>' + errorThrown + '</b></li></ul>');
            $('#success').hide();
            $("#progress").hide();
            $("#cmdProduceReport").prop("disabled", false);

            $('#cmdExportPdf').attr('disabled', 'disabled');
            $('#cmdExportXls').attr('disabled', 'disabled');

        }
    });
}

function ManageReportLevelFilters(reportLevel) {
    $("#ReportDisaggregation").val('');
    $("#ReportDisaggregation option[value='DT']").attr('disabled', false);

    if (reportLevel == 'FEDERAL') {
        $('#ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote').multiselect('clearSelection');
        $('#ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote').multiselect('disable');

        $("#ReportDisaggregation option[value='DT']").attr('disabled', true);
        $("#ReportDisaggregation").val('SM');
    }
    if (reportLevel == 'REGIONAL') {
        $('ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote').multiselect('clearSelection');
        $('#ReportRegion').multiselect('enable');
        $('#ReportWoreda,#ReportKebele,#ReportGote').multiselect('disable');
    }
    if (reportLevel == 'WOREDA') {
        $('ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote').multiselect('clearSelection');
        $('#ReportRegion, #ReportWoreda').multiselect('enable');
        $('#ReportKebele,#ReportGote').multiselect('disable');
    }
    if (reportLevel == 'KEBELE') {
        $('#ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote').multiselect('clearSelection');
        $('#ReportRegion,#ReportWoreda,#ReportKebele').multiselect('enable');
        $('#ReportGote').multiselect('disable');
    }
    if (reportLevel == 'GOTE') {
        $('ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote').multiselect('clearSelection');
        $('#ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote').multiselect('enable');
    }

    var reportName = $("#ReportName").val();
    if (reportName == 'RPT11' || reportName == 'RPT12' || reportName == 'RPT13') {
        $("#ReportDisaggregation option[value='DT']").attr('disabled', true);
        $("#ReportDisaggregation").val('SM');
    }
}

function ManageReportNameFilters(reportName) {
    resetFilters();

    // RPT1 Filters
    if (reportName == 'RPT1') {
        $('#divRpt1Services').show();
        $('#divRpt1Gender').show();
        $('#divRpt1Pregnant').show();
        $('#divRpt1Lactating').show();
        $('#divRpt1Disabled').show();
        $('#divRpt1ChronicallyIll').show();
        $('#divRpt1NutritionalStatus').show();
        $('#divRpt1ChildUnderTSForCMAM').show();
        $('#divRpt1EnrolledInSchool').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
        $('#divRpt1AgeRange').show();
    }

    // RPT2 Filters
    if (reportName == 'RPT2') {
        $('#divRpt2StartDateTDSPLW').show();
        $('#divRpt2EndDateTDSPLW').show();
        $('#divRpt2NutritionalStatusPLW').show();
        $('#divRpt2NutritionalStatusInfant').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
        $('#divRpt1AgeRange').show();
    }

    // RPT3 Filters
    if (reportName == 'RPT3') {
        $('#divRpt3MalnourishmentDegree').show();
        $('#divRpt3StartDateTDSCMC').show();
        $('#divRpt3EndDateTDSCMC').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
        $('#divRpt1AgeRange').show();
    }

    // RPT4 Filters
    if (reportName == 'RPT4') {
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT5 Filters
    if (reportName == 'RPT5') {
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT6 Filters
    if (reportName == 'RPT6') {
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT7 Filters
    if (reportName == 'RPT7') {
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT8 Filters
    if (reportName == 'RPT8') {
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT9 Filters
    if (reportName == 'RPT9') {
    }

    // RPT10 Filters
    if (reportName == 'RPT10') {
    }

    // RPT11 Filters
    if (reportName == 'RPT11') {
        $('#divRpt1Services').show();
        $('#divRpt1Gender').show();
        $('#divRpt1Pregnant').show();
        $('#divRpt1Lactating').show();
        $('#divRpt1Disabled').show();
        $('#divRpt1ChronicallyIll').show();
        $('#divRpt1NutritionalStatus').show();
        $('#divRpt1ChildUnderTSForCMAM').show();
        $('#divRpt1EnrolledInSchool').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
        $('#divRpt1AgeRange').show();

        $("#ReportDisaggregation option[value='DT']").attr('disabled', true);
        $("#cmdExportPdf").attr('disabled', true);
    }

    // RPT12 Filters
    if (reportName == 'RPT12') {
        $('#divRpt2StartDateTDSPLW').show();
        $('#divRpt2EndDateTDSPLW').show();
        $('#divRpt2NutritionalStatusPLW').show();
        $('#divRpt2NutritionalStatusInfant').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
        $('#divRpt1AgeRange').show();

        $("#ReportDisaggregation option[value='DT']").attr('disabled', true);
        $("#cmdExportPdf").attr('disabled', true);
    }

    // RPT13 Filters
    if (reportName == 'RPT13') {
        $('#divRpt3MalnourishmentDegree').show();
        $('#divRpt3StartDateTDSCMC').show();
        $('#divRpt3EndDateTDSCMC').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
        $('#divRpt1AgeRange').show();

        $("#ReportDisaggregation option[value='DT']").attr('disabled', true);
        $("#cmdExportPdf").attr('disabled', true);
    }
}


function resetFilters() {
    $('#divRpt1Services').hide();
    $('#divRpt1Gender').hide();
    $('#divRpt1Pregnant').hide();
    $('#divRpt1Lactating').hide();
    $('#divRpt1Disabled').hide();
    $('#divRpt1ChronicallyIll').hide();
    $('#divRpt1NutritionalStatus').hide();
    $('#divRpt1ChildUnderTSForCMAM').hide();
    $('#divRpt1EnrolledInSchool').hide();
    $('#divRpt1ChildProtectionRisk').hide();
    $('#divRpt1CBHIMembership').hide();
    $('#divRpt1AgeRange').hide();

    $('#divRpt2StartDateTDSPLW').hide();
    $('#divRpt2EndDateTDSPLW').hide();
    $('#divRpt2NutritionalStatusPLW').hide();
    $('#divRpt2NutritionalStatusInfant').hide();

    $('#divRpt3MalnourishmentDegree').hide();
    $('#divRpt3StartDateTDSCMC').hide();
    $('#divRpt3EndDateTDSCMC').hide();

    $('#divRpt4TypeOfClient').hide();
    $('#divRpt4TypeOfService').hide();

    $('#ServiceType').val('');
    $('#Gender').val('-');
    $('#Pregnant').val('');
    $('#Lactating').val('');
    $('#Handicapped').val('');
    $('#ChronicallyIll').val('');
    $('#NutritionalStatus').val('');
    $('#ChildUnderTSForCMAM').val('');
    $('#EnrolledInSchool').val('');
    $('#ChildProtectionRisk').val('');
    $('#CBHIMembership').val('');

    $('#StartDateTDSPLW').val('');
    $('#EndDateTDSPLW').val('');
    $('#NutritionalStatusPLW').val('');
    $('#NutritionalStatusInfant').val('');

    $('#MalnourishmentDegree').val('');
    $('#StartDateTDSCMC').val('');
    $('#EndDateTDSCMC').val('');

    $('#EndDateTDSCMC').val('');
    $('#EndDateTDSCMC').val('');

    $('#ReportStartDate').val('');
    $('#ReportEndDate').val('');

    $('#ClientType').val('-');
    $('#ServiceType').val('');
    
    $("#ReportDisaggregation option[value='DT']").attr('disabled', false);
    $('#ReportRegion,#ReportWoreda,#ReportKebele,#ReportGote,#SocialWorker,#AgeRange').multiselect('clearSelection');
   
    $('#ReportDisaggregation').val('');
    $('#ReportingLevel').val('');
    $('#FiscalYear').val('');
    $('#ReportingPeriod').val('');
}

function previewData() {

    var rptName = $("#ReportName").val();
    var rptGroupBy = $("#ReportingLevel").val();
    var rptLevel = $("#ReportDisaggregation").val();

    $("#rptViewSection").show();
    $('#cmdExportPdf').removeAttr('disabled');
    $('#cmdExportXls').removeAttr('disabled');

    /* RPT 1 */
    if (rptName == 'RPT1') {
        if (rptLevel == 'DT') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';

            gridHeader = gridHeader + 'UniqueID,Fiscal Year,Region,Woreda,Kebele,Gote,';
            gridHeader = gridHeader + 'Household Name,PSNP Number,Client<span class="HeaderChange">_</span>Name,Individual ID,DOB,Age,Sex,';
            gridHeader = gridHeader + 'Handicapped,Chronically Ill,Nutritional Status,Under TSF or CMAM,Enrolled In School,School Name,Pregnant,';
            gridHeader = gridHeader + 'Lactating,CCCC/BSPC Member,CBHI Membership,Child Protection,Collection Date,Social Worker';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'){
                mygrid.setInitWidths("0,100,100,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }

            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,100,100,100,100,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }
            
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);

            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if (rptLevel == 'SM') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,Fiscal Year,Region,Woreda,Kebele,Gote,';
            gridHeader = gridHeader + 'Households, Household Members,Enrolled In CBHI,Child Protection Cases,Non-Malnuorished,Malnourished,Severely Malnourished,Handicapped,';
            gridHeader = gridHeader + 'Chronically Ill,Lactating,Pregnant,Enrolled In School,Under TSF or CMAM';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            //mygrid.setInitWidths("0,100,100,100,100,0,100,100,100,100,100,100,100,100,100,100,100,100");

            if(rptGroupBy == 'REGIONAL' || rptGroupBy == 'FEDERAL'){
                mygrid.setInitWidths("0,100,100,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }

            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,100,100,100,100,0,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }

            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT2 */
    if (rptName == 'RPT2') {
        if (rptLevel == 'DT') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, ProfileTDSPLWID,ProfileTDSPLWDetailID, FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,Name Of PLW, PSNP Number, Medical Record Number, PLW Age, StartDate TDS, EndDate TDS,';
            gridHeader = gridHeader + 'Baby DOB, Baby Name, Baby Sex, Nutritional Status Of PLW, Nutritional Status Of Infant,CBHI Membership, Child Protection, CollectionDate, Social Worker';

            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'){
                mygrid.setInitWidths("0,0,0,100,100,0,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }

            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,0,0,100,100,100,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,0,0,100,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }

            //mygrid.setInitWidths("0,0,100,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if (rptLevel == 'SM') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, Fiscal Year,Region,Woreda,Kebele,Gote,';
            gridHeader = gridHeader + 'Households, Household Members,Enrolled In CBHI,Child Protection Cases,Non Malnuorished PLW,Malnourished PLW,Severely Malnourished PLW,';
            gridHeader = gridHeader + 'Non-Malnuorished Infants,Malnourished Infants,Severely Malnourished Infants';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL' || rptGroupBy == 'FEDERAL'){
                mygrid.setInitWidths("0,100,100,0,0,0,100,100,100,100,100,100,100,100,100,100");
            }

            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,100,100,100,100,0,100,100,100,100,100,100,100,100,100,100");
            }

            // mygrid.setInitWidths("0,0,120,120,120,120,0,0,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT3 */
    if (rptName == 'RPT3') {
        if (rptLevel == 'DT') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, ProfileTDSPLWID, Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,Caretaker ID,Name Of CareTaker,PSNP Number,Malnourished Child Name,Malnourished Child Sex,';
            gridHeader = gridHeader + 'ChildID,Child DOB,DateType Certificate,Malnourishment Degree,StartDate TDS,EndDate TDS,Next CN Status Date,CBHI Membership,Child Protection,CCCC/BSPC Member,Collection Date,Social Worker';

            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'){
                mygrid.setInitWidths("0,0,110,110,0,0,0,0,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110");
            }

            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,0,110,110,110,0,0,0,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,0,110,110,110,110,0,0,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110");
            }

            //mygrid.setInitWidths("0,0,110,110,110,110,0,0,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if (rptLevel == 'SM') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, Fiscal Year,Region,Woreda,Kebele,Gote,';
            gridHeader = gridHeader + 'Households, Household Members,Enrolled In CBHI,Child Protection Cases,Non-Malnuorished Children,Malnourished Children,Severely Malnourished Children';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            
            if(rptGroupBy == 'REGIONAL'|| rptGroupBy == 'FEDERAL'){
                mygrid.setInitWidths("0,140,140,0,0,0,140,140,140,140,140,140,140");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,140,140,140,0,0,140,140,140,140,140,140,140");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,140,140,140,140,0,140,140,140,140,140,140,140");
            }
            
            //mygrid.setInitWidths("0,0,140,140,140,140,0,0,140,140,140,140,140,140");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT4 */
    if (rptName == 'RPT4') {
        if (rptLevel == 'DT') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileHeaderID,Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'PSNP Number,Individual ID,Client<span class="HeaderChange">_</span>Type,Name OfHouseHold Member,Sex,Age,Pre-AnteNatal Care,Immunization,';
            gridHeader = gridHeader + 'Supplementary Feeding,Monthly GMPS essions,School Enrolment,BCC Sessions,Under TSF or CMAM,Birth Registration,HealthPost CheckUp,';
            gridHeader = gridHeader + 'CBHI Membership,Child Protection,Prenatal Visit 1,Prenatal Visit 3,PostNatal Visit,Collection Date,Social Worker';

            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'){
                mygrid.setInitWidths("0,0,100,100,0,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,0,100,100,100,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,0,100,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }

            //mygrid.setInitWidths("0,0,100,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if (rptLevel == 'SM') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Households,Household Members,Pre-AnteNatal Care,Immunization,Supplementary Feeding,Monthly GMP Sessions,School Enrolment,BCC Sessions,';
            gridHeader = gridHeader + 'Under TSF or CMAM,Birth Registration,CBHI Membership,Child Protection,PrenatalVisit 1,PrenatalVisit 3,PostNatal Visit';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            
            if(rptGroupBy == 'REGIONAL'|| rptGroupBy == 'FEDERAL'){
                mygrid.setInitWidths("0,120,120,0,0,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,120,120,120,0,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,120,120,120,120,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }

            //mygrid.setInitWidths("0,0,120,120,120,120,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT5 & RPT6 */
    if (rptName == 'RPT5' || rptName == 'RPT6') {
        if (rptLevel == 'DT') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileDSHeaderID,Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Client<span class="HeaderChange">_</span>Name,Client<span class="HeaderChange">_</span>Type,Individual ID,Age,Sex,Service ID,Service Name,';
            gridHeader = gridHeader + 'Complied,Collection Date,Social Worker';

            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'){
                mygrid.setInitWidths("0,0,100,100,0,0,0,0,100,100,100,100,100,0,100,0,100,100,100");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,0,100,100,100,0,0,0,100,100,100,100,100,0,100,0,100,100,100");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,0,100,100,100,100,0,0,100,100,100,100,100,0,100,0,100,100,100");
            }

            //mygrid.setInitWidths("0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,200,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if (rptLevel == 'SM') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,Fiscal Year,Region,Woreda,Kebele,Gote,Household,Household Members,';
            gridHeader = gridHeader + 'Pre-AnteNatal Care,Immunization,Under TSF/CMAM,GMP Sessions,School Enrolment,BCC Sessions,Birth Registration,';
            gridHeader = gridHeader + '3PreNatalCare Visits,1PreNatalCare Visit,CheckUp At Health Post,Supplementary Feeding,CBHI Membership,Child Protection';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'|| rptGroupBy == 'FEDERAL'){
                mygrid.setInitWidths("0,100,100,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,100,100,100,100,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            }

            //mygrid.setInitWidths("0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT7 & RPT8 */
    if (rptName == 'RPT7' || rptName == 'RPT8') {
        if (rptLevel == 'DT') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileDSHeaderID,FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Individual<span class="HeaderChange">_</span>ID,Client<span class="HeaderChange">_</span>Name,Client<span class="HeaderChange">_</span>Type,Sex,Age,Service';
            /*
            gridHeader = gridHeader + 'PreAnteNatal Care FollowUps,Immunization FollowUps,Under TSFCMAM FollowUps,GMP Sessions FollowUps,School Enrolment FollowUps,BCC Sessions FollowUps,Birth Registration FollowUps,';
            gridHeader = gridHeader + '3PreNatal Care Visits FollowUps,1PreNatal Care Visit FollowUps,CheckUp At Health Post FollowUps,Supplementary Feeding FollowUps,CBHI Membership FollowUps,Child Protection FollowUps';
            */

            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'){
                mygrid.setInitWidths("0,0,100,100,0,0,0,0,100,100,100,100,100,300");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,0,100,100,100,0,0,0,100,100,100,100,100,300");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,0,100,100,100,100,0,0,100,100,100,100,100,300");
            }

            //mygrid.setInitWidths("0,0,100,100,100,100,100,100,100,100,100,100,100,300");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if (rptLevel == 'SM') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,Fiscal Year,Region,Woreda,Kebele,Gote,';
            //gridHeader = gridHeader + 'Households,Household Members,';
            gridHeader = gridHeader + 'PreAnteNatal Care FollowUps,Immunization FollowUps,Under TSFCMAM FollowUps,GMP Sessions FollowUps,School Enrolment FollowUps,BCC Sessions FollowUps,Birth Registration FollowUps,';
            gridHeader = gridHeader + '3PreNatal Care Visits FollowUps,1PreNatal Care Visit FollowUps,CheckUp At Health Post FollowUps,Supplementary Feeding FollowUps,CBHI Membership FollowUps,Child Protection FollowUps';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'|| rptGroupBy == 'FEDERAL'){
                mygrid.setInitWidths("0,120,120,0,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,120,120,120,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,120,120,120,120,0,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }

            //mygrid.setInitWidths("0,0,120,120,120,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT9 & RPT10 */
    if (rptName == 'RPT9' || rptName == 'RPT10') {
        if (rptLevel == 'DT') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileHeaderID,FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'PSNP Number,Individual<span class="HeaderChange">_</span>ID,Client<span class="HeaderChange">_</span>Type,Client<span class="HeaderChange">_</span>Name,Sex,Age,PreAnteNatal Care,Immunization,';
            gridHeader = gridHeader + 'Supplementary Feeding,Monthly<span class="HeaderChange">_</span>GMP Sessions,School Enrolment,BCC Sessions,Under TSFCMAM,Birth Registration,HealthPost CheckUp,';
            gridHeader = gridHeader + 'CBHI Membership,Child Protection,1PreNatal Visit,3PreNatal Visit,PostNatal Visit,Collection Date,Social Worker';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'){
                mygrid.setInitWidths("0,0,100,100,0,0,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,0,0");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,0,100,100,100,0,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,0,0");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,0,100,100,100,100,0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,0,0");
            }

            //mygrid.setInitWidths("0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if (rptLevel == 'SM') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,FiscalYear,Region,Woreda,Kebele,Gote,';
            gridHeader = gridHeader + 'PreAnteNatal Care,Immunization,Supplementary Feeding,Monthly GMP Sessions,School Enrolment,BCC Sessions,Under TSFCMAM,';
            gridHeader = gridHeader + 'Birth Registration,Health PostCheckUp,CBHI Membership,Child Protection,1PreNatalVisit,3PreNatalVisits,PostNatal Visit';

            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'|| rptGroupBy == 'FEDERAL'){
                mygrid.setInitWidths("0,120,120,0,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,120,120,120,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }
            
            if(rptGroupBy == 'KEBELE'){
                mygrid.setInitWidths("0,120,120,120,120,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            }

            //mygrid.setInitWidths("0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    if (rptName == 'RPT11' || rptName == 'RPT12' || rptName == 'RPT13') {
        
        $('#cmdExportPdf').prop('disabled', true);

        if (rptLevel == 'DT') {
            
        }

        if (rptLevel == 'SM') {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,FiscalYear,Region,Woreda,Kebele,Gote,Households,Household<span class="HeaderChange">_</span>Members,Male,Female,';
            gridHeader = gridHeader + 'Male(Under<span class="HeaderChange">_</span>1),Male(1-<span class="HeaderChange">_</span>2<span class="HeaderChange">_</span>YRS),Male(3-<span class="HeaderChange">_</span>5<span class="HeaderChange">_</span>YRS),Male(6-<span class="HeaderChange">_</span>12<span class="HeaderChange">_</span>YRS),Male(13-<span class="HeaderChange">_</span>19<span class="HeaderChange">_</span>YRS),Male(20-<span class="HeaderChange">_</span>24<span class="HeaderChange">_</span>YRS),Male(25-<span class="HeaderChange">_</span>49<span class="HeaderChange">_</span>YRS),Male(46-<span class="HeaderChange">_</span>59<span class="HeaderChange">_</span>YRS),Male(Over<span class="HeaderChange">_</span>60<span class="HeaderChange">_</span>YRS),';
            gridHeader = gridHeader + 'Female(Under<span class="HeaderChange">_</span>1),Female(1-<span class="HeaderChange">_</span>2<span class="HeaderChange">_</span>YRS),Female(3-<span class="HeaderChange">_</span>5<span class="HeaderChange">_</span>YRS),Female(6-<span class="HeaderChange">_</span>12<span class="HeaderChange">_</span>YRS),Female(13-<span class="HeaderChange">_</span>19<span class="HeaderChange">_</span>YRS),Female(20-<span class="HeaderChange">_</span>24<span class="HeaderChange">_</span>YRS),Female(25-<span class="HeaderChange">_</span>49<span class="HeaderChange">_</span>YRS),Female(46-<span class="HeaderChange">_</span>59<span class="HeaderChange">_</span>YRS),Female(Over<span class="HeaderChange">_</span>60<span class="HeaderChange">_</span>YRS)';

            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");

            if(rptGroupBy == 'REGIONAL'|| rptGroupBy == 'FEDERAL'){
                mygrid.setInitWidths("0,100,100,0,0,0,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150");
            }
            
            if(rptGroupBy == 'WOREDA'){
                mygrid.setInitWidths("0,100,100,100,0,0,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150");
            }
            
            if(rptGroupBy == 'KEBELE'){
                 mygrid.setInitWidths("0,100,100,100,100,0,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150");
            }
            
            mygrid.setColAlign("left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel + "&GroupBy=" + rptGroupBy,
                function () {
                    mygrid.changePage(currentPage);
                });

            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                $('#currentPage').val(ind);
                $.ajax({
                    type: "POST",
                    url: "../Security/UpdateCurrentPage",
                    data: "{ 'CurrentPage':" + ind + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                });
            });
            dhtmlxError.catchError("ALL", my_error_handler);
        }

          $('#cmdExportPdf').attr('disabled','disabled');

        $('#cmdExportPdf').prop('disabled', true);
    }

    $("#ReportName").val('');
    //resetFilters();
}

function ExportReport(reportFormat) {
    var rptParams = {
        rptName: reportToExportName,
        rptLevel: reportToExportLevel,
        rptFormat: reportFormat,
        rptCenter: reportToExportCenter,
        rptFilter: reportFilters
    }

    $('#exportProgress').show();
    $.ajax({
        type: "POST",
        url: "/Security/StandardReportsExport",
        data: JSON.stringify(rptParams),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#exportProgress').hide();
            window.location.href = "../Security/StandardReportsDownload?path=" + data.Path + "&filename=" + data.FileName + "&format=" + data.Format;
        },
        error: function (xhr, error, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function regionChange() {
    var region = $("#ReportRegion").val();
    if (region == null) { region = 0; }

    $.ajax({
        type: "POST",
        url: "../Security/SelectWoreda",
        data: "{  'RegionID' : '" + region + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            alert(yy);
        },
        success: function (data) {
            $('#ReportWoreda').empty();
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                $('#ReportWoreda').append($("<option></option>").attr("value", d.WoredaID).text(d.WoredaName));
            }
            $('#ReportWoreda').multiselect('rebuild');
            woredaChange();
        }
    });
}

function woredaChange() {

    var woreda = $("#ReportWoreda").val();
    if (woreda == null) { woreda = 0; }

    $.ajax({
        type: "POST",
        url: "../Security/SelectKebele",
        data: "{  'WoredaID' : '" + woreda + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            alert(yy);
        },
        success: function (data) {
            $('#ReportKebele').empty();
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                $('#ReportKebele').append($("<option></option>").attr("value", d.KebeleID).text(d.KebeleName));
            }
            $('#ReportKebele').multiselect('rebuild');
            kebeleChange();
        }
    });
}

function kebeleChange() {
    /*
    var kebele = $("#ReportKebele").val();
    if (kebele == null) { kebele = 0; }

    $.ajax({
        type: "POST",
        url: "../Security/SelectGote",
        data: "{  'KebeleID' : '" + kebele + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            alert(yy);
        },
        success: function (data) {
            $('#ReportGote').empty(); 
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                console.log(d);
                $('#ReportGote').append($("<option></option>").attr("value", d.GoteName).text(d.GoteName));
            }
            $('#ReportGote').multiselect('rebuild');
        }
    });
    */
}

function fiscalYearChange() {
    $('#ReportingPeriod').empty();
    $.ajax({
        type: "POST",
        url: "../Household/SelectReportingPeriods",
        data: "{  'FiscalYear' : '" + $("#FiscalYear").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            alert(yy);
        },
        success: function (data) {
            $('#ReportingPeriod').append($("<option></option>").attr("value", "").text(""));
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                $('#ReportingPeriod').append($("<option></option>").attr("value", d.PeriodID).text(d.PeriodName));
            }
        }
    });
    $('#ReportingPeriod').val("");
}

function isEmpty(value) {
    return (value == null || value.length === 0);
}

function buildFilterString(rptFilters){
    var filterArray = [];
    var filterElement = [];
    var filterOutput = '';
    var filterString = '';

    $.each(rptFilters, function(n, elem) {
       if(!isEmpty(elem)) {
            filterElement = lookupHelper(n, elem);
            filterOutput = filterElement[0] + '=' + filterElement[1]
            filterArray.push(filterOutput);
       } 
    });

    filterString = 'Filtered By : ' + filterArray.join(',');
    return filterString;
}

function lookupHelper(n, elem)
{
    console.log(n);

    var f =  [];

    if(n == 'ReportName')
    {
        n = 'Name';
        if(elem == 'RPT1'){
            elem = 'Household Profile Report - PDS';
        }else if(elem == 'RPT2'){
            elem = 'Household Profile Report - TDS(PLW)';
        }else if(elem == 'RPT3'){
            elem = 'Household Profile Report - TDS(CMC)';
        }else if(elem == 'RPT4'){
            elem = 'Social Services Needs Report';
        }else if(elem == 'RPT5'){
            elem = 'Service Compliance Report';
        }else if(elem == 'RPT6'){
            elem = 'Service Non-Compliance Report';
        }else if(elem == 'RPT7'){
            elem = 'Case Management Compliance Report';
        }else if(elem == 'RPT8'){
            elem = 'Case Management Non-Compliance Report';
        }else if(elem == 'RPT9'){
            elem = 'Re-Targeting Report - New';
        }else if(elem == 'RPT10'){
            elem = 'Re-Targeting Report - Exit';
        }else if(elem == 'RPT11'){
            elem = 'Household Profile Report - PDS Disaggregated';
        }else if(elem == 'RPT12'){
            elem = 'Household Profile Report - TDS(PLW) Disaggregated';
        }else if(elem == 'RPT13'){
            elem = 'Household Profile Report - TDS(CMC) Disaggregated';
        }
        else{
            elem = elem;
        }
    }
    
    if(n == 'ReportDisaggregation')
    {
        n = 'Disaggregation'
        if(elem == 'DT'){
            elem = 'Detail';
        }else if(elem == 'SM'){
            elem = 'Summary';
        }
    }

    if(n == 'ReportingLevel')
    {
        n = 'Level'
        elem = titleCase(elem);
    }

    if(n == 'Region')
    {
        n = 'Region(s)'
            
        var selections = [];
        $("#ReportRegion option:selected").each(function () 
        {
            var $this = $(this);
            if ($this.length)
            {
                var selText = $this.text();
                if($this.val()>0){
                    selections.push(selText);
                }
            }
        });
        elem = selections.join( "|" );
    }

    if(n == 'Woreda')
    {
        n = 'Woreda(s)'
            
        var selections = [];
        $("#ReportWoreda option:selected").each(function () 
        {
            var $this = $(this);
            if ($this.length)
            {
                var selText = $this.text();
                if($this.val()>0){
                    selections.push(selText);
                }
            }
        });
        elem = selections.join( "|" );
    }

    if(n == 'Kebele')
    {
        n = 'Kebele(s)'
            
        var selections = [];
        $("#ReportKebele option:selected").each(function () 
        {
            var $this = $(this);
            if ($this.length)
            {
                var selText = $this.text();
                if($this.val()>0){
                    selections.push(selText);
                }
            }
        });
        elem = selections.join( "|" );
    }

    if(n == 'SocialWorker')
    {
        n = 'Social Worker(s)'
            
        var selections = [];
        $("#SocialWorker option:selected").each(function () 
        {
            var $this = $(this);
            if ($this.length)
            {
                var selText = $this.text();
                if($this.val()>0){
                    selections.push(selText);
                }
            }
        });
        elem = selections.join( "|" );
    }

    if(n == 'AgeRange')
    {
        n = 'Age Range(s)'
            
        var selections = [];
        $("#AgeRange option:selected").each(function () 
        {
            var $this = $(this);
            if ($this.length)
            {
                var selText = $this.text();
                if($this.val()>0){
                    selections.push(selText);
                }
            }
        });
        elem = selections.join( "|" );
    }

    if(n == 'FiscalYear')
    {
        n = 'Fiscal Year';
        elem = $('#FiscalYear option:selected').text();
    }

    if(n == 'ReportingPeriod')
    {
        n = 'Reporting Period';
        elem = $('#ReportingPeriod option:selected').text();
    }

    if(n == 'Gender')
    {
        n = 'Gender';
        elem = $('#Gender option:selected').text();
    }

    if(n == 'Pregnant')
    {
        n = 'Pregnant';
        elem = titleCase($('#Pregnant option:selected').text());
    }

    if(n == 'Lactating')
    {
        n = 'Lactating';
        elem = $('#Lactating option:selected').text();
    }

    if(n == 'Handicapped')
    {
        n = 'Disabled';
        elem = titleCase($('#Handicapped option:selected').text());
    }

    if(n == 'ChronicallyIll')
    {
        n = 'Chronically Ill';
        elem = titleCase($('#ChronicallyIll option:selected').text());
    }

    if(n == 'NutritionalStatus')
    {
        n = 'NutritionalStatus';
        elem = titleCase($('#NutritionalStatus option:selected').text());
    }

    if(n == 'ChildUnderTSForCMAM')
    {
        n = 'Under TSF or CMAM';
        elem = titleCase($('#ChildUnderTSForCMAM option:selected').text());
    }

    if(n == 'EnrolledInSchool')
    {
        n = 'Enrolled In School';
        elem = titleCase($('#EnrolledInSchool option:selected').text());
    }

    if(n == 'ChildProtectionRisk')
    {
        n = 'Child Protection Risk';
        elem = titleCase($('#ChildProtectionRisk option:selected').text());
    }

    if(n == 'CBHIMembership')
    {
        n = 'CBHI Membership';
        elem = titleCase($('#CBHIMembership option:selected').text());
    }

    if(n == 'StartDateTDSPLW')
    {
        n = 'Start Date TDS';
        elem = $('#StartDateTDSPLW option:selected').text();
    }

    if(n == 'EndDateTDSPLW')
    {
        n = 'End Date TDS';
        elem = $('#EndDateTDSPLW option:selected').text();
    }

    if(n == 'NutritionalStatusPLW')
    {
        n = 'Nutritional Status Of PLW';
        elem = titleCase($('#NutritionalStatusPLW option:selected').text());
    }

    if(n == 'NutritionalStatusInfant')
    {
        n = 'Nutritional Status Of Infant';
        elem = $('#NutritionalStatusInfant option:selected').text();
    }

    if(n == 'StartDateTDSCMC')
    {
        n = 'Start Date TDS';
        elem = $('#StartDateTDSCMC option:selected').text();
    }

    if(n == 'EndDateTDSCMC')
    {
        n = 'End Date TDS';
        elem = $('#EndDateTDSCMC option:selected').text();
    }

    if(n == 'MalnourishmentDegree')
    {
        n = 'Degree Of Malnourishment';
        elem = titleCase($('#MalnourishmentDegree option:selected').text());
    }

    if(n == 'ClientType')
    {
        n = 'Type Of Client';
        elem = titleCase($('#ClientType option:selected').text());
    }

    if(n == 'ServiceType')
    {
        n = 'Type Of Service';
        elem = titleCase($('#ServiceType option:selected').text());
    }
    

    f = [n, elem];

    return f;
}

function titleCase(str) {
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++) {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
    }
    return str.join(' ');
  }