﻿//var page_count = 10;
//var gridHeader = 'ColumnID,ProfileDSDetailID,Kebele,Service<span class="HeaderChange">_</span>Provider,HouseHold<span class="HeaderChange">_</span>Member,';
//gridHeader = gridHeader + 'HouseHold<span class="HeaderChange">_</span>ID,Age,Gender,Generator,Generate<span class="HeaderChange">_</span>On';
//var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
//var mygrid;

$(document).ready(function () {
    $('#thebox').jtable({
        title: 'Form 4A Compliant Listing',
        paging: true,
        messages: {
            editRecord: 'Modify User Details'
        },
        actions: {
            listAction: '/ComplianceCapture/CapturedForm4AMainList',
        },
        fields: {
            ColumnID: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            //CHILD TABLE DEFINITION FOR "PHONE NUMBERS"
            Members: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (memberData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="../DHTMLX/codebase/imgs/plus2.gif" title="view Household members" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#thebox').jtable('openChildTable',
                                $img.closest('tr'),
                                {
                                    title: memberData.record.ServiceName + ' - Household members',
                                    paging: true,
                                    actions: {
                                        listAction: '/ComplianceCapture/CapturedForm4AList?ReportingPeriodID=' + memberData.record.ReportingPeriodID + '&ServiceID=' + memberData.record.ServiceID + '&KebeleID=' + memberData.record.KebeleID,
                                    },
                                    fields: {
                                        ProfileDSDetailID: {
                                            key: true,
                                            create: false,
                                            edit: false,
                                            list: false
                                        },
                                        HouseHoldMemberName: {
                                            title: 'HouseHold Member Name',
                                            width: '15%',
                                            create: false,
                                            edit: false
                                        },
                                        HouseHoldIDNumber: {
                                            title: 'HouseHold ID#',
                                            width: '10%',
                                            create: false,
                                            edit: false
                                        },
                                        Age: {
                                            title: 'Age',
                                            width: '5%',
                                            create: false,
                                            edit: false
                                        },
                                        Sex: {
                                            title: 'Sex',
                                            width: '5%',
                                            create: false,
                                            edit: false
                                        },
                                        Complied: {
                                            title: 'Complied',
                                            width: '5%',
                                            create: false,
                                            edit: false
                                        },
                                        Remarks: {
                                            title: 'Remarks',
                                            width: '15%',
                                            create: false,
                                            edit: false
                                        },
                                        GeneratedOn: {
                                            title: 'Record date',
                                            width: '8%',
                                            //type: 'date',
                                            //displayFormat: 'dd-mm-yy',
                                            create: false,
                                            edit: false
                                        }
                                    }
                                }, function (data) { //opened handler
                                    data.childTable.jtable('load');
                                });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            ReportingPeriodID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            ReportingPeriod: {
                title: 'Reporting Period',
                width: '10%',
                create: false,
                edit: false,
            },
            KebeleName: {
                title: 'Kebele Name',
                width: '10%',
                create: false,
                edit: false,
            },
            ServiceProvider: {
                title: 'Service Provider',
                width: '10%',
                create: false,
                edit: false,
            },
            ServiceID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            KebeleID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            ServiceName: {
                title: 'Service Name',
                width: '20%',
                create: false,
                edit: false,
            },
            CreatedBy: {
                title: 'Created By',
                create: false,
                edit: false,
                width: '5%'
            }
        }
    });
    $('#thebox').jtable('load');

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Household Head Name'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Household ID'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Gote'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Gare'));

    $("#cmdNew").click(function () {
        window.location.href = "../ComplianceCapture/CaptureForm4A";
    });
    $('#loading').hide();
});