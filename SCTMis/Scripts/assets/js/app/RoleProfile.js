﻿$(document).ready(function () {
    $('#loading').hide();
    tree = new dhtmlXTreeObject("gridbox", "100%", "100%", 0);

    tree.setImagePath("../DHTMLX/codebase/imgs/csh_scbrblue/");
    tree.setSkin("dhx_skyblue");
    tree.enableCheckBoxes(1);
    tree.enableThreeStateCheckboxes(true);
    tree.enableTreeImages(false);
    tree.loadXML("/Security/GridProfilesData?RecCount=" + 1 + "&RoleID=" + $('#RoleID').val());
    
    // scrolling ni shida hivi!!!
    tree.attachEvent("onOpenEnd", function (id, state) {
        document.getElementById('gridbox').style.overflowY =
            (document.getElementById('gridbox').offsetHeight > 549) ? 'auto' : 'hidden';
    });

    $("#cmdSave").click(function () {  
        var jsonArray = [];
        var list = new Array();

        var items = tree.getAllChecked();
        var listOfIDs = (items != "") ? items.split(',') : "";

        for (var i in listOfIDs) {
            list.push({ TaskID: listOfIDs[i], TaskStatus: 1 });
        }

        var params = {
            url: "/Security/SaveProfile",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            traditional: true,
            data: JSON.stringify({ RoleID: $("#RoleID").val(), model: list }),
            //data: JSON.stringify(data),//JSON.stringify(data),{ RoleProfile: JSON.stringify(jsonArray), RoleID: $("#RoleID").val() } JSON.stringify(data)
            success: function (dt) {
                window.location.href = "/Security/Roles?RoleID=" + $("#RoleID").val();
            }
        };

        $.ajax(params);

    });

    $("#cmdBack").click(function () {
        window.location.href = "/Security/Roles?RoleID=" + $("#RoleID").val();
    });
});