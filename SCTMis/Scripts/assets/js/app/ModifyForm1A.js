﻿var EVENTID = null;
var arrObj = [];
var SerialCount = 0;
var page_count = 4;
var mygrid;
var gridHeader = 'ColumnID,ProfileDSDetailID,Member<span class="HeaderChange">_</span>Name,Individual<span class="HeaderChange">_</span>ID';
gridHeader = gridHeader + ',Pregnant,Lactating,DOB,Age,Gender,Handicapped,Chronically<span class="HeaderChange">_</span>Ill';
gridHeader = gridHeader + ',NSN,Nutritional Status,childUnderTSForCMAM';
gridHeader = gridHeader + ',School Enrolled,Grade,School<span class="HeaderChange">_</span>Name,Child Protection Risk,Edit,Delete';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img';

//var myDOB, myColDate;
$(document).ready(function () {
    //$("#CBHIMembership").change();

    $('#CollectionDate').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y'
    });

    $('#DateOfBirth').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y',
        //view: 'years',
        onChange: function (view, elements) {
            DobChange();
        }
    });

    $("#RegionID,#WoredaID").prop("disabled", true);
    $("#DateOfBirth,#HouseHoldMemberName,#Pregnant,#Lactating,#Age,#Sex,#IndividualID,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").prop("disabled", true);
    $("#HouseHoldMemberName,#Pregnant,#Lactating,#IndividualID,#DateOfBirth,#Age,#SchoolName,#Sex,#NutritionalStatus,#Grade").val("");
    $("#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#ChildProtectionRisk").val("-");

    var age = getAge($('#DateOfBirth').val());
    if (!(isNaN(age))) {
        $("#Age").val(age);
    }

    $("#cmdNewMember").prop("disabled", true);
    if ($("#AllowEdit").val() == 'True') {
        $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", false);
    } else {
        $('#Gote,#NameOfHouseHoldHead,#HouseHoldIDNumber,#Gare,#HouseHoldIDNumber,#CollectionDate,#SocialWorker,#CCCCBSPCMember,#Remarks,#CBHIMembership,#CBHINumber').prop("disabled", true);
        $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", true);
    }
    // $("#cmdModify,#cmdProceed").prop("disabled", true); //original -> before 05-Feb-2018, allow saving without editing members
    //$("#cmdProceed").prop("disabled", true);

    $('#loading').hide();
    //ProfileDSHeaderID
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,155,115,0,0,0,65,65,0,0,0,75,0,75,0,120,85,55,55");
    mygrid.setColAlign("left,left,left,right,left,right,right,left,left,right,left,right,right,right,right,right,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();
    mygrid.loadXML("/Household/FetchGridForm1ADetailsByID?RecCount=" + page_count + "&ProfileDSHeaderID=" + $("#ProfileDSHeaderID").val());
    dhtmlxError.catchError("ALL", my_error_handler);

    $.ajax({
        type: "POST",
        url: "../Household/FetchJsonForm1ADetailsByID",
        data: "{  'RecCount':" + page_count + ",ProfileDSHeaderID : '" + $("#ProfileDSHeaderID").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            //alert(yy);
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                MemberCount = data.length;
                if (MemberCount != undefined) {

                    for (var i = 0; i < data.length; i++) {
                        var theDtls = new Object;
                        var d = data[i];
                        SerialCount = SerialCount + 1;
                        theDtls.ColumnID = SerialCount;
                        theDtls.ProfileDSDetailID = d.ProfileDSDetailID;
                        theDtls.HouseHoldMemberName = d.HouseHoldMemberName;
                        theDtls.IndividualID = d.IndividualID;
                        theDtls.Pregnant = d.Pregnant;
                        theDtls.Lactating = d.Lactating;
                        theDtls.DateOfBirth = d.DateOfBirth;
                        theDtls.Age = getAge(d.DateOfBirth);
                        theDtls.Sex = d.Sex;
                        theDtls.Handicapped = d.Handicapped;
                        theDtls.ChronicallyIll = d.ChronicallyIll;
                        theDtls.NutritionalStatus = d.NutritionalStatus;
                        theDtls.NutritionalStatusName = d.NutritionalStatusName;
                        theDtls.childUnderTSForCMAM = d.childUnderTSForCMAM;
                        theDtls.EnrolledInSchool = d.EnrolledInSchool;
                        theDtls.Grade = d.Grade;
                        theDtls.SchoolName = d.SchoolName;
                        theDtls.ChildProtectionRisk = d.ChildProtectionRisk;
                        //theDtls.Remarks = d.Remarks;
                        
                        arrObj.push(theDtls);
                    }
                    $("#MemberXml").val(JSON.stringify(arrObj));
                    //console.log(JSON.stringify(arrObj))
                }                
            }
        }
    });

    $("#cmdBack").click(function () {
        window.location.href = "/Household/Form1A";
    });

    $('#KebeleID,#NameOfHouseHoldHead,#HouseHoldIDNumber,#CollectionDate,#SocialWorker,#CCCCBSPCMember').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    $('#HouseHoldMemberName,#DateOfBirth,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#ChildProtectionRisk').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    $("#DateOfBirth").focusout(function () {
        DobChange();
    });

    $("#Pregnant,#Lactating").change(function (e) {
        DobChange();
    });

    $('#Sex').change(function (e) {
        $('#Pregnant,#Lactating').prop("disabled", true);

        var TheAge = getAge($('#DateOfBirth').val());
        if ($('#Sex').val() == 'M') {
            $('#Pregnant,#Lactating').val('-');
            $('#Pregnant,#Lactating').prop("disabled", true);

            if (parseInt(TheAge) >= 6) {
                $("#NutritionalStatus option[value='N']").attr("disabled", "disabled");
                $("#NutritionalStatus option[value='M']").attr("disabled", "disabled");
                $("#NutritionalStatus option[value='SM']").attr("disabled", "disabled");
                $('#NutritionalStatus').val('-');
            }

            if (parseInt(TheAge) > 18) {
                $('#childUnderTSForCMAM,#EnrolledInSchool,#Grade,#SchoolName,#NutritionalStatus').prop("disabled", true);
            }

        } else {
            if (parseInt(TheAge) < 12 || parseInt(TheAge) > 50) {
                $('#Pregnant,#Lactating').val('-');
                $('#Pregnant,#Lactating').prop("disabled", true);
            } else {
                $('#Pregnant,#Lactating').prop("disabled", false);
            }
        }
    });

    //#EnrolledInSchool
    $('#EnrolledInSchool').change(function (e) {
        var TheAge = getAge($('#DateOfBirth').val());

        if ($('#EnrolledInSchool').val() !== 'YES') {
            $('#Grade,#SchoolName').prop("disabled", true);
            $('#SchoolName').attr('readonly', true);

        } else {
            $('#Grade,#SchoolName').prop("disabled", false);
            $('#SchoolName').attr('readonly', false);
        }
    });

    //#CBHIMembership
    $('#CBHIMembership').change(function (e) {
        var membership = $('#CBHIMembership').val();
        if (membership == 'YES') {
            $('#CBHINumber').prop("disabled", false);
        } else {
            $('#CBHINumber').prop("disabled", true);
            $('#CBHINumber').val("");
        }
    });

    $("#cmdNewMember").click(function () {
        EVENTID = "ADD";
        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#Grade,#ChildProtectionRisk").val("");
       
        $("#Handicapped,#ChronicallyIll,#NutritionalStatus,#childUnderTSForCMAM,#EnrolledInSchool,#SchoolName,#ChildProtectionRisk").val("-");
        $("#cmdNewMember").prop("disabled", true);
        $("#cmdProceed").prop("disabled", false);

        $("#HouseHoldMemberName,#Remarks,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").prop("disabled", false);
        $("#Pregnant,#Lactating,#Age,#childUnderTSForCMAM,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName").prop("disabled", true);

        $('#HouseHoldMemberName').focus();
    });

    $("#cmdProceed").click(function () {
        //EVENTID = "ADD";
        var errors = [];
        var html = '<ul>';
        valid = true;
        $('#errors2').empty();
        if ($('#HouseHoldMemberName').val() == '') {
            errors.push('<li>House Hold Member Name Required</li>');
            valid = false;
        }

        if ($('#DateOfBirth').val() == "" && parseInt($('#Age').val()) <= 0) {
            errors.push('<li>Date Of Birth or age required</li>');
            valid = false;
        }

        if ($('#Sex').val() == '') {
            errors.push('<li>Gender is Required</li>');
            valid = false;
        }

        /*
        if ($('#Handicapped').val() == '') {
            errors.push('<li>Please specify if Handicapped</li>');
            valid = false;
        }

        if ($('#ChronicallyIll').val() == '') {
            errors.push('<li>Please specify if Chronically Ill</li>');
            valid = false;
        }

        if ($('#NutritionalStatus').val() == '') {
            errors.push('<li>Please specify Nutritional Status</li>');
            valid = false;
        }

        if ($('#EnrolledInSchool').val() == '') {
            errors.push('<li>Please specify if Enrolled In School</li>');
            valid = false;
        }

        if ($('#ChildProtectionRisk').val() == '') {
            errors.push('<li>Please specify if Child Potentially at Risk</li>');
            valid = false;
        }

        */

        if ($('#EnrolledInSchool').val() == 'YES' && $('#Grade').val() == '') {
            errors.push('<li>Please specify Grade</li>');
            valid = false;
        }

        if ($('#SchoolName').val() == "" && $('#EnrolledInSchool').val() == 'YES') {
            errors.push('<li>School Name is Required</li>');
            valid = false;
        }

        if (!valid) {
            html += errors.join('') + '</ul>'
            $('#errors2').show();
            $('#errors2').append(html);
            return valid;
        }
        else {
            $('#errors2').hide();
        }
        
        var theDtls = new Object;
        if (EVENTID == "EDIT") {
            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == $("#ColumnID").val()) {
                        arrObj[i].ProfileDSDetailID = $("#ProfileDSDetailID").val();
                        arrObj[i].HouseHoldMemberName = $("#HouseHoldMemberName").val();
                        arrObj[i].IndividualID = $("#IndividualID").val();
                        arrObj[i].Pregnant = $("#Pregnant").val();
                        arrObj[i].Lactating = $("#Lactating").val();
                        arrObj[i].DateOfBirth = $("#DateOfBirth").val();
                        arrObj[i].Age = $("#Age").val();
                        arrObj[i].Sex = $("#Sex").val();
                        arrObj[i].SexName = $("#Sex").find('option:selected').text();
                        arrObj[i].Handicapped = $("#Handicapped").val();
                        arrObj[i].HandicappedName = $("#Handicapped").find('option:selected').text();
                        arrObj[i].ChronicallyIll = $("#ChronicallyIll").val();
                        arrObj[i].ChronicallyIllName = $("#ChronicallyIll").find('option:selected').text();
                        arrObj[i].NutritionalStatus = $("#NutritionalStatus").val();
                        arrObj[i].NutritionalStatusName = $("#NutritionalStatus").find('option:selected').text();
                        arrObj[i].childUnderTSForCMAM = $("#childUnderTSForCMAM").val();
                        arrObj[i].EnrolledInSchool = $("#EnrolledInSchool").val();                        
                        arrObj[i].EnrolledInSchoolName = $("#EnrolledInSchool").find('option:selected').text();
                        arrObj[i].Grade = $("#Grade").val();
                        arrObj[i].SchoolName = $("#SchoolName").val();
                        arrObj[i].ChildProtectionRisk = $("#ChildProtectionRisk").val();
                    }
                }
            }
        }
        else {
            $('#errors2').hide(); 
            
            SerialCount = SerialCount + 1;
            theDtls.ColumnID = SerialCount;
            theDtls.ProfileDSDetailID = $("#ProfileDSDetailID").val();
            theDtls.HouseHoldMemberName = $("#HouseHoldMemberName").val();
            theDtls.IndividualID = $("#IndividualID").val();
            theDtls.Pregnant = $("#Pregnant").val();
            theDtls.Lactating = $("#Lactating").val();
            theDtls.DateOfBirth = $("#DateOfBirth").val();
            theDtls.Age = $("#Age").val();
            theDtls.Sex = $("#Sex").val();
            theDtls.SexName = $("#Sex").find('option:selected').text();
            theDtls.Handicapped = $("#Handicapped").val();
            theDtls.HandicappedName = $("#Handicapped").find('option:selected').text();
            theDtls.ChronicallyIll = $("#ChronicallyIll").val();
            theDtls.ChronicallyIllName = $("#ChronicallyIll").find('option:selected').text();
            theDtls.NutritionalStatus = $("#NutritionalStatus").val();
            theDtls.NutritionalStatusName = $("#NutritionalStatus").find('option:selected').text();
            theDtls.childUnderTSForCMAM = $("#childUnderTSForCMAM").val();
            theDtls.EnrolledInSchool = $("#EnrolledInSchool").val();
            theDtls.EnrolledInSchoolName = $("#EnrolledInSchool").find('option:selected').text();
            theDtls.Grade = $("#Grade").val();
            theDtls.SchoolName = $("#SchoolName").val();
            theDtls.ChildProtectionRisk = $("#ChildProtectionRisk").val();
            //theDtls.Remarks = $("#Remarks").val();         

            arrObj.push(theDtls);
        }

        //console.log(arrObj);
        var myXml = createXmlstring(arrObj, 1);
        mygrid.clearAll();
        mygrid.parse(myXml);

        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName").val('');
        $("ChildProtectionRisk").val('-');
        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").prop("disabled", true);

        $("#cmdNewMember").prop("disabled", true);
        if ($("#AllowEdit").val() == 'True') {
            $("#cmdModify").prop("disabled", false);
            $("#cmdNewMember").prop("disabled", false);
        }
        
        $("#cmdProceed").prop("disabled", true);             
    });
});

function createWindow(_ColumnID) {
    var selectedRow = mygrid.getSelectedId();
    _ColumnID = mygrid.cells(selectedRow, 0).getValue();
    $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Age,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").val('')
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {

            //console.log(arrObj);
            if (arrObj[i].ColumnID == _ColumnID) {
                $("#ColumnID").val(arrObj[i].ColumnID);
                $("#HouseHoldMemberName").val(arrObj[i].HouseHoldMemberName);
                $("#IndividualID").val(arrObj[i].IndividualID);
                $("#DateOfBirth").val(arrObj[i].DateOfBirth);
                $("#Age").val(arrObj[i].Age);
                $("#Sex").val(arrObj[i].Sex);
                $("#Pregnant").val(arrObj[i].Pregnant);
                $("#Lactating").val(arrObj[i].Lactating);
                $("#Handicapped").val(arrObj[i].Handicapped);
                $("#ChronicallyIll").val(arrObj[i].ChronicallyIll);
                $("#NutritionalStatus").val(arrObj[i].NutritionalStatus);
                $("#childUnderTSForCMAM").val((arrObj[i].childUnderTSForCMAM == "N/A" ? "-" : arrObj[i].childUnderTSForCMAM));
                $("#EnrolledInSchool").val((arrObj[i].EnrolledInSchool=="N/A"?"-":arrObj[i].EnrolledInSchool));
                $("#Grade").val(arrObj[i].Grade);
                $("#SchoolName").val(arrObj[i].SchoolName);
                //$("#ChildProtectionRisk").val(arrObj[i].ChildProtectionRisk);
                $("#ChildProtectionRisk").val((arrObj[i].ChildProtectionRisk=="N/A"?"-":arrObj[i].ChildProtectionRisk));
                $('#Sex').change();
                $('#DateOfBirth').change();                
            }
        }
    }

    $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", true);
    if ($("#AllowEdit").val() == 'True') {
        $("#cmdProceed,#cmdNewMember").prop("disabled", false);
        $("#HouseHoldMemberName,#Remarks,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").prop("disabled", false);
        $('#HouseHoldMemberName').focus();

        DobChange();
    }

    $('#EnrolledInSchool').change();

    //$('#CBHIMembership').change();
    EVENTID = "EDIT";
}

function UpdateMembers(_ColumnID) {
    createWindow(_ColumnID);
}

function createXmlstring(arrObject, inMemory) {
    var xml;
    var gridID;
    var reclength = 0;
    xml = '';
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            reclength + reclength + 1;
            xml = xml + '<row id="' + i + '">';
            for (var j in arrObj[i]) {
                if (arrObj[i].hasOwnProperty(j)) {
                    if (j == "ColumnID") {
                        xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        gridID = arrObj[i][j];
                    }
                }
                if (j == "ProfileDSDetailID" || j == "HouseHoldMemberName" || j == "IndividualID" || j == "Pregnant"
                    || j == "Lactating" || j == "DateOfBirth" || j == "Age" || j == "Sex"
                    || j == "Handicapped" || j == "ChronicallyIll" || j == "NutritionalStatusName" || j == "NutritionalStatus"
                    || j == "childUnderTSForCMAM" || j == "EnrolledInSchool" || j == "Grade" || j == "SchoolName" || j == "ChildProtectionRisk") {
                    xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                }
            }
            xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
            xml = xml + '<cell>../DHTMLX/codebase/imgs/but_cut.gif^Delete record^javascript:DeleteRecord(' + gridID + ');^_self</cell>';
            xml = xml + '</row>';
        }
    }
    xml = '<rows total_count="' + reclength + '">' + xml + '</rows>';
    $("#MemberXml").val(JSON.stringify(arrObject));
    return xml;
}

function DobChange() {
    $("#NutritionalStatus option[value='N']").attr("disabled", "disabled");
    $("#NutritionalStatus option[value='M']").attr("disabled", "disabled");
    $("#NutritionalStatus option[value='SM']").attr("disabled", "disabled");
    //$("#ChronicallyIll option[value='-']").attr("disabled", "disabled");
    //$("#Handicapped option[value='-']").attr("disabled", "disabled");
    $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
    $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");
    $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').prop('disabled', true);

    $("#NutritionalStatus,#EnrolledInSchool,#Grade").val("-");

    if ($('#DateOfBirth').val() == "") {
        $('#DateOfBirth').focus();
    } else {
        var TheAge = getAge($('#DateOfBirth').val());

        $("#Age").val(TheAge);

        //FOR SCHOOL GOING CHILDREN
        if (parseInt(TheAge) > 5 && parseInt(TheAge) < 18) {
            $('#EnrolledInSchool').prop("disabled", false);

            $("#childUnderTSForCMAM").val("-");
            $('#childUnderTSForCMAM').attr('readonly', true);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

            $("#EnrolledInSchool,#Grade option[value='NO']").attr("disabled", false);
            $("#EnrolledInSchool,#Grade option[value='YES']").attr("disabled", false);

            DisableEnableGrade("E");
        }

        //THIS IS FOR PREGNANT WOMEN ABOVE 12 YEARS BUT NOT OLDER THAN 50 YEARS
        if (parseInt(TheAge) >= 12 && parseInt(TheAge) <= 50) {
            if ($("#Sex").val() == "F" && ($("#Pregnant").val() == "YES" || $("#Lactating").val() == "YES")) {
                $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').attr('readonly', true);
                $('#EnrolledInSchool,#Grade,#childUnderTSForCMAM,#NutritionalStatus').val('-');

                $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
                $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

                $('#NutritionalStatus').prop("disabled", false);

                $("#NutritionalStatus option[value='N']").attr("disabled", false);
                $("#NutritionalStatus option[value='M']").attr("disabled", false);
                $("#NutritionalStatus option[value='SM']").attr("disabled", false);
            }
        }
        //FOR INFANTS BELOW 5 YEARS
        if (parseInt(TheAge) <= 5) {
            $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').attr('readonly', true);
            $('#EnrolledInSchool,#Grade,#childUnderTSForCMAM,#NutritionalStatus').val('-');

            $('#NutritionalStatus').prop("disabled", false);

            $("#NutritionalStatus option[value='N']").attr("disabled", false);
            $("#NutritionalStatus option[value='M']").attr("disabled", false);
            $("#NutritionalStatus option[value='SM']").attr("disabled", false);
        }

        $('#Sex').change();
    }
}

$('#NutritionalStatus').change(function (e) {
    var TheAge = getAge($('#DateOfBirth').val());
    $("#childUnderTSForCMAM").val("-");
    $('#childUnderTSForCMAM').attr('readonly', true);
    $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
    $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");
    $('#SchoolName').prop("disabled", true);

    if (parseInt(TheAge) <= 5) {
        if ($("#NutritionalStatus").val() == "M" || $("#NutritionalStatus").val() == "SM") {
            //Not sure if this part should be enabled.
            $('#childUnderTSForCMAM').attr('readonly', false);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", false);
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", false);

            $('#childUnderTSForCMAM').prop("disabled", false);
        } else {
            $("#childUnderTSForCMAM").val("-");
            $('#childUnderTSForCMAM').attr('readonly', true);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");
        }
    }
});

function DeleteRecord(_ColumnID) {
    if ($("#AllowEdit").val().toUpperCase() != "FALSE") {
        var selectedRow = mygrid.getSelectedId();
        var columnID = mygrid.cells(selectedRow, 0).getValue();
        createWindow(columnID);
        var result = confirm("Do you want to delete The Record?");
        if (result) {
            for (var i in arrObj) {
                
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == columnID) {
                        var index = arrObj.indexOf(arrObj[i]);
                        if (index > -1) {
                            arrObj.splice(index, 1);

                            var myXml = createXmlstring(arrObj, 1);
                            mygrid.clearAll();
                            mygrid.parse(myXml);

                            break;
                        }
                    }
                }
            }
        }

        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Grade,#SchoolName").val('');
        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName").prop("disabled", true);
        $("#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#ChildProtectionRisk").val("-");
        $("#cmdModify").prop("disabled", false);
        $("#cmdProceed").prop("disabled", true);
    }
}

function DisableEnableGrade(Status) {
    if (Status == "E") {
        $("#Grade option[value='1']").attr("disabled", false);
        $("#Grade option[value='2']").attr("disabled", false);
        $("#Grade option[value='3']").attr("disabled", false);
        $("#Grade option[value='4']").attr("disabled", false);
        $("#Grade option[value='5']").attr("disabled", false);
        $("#Grade option[value='6']").attr("disabled", false);
        $("#Grade option[value='7']").attr("disabled", false);
        $("#Grade option[value='8']").attr("disabled", false);
        $("#Grade option[value='9']").attr("disabled", false);
        $("#Grade option[value='10']").attr("disabled", false);
        $("#Grade option[value='11']").attr("disabled", false);
        $("#Grade option[value='12']").attr("disabled", false);
        $("#Grade option[value='13']").attr("disabled", false);
        $("#Grade option[value='14']").attr("disabled", false);
    } else {
        $("#Grade option[value='1']").attr("disabled", "disabled");
        $("#Grade option[value='2']").attr("disabled", "disabled");
        $("#Grade option[value='3']").attr("disabled", "disabled");
        $("#Grade option[value='4']").attr("disabled", "disabled");
        $("#Grade option[value='5']").attr("disabled", "disabled");
        $("#Grade option[value='6']").attr("disabled", "disabled");
        $("#Grade option[value='7']").attr("disabled", "disabled");
        $("#Grade option[value='8']").attr("disabled", "disabled");
        $("#Grade option[value='9']").attr("disabled", "disabled");
        $("#Grade option[value='10']").attr("disabled", "disabled");
        $("#Grade option[value='11']").attr("disabled", "disabled");
        $("#Grade option[value='12']").attr("disabled", "disabled");
        $("#Grade option[value='13']").attr("disabled", "disabled");
        $("#Grade option[value='14']").attr("disabled", "disabled");
    }
}