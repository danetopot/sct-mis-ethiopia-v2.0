var myExampleData = {};

//pie Chart sample data and options
myExampleData.pieChartData = [{
	data : [[0, 4]],
	label : "Compliance"
}, {
	data : [[0, 3]],
	label : "Monitoring"
}, {
	data : [[0, 1.03]],
	label : "Reporting",
	pie : {
		explode : 50
	}
}, {
	data : [[0, 3.5]],
	label : "Co-responsibility"
}];

myExampleData.pieChartOptions = {
	HtmlText : false,
	grid : {
		verticalLines : false,
		horizontalLines : false
	},
	xaxis : {
		showLabels : false
	},
	yaxis : {
		showLabels : false
	},
	pie : {
		show : true,
		explode : 6
	},
	mouse : {
		track : true
	},
	legend : {
		position : "n",
		backgroundColor : "#D2E8FF"
	}
};

//Pie chart sample data ends here

//bar Chart sample data and options

myExampleData.constructBubbleChartData = function() {
	var d1 = [];
	var d2 = []
	var point
	var i;

	//d1 = [[1, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)], [5, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)], [9, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)], [13, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)], [17, 0, 0]];
	//d1 = [[1, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)], [5, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)], [9, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)], [13, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)], [17, 0, 0]];
	//d2 = [[1, 10], [2, 15], [3, 7], [4, 23]];

	var d1 = [[1, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)]];
	var d2 = [[5, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)]];
	var d3 = [[9, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)]];
	var d4 = [[13, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)]];
	var d5 = [[17, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)]];

	//for ( i = 0; i < 10; i++) {
	//	point = [i, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)];
	//	d1.push(point);

	//	point = [i, Math.ceil(Math.random() * 10), Math.ceil(Math.random() * 10)];
	//	d2.push(point);
	//} 
    //return [d1, d2];
	return [d1, d2, d3, d4, d5];
};
myExampleData.bubbleChartData = myExampleData.constructBubbleChartData();

myExampleData.bubbleChartOptions = {
	bubbles : {
		show : true,
		baseRadius : 5
	},
	xaxis : {
		min : -4,
		max: 14,
		ticks: [[1, "Disabled"], [5, "PLW"], [9, "Chronically Sick"], [13, "Malnourished"]]
	},
	yaxis : {
		min : -4,
		max : 14
	},
	mouse : {
		track : true,
		relative : true
	}
};

//bar chart sample data ends here

//bar Chart sample data and options

myExampleData.constructBarChartData = function() {
    var d1 = [[1, 12]];
    var d2 = [[2, 17]];
    //var d1 = [[1, 17], [2, 23], [3, 6]];
    var d3 = [[3, 22]];
    var d4 = [[4, 10]];
    var d5 = [[5, 6]];

	//var httpRequest = new XMLHttpRequest();
	//httpRequest.open('GET', "../Dashboard/HouseProfileByAge1");
	//httpRequest.send();
	////console.log(JSON.stringify(httpRequest.responseText));
	////d1 = JSON.parse(httpRequest.responseText);
	//d1 = httpRequest.responseText;
    return [d1, d2, d3, d4, d5];
};


myExampleData.barChartData = myExampleData.constructBarChartData();

myExampleData.barChartOptions = {
	bars : {
		show : true,
		horizontal : false,
		shadowSize : 0,
		barWidth : 0.5
	},
	mouse : {
		track : true,
		relative : true
	},
	yaxis : {
		min : 0,
		autoscaleMargin : 1
	},
	xaxis: { ticks: [[1, "Children(0-5)"], [2, "Children(6-18)"], [3, "Fit(18-59)"], [4, "Unfit(18-59)"], [5, "Elderly(60+)"]] }

};

//bar chart sample data ends here

//line Chart sample data and options

myExampleData.constructLineChartData = function() {
    var d1 = [[0, 100], [1, 150], [2, 50], [3, 43], [4, 75], [5, 84], [6, 20], [7, 100], [8, 50], [9, 8], [10, 45], [11, 12]];
    var d2 = [[0, 80], [1, 100], [2, 20], [3, 15], [4, 70], [5, 60], [6, 15], [7, 90], [8, 10], [9, 6], [10, 40], [11, 2]];
    var d3 = [[0, 20], [1, 50], [2, 30], [3, 27], [4, 5], [5, 24], [6, 5], [7, 5], [8, 40], [9, 2], [10, 5], [11, 10]];
	var i;

	//for ( i = 0; i < 14; i += 0.5) {
	//	d2.push([i, Math.sin(i)]);
	//}
	return [d1, d2, d3];
};
myExampleData.lineChartData = myExampleData.constructLineChartData();

myExampleData.lineChartOptions = {
	xaxis : {
	    minorTickFreq: 4,
	    ticks: [[0, "Jan"], [1, "Feb"], [2, "Mar"], [3, "Apr"], [4, "May"], [5, "Jun"], [6, "Jul"], [7, "Aug"], [8, "Sep"], [9, "Oct"], [10, "Nov"], [11, "Dec"]]
	},
	grid : {
		minorVerticalLines : true
	},
	selection : {
		mode : "x",
		fps : 30
	},
	legend: {
	    position: "se",
	    backgroundColor: "#D2E8FF"
	}
};

//line chart sample data ends here

//bar Chart sample data and options

myExampleData.constructBarChartData4 = function () {
    var d1 = [];
    d1 = [[1, 6], [2, 5], [3, 10], [4, 15]]
    d2 = [[1.45, 3], [2.45, 1], [3.45, 9], [4.45, 5]]
    return [d1,d2];
};


myExampleData.barChartData4 = myExampleData.constructBarChartData4();

myExampleData.barChartOptions4 = {
    bars: {
        show: true,
        horizontal: false,
        shadowSize: 0,
        barWidth: 0.45
    },
    mouse: {
        track: true,
        relative: true
    },
    yaxis: {
        min: 0,
        autoscaleMargin: 1
    },
    xaxis: { ticks: [[1, "Lack of school material"], [2, "Distance to school"], [3, "Drought or floods"], [4, "Moved to other Kebele"]] }

};

//bar chart sample data ends here

//table Widget sample data and options

myExampleData.constructTableWidgetData = function(){
	return ["Trident"+Math.ceil(Math.random() * 10), "IE" + Math.ceil(Math.random() * 10), "Win"+Math.ceil(Math.random() * 10)]
};

myExampleData.tableWidgetData = {
	"aaData" : [myExampleData.constructTableWidgetData(), 
	myExampleData.constructTableWidgetData(), 
	myExampleData.constructTableWidgetData(), 
	myExampleData.constructTableWidgetData(),
	myExampleData.constructTableWidgetData(),
	myExampleData.constructTableWidgetData(), 
	myExampleData.constructTableWidgetData()
	],

	"aoColumns" : [{
		"sTitle" : "Engine"
	}, {
		"sTitle" : "Browser"
	}, {
		"sTitle" : "Platform"
	}],
	"iDisplayLength": 25,
	"aLengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
	"bPaginate": true,
	"bAutoWidth": false
};
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//table widget sample data ends here
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
