﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml.Linq;

namespace SCTMis.Controllers
{

    [SessionConfig.SessionExpireFilter]
    public class ComplianceCaptureController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        ComplianceServices dbCompliance = new ComplianceServices();
        AccountServices dbAcService = new AccountServices();
        static int _currentPage = 0;
        string ip = System.Web.HttpContext.Current.Request.UserHostAddress;
        //
        // GET: /ComplianceCapture/

        [HttpPost]
        public void UpdateCurrentPage(PageParams pageParam)
        {
            _currentPage = pageParam.CurrentPage;
        }

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4AList()
        {
            ViewBag.Subtitle = "Form 4A Compliant Listing";
            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4AMainList(int jtStartIndex = 0, int jtPageSize = 0)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                Tuple<List<int>, List<CapturedForm4MainAGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4MainAGrid>(";Exec getCapturedForm4AMainList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4AList(CapturedForm4Models myModel)
        {
            try
            {

                Tuple<List<int>, List<CapturedForm4AGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4AGrid>(";Exec getCapturedForm4AList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID,@ServiceID",
                    new
                    {
                        jtStartIndex = myModel.jtStartIndex,
                        jtPageSize = myModel.jtPageSize,
                        KebeleID   = myModel.KebeleID,
                        ReportingPeriodID = myModel.ReportingPeriodID,
                        ServiceID = myModel.ServiceID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        
        //CaptureForm4A
        [Authorize]
        public ActionResult CaptureForm4A()
        {
            ViewBag.Subtitle = "Captured Form 4A Listing";

            CaptureForm4 models = new Models.CaptureForm4();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetForm4AKebeles();
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.serviceprovids = dbService.GetForm4AServiceProviders(models.KebeleID);
            models.intgrtedservis = dbService.GetForm4AServices(models.KebeleID, models.ServiceProviderID);

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }
        [Authorize]
        [HttpPost]
        public JsonResult CapturedForm4AData(CapturedForm4Model newmodel)
        {
            ViewBag.Subtitle = "Capture Form 4A Details";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4ANew(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK"});
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult CaptureForm4A(CaptureForm4 newmodel)
        {
            ViewBag.Subtitle = "Capture Form 4A Details";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4A(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4A Listing";

                        return RedirectToAction("Form4AList");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4AKebeles();
                newmodel.serviceprovids = dbService.GetForm4AServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4AServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                return View("CaptureForm4A", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4AKebeles();
                newmodel.serviceprovids = dbService.GetForm4AServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4AServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("CaptureForm4A", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4ADSMainList(Form4ADSMainModel model)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {

                Tuple<List<int>, List<getForm4ADSMainList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4ADSMainList>(";Exec getForm4ADSMainList @jtStartIndex, @jtPageSize,@KebeleID",
                    new
                    {
                        jtStartIndex = model.jtStartIndex,
                        jtPageSize = model.jtPageSize,
                        KebeleID = model.KebeleID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4ADSList(int jtStartIndex, int jtPageSize, string jtSorting,getForm4ADSMainList model)
        {
            try
            {
                Tuple<List<int>, List<getForm4ADSList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4ADSList>(";Exec getForm4ADSList @KebeleID, @ServiceID, @ReportingPeriodID, @jtStartIndex, @jtPageSize",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ServiceID = model.ServiceID,
                        ReportingPeriodID = model.ReportingPeriodID,
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4BList()
        {
            ViewBag.Subtitle = "Form 4B Compliant Listing";
            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4BMainList(int jtStartIndex = 0, int jtPageSize = 0)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4MainAGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4MainAGrid>(";Exec getCapturedForm4BMainList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4BList(CapturedForm4Models myModel)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4BGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4BGrid>(";Exec getCapturedForm4BList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID,@ServiceID",
                    new
                    {
                        jtStartIndex = myModel.jtStartIndex,
                        jtPageSize = myModel.jtPageSize,
                        KebeleID = myModel.KebeleID,
                        ReportingPeriodID = myModel.ReportingPeriodID,
                        ServiceID = myModel.ServiceID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //CaptureForm4B
        [Authorize]
        public ActionResult CaptureForm4B()
        {
            ViewBag.Subtitle = "Capture Form 4B";

            CaptureForm4 models = new Models.CaptureForm4();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetForm4BKebeles();
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.serviceprovids = dbService.GetForm4BServiceProviders(models.KebeleID);
            models.intgrtedservis = dbService.GetForm4BServices(models.KebeleID, models.ServiceProviderID);

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [Authorize]
        [HttpPost]
        public JsonResult CapturedForm4BData(CapturedForm4BModel newmodel)
        {
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4BNew(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK" });
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4BTDSPLWMainList(Form4ADSMainModel model)
        {
            try
            {
                Tuple<List<int>, List<getForm4ADSMainList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4ADSMainList>(";Exec getForm4BDSMainList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID",
                    new
                    {
                        jtStartIndex = model.jtStartIndex,
                        jtPageSize = model.jtPageSize,
                        KebeleID = model.KebeleID,
                        ReportingPeriodID = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4BTDSPLWList(int jtStartIndex, int jtPageSize, string jtSorting, getForm4ADSMainList model)
        {
            try
            {
                Tuple<List<int>, List<getForm4BTDSPLWList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4BTDSPLWList>(";Exec getForm4BTDSPLWList @KebeleID, @ServiceID,@ReportingPeriodID, @jtStartIndex, @jtPageSize",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ServiceID = model.ServiceID,
                        ReportingPeriodID = model.ReportingPeriodID,
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4CList()
        {
            ViewBag.Subtitle = "Form 4C Compliant Listing";
            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4CMainList(int jtStartIndex = 0, int jtPageSize = 0)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4MainAGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4MainAGrid>(";Exec getCapturedForm4CMainList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //[HttpPost]
        //[Authorize]
        //public JsonResult CapturedForm4BList(CapturedForm4Models myModel)
        //{
        //    try
        //    {
        //        Tuple<List<int>, List<CapturedForm4BGrid>> resultset
        //            = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4BGrid>(";Exec getCapturedForm4BList @jtStartIndex, @jtPageSize,@ReportingPeriodID,@ServiceID",
        //            new
        //            {
        //                jtStartIndex = myModel.jtStartIndex,
        //                jtPageSize = myModel.jtPageSize,
        //                ReportingPeriodID = myModel.ReportingPeriodID,
        //                ServiceID = myModel.ServiceID
        //            });

        //        return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4CList(CapturedForm4Models myModel)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4CGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4CGrid>(";Exec getCapturedForm4CList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID,@ServiceID",
                    new
                    {
                        jtStartIndex = myModel.jtStartIndex,
                        jtPageSize = myModel.jtPageSize,
                        KebeleID = myModel.KebeleID,
                        ReportingPeriodID = myModel.ReportingPeriodID,
                        ServiceID = myModel.ServiceID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult CapturedForm4CData(CapturedForm4CModel newmodel)
        {
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4CNew(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK" });
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //CaptureForm4C
        //CaptureForm4C
        [Authorize]
        public ActionResult CaptureForm4C()
        {
            ViewBag.Subtitle = "Capture Form 4C";

            CaptureForm4 models = new Models.CaptureForm4();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetForm4CKebeles();
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.serviceprovids = dbService.GetForm4CServiceProviders(models.KebeleID);
            models.intgrtedservis = dbService.GetForm4CServices(models.KebeleID, models.ServiceProviderID);

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }


        [Authorize]
        [HttpPost]
        public ActionResult CaptureForm4C(CaptureForm4 newmodel)
        {
            ViewBag.Subtitle = "Capture Form 4C Details";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4C(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4C Listing";

                        return RedirectToAction("Form4CList");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4CKebeles();
                newmodel.serviceprovids = dbService.GetForm4CServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4CServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                return View("CaptureForm4C", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4CKebeles();
                newmodel.serviceprovids = dbService.GetForm4CServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4CServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                ModelState.AddModelError("", ex.Message);
                GeneralServices.LogError(ex);
                return View("CaptureForm4C", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4CTDSCMCMainList(Form4ADSMainModel model)
        {
            try
            {
                Tuple<List<int>, List<getForm4ADSMainList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4ADSMainList>(";Exec getForm4CTDSCMCMainList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID",
                    new
                    {
                        jtStartIndex = model.jtStartIndex,
                        jtPageSize = model.jtPageSize,
                        KebeleID = model.KebeleID,
                        ReportingPeriodID = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4CTDSCMCList(int jtStartIndex, int jtPageSize, string jtSorting, getForm4ADSMainList model)
        {
            try
            {
                Tuple<List<int>, List<getForm4CTDSCMCList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4CTDSCMCList>(";Exec getForm4CTDSCMCList @KebeleID,@ServiceID,@ReportingPeriodID, @jtStartIndex, @jtPageSize",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ServiceID = model.ServiceID,
                        ReportingPeriodID = model.ReportingPeriodID,
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        // FORM 4D
        [Authorize]
        public ActionResult Form4DFormList()
        {
            ViewBag.Subtitle = "Cases Listing - Form 4D";
            ViewBag.CurrentPage = _currentPage;

            dbAcService.InsertAuditTrail(User.Identity.Name, "Child Protection", "Form4D List View", ip, "",
            "Form4D List Access  at " + DateTime.Now.ToString());

            return View();
        }

        [Authorize]
        [OutputCache(Duration = 10, VaryByParam = "none")]
        public ActionResult Form4DHHList(CaptureForm4D newModel)
        {
            ViewBag.Subtitle = "Household Listing - Form 4D";
            ViewBag.CurrentPage = _currentPage;

            dbAcService.InsertAuditTrail(User.Identity.Name, "Household", "Form4D Household Listing", ip, "",
            "Form4D List Access  at " + DateTime.Now.ToString());
            return View(newModel);
        }

        [Authorize]
        public ActionResult Form4DFollowUpList()
        {
            ViewBag.Subtitle = "Cases FollowUps Listing - Form 4D";
            ViewBag.CurrentPage = _currentPage;

            dbAcService.InsertAuditTrail(User.Identity.Name, "Child Protection", "Form4D List View", ip, "",
            "Form4D List Access  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchForm4DHHList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);

                var con = new PetaPoco.Database("conString");

                Tuple<List<int>, List<Form4DGrid>> resultset
                = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form4DGrid>(";Exec GetChildProtectionHHList @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize,
                    isSearch = jtisSearch,
                    SearchTypeID = jtSearchTypeID,
                    SearchKeyword = jtSearchKeyword
                });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4DGrid>(resultset.Item2);

                ViewBag.CurrentPage = _currentPage;

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 10);

                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchForm4DFormList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);

                var con = new PetaPoco.Database("conString");

                Tuple<List<int>, List<CapturedForm4DGrid>> resultset
                = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4DGrid>(";Exec GetChildProtectionFormList @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize,
                    isSearch = jtisSearch,
                    SearchTypeID = jtSearchTypeID,
                    SearchKeyword = jtSearchKeyword
                });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<CapturedForm4DGrid>(resultset.Item2);

                ViewBag.CurrentPage = _currentPage;

                var canApproveForm = dbAcService.GetTaskAccessRights(105);
                //var canCloseForm = dbAcService.GetTaskAccessRights(106);
                string sb;
                if (canApproveForm)
                {
                    sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 11);
                }
                else
                {
                    sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 12);
                }

                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchForm4DFollowUpList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);

                var con = new PetaPoco.Database("conString");

                Tuple<List<int>, List<CapturedForm4DFollowUpGrid>> resultset
                = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4DFollowUpGrid>(";Exec getCapturedForm4DFollowUpAdminList @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize,
                    isSearch = jtisSearch,
                    SearchTypeID = jtSearchTypeID,
                    SearchKeyword = jtSearchKeyword
                });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<CapturedForm4DFollowUpGrid>(resultset.Item2);

                ViewBag.CurrentPage = _currentPage;

                string sb;
                sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4DMainList(int jtStartIndex = 0, int jtPageSize = 0)
        {
            try

            {
                Tuple<List<int>, List<CapturedForm4MainAGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4MainAGrid>(";Exec getForm4DAdminList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult CaptureForm4D()
        {
            ViewBag.Subtitle = "Capture Cases - Form 4D";
            CaptureForm4D newModel = new CaptureForm4D();
            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.worker = dbService.GetSocialWorkers();

            try
            {
                string profileHeaderID = GeneralServices.strQueryString("ProfileDetailID", string.Empty);
                string clientTypeID = GeneralServices.strQueryString("ClientTypeID", string.Empty);
                TempData["profileHeaderID"] = profileHeaderID;
                TempData["clientTypeID"] = clientTypeID;

                Form4DDetails form4DDetails = new Form4DDetails();
                form4DDetails = dbCompliance.FetchForm4DDetailsByID(clientTypeID, profileHeaderID);
                newModel.RegionID = form4DDetails.RegionID;
                newModel.WoredaID = form4DDetails.WoredaID;
                newModel.KebeleID = form4DDetails.KebeleID;
                newModel.Gote = form4DDetails.Gote;
                newModel.Gare = form4DDetails.Gare;
                newModel.NameOfHouseHoldHead = form4DDetails.NameOfHouseHoldHead;
                newModel.HouseHoldMemberName = form4DDetails.HouseHoldMemberName;
                newModel.HouseHoldIDNumber = form4DDetails.HouseHoldIDNumber;
                newModel.HouseHoldMemberAge = form4DDetails.HouseHoldMemberAge;
                newModel.HouseHoldMemberSex = form4DDetails.HouseHoldMemberSex;
                newModel.ProfileDSHeaderID = profileHeaderID;
                newModel.ProfileDSDetailID = form4DDetails.ProfileDSDetailID;
                newModel.ClientTypeID = clientTypeID;

                return View(newModel);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return View(newModel);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult CaptureForm4D(CaptureForm4D newModel)
        {
            ViewBag.Subtitle = "Capture Cases - Form 4D";
            string errMessage = string.Empty;
            string profileHeaderID = newModel.ProfileDSHeaderID;
            string clientTypeID = newModel.ClientTypeID;

            Form4DDetails form4DDetails = new Form4DDetails();
            form4DDetails = dbCompliance.FetchForm4DDetailsByID(clientTypeID, profileHeaderID);
            newModel.RegionID = form4DDetails.RegionID;
            newModel.WoredaID = form4DDetails.WoredaID;
            newModel.KebeleID = form4DDetails.KebeleID;
            newModel.Gote = form4DDetails.Gote;
            newModel.Gare = form4DDetails.Gare;
            newModel.NameOfHouseHoldHead = form4DDetails.NameOfHouseHoldHead;
            newModel.HouseHoldMemberName = form4DDetails.HouseHoldMemberName;
            newModel.HouseHoldIDNumber = form4DDetails.HouseHoldIDNumber;
            newModel.HouseHoldMemberAge = form4DDetails.HouseHoldMemberAge;
            newModel.HouseHoldMemberSex = form4DDetails.HouseHoldMemberSex;
            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.worker = dbService.GetSocialWorkers();

            try
            {
                if (!string.IsNullOrEmpty(newModel.ChildProtectionID))
                {
                    var cpnumberexists = GeneralServices.ChildProtectionNumberExists(newModel.ChildProtectionID, AppConstants.Form4D, null, false);
                    ModelState.AddModelError("", string.Format("There exists a Child with the Child Protection ID {0}", newModel.ChildProtectionID));
                    return View(newModel);
                }

                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4D(newModel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4D Form Listing";
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Child Protection", "Save Form4D", ip, "",
                           "Form4D Details " + newModel.HouseHoldIDNumber + "-" + newModel.CapturedXml + " Save Successfuly  at " + DateTime.Now.ToString());
                        return RedirectToAction("Form4DFormList");
                    }
                }

                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);
            }
            catch(Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            return View(newModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult ModifyForm4D()
        {
            ViewBag.Subtitle = "Modify Cases - Form 4D";
            ModifyForm4D newModel = new ModifyForm4D();

            try
            {
                string formid = GeneralServices.strQueryString("Id", string.Empty);

                newModel = dbCompliance.FetchCapturedForm4DDetailsByID(formid);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ModifyForm4D(ModifyForm4D newModel)
        {
            ViewBag.Subtitle = "Modify Cases - Form 4D";
            string errMessage = string.Empty;

            try
            {

                if (!string.IsNullOrEmpty(newModel.ChildProtectionId))
                {
                    var cpnumberexists = GeneralServices.ChildProtectionNumberExists(newModel.ChildProtectionId, AppConstants.Form4D, null, false);
                    ModelState.AddModelError("", string.Format("There exists a Child with the Child Protection ID {0}", newModel.ChildProtectionId));
                    return View(newModel);
                }

                if (ModelState.IsValid)
                {
                    var result = dbCompliance.UpdateCapturedForm4D(newModel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4D Form Listing";
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Child Protection", "Update Form4D", ip, "",
                            "Form4D Details " + newModel.HouseHoldIDNumber + "-" + newModel.CapturedXml + " Update Successfuly  at " + DateTime.Now.ToString());
                        return RedirectToAction("Form4DFormList");
                    }
                }

                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel = dbCompliance.FetchCapturedForm4DDetailsByID(newModel.ChildProtectionHeaderID);
            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult ApproveForm4D()
        {
            ViewBag.Subtitle = "Approve Cases - Form 4D";
            ModifyForm4D newModel = new ModifyForm4D();

            try
            {
                string formid = GeneralServices.strQueryString("Id", string.Empty);

                newModel = dbCompliance.FetchCapturedForm4DDetailsByID(formid);

                var canApproveForm = dbAcService.GetTaskAccessRights(105);
                newModel.AllowApprove = canApproveForm ? 1 : 0;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ApproveForm4D(ModifyForm4D newModel)
        {
            ViewBag.Subtitle = "Approve Cases - Form 4D";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.ApproveCapturedForm4D(newModel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4D Form Listing";
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Child Protection", "Approve Form4D", ip, "",
                            "Form4D Details " + newModel.HouseHoldIDNumber + "-" + newModel.CapturedXml + " Approve Successfuly  at " + DateTime.Now.ToString());
                        return RedirectToAction("Form4DFormList");
                    }
                }

                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel = dbCompliance.FetchCapturedForm4DDetailsByID(newModel.ChildProtectionHeaderID);
            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult CloseForm4D()
        {
            ViewBag.Subtitle = "Close Cases - Form 4D";
            ModifyForm4D newModel = new ModifyForm4D();

            try
            {
                string formid = GeneralServices.strQueryString("Id", string.Empty);

                newModel = dbCompliance.FetchCapturedForm4DDetailsByID(formid);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.worker = dbService.GetSocialWorkers();
            newModel.AllowClose = 1;
            return View(newModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CloseForm4D(ModifyForm4D newModel)
        {
            ViewBag.Subtitle = "Close Cases - Form 4D";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.CloseCapturedForm4D(newModel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4D Form Listing";
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Child Protection", "Close Form4D", ip, "",
                            "Form4D Details " + newModel.HouseHoldIDNumber + "-" + newModel.CapturedXml + " Closed Successfuly  at " + DateTime.Now.ToString());
                        return RedirectToAction("Form4DFormList");
                    }
                }

                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel = dbCompliance.FetchCapturedForm4DDetailsByID(newModel.ChildProtectionHeaderID);
            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult CaptureFollowUpForm4D()
        {
            ViewBag.Subtitle = "Capture Case FollowUps - Form 4D";
            ModifyForm4D newModel = new ModifyForm4D();

            try
            {
                string formid = GeneralServices.strQueryString("Id", string.Empty);

                newModel = dbCompliance.FetchCapturedForm4DDetailsByID(formid);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.notaccessreason = dbService.GetCPReasonNotAccessService();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CaptureFollowUpForm4D(ModifyForm4D newModel)
        {
            ViewBag.Subtitle = "Capture FollowUp Form 4D";
            string errMessage = string.Empty;

            try
            {
                var result = dbCompliance.InsertCapturedForm4DFollowUp(newModel, out errMessage);
                if (string.IsNullOrEmpty(errMessage))
                {
                    ViewBag.Subtitle = "Captured Form 4D Form Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Child Protection", "FollowUp Form4D", ip, "",
                        "Form4D Details " + newModel.HouseHoldIDNumber + "-" + newModel.CapturedXml + " Update Successfuly  at " + DateTime.Now.ToString());
                    return RedirectToAction("Form4DFollowUpList");
                }

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel = dbCompliance.FetchCapturedForm4DDetailsByID(newModel.ChildProtectionHeaderID);
            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.notaccessreason = dbService.GetCPReasonNotAccessService();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult ModifyFollowUpForm4D()
        {
            ViewBag.Subtitle = "Modify FollowUp Form 4D";
            ModifyForm4D newModel = new ModifyForm4D();

            try
            {
                string formid = GeneralServices.strQueryString("Id", string.Empty);

                newModel = dbCompliance.FetchCapturedForm4DDetailsByID(formid);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.notaccessreason = dbService.GetCPReasonNotAccessService();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ModifyFollowUpForm4D(ModifyForm4D newModel)
        {
            ViewBag.Subtitle = "Modify Form 4D";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.UpdateCapturedForm4DFollowUp(newModel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4D Form Listing";
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Child Protection", "Update Form4D FollowUp", ip, "",
                            "Form4D FollowUp " + newModel.HouseHoldIDNumber + "-" + newModel.CapturedXml + " Update Form4D FollowUp Successfuly  at " + DateTime.Now.ToString());
                        return RedirectToAction("Form4DFollowUpList");
                    }
                }

                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            newModel = dbCompliance.FetchCapturedForm4DDetailsByID(newModel.ChildProtectionHeaderID);
            newModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            newModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            newModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            newModel.genders = dbService.GetGenders();
            newModel.yesnos = dbService.GetYesNo();
            newModel.risks = dbService.GetCPRiskTypes();
            newModel.services = dbService.GetCPServiceTypes();
            newModel.providers = dbService.GetCPProviderTypes();
            newModel.notaccessreason = dbService.GetCPReasonNotAccessService();
            newModel.worker = dbService.GetSocialWorkers();
            return View(newModel);
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchGridForm4DDetailsByID()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var headerId = Request.QueryString["Id"].ToString();

                Tuple<List<int>, List<Form4DMembersGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form4DMembersGrid>(";Exec FetchGridForm4DDetailsByID @ChildProtectionHeaderID",
                    new {
                        ChildProtectionHeaderID = headerId
                 });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4DMembersGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], 0, 0, 5);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return XDocument.Parse("<xml>error</xml>");
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchGridForm4DFollowUpDetailsByID()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var headerId = Request.QueryString["Id"].ToString();

                Tuple<List<int>, List<Form4DFollowUpMembersGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form4DFollowUpMembersGrid>(";Exec FetchGridForm4DFollowUpDetailsByID @ChildProtectionHeaderId",
                    new
                    {
                        ChildProtectionHeaderId = headerId
                    });

                // List <Form4DFollowUpMembersGrid > newList= (resultset.Item2).GroupBy(g => g.ChildProtectionDetailID).Select(g => g.First()).ToList();
                // List<Form4DFollowUpMembersGrid> newList = (resultset.Item2).GroupBy(g => g.ChildProtectionVisitHeaderID).Select(g => g.First()).ToList();

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4DFollowUpMembersGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], 0, 0, 5);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return XDocument.Parse("<xml>error</xml>");
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchJsonForm4DDetailsByID(ModifyForm4D oldModel)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var headerId = oldModel.ChildProtectionHeaderID;

                Tuple<List<int>, List<Form4DMembersGrid>> resultset = GeneralServices.NpocoConnection().FetchMultiple<int, Form4DMembersGrid>(";Exec FetchGridForm4DDetailsByID @ChildProtectionHeaderID",
                    new {
                        ChildProtectionHeaderID = headerId
                });

                return Json(resultset.Item2, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchJsonForm4DFollowUpDetailsByID(ModifyForm4D oldModel)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var headerId = oldModel.ChildProtectionHeaderID;

                Tuple<List<int>, List<Form4DFollowUpMembersGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form4DFollowUpMembersGrid>(";Exec FetchGridForm4DFollowUpDetailsByID @ChildProtectionHeaderId",
                    new
                    {
                        ChildProtectionHeaderId = headerId
                    });

                List<Form4DFollowUpMembersGrid> newList = (resultset.Item2).GroupBy(g => g.ChildProtectionDetailID).Select(g => g.First()).ToList();

                return Json(newList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchJsonForm4DIfExists(ModifyForm4D oldModel)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                List<int> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<int>(";Exec FetchForm4DChildProtectionDetails @ProfileDetailID",
                    new
                    {
                        ProfileDetailID = oldModel.ChildDetailID
                    });
                List<int> newList = (resultset).ToList();

                return Json(new { Result = newList[0], Message = "" });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //FORM 4 updates
        //Service Providers
        [HttpPost]
        [Authorize]
        public JsonResult Select4AServiceProvider(Kebele model)
        {
            var SPDetails = dbService.GetForm4AServiceProviders(model.KebeleID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Select4BServiceProvider(Kebele model)
        {
            var SPDetails = dbService.GetForm4BServiceProviders(model.KebeleID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Select4CServiceProvider(Kebele model)
        {
            var SPDetails = dbService.GetForm4CServiceProviders(model.KebeleID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }
            //Service Names

        [HttpPost]
        [Authorize]
        public JsonResult Select4AServiceNames(int KebeleID, int ServiceProviderID)
        {
            var SPDetails = dbService.GetForm4AServices(KebeleID, ServiceProviderID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Select4BServiceNames(int KebeleID, int ServiceProviderID)
        {
            var SPDetails = dbService.GetForm4BServices(KebeleID, ServiceProviderID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Select4CServiceNames(int KebeleID, int ServiceProviderID)
        {
            var SPDetails = dbService.GetForm4CServices(KebeleID, ServiceProviderID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

    }
}
//models.serviceprovids = dbService.GetForm4AServiceProviders(models.KebeleID);
//models.intgrtedservis = dbService.GetForm4AServices(models.KebeleID, models.ServiceProviderID);
