﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SCTMis.Models;
using SCTMis.Services;
using System.Xml.Linq;
using System.Data;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class CoResponsibilityController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        CoResponsibilityServices dbSCoResp = new CoResponsibilityServices();
        //
        // GET: /CoResponsibility/
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form2A()
        {
            ViewBag.Subtitle = "Form 2A Listing";
            //ViewBag.Message = "Your app description page.";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form2AListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                List<Form2AGrid> form2AList = con.Fetch<Form2AGrid>(";Exec getForm2AAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form2AGrid>(form2AList);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2AList.Count, jtStartIndex, 0,2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //ProduceForm2A
        [Authorize]
        public ActionResult ProduceForm2A()
        {
            ViewBag.Subtitle = "Produce Form 2A";

            ProduceForm2A models = new Models.ProduceForm2A();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("2A", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm2A(ProduceForm2A newmodel)
        {
            ViewBag.Subtitle = "Produce Form 2A";
            string strErrMessage = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbSCoResp.ProduceForm2A(newmodel, out strErrMessage);
                    if (string.IsNullOrEmpty(strErrMessage))
                    {
                        ViewBag.Subtitle = "Form 2A Listing";
                        return RedirectToAction("Form2A");
                    }

                }

                if (string.IsNullOrEmpty(strErrMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strErrMessage);
                }
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("2A", int.Parse(Session["WoredaID"].ToString()));

                return View("ProduceForm2A", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("2A", int.Parse(Session["WoredaID"].ToString()));

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm2A", newmodel);
            }
        }

        //FORM 2B

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form2B()
        {
            ViewBag.Subtitle = "Form 2B Listing";
            //ViewBag.Message = "Your app description page.";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form2BListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                List<Form2AGrid> form2BList = con.Fetch<Form2AGrid>(";Exec getForm2BAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form2AGrid>(form2BList);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2BList.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }
        //ProduceForm2B
        [Authorize]
        public ActionResult ProduceForm2B()
        {
            ViewBag.Subtitle = "Produce Form 2B1";

            ProduceForm2B models = new Models.ProduceForm2B();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("2B1", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm2B(ProduceForm2B newmodel)
        {
            ViewBag.Subtitle = "Produce Form 2B1";
            string strErrMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbSCoResp.ProduceForm2B(newmodel, out strErrMessage);
                    if (string.IsNullOrEmpty(strErrMessage))
                    {
                        ViewBag.Subtitle = "Form 2B1 Listing";
                        return RedirectToAction("Form2B");
                    }

                }
                if (string.IsNullOrEmpty(strErrMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strErrMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("2B1", int.Parse(Session["WoredaID"].ToString()));

                return View("ProduceForm2B", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("2B1", int.Parse(Session["WoredaID"].ToString()));

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm2B", newmodel);
            }
        }

        //FORM 2B2
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form2B2()
        {
            ViewBag.Subtitle = "Form 2B2 Listing";
            //ViewBag.Message = "Your app description page.";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form2B2ListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                List<Form2AGrid> form2B2List = con.Fetch<Form2AGrid>(";Exec getForm2B2AdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form2AGrid>(form2B2List);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2B2List.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }
        //ProduceForm2B
        [Authorize]
        public ActionResult ProduceForm2B2()
        {
            ViewBag.Subtitle = "Produce Form 2B2";

            ProduceForm2B2 models = new Models.ProduceForm2B2();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("2B2", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm2B2(ProduceForm2B2 newmodel)
        {
            ViewBag.Subtitle = "Produce Form 2B2";
            string strErrMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbSCoResp.ProduceForm2B2(newmodel, out strErrMessage);
                    if (string.IsNullOrEmpty(strErrMessage))
                    {
                        ViewBag.Subtitle = "Form 2B2 Listing";
                        return RedirectToAction("Form2B2");
                    }

                }
                if (string.IsNullOrEmpty(strErrMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strErrMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("2B2", int.Parse(Session["WoredaID"].ToString()));

                return View("ProduceForm2B2", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("2B2", int.Parse(Session["WoredaID"].ToString()));

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm2B2", newmodel);
            }
        }
        //FORM2B2 ENDS

        //FORM 2C

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form2C()
        {
            ViewBag.Subtitle = "Form 2C Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form2CListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                List<Form2AGrid> form2CList = con.Fetch<Form2AGrid>(";Exec getForm2CAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form2AGrid>(form2CList);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2CList.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }
        //ProduceForm2C
        [Authorize]
        public ActionResult ProduceForm2C()
        {
            ViewBag.Subtitle = "Produce Form 2C";

            ProduceForm2C models = new Models.ProduceForm2C();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("2C", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm2C(ProduceForm2C newmodel)
        {
            ViewBag.Subtitle = "Produce Form 2C";
            string strErrMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbSCoResp.ProduceForm2C(newmodel, out strErrMessage);

                    if (string.IsNullOrEmpty(strErrMessage))
                    {
                        ViewBag.Subtitle = "Form 2C Listing";
                        return RedirectToAction("Form2C");
                    }
                }
                if (string.IsNullOrEmpty(strErrMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strErrMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                return View("ProduceForm2C", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm2C", newmodel);
            }
        }
    }
}
