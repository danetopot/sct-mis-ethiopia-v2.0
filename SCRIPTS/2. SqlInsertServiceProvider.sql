USE [SCT-MIS]
GO
IF EXISTS (SELECT * FROM [ServiceProvider] WHERE ServiceProviderID = 4)
	BEGIN
		UPDATE [dbo].[ServiceProvider] SET [ServiceProviderName] = N'CBHI Membership Provider' WHERE [ServiceProviderID] = 4
	END
ELSE
	BEGIN
		INSERT [dbo].[ServiceProvider] ([ServiceProviderName]) VALUES (N'CBHI Membership Provider')
	END
GO

IF EXISTS (SELECT * FROM [ServiceProvider] WHERE ServiceProviderID = 5)
	BEGIN
		UPDATE [dbo].[ServiceProvider] SET [ServiceProviderName] = N'Child Protection Providerr' WHERE [ServiceProviderID] = 5
	END
ELSE
	BEGIN
		INSERT [dbo].[ServiceProvider] ([ServiceProviderName]) VALUES (N'Child Protection Provider')
	END
GO
