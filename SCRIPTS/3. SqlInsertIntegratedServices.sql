USE [SCT-MIS]
GO
IF EXISTS (SELECT * FROM [IntegratedServices] WHERE [ServiceID] = 12)
	BEGIN
		UPDATE [dbo].[IntegratedServices] SET  
		[ServiceProviderID] = 4,
		[ServiceName] = N'CBHI Membership Access & Utilization'
		WHERE [ServiceID] = 12
	END
ELSE
	BEGIN
		INSERT [dbo].[IntegratedServices] ([ServiceID], [ServiceProviderID], [ServiceName]) VALUES (12, 4, N'CBHI Membership Access & Utilization')
	END
GO

IF EXISTS (SELECT * FROM [IntegratedServices] WHERE [ServiceID] = 13)
	BEGIN
		UPDATE [dbo].[IntegratedServices] SET  
		[ServiceProviderID] = 5,
		[ServiceName] = N'Child Protection Case Management'
		WHERE [ServiceID] = 13
	END
ELSE
	BEGIN
		INSERT [dbo].[IntegratedServices] ([ServiceID], [ServiceProviderID], [ServiceName]) VALUES (13, 5, N'Child Protection Case Management')
	END
GO