IF EXISTS (SELECT * FROM [ServiceForm] WHERE ServiceID = 12 AND FormID = 1 AND ServiceProviderID = 4)
	BEGIN
		UPDATE [dbo].[ServiceForm] SET [ServiceID] = 12, [FormID] = 1, [ServiceProviderID] = 4 
		WHERE ServiceID = 12 AND FormID = 1 AND ServiceProviderID = 4
	END
ELSE
	BEGIN
		INSERT [dbo].[ServiceForm] ([ServiceID], [FormID], [ServiceProviderID]) VALUES (12, 1, 4)
	END
GO
IF EXISTS (SELECT * FROM [ServiceForm] WHERE ServiceID = 12 AND FormID = 2 AND ServiceProviderID = 4)
	BEGIN
		UPDATE [dbo].[ServiceForm] SET [ServiceID] = 12, [FormID] = 2, [ServiceProviderID] = 4 
		WHERE ServiceID = 12 AND FormID = 2 AND ServiceProviderID = 4
	END
ELSE
	BEGIN
		INSERT [dbo].[ServiceForm] ([ServiceID], [FormID], [ServiceProviderID]) VALUES (12, 2, 4)
	END
GO
IF EXISTS (SELECT * FROM [ServiceForm] WHERE ServiceID = 12 AND FormID = 3 AND ServiceProviderID = 4)
	BEGIN
		UPDATE [dbo].[ServiceForm] SET [ServiceID] = 12, [FormID] = 3, [ServiceProviderID] = 4 
		WHERE ServiceID = 12 AND FormID = 3 AND ServiceProviderID = 4
	END
ELSE
	BEGIN
		INSERT [dbo].[ServiceForm] ([ServiceID], [FormID], [ServiceProviderID]) VALUES (12, 3, 4)
	END
GO


IF EXISTS (SELECT * FROM [ServiceForm] WHERE ServiceID = 13 AND FormID = 1 AND ServiceProviderID = 5)
	BEGIN
		UPDATE [dbo].[ServiceForm] SET [ServiceID] = 13, [FormID] = 1, [ServiceProviderID] = 5 
		WHERE ServiceID = 13 AND FormID = 1 AND ServiceProviderID = 5
	END
ELSE
	BEGIN
		INSERT [dbo].[ServiceForm] ([ServiceID], [FormID], [ServiceProviderID]) VALUES (13, 1, 5)
	END
GO
IF EXISTS (SELECT * FROM [ServiceForm] WHERE ServiceID = 13 AND FormID = 2 AND ServiceProviderID = 5)
	BEGIN
		UPDATE [dbo].[ServiceForm] SET [ServiceID] = 13, [FormID] = 2, [ServiceProviderID] = 5 
		WHERE ServiceID = 13 AND FormID = 2 AND ServiceProviderID = 5
	END
ELSE
	BEGIN
		INSERT [dbo].[ServiceForm] ([ServiceID], [FormID], [ServiceProviderID]) VALUES (13, 2, 5)
	END
GO
IF EXISTS (SELECT * FROM [ServiceForm] WHERE ServiceID = 13 AND FormID = 3 AND ServiceProviderID = 5)
	BEGIN
		UPDATE [dbo].[ServiceForm] SET [ServiceID] = 13, [FormID] = 3, [ServiceProviderID] = 5 
		WHERE ServiceID = 13 AND FormID = 3 AND ServiceProviderID = 5
	END
ELSE
	BEGIN
		INSERT [dbo].[ServiceForm] ([ServiceID], [FormID], [ServiceProviderID]) VALUES (13, 3, 5)
	END
GO