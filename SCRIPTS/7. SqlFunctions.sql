USE [SCT-MIS]
GO
IF OBJECT_ID(N'[SplitString]', N'TF') IS NOT NULL
    DROP FUNCTION [SplitString]
GO
IF OBJECT_ID(N'[getForm4PendingServices]', N'TF') IS NOT NULL
    DROP FUNCTION [getForm4PendingServices]
GO
IF OBJECT_ID(N'[getFilePaths]', N'FN') IS NOT NULL
    DROP FUNCTION [getFilePaths]
GO
IF OBJECT_ID(N'[f_getYearMonth_Form1A]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getYearMonth_Form1A]
GO
IF OBJECT_ID(N'[f_getYearMonth]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getYearMonth]
GO
IF OBJECT_ID(N'[f_getYearlyQuater]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getYearlyQuater]
GO
IF OBJECT_ID(N'[f_GetUserNameByUserID]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetUserNameByUserID]
GO
IF OBJECT_ID(N'[f_getUserIDByUserName]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getUserIDByUserName]
GO
IF OBJECT_ID(N'[f_GetUserFullNameByUserID]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetUserFullNameByUserID]
GO
IF OBJECT_ID(N'[f_GetServiceProviderByService]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetServiceProviderByService]
GO
IF OBJECT_ID(N'[f_GetServiceName]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetServiceName]
GO
IF OBJECT_ID(N'[f_GetReportPeriod]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetReportPeriod]
GO
IF OBJECT_ID(N'[f_getNutritionalStatus]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getNutritionalStatus]
GO
IF OBJECT_ID(N'[f_getNonComplianceDSPLW]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getNonComplianceDSPLW]
GO
IF OBJECT_ID(N'[f_getNonComplianceDSCMC]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getNonComplianceDSCMC]
GO
IF OBJECT_ID(N'[f_getNonComplianceDS]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getNonComplianceDS]
GO
IF OBJECT_ID(N'[f_GetMemberCondition]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetMemberCondition]
GO
IF OBJECT_ID(N'[f_GetKebeleTDSPLWMembers]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetKebeleTDSPLWMembers]
GO
IF OBJECT_ID(N'[f_getKebele]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getKebele]
GO
IF OBJECT_ID(N'[f_GetHouseholdVisit]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetHouseholdVisit]
GO
IF OBJECT_ID(N'[f_GetHouseholdMembers]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetHouseholdMembers]
GO
IF OBJECT_ID(N'[f_GetForm2CTargetClientID]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetForm2CTargetClientID]
GO
IF OBJECT_ID(N'[f_GetForm2BTargetClientID]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetForm2BTargetClientID]
GO
IF OBJECT_ID(N'[f_GetForm2ATargetClientID]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetForm2ATargetClientID]
GO
IF OBJECT_ID(N'[f_getFiscalYearGetFormatted]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getFiscalYearGetFormatted]
GO
IF OBJECT_ID(N'[f_getFiscalYearFromPeriod]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getFiscalYearFromPeriod]
GO
IF OBJECT_ID(N'[f_getFiscalYearFromDate]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getFiscalYearFromDate]
GO
IF OBJECT_ID(N'[f_getDefaultKebeles]', N'TF') IS NOT NULL
    DROP FUNCTION [f_getDefaultKebeles]
GO
IF OBJECT_ID(N'[f_getCurrentFiscalYear]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getCurrentFiscalYear]
GO
IF OBJECT_ID(N'[f_GetCoResponsibilityPLW]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetCoResponsibilityPLW]
GO
IF OBJECT_ID(N'[f_GetCoResponsibilityHouseholds]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetCoResponsibilityHouseholds]
GO
IF OBJECT_ID(N'[f_GetCoResponsibilityCMC]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetCoResponsibilityCMC]
GO
IF OBJECT_ID(N'[f_getComplianceCount]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getComplianceCount]
GO
IF OBJECT_ID(N'[f_getComplianceCCount]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getComplianceCCount]
GO
IF OBJECT_ID(N'[f_getComplianceBCount]', N'FN') IS NOT NULL
    DROP FUNCTION [f_getComplianceBCount]
GO
IF OBJECT_ID(N'[f_GetChecklistHouseholds]', N'FN') IS NOT NULL
    DROP FUNCTION [f_GetChecklistHouseholds]
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetChecklistHouseholds]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_GetChecklistHouseholds]
(
	@CoRespDSHeaderID		Varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(CheckListDSID)
	FROM CheckListDS
	INNER JOIN CoResponsibilityDSDetail ON CoResponsibilityDSDetail.CoRespDSDetailID = CheckListDS.CoRespDSDetailID
	WHERE CoRespDSHeaderID = @CoRespDSHeaderID)

END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getComplianceBCount]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getComplianceBCount]
(
	@ComplianceID	Varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(ComplianceDtlID) 
	FROM ComplianceSTDSPLWDetail WHERE ComplianceID = @ComplianceID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getComplianceCCount]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getComplianceCCount]
(
	@ComplianceID	varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(ComplianceDtlID) 
	FROM ComplianceSTDSCMCDetail WHERE ComplianceID = @ComplianceID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getComplianceCount]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getComplianceCount]
(
	@ComplianceID	Varchar(100),
	@ServiceID		Int
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(ComplianceDtlID) 
	FROM ComplianceSDSDetail 
		INNER JOIN ComplianceSDSHeader ON ComplianceSDSDetail.ComplianceID = ComplianceSDSDetail.ComplianceID
	WHERE ComplianceSDSDetail.ComplianceID = @ComplianceID
	AND ServiceID = @ServiceID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetCoResponsibilityCMC]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_GetCoResponsibilityCMC]
(
	@CoRespTDSCMCWHeaderID	Varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(CoRespTDSCMCID) FROM CoResponsibilityTDSCMC WHERE CoRespTDSCMCWHeaderID = @CoRespTDSCMCWHeaderID)
END 
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetCoResponsibilityHouseholds]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_GetCoResponsibilityHouseholds]
(
	@CoRespDSHeaderID		Varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(CoResponsibilityDSDetail.ProfileDSDetailID)
	FROM CoResponsibilityDSDetail
	WHERE CoRespDSHeaderID = @CoRespDSHeaderID)

END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetCoResponsibilityPLW]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_GetCoResponsibilityPLW]
(
	@CoRespTDSPLWHeaderID	Varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(DISTINCT ProfileTDSPLWID) FROM CoResponsibilityTDSPLW WHERE CoRespTDSPLWHeaderID = @CoRespTDSPLWHeaderID)
END 
GO
/****** Object:  UserDefinedFunction [dbo].[f_getCurrentFiscalYear]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[f_getCurrentFiscalYear] () returns int
as  
	begin	
		declare @CurrentYear int

		select top 1 @CurrentYear = FiscalYear from FiscalYears where ( Status = 0 ) order by FiscalYear

		return @CurrentYear 
	end
GO
/****** Object:  UserDefinedFunction [dbo].[f_getDefaultKebeles]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[f_getDefaultKebeles] ()
RETURNS @TempS TABLE (WoredaID int)
AS
BEGIN
	declare @LocationType int

	select top 1 @LocationType = LocationType from Settings --1 - Woreda, 2 - Regional Office, 3-Federal HQ

	if (@LocationType = 1)
		begin
			insert into @TempS
			select WoredaID from DefaultWoreda
		end
	else
		begin
			insert into @TempS
			select WoredaID from Woreda
		end
	
	return
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getFiscalYearFromDate]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[f_getFiscalYearFromDate] (@Date datetime) returns int
as  
	begin	
		declare @CurrentYear int

		select @CurrentYear = FiscalYear from FiscalYears where ( @Date between StartDate and EndDate )
	
		return @CurrentYear 
	end
GO
/****** Object:  UserDefinedFunction [dbo].[f_getFiscalYearFromPeriod]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_getFiscalYearFromPeriod]
(
	@PeriodId	INT
)RETURNS VARCHAR(50)
AS
BEGIN
	RETURN(SELECT FiscalYear FROM ReportingPeriod WHERE PeriodId = @PeriodID )
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getFiscalYearGetFormatted]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[f_getFiscalYearGetFormatted] (@FiscalYear int)  
RETURNS varchar(20)  AS  
BEGIN 
	declare @FormattedFiscalYear varchar(10) 
	
	select @FormattedFiscalYear = ltrim(convert(varchar(10), @FiscalYear))
		
	if len(@FormattedFiscalYear) = 8 
		begin
			select @FormattedFiscalYear = substring(@FormattedFiscalYear, 1, 4) +'-' + substring(@FormattedFiscalYear, 5, 4)
		end

	return @FormattedFiscalYear
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetForm2ATargetClientID]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[f_GetForm2ATargetClientID]
(
	@Age				Int,
	@PWL				Varchar(5)
)RETURNS INT
AS
BEGIN
	Declare @TargetClientID	Int
	IF ISNULL(@Age,0) <5
	BEGIN
		SET @TargetClientID = 3
	END
	ELSE
	BEGIN
		SET @TargetClientID = 1
	END

	RETURN @TargetClientID
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetForm2BTargetClientID]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[f_GetForm2BTargetClientID]
(
	@Age				Int,
	@PWL				Varchar(5)
)RETURNS INT
AS
BEGIN
	Declare @TargetClientID	Int
	IF ISNULL(@Age,0) <5
	BEGIN
		SET @TargetClientID = 3
	END
	ELSE
	BEGIN
		SET @TargetClientID = 1
	END

	RETURN @TargetClientID
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetForm2CTargetClientID]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[f_GetForm2CTargetClientID]
(
	@MalnourishmentDegree	Varchar(5)
)RETURNS INT
AS
BEGIN
	--Declare @TargetClientID	Int
	--IF ISNULL(@Age,0) <5
	--BEGIN
	--	SET @TargetClientID = 3
	--END
	--ELSE
	--BEGIN
	--	SET @TargetClientID = 1
	--END

	RETURN 4
END

GO
/****** Object:  UserDefinedFunction [dbo].[f_GetHouseholdMembers]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[f_GetHouseholdMembers]
(
	@ProfileDSHeaderID	uniqueidentifier
)RETURNS INT
AS
BEGIN
	RETURN(SELECT ISNULL(COUNT(ProfileDSDetailID),0) FROM ProfileDSDetail
		WHERE ProfileDSHeaderID = @ProfileDSHeaderID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetHouseholdVisit]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_GetHouseholdVisit]
(
	@HouseHoldVisitID		SmallInt
)RETURNS Varchar(100)
AS
BEGIN
	RETURN(SELECT TOP 1 HouseholdVisitName FROM HouseholdVisits WHERE HouseHoldVisitID=@HouseHoldVisitID)
END


GO
/****** Object:  UserDefinedFunction [dbo].[f_getKebele]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[f_getKebele]
(
	@KebeleID	Int
)RETURNS Varchar(50)
AS
BEGIN
	RETURN(SELECT KebeleName FROM Kebele WHERE KebeleID = @KebeleID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetKebeleTDSPLWMembers]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Function [dbo].[f_GetKebeleTDSPLWMembers]
(
	@KebeleID	Int
)RETURNS INT
AS
BEGIN
	RETURN(SELECT ISNULL(COUNT(KebeleID),0) FROM ProfileTDSPLW
		WHERE KebeleID = @KebeleID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetMemberCondition]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_GetMemberCondition]
(
	@Age				Int,
	@Pregnant			Varchar(5),
	@Lactating			Varchar(5),
	@NutritionalStatus	Varchar(5)	
)RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @strReturn	Varchar(500),
			@NutrStatus	Varchar(100)

	SET @strReturn = ''
	IF ISNULL(@Age,0) < 0
	BEGIN
		SET @strReturn = @strReturn + 'Infants'
	END
	
	IF ISNULL(@Pregnant,'-') = 'YES'
	BEGIN
		IF LEN(@strReturn) > 2
			SET @strReturn = @strReturn + ' and Pregnant'
		ELSE
			SET @strReturn = @strReturn + 'Pregnant'
	END
	
	IF ISNULL(@Lactating,'-') = 'YES'
	BEGIN
		IF LEN(@strReturn) > 2
			SET @strReturn = @strReturn + ' and Lactating'
		ELSE
			SET @strReturn = @strReturn + 'Lactating'
	END

	IF @Age <=5
	BEGIN
		SET @NutrStatus= CASE @NutritionalStatus WHEN 'M' THEN 'Mod' WHEN  'SM' THEN 'Sev' ELSE '' END
	END

	IF LEN(@NutrStatus) > 1
	BEGIN
		IF LEN(@strReturn) > 2 
		BEGIN
			SET @strReturn = @strReturn + ' and ' + @NutrStatus
		END
		ELSE
		BEGIN
			SET @strReturn = @strReturn + @NutrStatus
		END
	END

	RETURN @strReturn
END





GO
/****** Object:  UserDefinedFunction [dbo].[f_getNonComplianceDS]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getNonComplianceDS]
(
	@NonComplianceID	Varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(NonComplianceDtlID) 
	FROM NonComplianceDSDetail WHERE NonComplianceID = @NonComplianceID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getNonComplianceDSCMC]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getNonComplianceDSCMC]
(
	@NonComplianceID	Varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(NonComplianceDtlID) 
	FROM NonComplianceTDSCMCDetail WHERE NonComplianceID = @NonComplianceID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getNonComplianceDSPLW]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getNonComplianceDSPLW]
(
	@NonComplianceID	varchar(100)
)RETURNS INT
AS
BEGIN
	RETURN(SELECT COUNT(NonComplianceDtlID) 
	FROM NonComplianceTDSPLWDetail WHERE NonComplianceID = @NonComplianceID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getNutritionalStatus]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getNutritionalStatus]
(
	@NutritionalStatus	Varchar(2)
)Returns Varchar(30)
AS
BEGIN
	Declare @NutritionalName	Varchar(30)
	SET @NutritionalName = CASE @NutritionalStatus WHEN 'N' THEN 'Normal'
												   WHEN 'SM' THEN 'Sev.'
												   WHEN 'M' THEN 'Mad.'
						   END
	RETURN @NutritionalName		
END


GO
/****** Object:  UserDefinedFunction [dbo].[f_GetReportPeriod]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_GetReportPeriod]
(
	@PeriodID		Varchar(5)
)RETURNS Varchar(100)
AS
BEGIN
	IF @PeriodID = 'AS'
		RETURN 'Demo Period'
	
	RETURN(SELECT TOP 1 PeriodName FROM ReportingPeriod WHERE PeriodID=@PeriodID)
END

GO
/****** Object:  UserDefinedFunction [dbo].[f_GetServiceName]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[f_GetServiceName](@ServiceID INT)
Returns Varchar(200)
AS
BEGIN
 RETURN(Select ServiceName FROM IntegratedServices WHERE ServiceID = @ServiceID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetServiceProviderByService]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[f_GetServiceProviderByService]
(
	@ServiceID		INT
)RETURNS VARCHAR(50)
AS
BEGIN
	RETURN(SELECT TOP 1 ServiceProviderName 
			FROM ViewServices WHERE ServiceID = @ServiceID)
END

GO
/****** Object:  UserDefinedFunction [dbo].[f_GetUserFullNameByUserID]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_GetUserFullNameByUserID]
(
	@UserID VARCHAR(50)
)RETURNS Varchar(50)
AS
BEGIN
IF(ISNUMERIC(@UserID)=1)
	RETURN (SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE UserID = @UserID)
RETURN @UserID
END

	
GO
/****** Object:  UserDefinedFunction [dbo].[f_getUserIDByUserName]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[f_getUserIDByUserName]
(
	@UserName	varchar(30)
)RETURNS BIGINT
AS
BEGIN
	RETURN(SELECT UserID FROM users WHERE UserName = @UserName)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetUserNameByUserID]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[f_GetUserNameByUserID]
(
	@UserID		Int
)RETURNS Varchar(50)
AS
BEGIN
	RETURN(Select UserName from Users
	WHERE UserID = @UserID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getYearlyQuater]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[f_getYearlyQuater]
(
	@Year		int,
	@CodeID		Char(1) --A=First Q, B= Second Q, C= Third Q and D=Fourth Q finally Z=Previous year Quater
)RETURNS SMALLDATETIME
AS
BEGIN
	DECLARE @QuarterEndEnd SmallDatetime
	
	SELECT @QuarterEndEnd = CASE @CodeID
		WHEN 'A' THEN '31/MAR/'+CAST(@Year AS varchar(4))
		WHEN 'B' THEN '30/JUN/'+CAST(@Year AS varchar(4))
		WHEN 'C' THEN '30/SEP/'+CAST(@Year AS varchar(4))
		WHEN 'D' THEN '31/DEC/'+CAST(@Year AS varchar(4))
		WHEN 'Z' THEN '31/DEC/'+CAST(@Year-1 AS varchar(4))
	ELSE '31/MAR/'+CAST(@Year AS varchar(4)) END
	--IF @CodeID = 'A'
	--BEGIN
	--	SET @QuarterEndEnd = '31/MAR/'+CAST(@Year AS varchar(4))
	--END
	
	--IF @CodeID = 'B'
	--BEGIN
	--	SET @QuarterEndEnd = '30/JUN/'+CAST(@Year AS varchar(4))
	--END
	
	--IF @CodeID = 'C'
	--BEGIN
	--	SET @QuarterEndEnd = '30/SEP/'+CAST(@Year AS varchar(4))
	--END
	
	--IF @CodeID = 'D'
	--BEGIN
	--	SET @QuarterEndEnd = '31/DEC/'+CAST(@Year AS varchar(4))
	--END
	
	--IF @CodeID = 'Z'
	--BEGIN
	--	SET @QuarterEndEnd = '31/DEC/'+CAST(@Year-1 AS varchar(4))
	--END

	RETURN @QuarterEndEnd
END
GO
/****** Object:  UserDefinedFunction [dbo].[f_getYearMonth]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getYearMonth]
(
	@mdob datetime,
	@AsOnDate datetime
) Returns Varchar(20)
AS
BEGIN
	Declare @YearPart	Int
	Declare @MonthPart	Int
	Declare @YearMonth	Varchar(10)

	SELECT 
	@YearPart = CASE 
	WHEN (DATEPART( DAY , @mdob)) > (DATEPART( DAY , @AsOnDate))
	and (DATEPART( Month,@mdob)) = (DATEPART(Month, @AsOnDate))
	THEN ((datediff(month,@mdob,@AsOnDate)-1)/12) 
	ELSE (datediff(month,@mdob,@AsOnDate)/12) 
	END,
	@MonthPart = CASE 
	WHEN (DATEPART( DAY , @mdob)) > (DATEPART( DAY , @AsOnDate)) 
	and (DATEPART( Month,@mdob)) = (DATEPART( Month,@AsOnDate)) 
	THEN ((datediff(month,@mdob,@AsOnDate)-1)% 12) 
	ELSE (datediff(month,@mdob,@AsOnDate)% 12) 
	END	
	
	IF @YearPart = 0
	BEGIN
		SET @YearMonth = cast(@MonthPart As varchar) + 'M'
	END
	ELSE
	BEGIN
		IF @MonthPart = 0
		BEGIN
			SET @YearMonth = cast(@YearPart As varchar) + 'Y'
		END
		ELSE
		BEGIN
			SET @YearMonth = cast(@YearPart As varchar) + 'Y ' + cast(@MonthPart As varchar) + 'M'
		END
	END

	RETURN @YearMonth
END

GO
/****** Object:  UserDefinedFunction [dbo].[f_getYearMonth_Form1A]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_getYearMonth_Form1A]
(
	@mdob datetime,
	@AsOnDate datetime
) Returns Varchar(20)
AS
BEGIN
	Declare @YearPart	Int
	Declare @MonthPart	Int
	Declare @YearMonth	Varchar(10)

	SELECT 
	@YearPart = CASE 
	WHEN (DATEPART( DAY , @mdob)) > (DATEPART( DAY , @AsOnDate))
	and (DATEPART( Month,@mdob)) = (DATEPART(Month, @AsOnDate))
	THEN ((datediff(month,@mdob,@AsOnDate)-1)/12) 
	ELSE (datediff(month,@mdob,@AsOnDate)/12) 
	END,
	@MonthPart = CASE 
	WHEN (DATEPART( DAY , @mdob)) > (DATEPART( DAY , @AsOnDate)) 
	and (DATEPART( Month,@mdob)) = (DATEPART( Month,@AsOnDate)) 
	THEN ((datediff(month,@mdob,@AsOnDate)-1)% 12) 
	ELSE (datediff(month,@mdob,@AsOnDate)% 12) 
	END	
	
	IF @YearPart = 0
		BEGIN
			SET @YearMonth = cast(@MonthPart As varchar) + 'M'
		END
	ELSE
		BEGIN
			SET @YearMonth = cast(@YearPart As varchar) + 'Y'
		END

	RETURN @YearMonth
END

GO
/****** Object:  UserDefinedFunction [dbo].[getFilePaths]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[getFilePaths]
(
	@TextColumn Varchar(Max)
)RETURNS VARCHAR(2000)
AS
BEGIN
	Declare @ReturnString	Varchar(Max),
			@FoundItem		Varchar(1000),
			@InterimItem	Varchar(1000),
			@IndexPOS		Int,
			@IndexOther		Int,
			@SearchItem		Varchar(10)

	SET @ReturnString = ''
	SET @SearchItem = '.pdf'

	WHILE CHARINDEX(@SearchItem, @TextColumn) > 1
	BEGIN
		SET @IndexPOS = CHARINDEX(@SearchItem, @TextColumn)

		SET @InterimItem = SUBSTRING ( @TextColumn ,0 , @IndexPOS+4 )

		SET @InterimItem = REVERSE(@InterimItem)
		SET @IndexOther = CHARINDEX('>', @InterimItem)
		SET @FoundItem = SUBSTRING ( @InterimItem ,0 , @IndexOther )
		SET @FoundItem = REVERSE(@FoundItem)

		SET @InterimItem = REVERSE(@InterimItem)
		SET @TextColumn = REPLACE(@TextColumn,@InterimItem,'')

		SET @ReturnString = @ReturnString + @FoundItem + ','
	END

	SET @ReturnString = LEFT(@ReturnString,LEN(@ReturnString)-1)

	RETURN @ReturnString
END
GO
/****** Object:  UserDefinedFunction [dbo].[getForm4PendingServices]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getForm4PendingServices]
(
	@FormCode			Varchar(5),
	@KebeleID			int,
	@ReportingPeriodID	INT
)RETURNS @TempS TABLE(ServiceProviderID Bigint,ServiceProviderName Varchar(100))
AS
BEGIN
	IF @FormCode = 'A'
	BEGIN
	DECLARE @@Form4ACompliance TABLE
	(
		ProfileDSDetailID		Varchar(100),
		Age						Varchar(20),
		Sex						Char(1),
		Pregnant				Varchar(5),
		Lactating				Varchar(5),
		NutritionalStatus		Varchar(5),
		EnrolledInSchool		Varchar(5),
		CurrentAge				Int
	)

	INSERT INTO @@Form4ACompliance(ProfileDSDetailID,Age,Sex,Pregnant,Lactating,NutritionalStatus,
		EnrolledInSchool,CurrentAge)
	SELECT ProfileDSDetailID,dbo.f_getYearMonth(dateOfBirth,GetDate()) Age,Sex,Pregnant,
		Lactating,NutritionalStatus,EnrolledInSchool,DATEDIFF(MONTH,dateOfBirth,GetDate())
	FROM [dbo].[ProfileDSDetail]
	INNER JOIN [dbo].[ProfileDSHeader] ON ProfileDSDetail.ProfileDSHeaderID = ProfileDSHeader.ProfileDSHeaderID
	WHERE KebeleID = @KebeleID
	AND ISNULL(ApprovedBy,'') <> ''
	AND ISNULL(Form2Produced,0) = 1
	AND ISNULL(Form3Generated,0) = 1
	--AND KebeleID NOT IN(
	--	SELECT KebeleID FROM ComplianceSDSHeader
	--	WHERE KebeleID = @KebeleID 
	--	AND ServiceID = @ServiceID
	--	AND ReportingPeriodID = @ReportingPeriodID 
	--)

	INSERT INTO @TempS(ServiceProviderID,ServiceProviderName)
	SELECT DISTINCT ServiceProvider.ServiceProviderID,ServiceProviderName FROM ServiceProvider
	INNER JOIN ServiceForm ON ServiceProvider.ServiceProviderID = ServiceForm.ServiceProviderID
	WHERE ServiceID IN(
	SELECT DISTINCT ServiceID FROM(
		SELECT 1 ServiceID
		FROM @@Form4ACompliance
		WHERE (ISNULL(Pregnant,'') ='YES' OR ISNULL(Lactating,'') ='YES')
		UNION ALL
		SELECT 2 ServiceID 
		FROM @@Form4ACompliance
		WHERE CurrentAge <= 12
		UNION ALL
		SELECT 3 ServiceID
		FROM @@Form4ACompliance
		WHERE NutritionalStatus IN('SM','M')
		AND CurrentAge <= 60
		UNION ALL
		SELECT 4 ServiceID
		FROM @@Form4ACompliance
		WHERE CurrentAge <= 24
		UNION ALL
		SELECT 5 ServiceID
		FROM @@Form4ACompliance
		WHERE CurrentAge >= 72 AND CurrentAge <=216
		UNION ALL
		SELECT 6 ServiceID
		FROM @@Form4ACompliance
		WHERE CurrentAge > 216 AND CurrentAge <= 719
		UNION ALL
		SELECT 7 ServiceID
		FROM @@Form4ACompliance
		WHERE CurrentAge <= 12) Services1)
	END
	ELSE IF @FormCode = 'B'
	BEGIN
		DECLARE @@Form4BCompliance TABLE
		(
			ProfileTDSPLWID			Varchar(100),
			PLWAge					Int,
			BabyName				varchar(200),
			BabyDateOfBirth			Smalldatetime,
			NutritionalStatusInfant	Varchar(5),
			MedicalRecordNumber		Varchar(20),
			PLW						Varchar(5)
		)

		INSERT INTO @@Form4BCompliance(ProfileTDSPLWID,BabyName,PLWAge,BabyDateOfBirth,NutritionalStatusInfant,MedicalRecordNumber,PLW)
		SELECT ProfileTDSPLW.ProfileTDSPLWID,BabyName,PLWAge ,BabyDateOfBirth,NutritionalStatusInfant,MedicalRecordNumber,
		CASE WHEN ISNULL(BabyName,'')='' THEN 'P' ELSE 'L' END PLW
		FROM ProfileTDSPLW
		LEFT JOIN ProfileTDSPLWDetail ON ProfileTDSPLWDetail.ProfileTDSPLWID = ProfileTDSPLW.ProfileTDSPLWID
		WHERE KebeleID = @KebeleID
		AND ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 1
		AND ISNULL(Form3Generated,0) = 1
		--AND KebeleID NOT IN(
		--	SELECT KebeleID FROM ComplianceSTDSPLWHeader
		--	WHERE KebeleID = @KebeleID  
		--	AND ServiceID = @ServiceID		
		--	AND ReportingPeriodID = @ReportingPeriodID
		--)
		INSERT INTO @TempS(ServiceProviderID,ServiceProviderName)
		SELECT DISTINCT ServiceProvider.ServiceProviderID,ServiceProviderName FROM ServiceProvider
		INNER JOIN ServiceForm ON ServiceProvider.ServiceProviderID = ServiceForm.ServiceProviderID
		WHERE ServiceID IN(
		SELECT DISTINCT ServiceID FROM(
			SELECT 8 ServiceID 
			FROM @@Form4BCompliance
			WHERE ISNULL(BabyName,'')=''
			UNION ALL
			SELECT 9 ServiceID 
			FROM @@Form4BCompliance
			WHERE ISNULL(BabyName,'')=''
			UNION ALL
			SELECT 6 ServiceID 
			FROM @@Form4BCompliance) Services1)

	END
	ELSE IF @FormCode = 'C'
	BEGIN
		DECLARE @@Form4CCompliance TABLE
		(
			ProfileTDSCMCID			Varchar(100),
			MalnourishedChildName	Varchar(200),
			MalnourishmentDegree	Varchar(5),
			ChildDateOfBirth		SmallDatetime,
			CaretakerID				Varchar(20)
		)

		INSERT INTO @@Form4CCompliance(ProfileTDSCMCID,MalnourishedChildName,MalnourishmentDegree,ChildDateOfBirth,CaretakerID)
		SELECT ProfileTDSCMCID,MalnourishedChildName,MalnourishmentDegree,ChildDateOfBirth,CaretakerID
		FROM ProfileTDSCMC
		WHERE KebeleID = @KebeleID
		AND ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 1
		AND ISNULL(Form3Generated,0) = 1
		--AND KebeleID NOT IN(
		--	SELECT KebeleID FROM ComplianceSTDSCMCHeader
		--	WHERE KebeleID = @KebeleID  
		--	AND ServiceID = @ServiceID		
		--	AND ReportingPeriodID = @ReportingPeriodID
		--)

	INSERT INTO @TempS(ServiceProviderID,ServiceProviderName)
	SELECT DISTINCT ServiceProvider.ServiceProviderID,ServiceProviderName FROM ServiceProvider
	INNER JOIN ServiceForm ON ServiceProvider.ServiceProviderID = ServiceForm.ServiceProviderID
	WHERE ServiceID IN(
	SELECT DISTINCT ServiceID FROM(		
		SELECT 10 ServiceID
		FROM @@Form4CCompliance
		UNION ALL
		SELECT 11 ServiceID
		FROM @@Form4CCompliance
			WHERE MalnourishmentDegree IN('SM','M')
			AND CASE WHEN ISNULL(MalnourishedChildName,'') <> '' THEN 
					CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) <= 60 THEN 'Y' ELSE '' END
				ELSE '' END = 'Y'
		UNION ALL
		SELECT 4 ServiceID
		FROM @@Form4CCompliance
		WHERE DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) <= 60
		UNION ALL
		SELECT 6 ServiceID
		FROM @@Form4CCompliance
		WHERE DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) > 216
	) Services1)

	END

	return
END
GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 12/07/2019 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitString] (
      @InputString                  VARCHAR(8000),
      @Delimiter                    VARCHAR(50)
)

RETURNS @Items TABLE (
      Item                          VARCHAR(8000)
)

AS
BEGIN
      IF @Delimiter = ' '
      BEGIN
            SET @Delimiter = ','
            SET @InputString = REPLACE(@InputString, ' ', @Delimiter)
      END

      IF (@Delimiter IS NULL OR @Delimiter = '')
            SET @Delimiter = ','

--INSERT INTO @Items VALUES (@Delimiter) -- Diagnostic
--INSERT INTO @Items VALUES (@InputString) -- Diagnostic

      DECLARE @Item           VARCHAR(8000)
      DECLARE @ItemList       VARCHAR(8000)
      DECLARE @DelimIndex     INT

      SET @ItemList = @InputString
      SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
      WHILE (@DelimIndex != 0)
      BEGIN
            SET @Item = SUBSTRING(@ItemList, 0, @DelimIndex)
            INSERT INTO @Items VALUES (@Item)

            -- Set @ItemList = @ItemList minus one less item
            SET @ItemList = SUBSTRING(@ItemList, @DelimIndex+1, LEN(@ItemList)-@DelimIndex)
            SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
      END -- End WHILE

      IF @Item IS NOT NULL -- At least one delimiter was encountered in @InputString
      BEGIN
            SET @Item = @ItemList
            INSERT INTO @Items VALUES (@Item)
      END

      -- No delimiters were encountered in @InputString, so just return @InputString
      ELSE INSERT INTO @Items VALUES (@InputString)

      RETURN

END -- End Function
GO
