USE [SCT-MIS]

GO


SELECT 

MAX(T3.RegionName) REGION,
MAX(T3.WoredaName) WOREDA,
MAX(T3.KebeleName) KEBELE,
COUNT(DISTINCT T1.ProfileDSHeaderID) AS HOUSEHOLDS,
COUNT(DISTINCT T2.ProfileDSDetailID) AS HOUSEHOLD_MEMBERS,
COUNT(DISTINCT T5.ProfileDSHeaderID) AS HOUSEHOLDS_WITH_CHILD_PROTECTION_CASES,
COUNT(DISTINCT T4.ChildDetailID) AS HOUSEHOLD_MEMBERS_WITH_CHILD_PROTECTION_CASES,
COUNT(DISTINCT T6.ChildDetailID) AS HOUSEHOLD_MEMBERS_SERVED,
SUM(CASE WHEN T7.RiskId=1 THEN 1 ELSE 0 END) MALTREATMENT,
SUM(CASE WHEN T7.RiskId=2 THEN 1 ELSE 0 END) BULLYING,
SUM(CASE WHEN T7.RiskId=3 THEN 1 ELSE 0 END) YOUTHVIOLENCE,
SUM(CASE WHEN T7.RiskId=4 THEN 1 ELSE 0 END) INTIMATEPARTNERVIOLENCE,
SUM(CASE WHEN T7.RiskId=5 THEN 1 ELSE 0 END) SEXUALVIOLENCE,
SUM(CASE WHEN T7.RiskId=6 THEN 1 ELSE 0 END) EMOTIONALPSYCHOLOGICALVIOLENCE,
SUM(CASE WHEN T7.RiskId=7 THEN 1 ELSE 0 END) GENDERBASEDVIOLENCE,
SUM(CASE WHEN T7.RiskId=8 THEN 1 ELSE 0 END) CHILDRENWITHOUTADEQUATEPARENTALCARE,
SUM(CASE WHEN T7.RiskId=9 THEN 1 ELSE 0 END) CHILDTRAFFICKING,
SUM(CASE WHEN T7.RiskId=10 THEN 1 ELSE 0 END) CHILDRENLIVINGWITHDISABILITIES,
SUM(CASE WHEN T7.RiskId=11 THEN 1 ELSE 0 END) CHILDLABOUR,
SUM(CASE WHEN T7.RiskId=12 THEN 1 ELSE 0 END) CHILDRENINCONTACTWITHTHELAW,
SUM(CASE WHEN T7.RiskId=13 THEN 1 ELSE 0 END) CHILDRENINONTHEMOVE,
SUM(CASE WHEN T7.RiskId=14 THEN 1 ELSE 0 END) CHILDRENINATRISKOFHARMFULPRACTICES,
SUM(CASE WHEN T7.RiskId=15 THEN 1 ELSE 0 END) CHILDRENINSTREETSITUATIONS,
COUNT(DISTINCT T8.ChildDetailID) REFERREDTOCASEMANAGEMENTSERVICES,
COUNT(DISTINCT T9.ChildDetailID) RECEIVEDSERVICES,
COUNT(DISTINCT T10.ChildDetailID) NOTRECEIVEDSERVICES,
COUNT(DISTINCT T12.ChildDetailID) OPENCASES,
COUNT(DISTINCT T11.ChildDetailID) CLOSEDCASES,
COUNT(DISTINCT T13.ChildProtectionVisitHeaderId) VISITS

FROM 
ProfileDSHeader T1 
INNER JOIN ProfileDSDetail T2 ON T1.ProfileDSHeaderID=T2.ProfileDSHeaderID
INNER JOIN AdminStructureView T3 ON T1.KebeleID=T3.KebeleID
LEFT JOIN ChildProtectionHeader T4 ON T2.ProfileDSDetailID=T4.ChildDetailID
LEFT JOIN 
(
	SELECT T1.ProfileDSHeaderID, T3.ChildProtectionHeaderID FROM ProfileDSHeader T1 
	INNER JOIN ProfileDSDetail T2 ON T1.ProfileDSHeaderID=T2.ProfileDSHeaderID
	INNER JOIN ChildProtectionHeader T3 ON T2.ProfileDSDetailID=T3.ChildDetailID
) T5 ON T4.ChildProtectionHeaderID=T5.ChildProtectionHeaderID
LEFT JOIN (
		SELECT T1.ChildDetailID, MAX(T3.IsServiceAccessed) IsServiceAccessed
		FROM
			ChildProtectionHeader T1
			INNER JOIN ChildProtectionDetail T2 ON T1.ChildProtectionHeaderID=T2.ChildProtectionHeaderID
			INNER JOIN ChildProtectionVisitDetail T3 ON T2.ChildProtectionDetailID = T3.ChildProtectionDetailID AND T3.IsServiceAccessed='YES'
		GROUP BY T1.ChildDetailID
) T6 ON T4.ChildDetailID = T6.ChildDetailID
LEFT JOIN (
	SELECT
	   T1.ChildProtectionHeaderID, T2.RiskId
	FROM ChildProtectionHeader T1 
		INNER JOIN ChildProtectionDetail T2 ON T1.ChildProtectionHeaderID=T2.ChildProtectionHeaderID
)T7 ON T4.ChildProtectionHeaderID =T7.ChildProtectionHeaderID
LEFT JOIN (
	SELECT
		 T1.ChildProtectionHeaderID, T1.CaseManagementReferral, T1.ChildDetailID
	FROM ChildProtectionHeader T1 WHERE T1.CaseManagementReferral='YES'
)T8 ON T4.ChildProtectionHeaderID =T8.ChildProtectionHeaderID
LEFT JOIN (
	SELECT T1.ChildDetailID, MAX(T2.RiskId) RiskId, MAX(T3.IsServiceAccessed) IsServiceAccessed
	FROM
		ChildProtectionHeader T1
		INNER JOIN ChildProtectionDetail T2 ON T1.ChildProtectionHeaderID=T2.ChildProtectionHeaderID
		LEFT JOIN ChildProtectionVisitDetail T3 ON T2.ChildProtectionDetailID = T3.ChildProtectionDetailID AND T3.IsServiceAccessed='YES'
	GROUP BY T1.ChildDetailID
) T9 ON T4.ChildDetailID = T9.ChildDetailID
LEFT JOIN (
	SELECT T1.ChildDetailID, MAX(T2.RiskId) RiskId, MAX(T3.IsServiceAccessed) IsServiceAccessed
	FROM
		ChildProtectionHeader T1
		INNER JOIN ChildProtectionDetail T2 ON T1.ChildProtectionHeaderID=T2.ChildProtectionHeaderID
		LEFT JOIN ChildProtectionVisitDetail T3 ON T2.ChildProtectionDetailID = T3.ChildProtectionDetailID AND T3.IsServiceAccessed='NO'
	GROUP BY T1.ChildDetailID
) T10 ON T4.ChildDetailID = T10.ChildDetailID
LEFT JOIN (
	SELECT
		 T1.ChildProtectionHeaderID, T1.CaseManagementReferral, T1.ChildDetailID
	FROM ChildProtectionHeader T1 WHERE T1.CaseStatusId=0
)T11 ON T4.ChildProtectionHeaderID =T11.ChildProtectionHeaderID
LEFT JOIN (
	SELECT
		 T1.ChildProtectionHeaderID, T1.CaseManagementReferral, T1.ChildDetailID
	FROM ChildProtectionHeader T1 WHERE T1.CaseStatusId=1
)T12 ON T4.ChildProtectionHeaderID =T12.ChildProtectionHeaderID
LEFT JOIN(
SELECT T2.ChildProtectionVisitHeaderId, MAX(T3.ChildProtectionHeaderID) ChildProtectionHeaderID
										FROM ChildProtectionDetail T1
											INNER JOIN ChildProtectionHeader T3 ON T1.ChildProtectionHeaderID=T3.ChildProtectionHeaderID
											INNER JOIN ChildProtectionVisitDetail T2 ON T1.ChildProtectionDetailID = T2.ChildProtectionDetailID
											GROUP BY T2.ChildProtectionVisitHeaderId
)T13 ON T4.ChildProtectionHeaderID =T13.ChildProtectionHeaderID 

GROUP BY T1.KebeleID

