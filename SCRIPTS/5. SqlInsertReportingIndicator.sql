/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ReportingIndicatorId]
      ,[ReportingIndicatorName]
  FROM [SCT-MIS].[dbo].[ReportingIndicator]

-- Insert Into ReportingIndicator
IF EXISTS (SELECT * FROM [ReportingIndicator] WHERE [ReportingIndicatorId] = 5)
	BEGIN
		UPDATE [dbo].[ReportingIndicator] SET [ReportingIndicatorId] = 5, [ReportingIndicatorName] = 'CBHI Membership' WHERE [ReportingIndicatorId] = 5
	END
ELSE
	BEGIN
		INSERT [dbo].[ReportingIndicator] ([ReportingIndicatorId], [ReportingIndicatorName]) VALUES (5, N'CBHI Membership')
	END
GO
IF EXISTS (SELECT * FROM [ReportingIndicator] WHERE [ReportingIndicatorId] = 6)
	BEGIN
		UPDATE [dbo].[ReportingIndicator] SET [ReportingIndicatorId] = 6, [ReportingIndicatorName] = 'Child Protection' WHERE [ReportingIndicatorId] = 6
	END
ELSE
	BEGIN
		INSERT [dbo].[ReportingIndicator] ([ReportingIndicatorId], [ReportingIndicatorName]) VALUES (6, N'Child Protection')
	END
GO



-- Insert Into ReportingIndicatorService
IF EXISTS (SELECT * FROM [ReportingIndicatorService] WHERE [ReportingIndicatorId]=5 AND [ServiceID] = 12)
	BEGIN
		UPDATE [dbo].[ReportingIndicatorService] SET [ReportingIndicatorId] = 5, [ServiceID] = 12 WHERE [ReportingIndicatorId]=5 AND [ServiceID] = 12
	END
ELSE
	BEGIN
		INSERT [dbo].[ReportingIndicatorService] ([ReportingIndicatorId], [ServiceID]) VALUES (5, 12)
	END
GO
IF EXISTS (SELECT * FROM [ReportingIndicatorService] WHERE [ReportingIndicatorId]=6 AND [ServiceID] = 13)
	BEGIN
		UPDATE [dbo].[ReportingIndicatorService] SET [ReportingIndicatorId]=6,[ServiceID] = 13 WHERE [ReportingIndicatorId]=6 AND [ServiceID] = 13
	END
ELSE
	BEGIN
		INSERT [dbo].[ReportingIndicatorService] ([ReportingIndicatorId], [ServiceID]) VALUES (6, 13)
	END
GO