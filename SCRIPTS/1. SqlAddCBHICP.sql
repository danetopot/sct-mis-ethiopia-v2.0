USE [SCT-MIS]
GO
ALTER TABLE ProfileDSHeader
ADD CBHIMembership VARCHAR(5), CBHINumber VARCHAR(20)

ALTER TABLE ProfileDSDetail
ADD ChildProtectionRisk VARCHAR(5)
-------------------------------------------------

ALTER TABLE ProfileTDSPLW
ADD ChildProtectionRisk VARCHAR(5), CBHIMembership VARCHAR(5), CBHINumber VARCHAR(20)

---------------------------------------------------

ALTER TABLE ProfileTDSCMC
ADD ChildProtectionRisk VARCHAR(5), CBHIMembership VARCHAR(5), CBHINumber VARCHAR(20)