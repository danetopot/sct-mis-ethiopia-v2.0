USE [SCT-MIS]
GO
IF OBJECT_ID('dbo.StandardRpt1', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt1]
GO
IF OBJECT_ID('dbo.StandardRpt2', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt2]
GO
IF OBJECT_ID('dbo.StandardRpt3', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt3]
GO
IF OBJECT_ID('dbo.StandardRpt4', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt4]
GO
IF OBJECT_ID('dbo.StandardRpt5', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt5]
GO
IF OBJECT_ID('dbo.StandardRpt6', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt6]
GO
IF OBJECT_ID('dbo.StandardRpt7', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt7]
GO
IF OBJECT_ID('dbo.StandardRpt8', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt8]
GO
IF OBJECT_ID('dbo.StandardRpt9', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt9]
GO
IF OBJECT_ID('dbo.StandardRpt10', 'U') IS NOT NULL 
DROP TABLE [dbo].[StandardRpt10]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt1](
	[UniqueID] [varchar](max) NULL,
	[ProfileDSHeaderID] [varchar](max) NULL,
	[ProfileDSDetailID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](100) NULL,
	[WoredaName] [varchar](100) NULL,
	[KebeleName] [varchar](100) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[HouseholdName] [varchar](100) NULL,
	[HouseholdIDNumber] [varchar](100) NULL,
	[ClientName] [varchar](100) NULL,
	[IndividualID] [varchar](100) NULL,
	[DateOfBirth] [date] NULL,
	[Age] [varchar](20) NULL,
	[Sex] [varchar](5) NULL,
	[Handicapped] [varchar](5) NULL,
	[ChronicallyIll] [varchar](5) NULL,
	[NutritionalStatus] [varchar](20) NULL,
	[ChildUnderTSForCMAM] [varchar](20) NULL,
	[EnrolledInSchool] [varchar](5) NULL,
	[SchoolName] [varchar](100) NULL,
	[Pregnant] [varchar](5) NULL,
	[Lactating] [varchar](5) NULL,
	[CCCCBSPCMember] [varchar](100) NULL,
	[CBHIMembership] [varchar](5) NULL,
	[ChildProtectionRisk] [varchar](5) NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt10]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt10](
	[UniqueID] [varchar](max) NULL,
	[ProfileHeaderID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](100) NULL,
	[WoredaName] [varchar](100) NULL,
	[KebeleName] [varchar](100) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[HouseHoldIDNumber] [varchar](20) NULL,
	[IndividualID] [varchar](20) NULL,
	[ClientType] [varchar](5) NULL,
	[NameOfHouseHoldMember] [varchar](200) NULL,
	[Sex] [varchar](5) NULL,
	[Age] [varchar](10) NULL,
	[PreAnteNatalCare] [varchar](5) NULL,
	[Immunization] [varchar](5) NULL,
	[SupplementaryFeeding] [varchar](5) NULL,
	[MonthlyGMPSessions] [varchar](5) NULL,
	[EnrolmentAttendance] [varchar](5) NULL,
	[BCCSessions] [varchar](5) NULL,
	[UnderTSF] [varchar](5) NULL,
	[BirthRegistration] [varchar](5) NULL,
	[HealthPostCheckUp] [varchar](5) NULL,
	[CBHIMembership] [varchar](5) NULL,
	[ChildProtectionRisk] [varchar](5) NULL,
	[PreNatalVisit1] [varchar](5) NULL,
	[PreNatalVisit3] [varchar](5) NULL,
	[PostNatalVisit] [varchar](5) NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt2]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt2](
	[UniqueID] [varchar](max) NULL,
	[ProfileTDSPLWID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](100) NULL,
	[WoredaName] [varchar](100) NULL,
	[KebeleName] [varchar](100) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[NameOfPLW] [varchar](100) NULL,
	[HouseholdIDNumber] [varchar](100) NULL,
	[MedicalRecordNumber] [varchar](100) NULL,
	[PLWAge] [varchar](20) NULL,
	[StartDateTDS] [varchar](20) NULL,
	[EndDateTDS] [varchar](20) NULL,
	[BabyDateOfBirth] [date] NULL,
	[BabyName] [varchar](20) NULL,
	[BabySex] [varchar](5) NULL,
	[NutritionalStatusPLW] [varchar](5) NULL,
	[NutritionalStatusInfant] [varchar](5) NULL,
	[CBHIMembership] [varchar](5) NULL,
	[ChildProtectionRisk] [varchar](5) NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt3]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt3](
	[UniqueID] [varchar](max) NULL,
	[ProfileTDSCMCID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](100) NULL,
	[WoredaName] [varchar](100) NULL,
	[KebeleName] [varchar](100) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[CaretakerID] [varchar](100) NULL,
	[NameOfCareTaker] [varchar](100) NULL,
	[HouseHoldIDNumber] [varchar](100) NULL,
	[MalnourishedChildName] [varchar](100) NULL,
	[MalnourishedChildSex] [varchar](20) NULL,
	[ChildID] [varchar](20) NULL,
	[ChildDateOfBirth] [varchar](20) NULL,
	[DateTypeCertificate] [varchar](20) NULL,
	[MalnourishmentDegree] [varchar](20) NULL,
	[StartDateTDS] [varchar](20) NULL,
	[EndDateTDS] [varchar](20) NULL,
	[NextCNStatusDate] [varchar](20) NULL,
	[CBHIMembership] [varchar](5) NULL,
	[ChildProtectionRisk] [varchar](5) NULL,
	[CCCCBSPCMember] [varchar](20) NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt4]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt4](
	[UniqueID] [varchar](max) NULL,
	[ProfileHeaderID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](100) NULL,
	[WoredaName] [varchar](100) NULL,
	[KebeleName] [varchar](100) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[HouseHoldIDNumber] [varchar](20) NULL,
	[IndividualID] [varchar](20) NULL,
	[ClientType] [varchar](20) NULL,
	[NameOfHouseHoldMember] [varchar](200) NULL,
	[Sex] [varchar](5) NULL,
	[Age] [varchar](10) NULL,
	[PreAnteNatalCare] [varchar](5) NULL,
	[Immunization] [varchar](5) NULL,
	[SupplementaryFeeding] [varchar](5) NULL,
	[MonthlyGMPSessions] [varchar](5) NULL,
	[EnrolmentAttendance] [varchar](5) NULL,
	[BCCSessions] [varchar](5) NULL,
	[UnderTSF] [varchar](5) NULL,
	[BirthRegistration] [varchar](5) NULL,
	[HealthPostCheckUp] [varchar](5) NULL,
	[CBHIMembership] [varchar](5) NULL,
	[ChildProtectionRisk] [varchar](5) NULL,
	[PreNatalVisit1] [varchar](5) NULL,
	[PreNatalVisit3] [varchar](5) NULL,
	[PostNatalVisit] [varchar](5) NULL,
	[CollectionDate] [varchar](20) NULL,
	[SocialWorker] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt5]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt5](
	[UniqueID] [varchar](max) NULL,
	[ProfileDSHeaderID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](50) NULL,
	[WoredaName] [varchar](50) NULL,
	[KebeleName] [varchar](50) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[ClientName] [varchar](50) NULL,
	[ClientType] [varchar](20) NULL,
	[IndividualID] [varchar](50) NULL,
	[Age] [varchar](20) NULL,
	[Sex] [varchar](5) NULL,
	[ServiceID] [varchar](5) NULL,
	[ServiceName] [varchar](100) NULL,
	[Complied] [varchar](5) NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt6]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt6](
	[UniqueID] [varchar](max) NULL,
	[ProfileDSHeaderID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](50) NULL,
	[WoredaName] [varchar](50) NULL,
	[KebeleName] [varchar](50) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[ClientName] [varchar](50) NULL,
	[ClientType] [varchar](20) NULL,
	[IndividualID] [varchar](50) NULL,
	[Age] [varchar](20) NULL,
	[Sex] [varchar](5) NULL,
	[ServiceID] [varchar](5) NULL,
	[ServiceName] [varchar](100) NULL,
	[Complied] [varchar](5) NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt7]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt7](
	[UniqueID] [varchar](max) NULL,
	[ProfileDSHeaderID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](50) NULL,
	[WoredaName] [varchar](50) NULL,
	[KebeleName] [varchar](50) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[IndividualID] [varchar](20) NULL,
	[ClientName] [varchar](50) NULL,
	[ClientType] [varchar](5) NULL,
	[Sex] [varchar](5) NULL,
	[Age] [varchar](20) NULL,
	[ServiceID] [varchar](5) NULL,
	[ServiceName] [varchar](100) NULL,
	[NoOfFollowUps] [int] NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt8]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt8](
	[UniqueID] [varchar](max) NULL,
	[ProfileDSHeaderID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](50) NULL,
	[WoredaName] [varchar](50) NULL,
	[KebeleName] [varchar](50) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[IndividualID] [varchar](20) NULL,
	[ClientName] [varchar](50) NULL,
	[ClientType] [varchar](5) NULL,
	[Sex] [varchar](5) NULL,
	[Age] [varchar](20) NULL,
	[ServiceID] [varchar](5) NULL,
	[ServiceName] [varchar](100) NULL,
	[NoOfFollowUps] [int] NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StandardRpt9]    Script Date: 12/07/2019 10:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardRpt9](
	[UniqueID] [varchar](max) NULL,
	[ProfileHeaderID] [varchar](max) NULL,
	[FiscalYear] [varchar](20) NULL,
	[RegionName] [varchar](100) NULL,
	[WoredaName] [varchar](100) NULL,
	[KebeleName] [varchar](100) NULL,
	[KebeleID] [int] NULL,
	[Gote] [varchar](100) NULL,
	[HouseHoldIDNumber] [varchar](20) NULL,
	[IndividualID] [varchar](20) NULL,
	[ClientType] [varchar](5) NULL,
	[NameOfHouseHoldMember] [varchar](200) NULL,
	[Sex] [varchar](5) NULL,
	[Age] [varchar](10) NULL,
	[PreAnteNatalCare] [varchar](5) NULL,
	[Immunization] [varchar](5) NULL,
	[SupplementaryFeeding] [varchar](5) NULL,
	[MonthlyGMPSessions] [varchar](5) NULL,
	[EnrolmentAttendance] [varchar](5) NULL,
	[BCCSessions] [varchar](5) NULL,
	[UnderTSF] [varchar](5) NULL,
	[BirthRegistration] [varchar](5) NULL,
	[HealthPostCheckUp] [varchar](5) NULL,
	[CBHIMembership] [varchar](5) NULL,
	[ChildProtectionRisk] [varchar](5) NULL,
	[PreNatalVisit1] [varchar](5) NULL,
	[PreNatalVisit3] [varchar](5) NULL,
	[PostNatalVisit] [varchar](5) NULL,
	[CollectionDate] [date] NULL,
	[SocialWorker] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
