begin tran
	delete NonComplianceTDSCMCDetail
	delete NonComplianceTDSCMCHeader

	delete NonComplianceTDSPLWDetail
	delete NonComplianceTDSPLWHeader

	delete NonComplianceDSDetail
	delete NonComplianceDSHeader

	delete GenerateNonCCTDSCMC
	delete GenerateNonCCTDSPLW
	delete GenerateNonCCDS

	delete GenerateCSPTDSCMC
	delete GenerateCSPTDSPLW
	delete GenerateCSPDS

	delete ComplianceSTDSCMCDetail
	delete ComplianceSTDSCMCHeader

	delete ComplianceSTDSPLWDetail
	delete ComplianceSTDSPLWHeader

	delete ComplianceSDSDetail
	delete ComplianceSDSHeader

	delete CheckListTDSPLW
	delete CheckListTDSCMC
	delete CheckListDS

	delete CoResponsibilityDSDetail
	delete CoResponsibilityDSHeader

	delete CoResponsibilityTDSCMC
	delete CoResponsibilityTDSCMCHeader


	delete CoResponsibilityTDSPLW
	delete CoResponsibilityTDSPLWHeader

	delete ProfileTDSPLW
	delete ProfileTDSCMC

	delete ProfileDSDetail
	delete ProfileDSHeader
commit tran