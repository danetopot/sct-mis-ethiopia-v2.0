ALTER Proc [dbo].[GetForm1AHouseholdHeadList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10,
	@isSearch		int = 0,
	@SearchTypeID	Varchar(5) = NULL,
	@SearchKeyword	Varchar(50) = NULL
)
AS
BEGIN
	Declare @TotalCount Int

	-- 04-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())
	
	IF (ISNULL(@isSearch,0) = 0)
	BEGIN
		SELECT @TotalCount = COUNT(ProfileDSHeaderID)  
		FROM  ProfileDSHeader
		WHERE ( FiscalYear = @CurrentFiscalYear ) AND KebeleID IN(
			SELECT KebeleID FROM DefaultKebeleView
		)

		SELECT  @TotalCount resultCount

		SELECT 
			ROW_NUMBER() OVER(ORDER BY ProfileDSHeaderID) AS ColumnID,
			CONVERT(VARCHAR(100), ProfileDSHeaderID) AS ProfileDSHeaderID,
			HouseHoldIDNumber, 
			NameOfHouseHoldHead,
			dbo.f_GetHouseholdMembers(ProfileDSHeaderID) RecordCount,
			dbo.f_getKebele(KebeleID) Kebele,
			Gote + CASE ISNULL(Gare,'') WHEN '' THEN '' ELSE '/'+ISNULL(Gare,'') END Gote,
			CreatedBy,
			dbo.f_GetUserNameByUserID(ApprovedBy) ApprovedBy,
			ReportFilePath,
			Remarks,
			CASE WHEN ISNULL(ApprovedBy,'') = '' THEN 'Pending' ELSE 'Approved' END StatusID
		FROM ProfileDSHeader
		WHERE ( FiscalYear = @CurrentFiscalYear ) AND KebeleID IN(
			SELECT KebeleID FROM DefaultKebeleView
		) AND dbo.f_GetHouseholdMembers(ProfileDSHeaderID) > 0
		ORDER BY CreatedOn Desc
		OFFSET @jtStartIndex ROWS
		FETCH NEXT @jtPageSize ROWS ONLY
	END
	ELSE
	BEGIN
		-- Search Cases
		SELECT @TotalCount = COUNT(ProfileDSHeaderID)  
		FROM  ProfileDSHeader
		WHERE CASE @SearchTypeID WHEN '1' THEN dbo.f_getKebele(KebeleID)
							     WHEN '2' THEN HouseHoldIDNumber
								 WHEN '3' THEN NameOfHouseHoldHead
								 WHEN '4' THEN Gote
								 WHEN '5' THEN Gare
								 ELSE NameOfHouseHoldHead + ' ' + HouseHoldIDNumber + ' ' + Gote + ' ' + Gare + ' ' + dbo.f_getKebele(KebeleID)
			  END LIKE '%'+@SearchKeyword+'%'
		AND KebeleID IN(
			SELECT KebeleID FROM DefaultKebeleView
		)

		SELECT  @TotalCount resultCount

		SELECT 
			ROW_NUMBER() OVER(ORDER BY ProfileDSHeaderID) AS ColumnID,
			CONVERT(VARCHAR(100), ProfileDSHeaderID) AS ProfileDSHeaderID,
			HouseHoldIDNumber, 
			NameOfHouseHoldHead,
			dbo.f_GetHouseholdMembers(ProfileDSHeaderID) RecordCount,
			dbo.f_getKebele(KebeleID) Kebele,
			Gote + CASE ISNULL(Gare,'') WHEN '' THEN '' ELSE '/'+ISNULL(Gare,'') END Gote,
			CreatedBy,
			dbo.f_GetUserNameByUserID(ApprovedBy) ApprovedBy,
			ReportFilePath,
			Remarks,
			CASE WHEN ISNULL(ApprovedBy,'') = '' THEN 'Pending' ELSE 'Approved' END StatusID
		FROM ProfileDSHeader
		WHERE CASE @SearchTypeID WHEN '1' THEN dbo.f_getKebele(KebeleID)
							     WHEN '2' THEN HouseHoldIDNumber
								 WHEN '3' THEN NameOfHouseHoldHead
								 WHEN '4' THEN Gote
								 WHEN '5' THEN Gare
								 ELSE NameOfHouseHoldHead + ' ' + HouseHoldIDNumber + ' ' + Gote + ' ' + Gare + ' ' + dbo.f_getKebele(KebeleID)
			  END LIKE '%'+@SearchKeyword+'%'
		AND KebeleID IN(
			SELECT KebeleID FROM DefaultKebeleView
		) AND dbo.f_GetHouseholdMembers(ProfileDSHeaderID) > 0
		ORDER BY CreatedOn Desc
		OFFSET @jtStartIndex ROWS
		FETCH NEXT @jtPageSize ROWS ONLY
	END
END

