ALTER PROC [dbo].[getForm2CAdminList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10
)
AS
BEGIN
	-- 04-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())

	SELECT 
		ROW_NUMBER() OVER(ORDER BY CoRespTDSCMCWHeaderID) AS ColumnID,
		CONVERT(VARCHAR(100),CoRespTDSCMCWHeaderID) AS CoRespTDSCMCWHeaderID,
		RegionName,
		WoredaName, 
		KebeleName,
		HouseholdCount,
		dbo.f_GetUserNameByUserID(GeneratedBy) GeneratedBy,
		GeneratedOn AS GeneratedOnOrder,
		CONVERT(VARCHAR(11), GeneratedOn, 106) GeneratedOn,
		ReportFileName
	FROM(
		SELECT DISTINCT
			CoRespTDSCMCWHeaderID,
			RegionName,
			WoredaName, 
			KebeleName,
			ReportFileName,
			GeneratedBy,
			GeneratedOn,
			HouseholdCount
	FROM ViewForm2CTDSCMCHeader WHERE FiscalYear = @CurrentFiscalYear) CoRespDS
	ORDER BY GeneratedOnOrder DESC
	OFFSET @jtStartIndex ROWS
	FETCH NEXT @jtPageSize ROWS ONLY


END