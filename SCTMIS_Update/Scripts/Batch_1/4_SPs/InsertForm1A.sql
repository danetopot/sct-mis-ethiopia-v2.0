ALTER Proc [dbo].[InsertForm1A]
(
	@KebeleID				Int,
	@Gote					varchar(200),		
	@Gare					varchar(200),
	@NameOfHouseHoldHead	varchar(200),
	@HouseHoldIDNumber		varchar(20),
	@Remarks				varchar(300),
	@CollectionDate			varchar(20),
	@SocialWorker			varchar(5),
	@CCCCBSPCMember			varchar(20),
	@MemberXML				Varchar(max),
	@CreatedBy				varchar(5)
)
AS
BEGIN
SET NOCOUNT ON

	DECLARE @DetailRecords		XML,
			@HeaderExists		Bit,
			@ProfileDSHeaderID	uniqueidentifier

	SET @HeaderExists = 1
	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD
			SET @DetailRecords = CONVERT(XML,@MemberXML)	
			
			SELECT @ProfileDSHeaderID = NEWID()

			INSERT INTO ProfileDSHeader(ProfileDSHeaderID, KebeleID,Gote,Gare,NameOfHouseHoldHead,HouseHoldIDNumber,
				Remarks,CollectionDate,SocialWorker,CCCCBSPCMember,CreatedBy,CreatedOn,FiscalYear)
			VALUES(@ProfileDSHeaderID, @KebeleID,@Gote,@Gare,@NameOfHouseHoldHead,@HouseHoldIDNumber,@Remarks,@CollectionDate,
				@SocialWorker,@CCCCBSPCMember,@CreatedBy,GETDATE(),dbo.f_getFiscalYearFromDate(GETDATE()))

			SET @HeaderExists = 0		

			CREATE TABLE #memberXML
			(
				HouseHoldMemberName	varchar(200),
				IndividualID		varchar(20),
				Pregnant			varchar(5),
				Lactating			varchar(5),
				DateOfBirth			smalldatetime,
				Age					float,
				Sex					varchar(5),
				PWL					varchar(5),
				Handicapped			varchar(5),
				ChronicallyIll		varchar(5),
				NutritionalStatus	varchar(5),
				childUnderTSForCMAM	varchar(5),
				EnrolledInSchool	varchar(5),
				Grade				varchar(2),
				SchoolName			varchar(50)
			)

			INSERT INTO #memberXML(HouseHoldMemberName,IndividualID,Pregnant,Lactating,DateOfBirth,Age,Sex,PWL,
						Handicapped,ChronicallyIll,NutritionalStatus,childUnderTSForCMAM,EnrolledInSchool,Grade,SchoolName)
			SELECT
				m.c.value('(HouseHoldMemberName)[1]', 'varchar(200)'),			
				m.c.value('(IndividualID)[1]', 'Varchar(20)'),
				m.c.value('(Pregnant)[1]', 'Varchar(5)'),
				m.c.value('(Lactating)[1]', 'Varchar(5)'),
				m.c.value('(DateOfBirth)[1]', 'smalldatetime'),
				m.c.value('(Age)[1]', 'float'),
				m.c.value('(Sex)[1]', 'varchar(5)'),
				m.c.value('(PWL)[1]', 'varchar(5)'),
				m.c.value('(Handicapped)[1]', 'varchar(5)'),
				m.c.value('(ChronicallyIll)[1]', 'varchar(5)'),
				m.c.value('(NutritionalStatus)[1]', 'varchar(5)'),
				m.c.value('(childUnderTSForCMAM)[1]', 'varchar(5)'),
				m.c.value('(EnrolledInSchool)[1]', 'varchar(5)'),
				m.c.value('(Grade)[1]', 'varchar(2)'),
				m.c.value('(SchoolName)[1]', 'varchar(50)')
			FROM @DetailRecords.nodes('/members/row') AS m(c)

			INSERT INTO ProfileDSDetail(ProfileDSDetailID,ProfileDSHeaderID,HouseHoldMemberName,IndividualID,Pregnant,Lactating,DateOfBirth,Age,Sex,PWL,
			            Handicapped,ChronicallyIll,NutritionalStatus,childUnderTSForCMAM,EnrolledInSchool,Grade,SchoolName)
			SELECT
				NEWID(),@ProfileDSHeaderID,HouseHoldMemberName,IndividualID,Pregnant,Lactating,DateOfBirth,Age,Sex,PWL,
			    Handicapped,ChronicallyIll,NutritionalStatus,childUnderTSForCMAM,EnrolledInSchool,Grade,SchoolName
			FROM #memberXML
		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END
