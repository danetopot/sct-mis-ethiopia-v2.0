ALTER Proc [dbo].[InsertForm2C]
(
	@KebeleID				Int,
	@ReportFileName			varchar(50),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON

	DECLARE @CoRespTDSCMCWHeaderID	uniqueidentifier

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD
		
		SELECT @CoRespTDSCMCWHeaderID = NEWID()

		INSERT INTO CoResponsibilityTDSCMCHeader(CoRespTDSCMCWHeaderID,ReportFileName,FiscalYear)
		VALUES(@CoRespTDSCMCWHeaderID,@ReportFileName,dbo.f_getFiscalYearFromDate(GETDATE()))			

		INSERT INTO CoResponsibilityTDSCMC(CoRespTDSCMCID,CoRespTDSCMCWHeaderID,ProfileTDSCMCID,TargetClientID,GeneratedBy,GeneratedOn)
		SELECT NEWID(),@CoRespTDSCMCWHeaderID,ProfileTDSCMCID,dbo.f_GetForm2CTargetClientID(MalnourishmentDegree),
				dbo.f_getUserIDByUserName(@CreatedBy),GETDATE()
		FROM ProfileTDSCMC
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''
		
		UPDATE ProfileTDSCMC
		SET Form2Produced = 1
		FROM ProfileTDSCMC
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END


