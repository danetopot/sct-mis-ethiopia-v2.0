ALTER Proc [dbo].[InsertForm1C]
(
	@KebeleID				Int,
	@Gote					varchar(200),		
	@Gare					varchar(200),
	@CollectionDate			smalldatetime,
	@SocialWorker			varchar(5),
	@CCCCBSPCMember			varchar(20),
	@MemberXML				Varchar(max),
	@CreatedBy				varchar(20)
)
AS
BEGIN
SET NOCOUNT ON
	DECLARE @DetailRecords		XML,
			@ProfileTDSCMCID	uniqueidentifier

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD	
			SET @DetailRecords = CONVERT(XML,@MemberXML)	

			SELECT @ProfileTDSCMCID = NEWID()

			CREATE TABLE #memberXML
			(
				NameOfCareTaker			varchar(200),
				CaretakerID				varchar(20),
				HouseHoldIDNumber		varchar(20),
				ChildID					varchar(20),
				MalnourishedChildName	varchar(200),
				MalnourishedChildSex	Char(1),
				ChildDateOfBirth		smalldatetime,
				DateTypeCertificate		varchar(200),
				MalnourishmentDegree	varchar(5),
				StartDateTDS			varchar(20),
				NextCNStatusDate		varchar(20),
				EndDateTDS				varchar(20),
				Remarks					varchar(300)
			)
			INSERT INTO #memberXML(NameOfCareTaker,CaretakerID,HouseHoldIDNumber,ChildID,MalnourishedChildName,MalnourishedChildSex,
			        ChildDateOfBirth,DateTypeCertificate,MalnourishmentDegree,StartDateTDS,NextCNStatusDate,EndDateTDS,Remarks)
			SELECT
				m.c.value('(NameOfCareTaker)[1]', 'varchar(200)'),			
				m.c.value('(CaretakerID)[1]', 'varchar(20)'),			
				m.c.value('(HouseHoldIDNumber)[1]', 'varchar(20)'),			
				m.c.value('(ChildID)[1]', 'varchar(20)'),	
				m.c.value('(MalnourishedChildName)[1]', 'varchar(200)'),
				m.c.value('(MalnourishedChildSex)[1]', 'Char(1)'),
				m.c.value('(ChildDateOfBirth)[1]', 'smalldatetime'),
				m.c.value('(DateTypeCertificate)[1]', 'varchar(200)'),
				m.c.value('(MalnourishmentDegree)[1]', 'varchar(5)'),
				m.c.value('(StartDateTDS)[1]', 'varchar(20)'),
				m.c.value('(NextCNStatusDate)[1]', 'varchar(20)'),
				m.c.value('(EndDateTDS)[1]', 'varchar(20)'),
				m.c.value('(Remarks)[1]', 'varchar(300)')
			FROM @DetailRecords.nodes('/members/row') AS m(c)

			INSERT INTO ProfileTDSCMC(ProfileTDSCMCID,KebeleID,Gote,Gare,NameOfCareTaker,CaretakerID,HouseHoldIDNumber,ChildID,MalnourishedChildName,
						MalnourishedChildSex,ChildDateOfBirth,DateTypeCertificate,MalnourishmentDegree,StartDateTDS,
						NextCNStatusDate,EndDateTDS,CollectionDate,SocialWorker,CCCCBSPCMember,CreatedBy,CreatedOn,Remarks,FiscalYear)
			SELECT @ProfileTDSCMCID,@KebeleID,@Gote,@Gare,NameOfCareTaker,CaretakerID,HouseHoldIDNumber,ChildID,MalnourishedChildName,
			            MalnourishedChildSex,ChildDateOfBirth,DateTypeCertificate,MalnourishmentDegree,StartDateTDS,
						NextCNStatusDate,EndDateTDS,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@CreatedBy,GETDATE(),Remarks,dbo.f_getFiscalYearFromDate(GETDATE())
			FROM #memberXML

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END
