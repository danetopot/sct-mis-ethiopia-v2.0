ALTER Proc [dbo].[InsertForm4A]
(
	@KebeleID				Int,
	@ServiceID				int,
	@ReportFileName			varchar(50),
	@ReportingPeriodID		Int,	
	@MemberXML				Varchar(max),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON
	Declare @ComplianceID		varchar(100)
	DECLARE @DetailRecords		XML

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD	
		SET @DetailRecords = CONVERT(XML,@MemberXML)

		SELECT @ComplianceID = NEWID()

		INSERT INTO ComplianceSDSHeader(ComplianceID,ServiceID,KebeleID,GeneratedBy,GeneratedOn,ReportFileName,ReportingPeriodID,FiscalYear)
		VALUES(@ComplianceID,@ServiceID,@KebeleID,dbo.f_getUserIDByUserName(@CreatedBy),GETDATE(),@ReportFileName,@ReportingPeriodID,dbo.f_getFiscalYearFromDate(GETDATE()))
			

		DECLARE @@Form4ACompliance TABLE
		(
			ProfileDSDetailID		varchar(100)
		)

		INSERT INTO @@Form4ACompliance(ProfileDSDetailID)
		SELECT
			m.c.value('(ProfileDSDetailID)[1]', 'varchar(100)')
		FROM @DetailRecords.nodes('/members/row') AS m(c)

		INSERT INTO ComplianceSDSDetail(ComplianceDtlID,ComplianceID,ProfileDSDetailID,Complied,Remarks)		
		SELECT NEWID(),@ComplianceID,ProfileDSDetailID,0,''
		FROM @@Form4ACompliance

		UPDATE ProfileDSDetail
		SET Form4Produced = 1
		FROM ProfileDSDetail
		INNER JOIN @@Form4ACompliance F4ACompliance ON F4ACompliance.ProfileDSDetailID = ProfileDSDetail.ProfileDSDetailID


		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END



