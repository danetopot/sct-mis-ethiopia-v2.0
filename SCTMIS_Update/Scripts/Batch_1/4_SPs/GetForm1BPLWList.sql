ALTER Proc [dbo].[GetForm1BPLWList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10,
	@isSearch		int = 0,
	@SearchTypeID	Varchar(5) = NULL,
	@SearchKeyword	Varchar(50) = NULL
)
AS
BEGIN
	-- 04-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())

	IF (ISNULL(@isSearch,0) = 0)
	BEGIN
		SELECT COUNT(ProfileTDSPLWID) resultCount
		FROM ProfileTDSPLW
		WHERE ( FiscalYear = @CurrentFiscalYear ) AND KebeleID IN(
			SELECT KebeleID FROM DefaultKebeleView
		)

		SELECT 
			ROW_NUMBER() OVER(ORDER BY ProfileTDSPLWID) AS ColumnID,
			CONVERT(VARCHAR(100), ProfileTDSPLWID) AS ProfileTDSPLWID,
			HouseHoldIDNumber,
			NameOfPLW, 
			dbo.f_getKebele(KebeleID) Kebele,
			Gote + CASE ISNULL(Gare,'') WHEN '' THEN '' ELSE '/'+ISNULL(Gare,'') END Gote,
			CreatedBy,
			dbo.f_GetUserNameByUserID(ApprovedBy) ApprovedBy,
			FilePath ReportFilePath,
			CASE WHEN ISNULL(ApprovedBy,'') = '' THEN 'Pending' ELSE 'Approved' END StatusID
		FROM ProfileTDSPLW
		WHERE ( FiscalYear = @CurrentFiscalYear ) AND KebeleID IN(
			SELECT KebeleID FROM DefaultKebeleView
		)
		ORDER BY CreatedOn Desc
		OFFSET @jtStartIndex ROWS
		FETCH NEXT @jtPageSize ROWS ONLY
	END
	ELSE
	BEGIN
		-- Search Cases
		SELECT COUNT(ProfileTDSPLWID) resultCount
		FROM ProfileTDSPLW
		WHERE CASE @SearchTypeID WHEN '1' THEN dbo.f_getKebele(KebeleID)
								 WHEN '2' THEN HouseHoldIDNumber
								 WHEN '3' THEN NameOfPLW
								 WHEN '4' THEN Gote
								 WHEN '5' THEN Gare
								 ELSE NameOfPLW + ' ' + HouseHoldIDNumber + ' ' + ISNULL(Gote,'') + ' ' + ISNULL(Gare,'') + ' ' + dbo.f_getKebele(KebeleID)
			  END LIKE '%'+@SearchKeyword+'%'
		AND KebeleID IN(
			SELECT KebeleID FROM DefaultKebeleView
		)

		SELECT 
			ROW_NUMBER() OVER(ORDER BY ProfileTDSPLWID) AS ColumnID,
			CONVERT(VARCHAR(100), ProfileTDSPLWID),
			HouseHoldIDNumber,
			NameOfPLW, 
			dbo.f_getKebele(KebeleID) Kebele,
			Gote + CASE ISNULL(Gare,'') WHEN '' THEN '' ELSE '/'+ISNULL(Gare,'') END Gote,
			CreatedBy,
			dbo.f_GetUserNameByUserID(ApprovedBy) ApprovedBy,
			FilePath ReportFilePath,
			CASE WHEN ISNULL(ApprovedBy,'') = '' THEN 'Pending' ELSE 'Approved' END StatusID
		FROM ProfileTDSPLW
		WHERE CASE @SearchTypeID WHEN '1' THEN dbo.f_getKebele(KebeleID)
								 WHEN '2' THEN HouseHoldIDNumber
								 WHEN '3' THEN NameOfPLW
								 WHEN '4' THEN Gote
								 WHEN '5' THEN Gare
								 ELSE NameOfPLW + ' ' + HouseHoldIDNumber + ' ' + ISNULL(Gote,'') + ' ' + ISNULL(Gare,'') + ' ' + dbo.f_getKebele(KebeleID)
			  END LIKE '%'+@SearchKeyword+'%'
		AND KebeleID IN(
			SELECT KebeleID FROM DefaultKebeleView
		)
		ORDER BY CreatedOn Desc
		OFFSET @jtStartIndex ROWS
		FETCH NEXT @jtPageSize ROWS ONLY
	END
END

