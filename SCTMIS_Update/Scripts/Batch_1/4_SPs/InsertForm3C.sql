ALTER Proc [dbo].[InsertForm3C]
(
	@KebeleID				Int,
	@ReportFileName			varchar(50),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD

		INSERT INTO CheckListTDSCMC(CheckListTDSCMCID,CoRespTDSCMCID,GeneratedBy,GeneratedOn,ReportFileName,FiscalYear)
		SELECT NEWID(),CoRespTDSCMCID,dbo.f_getUserIDByUserName(@CreatedBy),GETDATE(),@ReportFileName,dbo.f_getFiscalYearFromDate(GETDATE()) 
		FROM ViewForm1CTDSCMCAdmin
		INNER JOIN ProfileTDSCMC ON ViewForm1CTDSCMCAdmin.KebeleID = ProfileTDSCMC.KebeleID
		INNER JOIN CoResponsibilityTDSCMC ON CoResponsibilityTDSCMC.ProfileTDSCMCID = ProfileTDSCMC.ProfileTDSCMCID
		WHERE ProfileTDSCMC.KebeleID = @KebeleID
		AND ISNULL(ApprovedBy,'') <> ''
		AND CoRespTDSCMCID NOT IN(SELECT CoRespTDSCMCID FROM CheckListTDSCMC)

		UPDATE ProfileTDSCMC
		SET Form3Generated = 1
		FROM ProfileTDSCMC
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 1
		AND ISNULL(Form3Generated,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END
