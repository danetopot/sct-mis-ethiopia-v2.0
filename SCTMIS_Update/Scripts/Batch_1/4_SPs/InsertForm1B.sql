ALTER Proc [dbo].[InsertForm1B]
(
	@KebeleID				Int,
	@Gote					varchar(200),		
	@Gare					varchar(200),
    @CollectionDate			SmallDatetime,
    @SocialWorker			varchar(5),
    @CCCCBSPCMember			varchar(20),
	@MemberXML				Varchar(max),
	@CreatedBy				varchar(5)
)
AS
BEGIN
SET NOCOUNT ON
	
	DECLARE @DetailRecords		XML,
			@ProfileTDSPLWID	uniqueidentifier

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD
			SET @DetailRecords = CONVERT(XML,@MemberXML)	
				
			SELECT @ProfileTDSPLWID = NEWID()	
					

			CREATE TABLE #memberXML
			(
				PLW						varchar(5),
				NameOfPLW				varchar(200),
				HouseHoldIDNumber		varchar(20),
				MedicalRecordNumber		varchar(20),
				PLWAge					int,
				StartDateTDS			varchar(20),
				BabyDateOfBirth			smalldatetime,
				BabyName				varchar(200),
				BabySex					varchar(5),
				EndDateTDS				varchar(20),
				NutritionalStatusPLW	varchar(5),
				NutritionalStatusInfant	varchar(5),
				Remarks					varchar(300)
			)
			INSERT INTO #memberXML(PLW,NameOfPLW,HouseHoldIDNumber,MedicalRecordNumber,PLWAge,StartDateTDS,BabyDateOfBirth,
					BabyName,BabySex,EndDateTDS,NutritionalStatusPLW,NutritionalStatusInfant,Remarks)
			SELECT
				m.c.value('(PLW)[1]', 'varchar(5)'),			
				m.c.value('(NameOfPLW)[1]', 'varchar(200)'),
				m.c.value('(HouseHoldIDNumber)[1]', 'varchar(20)'),
				m.c.value('(MedicalRecordNumber)[1]', 'varchar(20)'),
				m.c.value('(PLWAge)[1]', 'int'),
				m.c.value('(StartDateTDS)[1]', 'varchar(20)'),
				m.c.value('(BabyDateOfBirth)[1]', 'smalldatetime'),
				m.c.value('(BabyName)[1]', 'varchar(200)'),
				m.c.value('(BabySex)[1]', 'varchar(5)'),
				m.c.value('(EndDateTDS)[1]', 'varchar(20)'),
				m.c.value('(NutritionalStatusPLW)[1]', 'varchar(5)'),
				m.c.value('(NutritionalStatusInfant)[1]', 'varchar(5)'),
				m.c.value('(Remarks)[1]', 'varchar(300)')
			FROM @DetailRecords.nodes('/members/row') AS m(c)

			INSERT INTO ProfileTDSPLW(ProfileTDSPLWID,KebeleID,Gote,Gare,PLW,NameOfPLW,HouseHoldIDNumber,MedicalRecordNumber,PLWAge,
					StartDateTDS,BabyDateOfBirth,BabyName,BabySex,EndDateTDS,NutritionalStatusPLW,
					NutritionalStatusInfant,CollectionDate,SocialWorker,CCCCBSPCMember,CreatedBy,CreatedOn,Remarks,FiscalYear)
			SELECT @ProfileTDSPLWID,@KebeleID,@Gote,@Gare,PLW,NameOfPLW,HouseHoldIDNumber,
					MedicalRecordNumber,PLWAge,StartDateTDS,
					BabyDateOfBirth,BabyName,BabySex,EndDateTDS,NutritionalStatusPLW,NutritionalStatusInfant,
					@CollectionDate,@SocialWorker,@CCCCBSPCMember,@CreatedBy,GETDATE(),Remarks,dbo.f_getFiscalYearFromDate(GETDATE())
			FROM #memberXML

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END
