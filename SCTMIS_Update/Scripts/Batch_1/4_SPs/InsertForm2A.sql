ALTER Proc [dbo].[InsertForm2A]
(
	@KebeleID				Int,
	@ReportFileName			varchar(50),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON

	DECLARE @CoRespDSHeaderID		uniqueidentifier

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD
		
		SELECT @CoRespDSHeaderID = NEWID()

		INSERT INTO CoResponsibilityDSHeader(CoRespDSHeaderID,GeneratedBy,GeneratedOn,ReportFileName,FiscalYear)
		VALUES(@CoRespDSHeaderID,dbo.f_getUserIDByUserName(@CreatedBy),GETDATE(),@ReportFileName,dbo.f_getFiscalYearFromDate(GETDATE()))			

		INSERT INTO CoResponsibilityDSDetail(CoRespDSDetailID,CoRespDSHeaderID,ProfileDSDetailID,TargetClientID)
		SELECT NEWID(),@CoRespDSHeaderID,ProfileDSDetailID,dbo.f_GetForm2ATargetClientID(Age,PWL)
		FROM ProfileDSHeader
		INNER JOIN ProfileDSDetail ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''

		UPDATE ProfileDSDetail
		SET Form2Produced = 1
		FROM ProfileDSHeader
		INNER JOIN ProfileDSDetail ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''

		--AND ProfileDSDetailID NOT IN(SELECT ProfileDSDetailID 
		--	FROM CoResponsibilityDSDetail WHERE CoRespDSHeaderID = @CoRespDSHeaderID )
		
		--Select * from [dbo].[CoResponsibilityDSHeader]
		--Select * from [dbo].[CoResponsibilityDSDetail]

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END


