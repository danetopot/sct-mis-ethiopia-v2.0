ALTER PROC [dbo].[getForm3AAdminList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10
)
AS
BEGIN
	-- 04-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())

	SELECT ROW_NUMBER() OVER(ORDER BY CoRespDSHeaderID) AS ColumnID,
		CONVERT(VARCHAR(100),CoRespDSHeaderID) AS CoRespDSHeaderID,
		RegionName,
		WoredaName, 
		KebeleName,
		HouseholdCount,
		dbo.f_GetChecklistHouseholds(CoRespDSHeaderID) HouseholdCount,
		dbo.f_GetUserNameByUserID(GeneratedBy) GeneratedBy,
		CONVERT(VARCHAR(11), GeneratedOn, 106) GeneratedOn,
		ReportFileName,
		GeneratedOn CreatedOn
	FROM(
	SELECT 
		--ROW_NUMBER() OVER(ORDER BY CoRespDSHeaderID) AS ColumnID,
		Distinct 
		CoRespDSHeaderID,
		RegionName,
		WoredaName, 
		KebeleName,
		HouseholdCount,
		ReportFileName,
		GeneratedBy,
		GeneratedOn
	FROM ViewForm3ADSHeader WHERE FiscalYear = @CurrentFiscalYear) FORM3A
	ORDER BY CreatedOn desc
	OFFSET @jtStartIndex ROWS
	FETCH NEXT @jtPageSize ROWS ONLY

END


