ALTER PROC [dbo].[getForm2BAdminList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10
)
AS
BEGIN
	-- 04-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())

	SELECT 
		ROW_NUMBER() OVER(ORDER BY CoRespTDSPLWHeaderID) AS ColumnID,
		CoRespTDSPLWHeaderID,
		RegionName,
		WoredaName, 
		KebeleName,
		HouseholdCount,
		dbo.f_GetUserNameByUserID(GeneratedBy) GeneratedBy,
		CONVERT(VARCHAR(11), GeneratedOn, 106) GeneratedOn,
		ReportFileName,
		GeneratedOn CreatedOn
	FROM(
		SELECT 	DISTINCT
			CoRespTDSPLWHeaderID,
			RegionName,
			WoredaName, 
			KebeleName,
			ReportFileName,
			GeneratedBy,
			GeneratedOn,
			HouseholdCount
	FROM ViewForm2BTDSPLWHeader WHERE PLW = 'P' AND FiscalYear = @CurrentFiscalYear) CoRespDS
	ORDER BY CreatedOn Desc
	OFFSET @jtStartIndex ROWS
	FETCH NEXT @jtPageSize ROWS ONLY

END

