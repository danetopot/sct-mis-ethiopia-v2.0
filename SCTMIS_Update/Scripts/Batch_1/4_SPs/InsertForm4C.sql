ALTER Proc [dbo].[InsertForm4C]
(
	@KebeleID				Int,
	@ServiceID				int,
	@ReportFileName			varchar(50),
	@ReportingPeriodID		Int,		
	@MemberXML				Varchar(max),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON
	Declare @ComplianceID		uniqueidentifier
	DECLARE @DetailRecords		XML

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD	
		SET @DetailRecords = CONVERT(XML,@MemberXML)

		SELECT @ComplianceID = NEWID()

		INSERT INTO ComplianceSTDSCMCHeader(ComplianceID,ServiceID,KebeleID,CapturedBy,CapturedOn,ReportFileName,ReportingPeriodID,FiscalYear)
		VALUES(@ComplianceID,@ServiceID,@KebeleID,dbo.f_getUserIDByUserName(@CreatedBy),GETDATE(),@ReportFileName,@ReportingPeriodID,dbo.f_getFiscalYearFromDate(GETDATE()))	

		DECLARE @@Form4CCompliance TABLE
		(
			ProfileTDSCMCID			Varchar(100)
		)

		INSERT INTO @@Form4CCompliance(ProfileTDSCMCID)
		SELECT
			m.c.value('(ProfileTDSCMCID)[1]', 'Varchar(100)')
		FROM @DetailRecords.nodes('/members/row') AS m(c)

		INSERT INTO ComplianceSTDSCMCDetail(ComplianceDtlID,ComplianceID,ProfileTDSCMCID,Complied,Remarks)
		SELECT NEWID(),@ComplianceID,ProfileTDSCMCID,0,'' FROM @@Form4CCompliance

		UPDATE ProfileTDSCMC
		SET Form4Produced = 1
		FROM ProfileTDSCMC
		INNER JOIN @@Form4CCompliance F4ACompliance ON F4ACompliance.ProfileTDSCMCID = ProfileTDSCMC.ProfileTDSCMCID

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END



