ALTER PROC [dbo].[getForm2AAdminList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10
)
AS
BEGIN
	-- 04-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())

	SELECT 
		ROW_NUMBER() OVER(ORDER BY CoRespDSHeaderID) AS ColumnID,
		CONVERT(VARCHAR(100), CoRespDSHeaderID) AS CoRespDSHeaderID,
		RegionName,
		WoredaName, 
		KebeleName,
		dbo.f_GetCoResponsibilityHouseholds(CoRespDSHeaderID) HouseholdCount,
		dbo.f_GetUserNameByUserID(GeneratedBy) GeneratedBy,
		CONVERT(VARCHAR(11), GeneratedOn, 106) GeneratedOn,
		ReportFileName,
		GeneratedOn CreatedOn
	FROM(
		SELECT 	DISTINCT
			CoRespDSHeaderID,
			RegionName,
			WoredaName, 
			KebeleName,
			ReportFileName,
			GeneratedBy,
			GeneratedOn
	FROM ViewCoResponsibilityDS WHERE FiscalYear = @CurrentFiscalYear) CoRespDS
	ORDER BY CreatedOn DESC
	OFFSET @jtStartIndex ROWS
	FETCH NEXT @jtPageSize ROWS ONLY

END
--Select * from CoResponsibilityDSHeader

