IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[f_getFiscalYearFromDate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[f_getFiscalYearFromDate]
GO

create function f_getFiscalYearFromDate (@Date datetime) returns int
as  
	begin	
		declare @Month	int, @Year	int, @CurrentYear int, @NextYear int
		
		select @Month = Month(@Date)
		select @Year = Year(@Date)			
	
		if @Month BETWEEN 1 AND 6
			begin
				select @CurrentYear = @Year - 1
				select @NextYear = @Year 
			end
		else
			begin
				select @CurrentYear = @Year 
				select @NextYear = @Year + 1
			end	
	
		return convert(int, (convert(varchar(10), @CurrentYear) + convert(varchar(10), @NextYear)))	 
	end