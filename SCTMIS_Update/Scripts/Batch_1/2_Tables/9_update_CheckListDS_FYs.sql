alter table CheckListDS add FiscalYear int
go

alter table CheckListDS with check add constraint FK_CheckListDS_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update CheckListDS set FiscalYear = dbo.f_getFiscalYearFromDate(GeneratedOn)

alter table CheckListDS alter column FiscalYear int not null
go