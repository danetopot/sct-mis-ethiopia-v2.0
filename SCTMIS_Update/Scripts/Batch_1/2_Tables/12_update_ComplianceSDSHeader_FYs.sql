alter table ComplianceSDSHeader add FiscalYear int
go

alter table ComplianceSDSHeader with check add constraint FK_ComplianceSDSHeader_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update ComplianceSDSHeader set FiscalYear = dbo.f_getFiscalYearFromDate(GeneratedOn)

alter table ComplianceSDSHeader alter column FiscalYear int not null
go