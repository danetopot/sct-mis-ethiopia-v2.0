alter table ProfileTDSPLW add FiscalYear int
go

alter table ProfileTDSPLW with check add constraint FK_ProfileTDSPLW_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update ProfileTDSPLW set FiscalYear = dbo.f_getFiscalYearFromDate(CreatedOn)
go

alter table ProfileTDSPLW alter column FiscalYear int not null
go