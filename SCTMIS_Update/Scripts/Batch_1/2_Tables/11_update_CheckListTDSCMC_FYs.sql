alter table CheckListTDSCMC add FiscalYear int
go

alter table CheckListTDSCMC with check add constraint FK_CheckListTDSCMC_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update CheckListTDSCMC set FiscalYear = dbo.f_getFiscalYearFromDate(GeneratedOn)

alter table CheckListTDSCMC alter column FiscalYear int not null
go