alter table CheckListTDSPLW add FiscalYear int
go

alter table CheckListTDSPLW with check add constraint FK_CheckListTDSPLW_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update CheckListTDSPLW set FiscalYear = dbo.f_getFiscalYearFromDate(GeneratedOn)

alter table CheckListTDSPLW alter column FiscalYear int not null
go