alter table CoResponsibilityDSHeader add FiscalYear int
go

alter table CoResponsibilityDSHeader with check add constraint FK_CoResponsibilityDSHeader_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update CoResponsibilityDSHeader set FiscalYear = dbo.f_getFiscalYearFromDate(GeneratedOn)
go

alter table CoResponsibilityDSHeader alter column FiscalYear int not null
go