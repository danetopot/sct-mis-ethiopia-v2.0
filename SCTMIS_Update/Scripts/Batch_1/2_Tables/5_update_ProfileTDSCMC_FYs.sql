alter table ProfileTDSCMC add FiscalYear int
go

alter table ProfileTDSCMC with check add constraint FK_ProfileTDSCMC_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update ProfileTDSCMC set FiscalYear = dbo.f_getFiscalYearFromDate(CreatedOn)
go

alter table ProfileTDSCMC alter column FiscalYear int not null
go