declare @UserId int

select top 1 @UserId = UserID from Users order by UserID

insert into FiscalYears (FiscalYear, FiscalYearName, OpenedBy, OpenedOn, [Status], ClosedBy, ClosedOn)
values (20142015, '2014-2015', @UserId, '01-July-2014', 1, null, null)

insert into FiscalYears (FiscalYear, FiscalYearName, OpenedBy, OpenedOn, [Status], ClosedBy, ClosedOn)
values (20152016, '2015-2016', @UserId, '01-July-2015', 1, null, null)

insert into FiscalYears (FiscalYear, FiscalYearName, OpenedBy, OpenedOn, [Status], ClosedBy, ClosedOn)
values (20162017, '2016-2017', @UserId, '01-July-2017', 1, null, null)

insert into FiscalYears (FiscalYear, FiscalYearName, OpenedBy, OpenedOn, [Status], ClosedBy, ClosedOn)
values (20172018, '2017-2018', @UserId, '01-July-2017', 0, null, null)