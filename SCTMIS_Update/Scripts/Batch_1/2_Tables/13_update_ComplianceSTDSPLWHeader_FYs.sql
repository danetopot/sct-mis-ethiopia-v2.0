alter table ComplianceSTDSPLWHeader add FiscalYear int
go

alter table ComplianceSTDSPLWHeader with check add constraint FK_ComplianceSTDSPLWHeader_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update ComplianceSTDSPLWHeader set FiscalYear = dbo.f_getFiscalYearFromDate(CapturedOn)

alter table ComplianceSTDSPLWHeader alter column FiscalYear int not null
go