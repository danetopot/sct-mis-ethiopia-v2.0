alter table ComplianceSTDSCMCHeader add FiscalYear int
go

alter table ComplianceSTDSCMCHeader with check add constraint FK_ComplianceSTDSCMCHeader_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update ComplianceSTDSCMCHeader set FiscalYear = dbo.f_getFiscalYearFromDate(CapturedOn)

alter table ComplianceSTDSCMCHeader alter column FiscalYear int not null
go