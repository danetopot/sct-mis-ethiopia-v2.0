alter table ProfileDSHeader add FiscalYear int
go

alter table ProfileDSHeader with check add constraint FK_ProfileDSHeader_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update ProfileDSHeader set FiscalYear = dbo.f_getFiscalYearFromDate(CreatedOn)
go

alter table ProfileDSHeader alter column FiscalYear int not null
go