ALTER View [dbo].[ViewForm2BTDSPLWHeader]
AS
SELECT  CoResponsibilityTDSPLWHeader.CoRespTDSPLWHeaderID,
		ProfileTDSPLW.KebeleID,
		KebeleName,
		AdminStructureView.WoredaID,
		AdminStructureView.WoredaName,
		AdminStructureView.RegionID,
		AdminStructureView.RegionName,
		CoResponsibilityTDSPLWHeader.PLW,
		ReportFileName, 
		GeneratedBy,
		GeneratedOn,
		CoRespTDSPLWID,
		CoResponsibilityTDSPLW.ProfileTDSPLWID,
		TargetClientID,
		dbo.f_GetCoResponsibilityPLW(CoResponsibilityTDSPLWHeader.CoRespTDSPLWHeaderID) HouseholdCount,
		CoResponsibilityTDSPLWHeader.FiscalYear
FROM CoResponsibilityTDSPLWHeader
INNER JOIN CoResponsibilityTDSPLW ON CoResponsibilityTDSPLWHeader.CoRespTDSPLWHeaderID = CoResponsibilityTDSPLW.CoRespTDSPLWHeaderID
INNER JOIN ProfileTDSPLW ON CoResponsibilityTDSPLW.ProfileTDSPLWID = ProfileTDSPLW.ProfileTDSPLWID
INNER JOIN AdminStructureView ON ProfileTDSPLW.KebeleID = AdminStructureView.KebeleID
WHERE ISNULL(Form2Produced,0) = 1
GO


