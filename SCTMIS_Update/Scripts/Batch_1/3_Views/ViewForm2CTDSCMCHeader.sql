ALTER View [dbo].[ViewForm2CTDSCMCHeader]
AS
SELECT CoResponsibilityTDSCMCHeader.CoRespTDSCMCWHeaderID,
		ProfileTDSCMC.KebeleID,
		KebeleName,
		AdminStructureView.WoredaID,
		AdminStructureView.WoredaName,
		AdminStructureView.RegionID,
		AdminStructureView.RegionName,
		ReportFileName,
		GeneratedBy,
		GeneratedOn,
		CoRespTDSCMCID,
		CoResponsibilityTDSCMC.ProfileTDSCMCID,
		TargetClientID,
		dbo.f_GetCoResponsibilityCMC(CoResponsibilityTDSCMCHeader.CoRespTDSCMCWHeaderID) HouseholdCount,
		CoResponsibilityTDSCMCHeader.FiscalYear
FROM CoResponsibilityTDSCMCHeader
INNER JOIN CoResponsibilityTDSCMC ON CoResponsibilityTDSCMCHeader.CoRespTDSCMCWHeaderID = CoResponsibilityTDSCMC.CoRespTDSCMCWHeaderID
INNER JOIN ProfileTDSCMC ON CoResponsibilityTDSCMC.ProfileTDSCMCID = ProfileTDSCMC.ProfileTDSCMCID
INNER JOIN AdminStructureView ON ProfileTDSCMC.KebeleID = AdminStructureView.KebeleID
WHERE ISNULL(Form2Produced,0) = 1
GO


