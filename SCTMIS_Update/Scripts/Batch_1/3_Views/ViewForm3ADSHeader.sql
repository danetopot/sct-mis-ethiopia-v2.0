ALTER View [dbo].[ViewForm3ADSHeader]
AS
SELECT  CheckListDSID, CoResponsibilityDSDetail.CoRespDSHeaderID,ProfileDSHeader.KebeleID,KebeleName,
		WoredaID,WoredaName,RegionID,0 HouseholdCount,
		RegionName,CheckListDS.ReportFileName,CheckListDS.GeneratedBy,CheckListDS.GeneratedOn,CheckListDS.FiscalYear
FROM CheckListDS
INNER JOIN CoResponsibilityDSDetail ON CheckListDS.CoRespDSDetailID = CoResponsibilityDSDetail.CoRespDSDetailID
INNER JOIN ProfileDSDetail ON ProfileDSDetail.ProfileDSDetailID = CoResponsibilityDSDetail.ProfileDSDetailID
INNER JOIN ProfileDSHeader ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
INNER JOIN AdminStructureView ON ProfileDSHeader.KebeleID = AdminStructureView.KebeleID
WHERE ISNULL(Form2Produced,0) = 1 AND ISNULL(Form3Generated,0) = 1
GO


