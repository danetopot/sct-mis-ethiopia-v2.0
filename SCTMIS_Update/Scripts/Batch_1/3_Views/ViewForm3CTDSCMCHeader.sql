ALTER View [dbo].[ViewForm3CTDSCMCHeader]
AS
SELECT  Distinct
		ProfileTDSCMC.KebeleID,KebeleName,
		WoredaID,WoredaName,RegionID,HouseholdCount,
		RegionName,CheckListTDSCMC.ReportFileName,
		CheckListTDSCMC.GeneratedBy,
		CheckListTDSCMC.GeneratedOn,
		CheckListTDSCMC.FiscalYear
FROM CheckListTDSCMC
INNER JOIN CoResponsibilityTDSCMC ON CheckListTDSCMC.CoRespTDSCMCID = CoResponsibilityTDSCMC.CoRespTDSCMCID
INNER JOIN ProfileTDSCMC ON CoResponsibilityTDSCMC.ProfileTDSCMCID = ProfileTDSCMC.ProfileTDSCMCID
INNER JOIN ViewForm2CTDSCMCHeader ON ViewForm2CTDSCMCHeader.KebeleID = ProfileTDSCMC.KebeleID

GO