﻿using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SCTMIS_Update
{
    public partial class formUpdateDatabase : Form
    {
        public formUpdateDatabase()
        {
            InitializeComponent();
        }

        private IDatabase _database;
        private void formUpdateDatabase_Load(object sender, EventArgs e)
        {
            

            var connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings[0];
            //System.Configuration.ConnectionStringSettings connectionString;

            //if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
            //{
            //    connectionString = rootWebConfig.ConnectionStrings.ConnectionStrings[0];

            //    //_database = new Database(connectionString);
            //}

            //
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdateDatabase_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                var scriptName = "Scripts.Batch_1.1_Functions.f_getFiscalYearFromDate.sql";
                var sqlToExecute = LoadSqlFromAssembly(scriptName);

                var result = _database.Execute(sqlToExecute);

            }
            catch (Exception exception)
            {
                Cursor = Cursors.Default;
                MessageBox.Show(exception.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor = Cursors.Default;
        }

        private string LoadSqlFromAssembly(string name)
        {
            try
            {
                var stream = GetType().Assembly.GetManifestResourceStream(name);

                if (stream != null)
                {
                    var reader = new System.IO.StreamReader(stream);

                    var s = reader.ReadToEnd();
                    reader.Close();

                    return s;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(name + Environment.NewLine + Environment.NewLine + ex.Message);
            }

            return null;
        }
    }
}
