USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[ServiceNonComplianceReport]    Script Date: 07/08/2019 15:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[ServiceNonComplianceReport]
(
   -- General Filters
	@RegionID					VARCHAR(50) = NULL,
    @WoredaID					VARCHAR(50) = NULL,
    @KebeleID					VARCHAR(50) = NULL,
	@GoteID						VARCHAR(50) = NULL,
    @ReportType					VARCHAR(50) = NULL,
    @ReportLevel				VARCHAR(50) = NULL,
	@ReportAggregation			VARCHAR(50) = NULL,
    @ReportPeriod				INT = 0,
    @SocialWorker				VARCHAR(50) = NULL,
    @FiscalYear					VARCHAR(50) = NULL,

    @ClientType         VARCHAR(10) = NULL,
    @ServiceType        VARCHAR(10) = NULL
)
AS
BEGIN
    DECLARE @delimiter nvarchar(MAX) = N','
	DECLARE @TotalCount INT
	DECLARE @ReportingPeriodStartDate VARCHAR(20),@ReportingPeriodENDDate VARCHAR(20)
    DECLARE  
        @SqlPDS NVARCHAR(MAX), @SqlPLW NVARCHAR(MAX), @SqlCMC  NVARCHAR(MAX), @SqlSelect NVARCHAR(MAX)

	IF OBJECT_ID('dbo.temp_RegionFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_RegionFilter; 
    CREATE TABLE temp_RegionFilter (RegionID INT)
	DELETE FROM temp_RegionFilter
	INSERT INTO temp_RegionFilter
	SELECT 0 
	UNION
	SELECT regionids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@RegionID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_WoredaFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_WoredaFilter; 
    CREATE TABLE temp_WoredaFilter (WoredaID INT)
	DELETE FROM temp_WoredaFilter
	INSERT INTO temp_WoredaFilter
	SELECT 0 
	UNION
	SELECT woredaids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@WoredaID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_KebeleFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_KebeleFilter; 
    CREATE TABLE temp_KebeleFilter (KebeleID INT)
	DELETE FROM temp_KebeleFilter
	INSERT INTO temp_KebeleFilter
	SELECT 0 
	UNION
	SELECT kebeleids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@KebeleID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_GoteFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_GoteFilter; 
    CREATE TABLE temp_GoteFilter (GoteName VARCHAR(100))
	DELETE FROM temp_GoteFilter
	INSERT INTO temp_GoteFilter
	SELECT 0 
	UNION
	SELECT gotenames = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@GoteID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	/********************************************************************************************************************************************************************************/
    
    
    /* PDS */
    SELECT @SqlPDS = 'SELECT NEWID(),T1.ProfileDSDetailID, T1.FiscalYear,T2.RegionName,T2.WoredaName,T2.KebeleName,T2.KebeleID,T1.Gote,' +
                                'T1.HouseHoldMemberName,''PDS'' ClientType,T1.IndividualID,T1.Age, T1.Sex,T1.ServiceID, T3.ServiceName, ''NO'' Compiled,' +
								'CollectionDate, SocialWorker ' +
                                'FROM ViewGeneratedNonComplianceDetails T1 '+
                                'INNER JOIN AdminStructureView T2 ON T1.KebeleID=T2.KebeleID ' + 
								'INNER JOIN IntegratedServices T3 ON T1.ServiceID=T3.ServiceID ' +
                                'WHERE T1.ProfileDSDetailID IS NOT NULL  '
    IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
        SELECT @SqlPDS += 'AND T2.RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
    IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
        SELECT @SqlPDS += 'AND T2.WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
    IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
        SELECT @SqlPDS += 'AND T2.KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
    IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
        SELECT @SqlPDS += 'AND T1.Gote IN (SELECT GoteName FROM temp_GoteFilter) '
    IF @SocialWorker IS NOT NULL
        SELECT @SqlPDS += 'AND T1.SocialWorker= '''+ @SocialWorker +''' '
    IF @FiscalYear IS NOT NULL
        SELECT @SqlPDS += 'AND T1.FiscalYear= '''+ @FiscalYear +''' '
	IF (@ReportPeriod <> 0)
		BEGIN
			SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @SqlPDS += 'AND T1.CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND T1.CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
		END
	IF @ClientType IS NOT NULL
        SELECT @SqlPDS += 'AND (CASE WHEN T1.ProfileDSDetailID IS NOT NULL THEN ''PDS'' ELSE ''UNK'' END)='''+ @ClientType +''' '
	IF @ServiceType IS NOT NULL
        SELECT @SqlPDS += 'AND T1.ServiceID= '''+ @ServiceType +''' '

    /* PLW */
    SELECT @SqlPLW = 'SELECT NEWID(),T1.ProfileTDSPLWID,T1.FiscalYear,T2.RegionName,T2.WoredaName,T2.KebeleName,T2.KebeleID,T1.Gote,' +
                    'T1.NameOfPLW,''PLW'' ClientType,T1.MedicalRecordNumber,T1.PLWAge, ''F'' Sex,T1.ServiceID, T3.ServiceName, ''NO'' Compiled,' +
					'CollectionDate, SocialWorker ' +
                    'FROM ViewGeneratedNonComplianceTDSPLWDetails T1 ' +
                    'INNER JOIN AdminStructureView T2 ON T1.KebeleID=T2.KebeleID ' + 
					'INNER JOIN IntegratedServices T3 ON T1.ServiceID=T3.ServiceID ' +
                    'WHERE T1.ProfileTDSPLWID IS NOT NULL  '
    IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
        SELECT @SqlPLW += 'AND T2.RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
    IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
        SELECT @SqlPLW += 'AND T2.WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
    IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
        SELECT @SqlPLW += 'AND T2.KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
    IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
        SELECT @SqlPLW += 'AND T1.Gote IN (SELECT GoteName FROM temp_GoteFilter) '
    IF @SocialWorker IS NOT NULL
        SELECT @SqlPLW += 'AND T1.SocialWorker= '''+ @SocialWorker +''' '
    IF @FiscalYear IS NOT NULL
        SELECT @SqlPLW += 'AND T1.FiscalYear= '''+ @FiscalYear +''' '
	IF (@ReportPeriod <> 0)
		BEGIN
			SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @SqlPLW += 'AND T1.CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND T1.CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
		END
	IF @ClientType IS NOT NULL
        SELECT @SqlPLW += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''PLW'' ELSE ''UNK'' END)= '''+ @ClientType +''' '
	IF @ServiceType IS NOT NULL
        SELECT @SqlPLW += 'AND T1.ServiceID= '''+ @ServiceType +''' '

    
     /* CMC */
    SELECT @SqlCMC = 'SELECT NEWID(),T1.ProfileTDSCMCID,T1.FiscalYear,T2.RegionName,T2.WoredaName,T2.KebeleName,T2.KebeleID,T1.Gote,' +
                    'T1.NameOfCareTaker,''CMC'' ClientType, T1.CaretakerID, ''N/A'' Sex, ''N/A'' Age,T1.ServiceID, T3.ServiceName, ''NO'' Compiled,' +
					'CollectionDate, SocialWorker ' +
                    'FROM ViewGeneratedNonComplianceTDSCMCDetails T1 ' +
                    'INNER JOIN AdminStructureView T2 ON T1.KebeleID=T2.KebeleID ' + 
					'INNER JOIN IntegratedServices T3 ON T1.ServiceID=T3.ServiceID ' +
                    'WHERE T1.ProfileTDSCMCID IS NOT NULL  '
    IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
        SELECT @SqlCMC += 'AND T2.RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
    IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
        SELECT @SqlCMC += 'AND T2.WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
    IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
        SELECT @SqlCMC += 'AND T2.KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
    IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
        SELECT @SqlCMC += 'AND T1.Gote IN (SELECT GoteName FROM temp_GoteFilter) '
    IF @SocialWorker IS NOT NULL
        SELECT @SqlCMC += 'AND T1.SocialWorker= '''+ @SocialWorker +''' '
    IF @FiscalYear IS NOT NULL
        SELECT @SqlCMC += 'AND T1.FiscalYear= '''+ @FiscalYear +''' '
	IF (@ReportPeriod <> 0)
		BEGIN
			SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @SqlCMC += 'AND T1.CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND T1.CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
		END
	IF @ClientType IS NOT NULL
        SELECT @SqlCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''CMC'' ELSE ''UNK'' END)='''+ @ClientType +''' '
	IF @ServiceType IS NOT NULL
        SELECT @SqlCMC += 'AND T1.ServiceID= '''+ @ServiceType +''' '
    
        
    IF(OBJECT_ID('dbo.StandardRpt6') IS NOT NULL)
	BEGIN
		DROP TABLE StandardRpt6
	END
    CREATE TABLE StandardRpt6
    (
		UniqueID  VARCHAR(MAX),
		ProfileDSHeaderID VARCHAR(MAX),
        FiscalYear VARCHAR(20),
        RegionName VARCHAR(50),
        WoredaName VARCHAR(50),
        KebeleName VARCHAR(50),
        KebeleID INT,
        Gote VARCHAR(100),
        ClientName VARCHAR(50),
        ClientType VARCHAR(20),
        IndividualID VARCHAR(50),
        Age VARCHAR(20),
        Sex VARCHAR(5),
        ServiceID VARCHAR(5),
        ServiceName VARCHAR(100),
        Complied VARCHAR(5),
		CollectionDate DATE, 
		SocialWorker  VARCHAR(20)
    )
    INSERT INTO StandardRpt6
    EXEC SP_EXECUTESQL @SqlPDS

    INSERT INTO StandardRpt6
    EXEC SP_EXECUTESQL @SqlPLW

    INSERT INTO StandardRpt6
    EXEC SP_EXECUTESQL @SqlCMC
	

	SELECT @TotalCount=COUNT(ProfileDSHeaderID) FROM StandardRpt6
	SELECT @TotalCount
	
END
