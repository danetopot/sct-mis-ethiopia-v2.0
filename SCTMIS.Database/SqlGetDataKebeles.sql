USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[GetDataKebeles]    Script Date: 25/07/2019 15:18:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Proc [dbo].[GetDataKebeles]
(
	@FormNumber	Varchar(10),
	@WoredaID	Int
)
AS
BEGIN
SET NOCOUNT ON
	DECLARE @@TheKebeles TABLE
	(
		KebeleID	Varchar(10),
		KebeleName	Varchar(100)
	)

	INSERT INTO @@TheKebeles(KebeleID,KebeleName)
	SELECT 0 KebeleID, '--- Select Kebele --' KebeleName
	--FORM 2s
	IF ISNULL(@FormNumber,'')='2A'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileDSHeader 
		INNER JOIN ProfileDSDetail ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 0)
	END

	IF ISNULL(@FormNumber,'')='2B1'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileTDSPLW 
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 0 AND PLW = 'P')
	END

	IF ISNULL(@FormNumber,'')='2B2'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileTDSPLW 
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 0 AND PLW = 'L')
	END

	IF ISNULL(@FormNumber,'')='2C'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileTDSCMC 
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 0)
	END
	--FORM 3s
	IF ISNULL(@FormNumber,'')='3A'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileDSHeader 
		INNER JOIN ProfileDSDetail ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 1
		AND ISNULL(Form3Generated,0) = 0)
	END

	IF ISNULL(@FormNumber,'')='3B1'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileTDSPLW 
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 1 AND ISNULL(Form3Generated,0) = 0 AND PLW = 'P')
	END

	IF ISNULL(@FormNumber,'')='3B2'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileTDSPLW 
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 1 AND ISNULL(Form3Generated,0) = 0 AND PLW = 'L')
	END

	IF ISNULL(@FormNumber,'')='3C'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileTDSCMC 
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 1 AND ISNULL(Form3Generated,0) = 0 )
	END
	--FORM 4s
	IF ISNULL(@FormNumber,'')='4A'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileDSHeader 
		INNER JOIN ProfileDSDetail ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form3Generated,0) = 1)
	END

	IF ISNULL(@FormNumber,'')='4B'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileTDSPLW 
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form3Generated,0) = 1)
	END

	IF ISNULL(@FormNumber,'')='4C'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT KebeleID FROM ProfileTDSCMC 
		WHERE ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form3Generated,0) = 1)
	END

	IF ISNULL(@FormNumber,'')='4D'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
	END

	IF ISNULL(@FormNumber,'')='5A1'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT DISTINCT KebeleID FROM ViewGeneratedComplianceDetails 
		WHERE DATEDIFF(MONTH,DateOfBirth,GETDATE()) BETWEEN 60 AND 216)
	END

	IF ISNULL(@FormNumber,'')='5A2'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT DISTINCT KebeleID FROM ViewGeneratedComplianceDetails 
		WHERE  (DATEDIFF(MONTH,DateOfBirth,GETDATE()) >216 OR DATEDIFF(MONTH,DateOfBirth,GETDATE()) <60))
	END

	IF ISNULL(@FormNumber,'')='5B'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT DISTINCT KebeleID FROM ViewGeneratedComplianceTDSPLWDetails)
	END

	IF ISNULL(@FormNumber,'')='5C'
	BEGIN
		INSERT INTO @@TheKebeles(KebeleID,KebeleName)
		SELECT KebeleID,KebeleName 
		FROM Kebele
		WHERE WoredaID = @WoredaID 
		AND KebeleID IN(SELECT DISTINCT KebeleID FROM ViewGeneratedComplianceTDSCMCDetails)
	END

	SELECT KebeleID,KebeleName FROM @@TheKebeles
SET NOCOUNT OFF
END

