USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[getForm4DAdminList]    Script Date: 26/07/2019 09:54:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[getForm4DAdminList]
(
	@jtStartIndex	INT = 0,
	@jtPageSize		INT	= 10
)
AS
BEGIN
	DECLARE 
	@ErrorMsg VARCHAR(256),
	@CurrentFiscalYear INT
	SELECT @CurrentFiscalYear = dbo.f_getCurrentFiscalYear()

	SELECT COUNT(1) RecCount 
	FROM ChildProtectionForm
	INNER JOIN AdminStructureView ON ChildProtectionForm.KebeleID = AdminStructureView.KebeleID
	WHERE ( FiscalYear = @CurrentFiscalYear )



	SELECT 
		ROW_NUMBER() OVER(ORDER BY GeneratedID) AS ColumnID,
		CONVERT(VARCHAR(100), GeneratedID) AS CPHeaderID,
		RegionName,
		WoredaName, 
		KebeleName,
		HouseholdCount,
		ReportFileName, 
		dbo.f_GetUserNameByUserID(CreatedBy) GeneratedBy,
		CONVERT(VARCHAR(11), CreatedOn, 106) GeneratedOn,
		CreatedOn CreatedOn
	FROM ChildProtectionForm
	INNER JOIN AdminStructureView ON ChildProtectionForm.KebeleID = AdminStructureView.KebeleID
	WHERE ( FiscalYear = @CurrentFiscalYear )
	ORDER BY CreatedOn DESC
	OFFSET @jtStartIndex ROWS
	FETCH NEXT @jtPageSize ROWS ONLY
	
END