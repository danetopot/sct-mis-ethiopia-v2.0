USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[SocialServicesNeedsReport]    Script Date: 07/08/2019 12:58:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[SocialServicesNeedsReport]
(
    -- General Filters
	@RegionID					VARCHAR(50) = NULL,
    @WoredaID					VARCHAR(50) = NULL,
    @KebeleID					VARCHAR(50) = NULL,
	@GoteID						VARCHAR(50) = NULL,
    @ReportType					VARCHAR(50) = NULL,
    @ReportLevel				VARCHAR(50) = NULL,
	@ReportAggregation			VARCHAR(50) = NULL,
    @ReportPeriod				INT = 0,
    @SocialWorker				VARCHAR(50) = NULL,
    @FiscalYear					VARCHAR(50) = NULL,
    
    @ClientType         VARCHAR(10) = NULL,
    @ServiceType        VARCHAR(10) = NULL

)
AS
BEGIN
	DECLARE @delimiter nvarchar(MAX) = N','
	DECLARE @TotalCount INT
	DECLARE @ReportingPeriodStartDate VARCHAR(20),@ReportingPeriodENDDate VARCHAR(20)
    DECLARE  
        @SqlImportPDS NVARCHAR(MAX), @SqlImportPregnant NVARCHAR(MAX), @SqlImportLactating NVARCHAR(MAX), @SqlImportCMC NVARCHAR(MAX),
        @SqlServiceType NVARCHAR(MAX), @SqlSelect NVARCHAR(MAX)


	IF OBJECT_ID('dbo.temp_RegionFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_RegionFilter; 
    CREATE TABLE temp_RegionFilter (RegionID INT)
	DELETE FROM temp_RegionFilter
	INSERT INTO temp_RegionFilter
	SELECT 0 
	UNION
	SELECT regionids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@RegionID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_WoredaFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_WoredaFilter; 
    CREATE TABLE temp_WoredaFilter (WoredaID INT)
	DELETE FROM temp_WoredaFilter
	INSERT INTO temp_WoredaFilter
	SELECT 0 
	UNION
	SELECT woredaids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@WoredaID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_KebeleFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_KebeleFilter; 
    CREATE TABLE temp_KebeleFilter (KebeleID INT)
	DELETE FROM temp_KebeleFilter
	INSERT INTO temp_KebeleFilter
	SELECT 0 
	UNION
	SELECT kebeleids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@KebeleID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_GoteFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_GoteFilter; 
    CREATE TABLE temp_GoteFilter (GoteName VARCHAR(100))
	DELETE FROM temp_GoteFilter
	INSERT INTO temp_GoteFilter
	SELECT 0 
	UNION
	SELECT gotenames = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@GoteID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	/********************************************************************************************************************************************************************************/
        

    /** SqlPDS**/   
    SELECT @SqlImportPDS = 'SELECT NEWID(), T2.ProfileDSDetailID, T1.FiscalYear,T3.RegionName,T3.WoredaName,T3.KebeleName,T3.KebeleID,T1.Gote,' +
                  'HouseHoldIDNumber,IndividualID,''PDS'' ClientType,' + 
				  'HouseHoldMemberName,Sex,dbo.f_getYearMonth(dateOfBirth,GetDate()) Age,' +
                  'CASE WHEN (ISNULL(Pregnant,'''') = ''YES'' OR ISNULL(Lactating,'''') = '''') THEN ''YES'' ELSE ''NO'' END PreAnteNatalCare,' +
                  'CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 24 THEN ''YES'' ELSE ''NO'' END Immunization,' + 
                  'CASE WHEN NutritionalStatus IN(''SM'',''M'') THEN CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 60 THEN ''YES'' ELSE ''NO'' END ELSE ''NO'' END SupplementaryFeeding,' +
                  'CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 24 THEN ''YES'' ELSE ''NO'' END MonthlyGMPSessions,' +
                  'CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) >= 72 AND DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 216 THEN ''YES''  ELSE ''NO'' END EnrolmentAttendance,' +
                  'CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) >= 216 AND DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 719 THEN ''YES''  ELSE ''NO'' END BCCSessions,' +
                  'CASE WHEN NutritionalStatus IN(''SM'',''M'') THEN CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 60 THEN ''YES'' ELSE ''NO'' END ELSE ''NO'' END UnderTSF,' +
                  'CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 12 THEN ''YES''  ELSE ''NO'' END BirthRegistration, ''NO'' HealthPostCheckUp,' + 
                  'CASE WHEN ISNULL(CBHIMembership,'''') = '''' OR ISNULL(CBHIMembership,'''') = ''NO'' THEN ''YES'' ELSE ''NO'' END CBHIMembership,' +
                  'CASE WHEN (ISNULL(ChildProtectionRisk,'''') = ''YES'') THEN ''YES'' ELSE ''NO'' END ChildProtectionRisk,' +
                  '''NO'' PreNatalVisit1, ''NO'' PreNatalVisit3,''NO''PostNatalVisit,CollectionDate, SocialWorker ' +
                  'FROM ProfileDSHeader T1 ' +
                  'INNER JOIN ProfileDSDetail T2 ON T1.ProfileDSHeaderID = T2.ProfileDSHeaderID ' +
                  'INNER JOIN AdminStructureView T3 ON T1.KebeleID = T3.KebeleID ' +
                  'WHERE T1.ProfileDSHeaderID IS NOT NULL  '
    IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
        SELECT @SqlImportPDS += 'AND RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
    IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
        SELECT @SqlImportPDS += 'AND WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
    IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
        SELECT @SqlImportPDS += 'AND KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
    IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
        SELECT @SqlImportPDS += 'AND Gote IN (SELECT GoteName FROM temp_GoteFilter) '
    IF @SocialWorker IS NOT NULL
        SELECT @SqlImportPDS += 'AND SocialWorker= '''+ @SocialWorker +''' '
    IF @FiscalYear IS NOT NULL
        SELECT @SqlImportPDS += 'AND FiscalYear= '''+ @FiscalYear +''' '
	IF (@ReportPeriod <> 0)
		BEGIN
			SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @SqlImportPDS += 'AND CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
		END

    IF @ClientType IS NOT NULL
        SELECT @SqlImportPDS += 'AND (CASE WHEN T2.ProfileDSDetailID IS NOT NULL THEN ''PDS'' ELSE ''UNK'' END)= '''+ @ClientType +''' ' --ClientType
	IF @ServiceType = '1'
        SELECT @SqlImportPDS += 'AND (CASE WHEN (ISNULL(Pregnant,'''') = ''YES'' OR ISNULL(Lactating,'''') = '''') THEN ''YES'' ELSE ''NO'' END)=''YES'' ' --PreAnteNatalCare
	IF @ServiceType = '2'
		SELECT @SqlImportPDS += 'AND (CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 24 THEN ''YES'' ELSE ''NO'' END)=''YES'' '  --Immunization
	IF @ServiceType = '3'
        SELECT @SqlImportPDS += 'AND (CASE WHEN NutritionalStatus IN(''SM'',''M'') THEN CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 60 THEN ''YES'' ELSE ''NO'' END ELSE ''NO'' END)=''YES'' '  -- UnderTSF
	IF @ServiceType = '4'
        SELECT @SqlImportCMC += 'AND (CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 24 THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- MonthlyGMPSessions  
	IF @ServiceType = '5'
        SELECT @SqlImportPDS += 'AND (CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) >= 72 AND DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 216 THEN ''YES''  ELSE ''NO'' END)=''YES'' '  --EnrolmentAttendance
	IF @ServiceType = '6'
        SELECT @SqlImportPDS += 'AND (CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) >= 216 AND DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 719 THEN ''YES''  ELSE ''NO'' END)=''YES'' '  -- BCCSessions
	IF @ServiceType = '7'
        SELECT @SqlImportPDS += 'AND (CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 12 THEN ''YES''  ELSE ''NO'' END)=''YES'' '  --BirthRegistration
	IF @ServiceType = '8'
        SELECT @SqlImportPDS += 'AND (CASE WHEN T2.ProfileDSDetailID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --PreNatalVisit3
	IF @ServiceType = '9'
        SELECT @SqlImportPDS += 'AND (CASE WHEN T2.ProfileDSDetailID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --PreNatalVisit1
	IF @ServiceType = '10'
        SELECT @SqlImportPDS += 'AND (CASE WHEN T2.ProfileDSDetailID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --HealthPostCheckUp
	IF @ServiceType = '11'
		SELECT @SqlImportPDS += 'AND (CASE WHEN NutritionalStatus IN(''SM'',''M'') THEN CASE WHEN DATEDIFF(MONTH,dateOfBirth,GetDate()) <= 60 THEN ''YES'' ELSE ''NO'' END ELSE ''NO'' END)=''YES'' '  --SupplementaryFeeding
	IF @ServiceType = '12'
		SELECT @SqlImportPDS += 'AND (CASE WHEN ISNULL(CBHIMembership,'''') = '''' OR ISNULL(CBHIMembership,'''') = ''NO'' THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- CBHIMembership 
	IF @ServiceType = '13'
		SELECT @SqlImportPDS += 'AND (CASE WHEN (ISNULL(ChildProtectionRisk,'''') = ''YES'') THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- ChildProtectionRisk

    /** SqlPregnant**/
    SELECT @SqlImportPregnant = 'SELECT NEWID(), T1.ProfileTDSPLWID, T1.FiscalYear,T3.RegionName,T3.WoredaName,T3.KebeleName,T3.KebeleID,T1.Gote,' +
                  'HouseHoldIDNumber, MedicalRecordNumber, ''PLW'' ClientType,NameOfPLW,''F'' Sex, PLWAge,' +
                  'CASE WHEN ISNULL(PLW,'''') = ''P'' THEN ''YES'' ELSE ''NO'' END PreNatalHealthCare,' +
                  '''NO'' Immunization, ''NO'' SupplementaryFeeding, ''NO'' MonthlyGMPSessions, ''NO'' EnrolmentAttendance,' +
                  '''YES'' BCCSessions, ''NO'' UnderTSF, ''NO'' BirthRegistration,''NO'' HealthPostCheckUp,' +
                  'CASE WHEN ISNULL(CBHIMembership,'''') = '''' OR ISNULL(CBHIMembership,'''') = ''NO'' THEN ''YES'' ELSE ''NO'' END CBHIMembership,' +
                  'CASE WHEN ISNULL(ChildProtectionRisk,'''') = '''' OR ISNULL(ChildProtectionRisk,'''') = ''NO'' THEN ''NO'' ELSE ''YES'' END ChildProtectionRisk,' +
                  'CASE WHEN ISNULL(BabyName,'''') = '''' THEN ''YES'' ELSE ''NO'' END PreNatalVisit1,' +
                  'CASE WHEN ISNULL(BabyName,'''') = '''' THEN ''YES'' ELSE ''NO'' END PreNatalVisit3,' +
                  ' ''NO'' PostNatalVisit, CollectionDate, SocialWorker ' +
                  'FROM ProfileTDSPLW T1 ' +
                  'LEFT JOIN ProfileTDSPLWDetail T2 ON T1.ProfileTDSPLWID = T2.ProfileTDSPLWID ' +
                  'INNER JOIN AdminStructureView T3 ON T1.KebeleID = T3.KebeleID ' +
                  'AND PLW = ''P''  '
    IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
        SELECT @SqlImportPregnant += 'AND RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
    IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
        SELECT @SqlImportPregnant += 'AND WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
    IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
        SELECT @SqlImportPregnant += 'AND KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
    IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
        SELECT @SqlImportPregnant += 'AND Gote IN (SELECT GoteName FROM temp_GoteFilter) '
    IF @SocialWorker IS NOT NULL
        SELECT @SqlImportPregnant += 'AND SocialWorker= '''+ @SocialWorker +''' '
    IF @FiscalYear IS NOT NULL
        SELECT @SqlImportPregnant += 'AND FiscalYear= '''+ @FiscalYear +''' '
	IF (@ReportPeriod <> 0)
		BEGIN
			SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @SqlImportPregnant += 'AND CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
		END

    IF @ClientType IS NOT NULL
        SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''PLW'' ELSE ''UNK'' END)= '''+ @ClientType +''' ' --ClientType
	IF @ServiceType = '1'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN ISNULL(PLW,'''') = ''P'' THEN ''YES'' ELSE ''NO'' END)=''YES'' ' --PreAnteNatalCare
	IF @ServiceType = '2'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' ' --Immunization
	IF @ServiceType = '3'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --UnderTSF
	IF @ServiceType = '4'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --MonthlyGMPSessions
	IF @ServiceType = '5'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --EnrolmentAttendance
	IF @ServiceType = '6'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''YES'' ELSE ''UNK'' END)=''YES'' '  --BCCSessions
	IF @ServiceType = '7'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --BirthRegistration
	IF @ServiceType = '8'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN ISNULL(BabyName,'''') = '''' THEN ''YES'' ELSE ''NO'' END)=''YES'' '  --PreNatalVisit3
	IF @ServiceType = '9'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN ISNULL(BabyName,'''') = '''' THEN ''YES'' ELSE ''NO'' END)=''YES'' '  --PreNatalVisit1
	IF @ServiceType = '10'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --HealthPostCheckUp
	IF @ServiceType = '11'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --SupplementaryFeeding
	IF @ServiceType = '12'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN ISNULL(CBHIMembership,'''') = '''' OR ISNULL(CBHIMembership,'''') = ''NO'' THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- CBHIMembership 
	IF @ServiceType = '13'
		SELECT @SqlImportPregnant += 'AND (CASE WHEN (ISNULL(ChildProtectionRisk,'''') = ''YES'') THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- ChildProtectionRisk

    /** SqlLactating**/
    SELECT @SqlImportLactating = 'SELECT NEWID(), T1.ProfileTDSPLWID,T1.FiscalYear,T3.RegionName,T3.WoredaName,T3.KebeleName,T3.KebeleID,T1.Gote,' +
                  'HouseHoldIDNumber, MedicalRecordNumber, ''PLW'' ClientType,NameOfPLW,''F'' Sex, PLWAge,' +
                  'CASE WHEN ISNULL(PLW,'''') = ''P'' THEN ''YES'' ELSE ''NO'' END PreNatalHealthCare,' +
                  '''NO'' Immunization, ''NO'' SupplementaryFeeding, ''NO'' MonthlyGMPSessions, ''NO'' EnrolmentAttendance,' +
                  '''YES'' BCCSessions, ''NO'' UnderTSF, ''NO'' BirthRegistration,''NO'' HealthPostCheckUp,' +
                  'CASE WHEN ISNULL(CBHIMembership,'''') = '''' OR ISNULL(CBHIMembership,'''') = ''NO'' THEN ''YES'' ELSE ''NO'' END CBHIMembership,' +
                  'CASE WHEN ISNULL(ChildProtectionRisk,'''') = '''' OR ISNULL(ChildProtectionRisk,'''') = ''NO'' THEN ''NO'' ELSE ''YES'' END ChildProtectionRisk,' +
                  'CASE WHEN ISNULL(BabyName,'''') = '''' THEN ''YES'' ELSE ''NO'' END PreNatalVisit1,' +
                  'CASE WHEN ISNULL(BabyName,'''') = '''' THEN ''YES'' ELSE ''NO'' END PreNatalVisit3,' +
                  ' ''NO'' PostNatalVisit,CollectionDate, SocialWorker ' +
                  'FROM ProfileTDSPLW T1 ' +
                  'LEFT JOIN ProfileTDSPLWDetail T2 ON T1.ProfileTDSPLWID = T2.ProfileTDSPLWID ' +
                  'INNER JOIN AdminStructureView T3 ON T1.KebeleID = T3.KebeleID ' +
				'AND PLW = ''L''  '
    IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
        SELECT @SqlImportLactating += 'AND RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
    IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
        SELECT @SqlImportLactating += 'AND WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
    IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
        SELECT @SqlImportLactating += 'AND KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
    IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
        SELECT @SqlImportLactating += 'AND Gote IN (SELECT GoteName FROM temp_GoteFilter) '
    IF @SocialWorker IS NOT NULL
        SELECT @SqlImportLactating += 'AND SocialWorker= '''+ @SocialWorker +''' '
    IF @FiscalYear IS NOT NULL
        SELECT @SqlImportLactating += 'AND FiscalYear= '''+ @FiscalYear +''' '
	IF (@ReportPeriod <> 0)
	BEGIN
		SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
		SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
		SELECT @SqlImportLactating += 'AND CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
	END

	IF @ClientType IS NOT NULL
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''PLW'' ELSE ''UNK'' END)= '''+ @ClientType +''' ' --ClientType
	IF @ServiceType = '1'
		SELECT @SqlImportLactating += 'AND (CASE WHEN ISNULL(PLW,'''') = ''P'' THEN ''YES'' ELSE ''NO'' END)=''YES'' ' --PreAnteNatalCare
	IF @ServiceType = '2'
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' ' --Immunization
	IF @ServiceType = '3'
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --UnderTSF
	IF @ServiceType = '4'
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --MonthlyGMPSessions
	IF @ServiceType = '5'
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --EnrolmentAttendance
	IF @ServiceType = '6'
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''YES'' ELSE ''UNK'' END)=''YES'' '  --BCCSessions
	IF @ServiceType = '7'
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --BirthRegistration
	IF @ServiceType = '8'
		SELECT @SqlImportLactating += 'AND (CASE WHEN ISNULL(BabyName,'''') = '''' THEN ''YES'' ELSE ''NO'' END)=''YES'' '  --PreNatalVisit3
	IF @ServiceType = '9'
		SELECT @SqlImportLactating += 'AND (CASE WHEN ISNULL(BabyName,'''') = '''' THEN ''YES'' ELSE ''NO'' END)=''YES'' '  --PreNatalVisit1
	IF @ServiceType = '10'
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --HealthPostCheckUp
	IF @ServiceType = '11'
		SELECT @SqlImportLactating += 'AND (CASE WHEN T1.ProfileTDSPLWID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --SupplementaryFeeding
	IF @ServiceType = '12'
		SELECT @SqlImportLactating += 'AND (CASE WHEN ISNULL(CBHIMembership,'''') = '''' OR ISNULL(CBHIMembership,'''') = ''NO'' THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- CBHIMembership 
	IF @ServiceType = '13'
		SELECT @SqlImportLactating += 'AND (CASE WHEN (ISNULL(ChildProtectionRisk,'''') = ''YES'') THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- ChildProtectionRisk

    
    /** SqlCMC**/
    SELECT @SqlImportCMC = 'SELECT NEWID(), T1.ProfileTDSCMCID,T1.FiscalYear,T3.RegionName,T3.WoredaName,T3.KebeleName,T3.KebeleID,T1.Gote,' +
                  'T1.HouseHoldIDNumber, T1.CaretakerID, ''CMC'' ClientType, T1.NameOfCareTaker, ''N/A'' Sex, ''N/A'' Age, ''NO'' PreNatalHealthCare,' +
                  'CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GetDate()) <= 24 THEN ''YES'' ELSE ''NO'' END Immunization,' +
                  'CASE WHEN MalnourishmentDegree IN(''SM'',''M'') THEN CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) <= 60 THEN ''YES'' ELSE ''NO'' END ELSE ''NO'' END SupplementaryFeeding,' +
                  'CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) <= 60 THEN ''YES'' ELSE ''NO'' END MonthlyGMPSessions,' +
                  '''NO'' EnrolmentAttendance, ''YES'' BCCSessions, ''NO'' UnderTSF,' +
                  'CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) <= 12 THEN ''YES'' ELSE ''NO'' END BirthRegistration,''NO'' HealthPostCheckUp,' +
                  'CASE WHEN ISNULL(CBHIMembership,'''') = '''' OR ISNULL(CBHIMembership,'''') = ''NO'' THEN ''YES'' ELSE ''NO'' END CBHIMembership,'+
                  'CASE WHEN (ISNULL(ChildProtectionRisk,'''') = ''YES'') THEN ''YES'' ELSE ''NO'' END ChildProtectionRisk,' +
                  ' ''NO'' PreNatalVisit1, ''NO'' PreNatalVisit3, ''NO'' PostNatalVisit,CollectionDate, SocialWorker ' +
                  'FROM ProfileTDSCMC T1 ' +
                  'INNER JOIN AdminStructureView T3 ON T1.KebeleID = T3.KebeleID '
    IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
        SELECT @SqlImportCMC += 'AND RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
    IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
        SELECT @SqlImportCMC += 'AND WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
    IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
        SELECT @SqlImportCMC += 'AND KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
    IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
        SELECT @SqlImportCMC += 'AND Gote IN (SELECT GoteName FROM temp_GoteFilter) '
    IF @SocialWorker IS NOT NULL
        SELECT @SqlImportCMC += 'AND SocialWorker= '''+ @SocialWorker +''' '
    IF @FiscalYear IS NOT NULL
        SELECT @SqlImportCMC += 'AND FiscalYear= '''+ @FiscalYear +''' '
	IF (@ReportPeriod <> 0)
		BEGIN
			SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
			SELECT @SqlImportCMC += 'AND CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
		END
    IF @ClientType IS NOT NULL
        SELECT @SqlImportCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''CMC'' ELSE ''UNK'' END)= '''+ @ClientType +''' ' -- ClientType
	IF @ServiceType = '1'
		SELECT @SqlImportCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' ' -- PreAnteNatalCare
	IF @ServiceType = '2'
		SELECT @SqlImportCMC += 'AND (CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GetDate()) <= 24 THEN ''YES'' ELSE ''NO'' END)=''YES'' '  -- Immunization
	IF @ServiceType = '3'
		SELECT @SqlImportCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  -- UnderTSF
	IF @ServiceType = '4'
		SELECT @SqlImportCMC += 'AND (CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) <= 60 THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- MonthlyGMPSessions  
	IF @ServiceType = '5'
		SELECT @SqlImportCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  -- EnrolmentAttendance
	IF @ServiceType = '6'
		SELECT @SqlImportCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''YES'' ELSE ''UNK'' END)=''YES'' '  --BCCSessions
	IF @ServiceType = '7'
		SELECT @SqlImportCMC += 'AND (CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) <= 12 THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- BirthRegistration 
	IF @ServiceType = '8'
		SELECT @SqlImportCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --PreNatalVisit3
	IF @ServiceType = '9'
		SELECT @SqlImportCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --PreNatalVisit1
	IF @ServiceType = '10'
		SELECT @SqlImportCMC += 'AND (CASE WHEN T1.ProfileTDSCMCID IS NOT NULL THEN ''NO'' ELSE ''UNK'' END)=''YES'' '  --HealthPostCheckUp
	IF @ServiceType = '11'
		SELECT @SqlImportCMC += 'AND (CASE WHEN MalnourishmentDegree IN(''SM'',''M'') THEN CASE WHEN DATEDIFF(MONTH,ChildDateOfBirth,GETDATE()) <= 60 THEN ''YES'' ELSE ''NO'' END ELSE ''NO'' END)=''YES'' '  --SupplementaryFeeding
	IF @ServiceType = '12'
		SELECT @SqlImportCMC += 'AND (CASE WHEN ISNULL(CBHIMembership,'''') = '''' OR ISNULL(CBHIMembership,'''') = ''NO'' THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- CBHIMembership 
	IF @ServiceType = '13'
		SELECT @SqlImportCMC += 'AND (CASE WHEN (ISNULL(ChildProtectionRisk,'''') = ''YES'') THEN ''YES'' ELSE ''NO'' END)=''YES'' ' -- ChildProtectionRisk
     
   IF(OBJECT_ID('dbo.StandardRpt4') IS NOT NULL)
	BEGIN
		DROP TABLE StandardRpt4
	END
    CREATE TABLE StandardRpt4
    (
		UniqueID VARCHAR(MAX),
		ProfileHeaderID VARCHAR(MAX),
        FiscalYear VARCHAR(20),
        RegionName VARCHAR(100),
        WoredaName VARCHAR(100),
        KebeleName VARCHAR(100),
        KebeleID INT,
        Gote VARCHAR(100),
        HouseHoldIDNumber		VARCHAR(20),
        IndividualID            VARCHAR(20),
        ClientType              VARCHAR(20),
        NameOfHouseHoldMember	VARCHAR(200),
        Sex						VARCHAR(5),
        Age						VARCHAR(10),
        PreAnteNatalCare		VARCHAR(5),
        Immunization            VARCHAR(5),
        SupplementaryFeeding    VARCHAR(5),
        MonthlyGMPSessions      VARCHAR(5),
        EnrolmentAttendance		VARCHAR(5),
		BCCSessions				VARCHAR(5),
		UnderTSF				VARCHAR(5),
		BirthRegistration		VARCHAR(5),
        HealthPostCheckUp       VARCHAR(5),
		CBHIMembership			VARCHAR(5),
		ChildProtectionRisk		VARCHAR(5),
		PreNatalVisit1			VARCHAR(5),
        PreNatalVisit3			VARCHAR(5),
        PostNatalVisit			VARCHAR(5),
		CollectionDate			VARCHAR(20),
		SocialWorker			VARCHAR(20)
    )
	PRINT @SqlImportPDS
    INSERT INTO StandardRpt4
    EXEC SP_EXECUTESQL @SqlImportPDS

    INSERT INTO StandardRpt4
    EXEC SP_EXECUTESQL @SqlImportPregnant

    INSERT INTO StandardRpt4
    EXEC SP_EXECUTESQL @SqlImportLactating

    INSERT INTO StandardRpt4
    EXEC SP_EXECUTESQL @SqlImportCMC

	

	SELECT @TotalCount=COUNT(ProfileHeaderID) FROM StandardRpt4
	SELECT @TotalCount
END
