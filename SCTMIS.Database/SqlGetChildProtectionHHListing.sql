USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[GetChildProtectionHHList]    Script Date: 30/07/2019 10:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[GetChildProtectionHHList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10,
	@isSearch		int = 0,
	@SearchTypeID	Varchar(5) = NULL,
	@SearchKeyword	Varchar(50) = NULL
)
AS
BEGIN
	DECLARE 
	@ErrorMsg VARCHAR(256),
	@CurrentFiscalYear INT
	SELECT @CurrentFiscalYear = dbo.f_getCurrentFiscalYear()

	CREATE TABLE #ChildProtectionHHList 
    (
		UniqueID			VARCHAR(MAX),
		ProfileDetailID		VARCHAR(100),
		RegionName			VARCHAR(100),
		KebeleName			VARCHAR(100),
		KebeleID			VARCHAR(100),
		WoredaName			VARCHAR(100),
		GoteGare			VARCHAR(100),
		ClientType  		VARCHAR(100),
		HouseHoldIDNumber	VARCHAR(100),
		NameOfHouseHoldHead	VARCHAR(100),
		HouseHoldMemberName VARCHAR(100),
		HouseHoldMemberSex	VARCHAR(100),
		HouseHoldMemberAge	VARCHAR(100),
		FiscalYear			VARCHAR(100)
    )
	
INSERT INTO #ChildProtectionHHList(UniqueID,ProfileDetailID,RegionName,KebeleName,KebeleID,WoredaName,GoteGare,ClientType,HouseHoldIDNumber,
NameOfHouseHoldHead,HouseHoldMemberName,HouseHoldMemberSex,HouseHoldMemberAge,FiscalYear)
SELECT
	NEWID(), T1.ProfileDSDetailID,
	T3.RegionName, T3.KebeleName,T3.KebeleID, T3.WoredaName,T2.Gote+'/'+T2.Gare AS GoteGare,'PDS' ClientType,
	T2.HouseHoldIDNumber, T2.NameOfHouseHoldHead, T1.HouseHoldMemberName, CASE WHEN T1.Sex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
	T1.Age HouseHoldMemberAge, 
	T2.FiscalYear
FROM ProfileDSDetail T1 
	INNER JOIN ProfileDSHeader T2 ON T1.ProfileDSHeaderID=T2.ProfileDSHeaderID
	INNER JOIN AdminStructureView T3 ON T2.KebeleID=T3.KebeleID
WHERE 
	DATEDIFF(MONTH,T1.DateOfBirth,GETDATE()) BETWEEN 72 AND 216
	AND (T2.FiscalYear = @CurrentFiscalYear)


INSERT INTO #ChildProtectionHHList(UniqueID,ProfileDetailID,RegionName,KebeleName,KebeleID,WoredaName,GoteGare,ClientType,HouseHoldIDNumber,
	NameOfHouseHoldHead,HouseHoldMemberName,HouseHoldMemberSex,HouseHoldMemberAge,FiscalYear)
SELECT
	NEWID(), T1.ProfileTDSPLWDetailID,
	T3.RegionName, T3.KebeleName,T3.KebeleID, T3.WoredaName,T2.Gote+'/'+T2.Gare AS GoteGare,'PLW' ClientType,
	T2.HouseHoldIDNumber, T2.NameOfPLW NameOfHouseHoldHead, ISNULL(T1.BabyName, 'Mother Pregnant') HouseHoldMemberName,
	CASE WHEN T1.BabySex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
	CASE WHEN DATEDIFF(YEAR, T1.BabyDateOfBirth, GETDATE()) > 1 THEN CAST(DateDiff(Year, T1.BabyDateOfBirth, GETDATE()) AS VARCHAR) ELSE 0  END HouseHoldMemberAge,
	T2.FiscalYear
FROM ProfileTDSPLWDetail T1 
	INNER JOIN ProfileTDSPLW T2 ON T2.ProfileTDSPLWID = T1.ProfileTDSPLWDetailID
	INNER JOIN AdminStructureView T3 ON T2.KebeleID=T3.KebeleID
WHERE (T2.FiscalYear = @CurrentFiscalYear)


INSERT INTO #ChildProtectionHHList(UniqueID,ProfileDetailID,RegionName,KebeleName,KebeleID,WoredaName,GoteGare,ClientType,HouseHoldIDNumber,
	NameOfHouseHoldHead,HouseHoldMemberName,HouseHoldMemberSex,HouseHoldMemberAge,FiscalYear)
SELECT 
	NEWID(), T1.ProfileTDSCMCID,
	T3.RegionName, T3.KebeleName,T3.KebeleID, T3.WoredaName,T1.Gote+'/'+T1.Gare AS GoteGare,'CMC' ClientType,
	T1.HouseHoldIDNumber, T1.NameOfCareTaker NameOfHouseHoldHead, T1.MalnourishedChildName HouseHoldMemberName,
	CASE WHEN T1.MalnourishedChildSex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
	CASE WHEN DATEDIFF(YEAR, T1.ChildDateOfBirth, GETDATE()) > 1 THEN CAST(DateDiff(Year, T1.ChildDateOfBirth, GETDATE()) AS VARCHAR) ELSE 0  END HouseHoldMemberAge,
	T1.FiscalYear
FROM ProfileTDSCMC T1
	INNER JOIN AdminStructureView T3 ON T1.KebeleID=T3.KebeleID
WHERE (T1.FiscalYear = @CurrentFiscalYear)


/*
     
	IF (ISNULL(@isSearch,0) = 0)
		BEGIN
			SELECT COUNT(*) FROM #ChildProtectionHHList T1

			SELECT T1.* FROM #ChildProtectionHHList T1
			ORDER BY T1.NameOfHouseHoldHead,T1.ClientType,T1.KebeleName Desc
			OFFSET @jtStartIndex ROWS
			FETCH NEXT @jtPageSize ROWS ONLY
		END
	ELSE
	BEGIN
		SELECT COUNT(*) FROM #ChildProtectionHHList T1
		WHERE
			CASE @SearchTypeID 
				WHEN '1' THEN dbo.f_getKebele(T1.KebeleID)
				WHEN '2' THEN HouseHoldIDNumber
				WHEN '3' THEN NameOfHouseHoldHead
				WHEN '4' THEN GoteGare
				WHEN '5' THEN GoteGare
				WHEN '6' THEN WoredaName
				ELSE NameOfHouseHoldHead + ' ' + HouseHoldIDNumber + ' ' + GoteGare + ' ' + GoteGare + ' ' + dbo.f_getKebele(T1.KebeleID)
			END LIKE '%'+@SearchKeyword+'%'


		SELECT T1.* FROM #ChildProtectionHHList T1
		WHERE
		CASE @SearchTypeID 
			WHEN '1' THEN dbo.f_getKebele(T1.KebeleID)
			WHEN '2' THEN HouseHoldIDNumber
			WHEN '3' THEN NameOfHouseHoldHead
			WHEN '4' THEN GoteGare
			WHEN '5' THEN GoteGare
			WHEN '6' THEN WoredaName
			ELSE NameOfHouseHoldHead + ' ' + HouseHoldIDNumber + ' ' + GoteGare + ' ' + GoteGare + ' ' + dbo.f_getKebele(T1.KebeleID)
		END LIKE '%'+@SearchKeyword+'%'
		ORDER BY T1.NameOfHouseHoldHead,T1.ClientType,T1.KebeleName Desc
		OFFSET @jtStartIndex ROWS
		FETCH NEXT @jtPageSize ROWS ONLY
	END
	*/
	SELECT COUNT(T1.UniqueID) FROM #ChildProtectionHHList T1

			SELECT T1.* FROM #ChildProtectionHHList T1
			ORDER BY T1.NameOfHouseHoldHead
			OFFSET @jtStartIndex ROWS
			FETCH NEXT @jtPageSize ROWS ONLY
END


GO
EXEC [dbo].[GetChildProtectionHHList]
