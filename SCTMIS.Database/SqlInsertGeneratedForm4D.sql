USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[InsertForm4C]    Script Date: 26/07/2019 13:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[InsertForm4D]
(
	@KebeleID				INT,
	@HouseholdCount			INT,
	@ReportFileName			VARCHAR(MAX),
	@ReportingPeriodID		INT,
	@CreatedBy				VARCHAR(20),
	@CreatedOn				DATETIME
)
AS
BEGIN
DECLARE @GeneratedID VARCHAR(MAX)
SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD

		SELECT @GeneratedID = NEWID()

		INSERT INTO ChildProtectionForm(GeneratedID,KebeleID,HouseholdCount,ReportFileName,ReportingPeriodID,CreatedBy,CreatedOn, FiscalYear)
		VALUES(@GeneratedID,@KebeleID,@HouseholdCount,@ReportFileName,@ReportingPeriodID,dbo.f_getUserIDByUserName(@CreatedBy),GETDATE(),dbo.f_getCurrentFiscalYear())	

		

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END



