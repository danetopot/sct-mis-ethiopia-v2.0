USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[InsertCapturedForm4D]    Script Date: 29/07/2019 10:48:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[InsertCapturedForm4D]
(
	@ChildProtectionId						VARCHAR(200),		
	@CaseManagementReferral					VARCHAR(200),
	@CaseStatusId							VARCHAR(200),
	@ClientTypeID							VARCHAR(200),
	@CollectionDate							VARCHAR(200),
	@SocialWorker							VARCHAR(200),
	@Remarks								VARCHAR(MAX),
	@CreatedBy								VARCHAR(50),
	@CreatedOn								VARCHAR(50),
	@FiscalYear								VARCHAR(20),
	@ChildDetailID							VARCHAR(200),	
	@memberXML								VARCHAR(MAX)
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @memberRecords				XML,
			@ChildProtectionHeaderID	UNIQUEIDENTIFIER,
			@HeaderExists				BIT

	SET @HeaderExists = 1
	BEGIN TRY
	BEGIN TRANSACTION INSERTRECORD
		SET @memberRecords = CONVERT(XML,@memberXML)
		SELECT @ChildProtectionHeaderID = NEWID()

		INSERT INTO dbo.ChildProtectionHeader(ChildProtectionHeaderID,ChildProtectionId,CaseManagementReferral,CaseStatusId,ClientTypeID,
		CollectionDate,SocialWorker,Remarks,CreatedBy,CreatedOn,FiscalYear,ChildDetailID)
		VALUES (@ChildProtectionHeaderID,@ChildProtectionId,@CaseManagementReferral,@CaseStatusId,@ClientTypeID,@CollectionDate
           ,@SocialWorker,@Remarks,@CreatedBy,@CreatedOn,@FiscalYear,@ChildDetailID)

		SET @HeaderExists = 0	

		CREATE TABLE #memberXML
		(
			RiskId					VARCHAR(50),
			ServiceId				VARCHAR(50),
			ProviderId				VARCHAR(50),
			CreatedBy				VARCHAR(50),
			CreatedOn				VARCHAR(50),
			UpdatedBy				VARCHAR(50),
			UpdatedOn				VARCHAR(50),
			ChildProtectionHeaderID	VARCHAR(50)
		)
		INSERT INTO #memberXML(RiskId,ServiceId,ProviderId,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn)
		SELECT
			m.c.value('(RiskId)[1]', 'VARCHAR(5)'),	
			m.c.value('(ServiceId)[1]', 'VARCHAR(5)'),
			m.c.value('(ProviderId)[1]', 'VARCHAR(5)'),
			m.c.value('(CreatedBy)[1]', 'VARCHAR(20)'),
			m.c.value('(CreatedOn)[1]', 'VARCHAR(20)'),
			m.c.value('(UpdatedBy)[1]', 'VARCHAR(20)'),		
			m.c.value('(UpdatedOn)[1]', 'VARCHAR(20)')
		FROM @memberRecords.nodes('/members/row') AS m(c)

		INSERT INTO ChildProtectionDetail(ChildProtectionDetailID,RiskId,ServiceId,ProviderId,
		CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,ChildProtectionHeaderID)
		SELECT NEWID(),RiskId,ServiceId,ProviderId,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,ChildProtectionHeaderID FROM #memberXML

	COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
	   END 

	   DECLARE @ERROR_MESSAGE	VARCHAR(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

	SET NOCOUNT OFF
END
