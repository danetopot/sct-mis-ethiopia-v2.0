USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[HouseholdProfileReport]    Script Date: 07/08/2019 12:55:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[HouseholdProfileReport]
(
    -- General Filters
	@RegionID					VARCHAR(50) = NULL,
    @WoredaID					VARCHAR(50) = NULL,
    @KebeleID					VARCHAR(50) = NULL,
	@GoteID						VARCHAR(50) = NULL,
    @ReportType					VARCHAR(50) = NULL,
    @ReportLevel				VARCHAR(50) = NULL,
	@ReportAggregation			VARCHAR(50) = NULL,
    @ReportPeriod				INT = 0,
    @SocialWorker				VARCHAR(50) = NULL,
    @FiscalYear					VARCHAR(50) = NULL,

    -- PDS Filters
    @Gender						VARCHAR(10) = NULL,
    @Pregnant					VARCHAR(10) = NULL, 
    @Lactating					VARCHAR(10) = NULL, 
    @Handicapped				VARCHAR(10) = NULL, 
    @ChronicallyIll				VARCHAR(10) = NULL, 
    @NutritionalStatus			VARCHAR(10) = NULL, 
    @ChildUnderTSForCMAM		VARCHAR(10) = NULL,
    @EnrolledInSchool			VARCHAR(10) = NULL, 
    @ChildProtectionRisk		VARCHAR(10) = NULL, 
    @CBHIMembership				VARCHAR(10) = NULL,

    -- TDS(PLW) Filters
    @StartDateTDS               VARCHAR(10) = NULL,
    @EndDateTDS                 VARCHAR(10) = NULL,
    @NutritionalStatusPLW       VARCHAR(10) = NULL, 
    @NutritionalStatusInfant    VARCHAR(10) = NULL,

    -- TDS(CMC)
    @MalnutritionDegree         VARCHAR(10) = NULL
    
)
AS
BEGIN
	DECLARE @delimiter nvarchar(MAX) = N','
    DECLARE  @Sql NVARCHAR(MAX)=''
	DECLARE @TotalCount INT
	DECLARE @ReportingPeriodStartDate VARCHAR(20),@ReportingPeriodENDDate VARCHAR(20)

	IF OBJECT_ID('dbo.temp_RegionFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_RegionFilter; 
    CREATE TABLE temp_RegionFilter (RegionID INT)
	DELETE FROM temp_RegionFilter
	INSERT INTO temp_RegionFilter
	SELECT 0 
	UNION
	SELECT regionids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@RegionID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_WoredaFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_WoredaFilter; 
    CREATE TABLE temp_WoredaFilter (WoredaID INT)
	DELETE FROM temp_WoredaFilter
	INSERT INTO temp_WoredaFilter
	SELECT 0 
	UNION
	SELECT woredaids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@WoredaID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_KebeleFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_KebeleFilter; 
    CREATE TABLE temp_KebeleFilter (KebeleID INT)
	DELETE FROM temp_KebeleFilter
	INSERT INTO temp_KebeleFilter
	SELECT 0 
	UNION
	SELECT kebeleids = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@KebeleID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	IF OBJECT_ID('dbo.temp_GoteFilter', 'U') IS NOT NULL 
		DROP TABLE dbo.temp_GoteFilter; 
    CREATE TABLE temp_GoteFilter (GoteName VARCHAR(100))
	DELETE FROM temp_GoteFilter
	INSERT INTO temp_GoteFilter
	SELECT 0 
	UNION
	SELECT gotenames = LTRIM(RTRIM(vals.node.value('(./text())[1]', 'nvarchar(4000)')))
			FROM (
				SELECT x = CAST('<root><data>' + REPLACE(@GoteID, @delimiter, '</data><data>') + '</data></root>' AS XML).query('.')
			) v
			CROSS APPLY x.nodes('/root/data') vals(node);

	/********************************************************************************************************************************************************************************/

    IF (@ReportType='RPT1')
        BEGIN
            SELECT @Sql = 'SELECT ROW_NUMBER() OVER(ORDER BY ProfileDSDetailID) AS UniqueID, ProfileDSDetailID, FiscalYear,RegionName,WoredaName,KebeleName,KebeleID,Gote,' +
                          'NameOfHouseHoldHead,HouseholdIDNumber,HouseHoldMemberName,IndividualID,DateOfBirth,' +
                          'CASE WHEN DateDiff(Year, DateOfBirth, Getdate()) > 1 THEN CAST(DateDiff(Year, DateOfBirth, Getdate()) AS Varchar) + '' Years'' ELSE CAST(DateDiff(Month, DateOfBirth, Getdate()) AS VARCHAR) + '' Months'' END Age,' +
                          'Sex,Handicapped,ChronicallyIll,'+
                          'CASE ISNULL(NutritionalStatus,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''N/A'' ELSE NutritionalStatus END NutritionalStatus,'+
                          'CASE ISNULL(childUnderTSForCMAM,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''N/A'' ELSE childUnderTSForCMAM END childUnderTSForCMAM,'+
                          'CASE ISNULL(EnrolledInSchool,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE EnrolledInSchool END EnrolledInSchool,'+
                          'CASE ISNULL(SchoolName,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''N/A'' ELSE SchoolName END SchoolName,'+
                          'CASE ISNULL(Pregnant,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE Pregnant END Pregnant,'+
                          'CASE ISNULL(Lactating,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE Lactating END Lactating,CCCCBSPCMember,'+
                          'CASE ISNULL(CBHIMembership,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE CBHIMembership END CBHIMembership,'+
                          'CASE ISNULL(ChildProtectionRisk,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE ChildProtectionRisk END ChildProtectionRisk,'+
                          'CollectionDate,SocialWorker ' +
                          'FROM ViewForm1ADetails WHERE ProfileDSHeaderID IS NOT NULL '
            IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
                SELECT @Sql += 'AND RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
            IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
                SELECT @Sql += 'AND WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
            IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
                SELECT @Sql += 'AND KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
            IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
                SELECT @Sql += 'AND Gote IN (SELECT GoteName FROM temp_GoteFilter) '
            IF @SocialWorker IS NOT NULL
                SELECT @Sql += 'AND SocialWorker= '''+ @SocialWorker +''' '
            IF @FiscalYear IS NOT NULL
                SELECT @Sql += 'AND FiscalYear= '''+ @FiscalYear +''' '
            IF @Gender IS NOT NULL
                SELECT @Sql += 'AND Sex= '''+ @Gender +''' '
            IF @Pregnant IS NOT NULL
                SELECT @Sql += 'AND Pregnant= '''+ @Pregnant +''' '
            IF @Lactating IS NOT NULL
                SELECT @Sql += 'AND Lactating= '''+ @Lactating +''' '
            IF @Handicapped IS NOT NULL
                SELECT @Sql += 'AND Handicapped= '''+ @Handicapped +''' '
            IF @ChronicallyIll IS NOT NULL
                SELECT @Sql += 'AND ChronicallyIll= '''+ @ChronicallyIll +''' '
            IF @NutritionalStatus IS NOT NULL
                SELECT @Sql += 'AND NutritionalStatus= '''+ @NutritionalStatus +''' '
            IF @ChildUnderTSForCMAM IS NOT NULL
                SELECT @Sql += 'AND childUnderTSForCMAM= '''+ @ChildUnderTSForCMAM +''''
             IF @EnrolledInSchool IS NOT NULL
                SELECT @Sql += 'AND EnrolledInSchool= '''+ @EnrolledInSchool +''' '
            IF @ChildProtectionRisk IS NOT NULL
                SELECT @Sql += 'AND ChildProtectionRisk= '''+ @ChildProtectionRisk +''' '
            IF @CBHIMembership IS NOT NULL
                SELECT @Sql += 'AND CBHIMembership= '''+ @CBHIMembership +''' '
			IF (@ReportPeriod <> 0)
			BEGIN
				SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
				SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
				SELECT @Sql += 'AND CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
			END

            SELECT @Sql += ' ORDER BY RegionName,WoredaName,KebeleName'

			IF(OBJECT_ID('dbo.StandardRpt1') IS NOT NULL)
			BEGIN
				DROP TABLE StandardRpt1
			END
            CREATE TABLE StandardRpt1
            (
				UniqueID INT,
				ProfileDSDetailID VARCHAR(MAX),
                FiscalYear VARCHAR(20),
                RegionName VARCHAR(100),
                WoredaName VARCHAR(100),
                KebeleName VARCHAR(100),
                KebeleID INT,
                Gote VARCHAR(100),
                HouseholdName VARCHAR(100),
                HouseholdIDNumber VARCHAR(100),
                ClientName VARCHAR(100),
                IndividualID VARCHAR(100),
                DateOfBirth DATE,
                Age VARCHAR(20),
                Sex VARCHAR(5),
                Handicapped VARCHAR(5),
                ChronicallyIll VARCHAR(5),
                NutritionalStatus VARCHAR(20),
                ChildUnderTSForCMAM VARCHAR(20),
                EnrolledInSchool VARCHAR(5),
                SchoolName VARCHAR(100),
                Pregnant VARCHAR(5),
                Lactating VARCHAR(5),
                CCCCBSPCMember VARCHAR(100),
                CBHIMembership VARCHAR(5),
                ChildProtectionRisk VARCHAR(5),
                CollectionDate DATE,
                SocialWorker VARCHAR(50),
            )
            INSERT INTO StandardRpt1
            EXEC SP_EXECUTESQL @Sql

			SELECT @TotalCount=COUNT(ProfileDSDetailID) FROM StandardRpt1
			SELECT @TotalCount
        END

    IF (@ReportType='RPT2')
        BEGIN
            SELECT @Sql = 'SELECT NEWID(), ProfileTDSPLWID,FiscalYear,RegionName,WoredaName,KebeleName,KebeleID,Gote,' +
                          'NameOfPLW, HouseholdIDNumber,MedicalRecordNumber,CAST(PLWAge AS VARCHAR) + '' Years'',StartDateTDS,EndDateTDS,' +
                          'BabyDateOfBirth,BabyName,BabySex,' +
                          'CASE ISNULL(NutritionalStatusPLW,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''N/A'' ELSE NutritionalStatusPLW END NutritionalStatusPLW,'+
                          'CASE ISNULL(NutritionalStatusInfant,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''N/A'' ELSE NutritionalStatusInfant END NutritionalStatusInfant,'+
                          'CASE ISNULL(CBHIMembership,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE CBHIMembership END CBHIMembership,'+
                          'CASE ISNULL(ChildProtectionRisk,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE ChildProtectionRisk END ChildProtectionRisk,'+
                          'CollectionDate,SocialWorker ' +
                          'FROM ViewForm1BDetails WHERE HouseholdIDNumber IS NOT NULL '
            IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
                SELECT @Sql += 'AND RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
            IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
                SELECT @Sql += 'AND WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
            IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
                SELECT @Sql += 'AND KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
            IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
                SELECT @Sql += 'AND Gote IN (SELECT GoteName FROM temp_GoteFilter) '
            IF @SocialWorker IS NOT NULL
                SELECT @Sql += 'AND SocialWorker= '''+ @SocialWorker +''' '
            IF @FiscalYear IS NOT NULL
                SELECT @Sql += 'AND FiscalYear= '''+ @FiscalYear +''' '
           IF @StartDateTDS IS NOT NULL
                SELECT @Sql += 'AND StartDateTDS=CONVERT(VARCHAR, '''+  @StartDateTDS +''', 23) '
            IF @EndDateTDS IS NOT NULL
                SELECT @Sql += 'AND EndDateTDS=CONVERT(VARCHAR, '''+ @EndDateTDS +''', 23) '
             IF @StartDateTDS IS NOT NULL AND @EndDateTDS IS NOT NULL
                SELECT @Sql += 'AND StartDateTDS=CONVERT(VARCHAR, '''+  @StartDateTDS +''', 23) AND EndDateTDS=CONVERT(VARCHAR, '''+ @EndDateTDS +''', 23) '
            IF @NutritionalStatusPLW IS NOT NULL
                SELECT @Sql += 'AND NutritionalStatusPLW= '''+ @NutritionalStatusPLW +''' '
            IF @NutritionalStatusInfant IS NOT NULL
                SELECT @Sql += 'AND NutritionalStatusInfant= '''+ @NutritionalStatusInfant +''' '
            IF @ChildProtectionRisk IS NOT NULL
                SELECT @Sql += 'AND ChildProtectionRisk= '''+ @ChildProtectionRisk +''' '
            IF @CBHIMembership IS NOT NULL
                SELECT @Sql += 'AND CBHIMembership= '''+ @CBHIMembership +''' '
				
			IF (@ReportPeriod <> 0)
			BEGIN
				SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
				SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
				SELECT @Sql += 'AND CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
			END

            SELECT @Sql += ' ORDER BY RegionName,WoredaName,KebeleName'


			IF(OBJECT_ID('dbo.StandardRpt2') IS NOT NULL)
			BEGIN
				DROP TABLE StandardRpt2
			END
            CREATE TABLE StandardRpt2
            (
				UniqueID VARCHAR(MAX),
				ProfileTDSPLWID VARCHAR(MAX),
                FiscalYear VARCHAR(20),
                RegionName VARCHAR(100),
                WoredaName VARCHAR(100),
                KebeleName VARCHAR(100),
                KebeleID INT,
                Gote VARCHAR(100),
                NameOfPLW VARCHAR(100),
                HouseholdIDNumber VARCHAR(100),
                MedicalRecordNumber VARCHAR(100),
                PLWAge VARCHAR(20),
                StartDateTDS VARCHAR(20),
                EndDateTDS VARCHAR(20),
                BabyDateOfBirth DATE,
                BabyName VARCHAR(20),
                BabySex VARCHAR(5),
                NutritionalStatusPLW VARCHAR(5),
                NutritionalStatusInfant VARCHAR(5),
                CBHIMembership VARCHAR(5),
                ChildProtectionRisk VARCHAR(5),
                CollectionDate DATE,
                SocialWorker VARCHAR(50)
            )
            INSERT INTO StandardRpt2
            EXEC SP_EXECUTESQL @Sql

			SELECT @TotalCount=COUNT(ProfileDSDetailID) FROM StandardRpt1
			SELECT @TotalCount
        END

    IF (@ReportType='RPT3')
        BEGIN
            SELECT @Sql = 'SELECT  NEWID(),ProfileTDSCMCID,FiscalYear,RegionName,WoredaName,KebeleName,KebeleID,Gote,' +
                          'CaretakerID,NameOfCareTaker,HouseHoldIDNumber,MalnourishedChildName,MalnourishedChildSex,ChildID,CONVERT(VARCHAR(11), ChildDateOfBirth, 106) AS ChildBirthDate,'+
                          'CONVERT(VARCHAR(11), DateTypeCertificate, 106) AS DateTypeCertificate,dbo.f_getNutritionalStatus(MalnourishmentDegree) AS MalnourishmentDegree,' +
                          'CONVERT(VARCHAR(11), StartDateTDS, 106),CONVERT(VARCHAR(11), EndDateTDS, 106),CONVERT(VARCHAR(11), NextCNStatusDate, 106),' +
                          'CASE ISNULL(CBHIMembership,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE CBHIMembership END CBHIMembership,'+
                          'CASE ISNULL(ChildProtectionRisk,''-'') WHEN ''-'' THEN ''N/A'' WHEN '''' THEN ''NO'' ELSE ChildProtectionRisk END ChildProtectionRisk,'+
                          'CCCCBSPCMember,CollectionDate,SocialWorker  '+
                          'FROM ViewForm1CDetails  WHERE HouseholdIDNumber IS NOT NULL  '
            IF EXISTS(SELECT 1 FROM temp_RegionFilter WHERE RegionID<>0)
                SELECT @Sql += 'AND RegionID IN (SELECT RegionID FROM temp_RegionFilter) '
            IF EXISTS(SELECT 1 FROM temp_WoredaFilter WHERE WoredaID<>0)
                SELECT @Sql += 'AND WoredaID IN (SELECT WoredaID FROM temp_WoredaFilter) '
            IF EXISTS(SELECT 1 FROM temp_KebeleFilter WHERE KebeleID<>0)
                SELECT @Sql += 'AND KebeleID IN (SELECT KebeleID FROM temp_KebeleFilter) '
            IF EXISTS(SELECT 1 FROM temp_GoteFilter WHERE GoteName<>0)
                SELECT @Sql += 'AND Gote IN (SELECT GoteName FROM temp_GoteFilter) '
            IF @SocialWorker IS NOT NULL
                SELECT @Sql += 'AND SocialWorker= '''+ @SocialWorker +''' '
            IF @FiscalYear IS NOT NULL
                SELECT @Sql += 'AND FiscalYear= '''+ @FiscalYear +''' '
            IF @StartDateTDS IS NOT NULL
                SELECT @Sql += 'AND StartDateTDS=CONVERT(VARCHAR, '''+  @StartDateTDS +''', 23) '
            IF @EndDateTDS IS NOT NULL
                SELECT @Sql += 'AND EndDateTDS=CONVERT(VARCHAR, '''+ @EndDateTDS +''', 23) '
            IF @StartDateTDS IS NOT NULL AND @EndDateTDS IS NOT NULL
                SELECT @Sql += 'AND StartDateTDS=CONVERT(VARCHAR, '''+  @StartDateTDS +''', 23) AND EndDateTDS=CONVERT(VARCHAR, '''+ @EndDateTDS +''', 23) '
            IF @MalnutritionDegree IS NOT NULL
                SELECT @Sql += 'AND MalnourishmentDegree= '''+ @MalnutritionDegree +''' '
            IF @ChildProtectionRisk IS NOT NULL
                SELECT @Sql += 'AND ChildProtectionRisk= '''+ @ChildProtectionRisk +''' '
            IF @CBHIMembership IS NOT NULL
                SELECT @Sql += 'AND CBHIMembership= '''+ @CBHIMembership +''' '
			IF (@ReportPeriod <> 0)
			BEGIN
				SELECT @ReportingPeriodStartDate = StartDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
				SELECT @ReportingPeriodENDDate = EndDate FROM ReportingPeriod WHERE PeriodID =@ReportPeriod
				SELECT @Sql += 'AND CollectionDate>= '''+ @ReportingPeriodStartDate +''' AND CollectionDate<= ''' + @ReportingPeriodENDDate + ''' '
			END
            SELECT @Sql += ' ORDER BY RegionName,WoredaName,KebeleName'


			IF(OBJECT_ID('dbo.StandardRpt3') IS NOT NULL)
			BEGIN
				DROP TABLE StandardRpt3
			END
            CREATE TABLE StandardRpt3
            (
				UniqueID VARCHAR(MAX),
				ProfileTDSCMCID VARCHAR(MAX),
                FiscalYear VARCHAR(20),
                RegionName VARCHAR(100),
                WoredaName VARCHAR(100),
                KebeleName VARCHAR(100),
                KebeleID INT,
                Gote VARCHAR(100),
                CaretakerID VARCHAR(100),
                NameOfCareTaker VARCHAR(100),
                HouseHoldIDNumber VARCHAR(100),
                MalnourishedChildName VARCHAR(100),
                MalnourishedChildSex VARCHAR(20),
                ChildID VARCHAR(20),
                ChildDateOfBirth VARCHAR(20),
                DateTypeCertificate VARCHAR(20),
                MalnourishmentDegree VARCHAR(20),
                StartDateTDS VARCHAR(20),
                EndDateTDS VARCHAR(20),
                NextCNStatusDate VARCHAR(20),
                CBHIMembership VARCHAR(5),
                ChildProtectionRisk VARCHAR(5),
                CCCCBSPCMember VARCHAR(20),
                CollectionDate DATE,
                SocialWorker VARCHAR(50)
            )
            INSERT INTO StandardRpt3
            EXEC SP_EXECUTESQL @Sql

			SELECT @TotalCount=COUNT(ProfileDSDetailID) FROM StandardRpt1
			SELECT @TotalCount
        END
END
